import numpy as np
import struct
from ..misc import my_math
#import h5py
import pdb

import sys

def n2py_stats(indata, outdata):
    """
    Convert statistics collected in Nek5000 to an uncompressed .npz format.

    Parameters
    ----------
    indata : string
        Location of the input file.
    outdata : string
        Location of the output file.

    """
    
    fname = indata

    print("Processing " + fname)

    with open(fname, 'rb') as f:
        # Number of characters in the header
        hdr_n = struct.unpack('<i',f.read(4))[0]
        # Read the header as bytes
        header_b = struct.unpack('<'+int(hdr_n)*'s',f.read(int(hdr_n)))
        # Use list comprehension to convert from byte to utf8 string
        # and join all elements of the tuple
        header_str = ''.join([s.decode("utf-8") for s in header_b])
        dum5 = struct.unpack('<d',f.read(8))[0]     # Dummy
        Re_b = struct.unpack('<d',f.read(8))[0]     # Bulk Reynolds number
        domain = struct.unpack('<3d',f.read(8*3))   # Domain size
        nel = struct.unpack('<3i',f.read(3*4))      # Number of elements
        poly = struct.unpack('<3i',f.read(3*4))     # Polynomial order
        nstat = struct.unpack('<i',f.read(4))[0]    # Number of stats
        nderiv = struct.unpack('<i',f.read(4))[0]   # Number of derivatives
        times = struct.unpack('<d',f.read(8))[0]    # Starting time of the first stat file
        timee = struct.unpack('<d',f.read(8))[0]    # End time of the last stat file
        atime = struct.unpack('<d',f.read(8))[0]    # Effective averaging time
        dt = struct.unpack('<d',f.read(8))[0]       # Time step
        nrec = struct.unpack('<i',f.read(4))[0]     # Number of recorded statistics each xx timesteps
        tint = struct.unpack('<d',f.read(8))[0]     # Time interval
        npoints = struct.unpack('<i',f.read(4))[0]  # Number of points
    # I deal with that one later
    #    dum6 = struct.unpack('<d',f.read(8))[0]     # Dummy
    
    ## Read mesh parameters
        x_file = './interpmesh/x_pts.bin'
        with open(x_file, 'rb') as f_x:
            dum1_x = struct.unpack('<i',f_x.read(4))[0]                             # Dummy: length of n_pts record
            n_pts_x = struct.unpack('<i',f_x.read(4))[0]                            # Number of points
            dum2_x = struct.unpack('<i',f_x.read(4))[0]                             # Dummy: length of n_pts record
            dum3_x = struct.unpack('<i',f_x.read(4))[0]                             # Dummy: length of data record
            x_pts = np.asarray(struct.unpack('<'+npoints*'d',f_x.read(npoints*8)))  # x_pts
            dum4_x = struct.unpack('<i',f_x.read(4))[0]                             # Dummy: length of data record
    
        y_file = './interpmesh/y_pts.bin'
        with open(y_file, 'rb') as f_y:
            dum1_y = struct.unpack('<i',f_y.read(4))[0]                             # Dummy: length of n_pts record
            n_pts_y = struct.unpack('<i',f_y.read(4))[0]                            # Number of points
            dum2_y = struct.unpack('<i',f_y.read(4))[0]                             # Dummy: length of n_pts record
            dum3_y = struct.unpack('<i',f_y.read(4))[0]                             # Dummy: length of data record
            y_pts = np.asarray(struct.unpack('<'+npoints*'d',f_y.read(npoints*8)))  # y_pts
            dum4_y = struct.unpack('<i',f_y.read(4))[0]                             # Dummy: length of data record
    
        # Radial and circumferential distribution
        # Read from file
        n_pts_file = 'interpmesh/number_of_points.txt'
        n_r, n_phi = np.loadtxt(n_pts_file, dtype='int')

        r_pts = x_pts[-n_r:]
    
        # Variable for storing circumferential points
        phi_pts = np.zeros(n_phi)
        # Keep redundant definition of 0°=360° at this position???
        phi_pts = np.linspace(0,2*np.pi,n_phi)
        
        # Mesh generation in polar coordinates
        theta, rho = np.meshgrid(phi_pts, r_pts, indexing='xy')
        # Mesh transformation in cartesian coordinates
        X, Y = my_math.pol2cart(theta, rho)
    
    ## Read in all the statistics and derivatives
    #--------------------------------------------
        # Arrange fld in matrix form
        # first column is all radial points for phi[0] and so on
        fld_m = np.zeros((n_r,n_phi))               # Current field in 2D array 
        stats_m = np.zeros((n_r,n_phi, nstat))      # Store all fields in a 3D array 
        deriv_m = np.zeros((n_r,n_phi, nderiv))
        for k in range(0,nstat):
            # Offset by 8 bytes
            f.seek(8,1)
    
            # Store current field as a 1D array
            fld_ar = np.asarray(struct.unpack('<'+npoints*'d',f.read(8*npoints)))
    
            for i in range(0,n_phi):
                fld_m[:,i] = fld_ar[i*n_r:(i+1)*n_r]
    
            stats_m[:,:,k] = fld_m
            
        for k in range(0,nderiv):
            # Offset by 8 bytes
            f.seek(8,1)
    
            # Store current field as a 1D array
            fld_ar = np.asarray(struct.unpack('<'+npoints*'d',f.read(8*npoints)))
    
            for i in range(0,n_phi):
                fld_m[:,i] = fld_ar[i*n_r:(i+1)*n_r]
    
            deriv_m[:,:,k] = fld_m
    
    # Save the data
    np.savez(outdata, stats_m=stats_m, deriv_m=deriv_m, r=r_pts, phi=phi_pts,
        X=X, Y=Y)


def n2py_3d(indata, outdata, n_pts, n_fields, ifverbose=False):
    """ 
    Convert nek to python format. 
    
    This function reads data generated by Nek5000 and saved in binary format
    and saves them in uncompressed .npz format

    Parameters
    ----------
    indata : string
        Location of the input file(s).
    outdata : string
        Location of the output file(s).
    n_pts : int
        Number of points to be read from the binary file.
    n_fields : int
        Number of fields to convert.
    ifverbose : boolean
        Give some more verbose output

    """

    ## Read mesh parameters
    x_file = 'interpmesh/x_pts.bin'
    with open(x_file, 'rb') as f_x:
        x_pts = np.asarray(struct.unpack('<'+n_pts*'d',f_x.read(n_pts*8)))  
    y_file = 'interpmesh/y_pts.bin'
    with open(y_file, 'rb') as f_y:
        y_pts = np.asarray(struct.unpack('<'+n_pts*'d',f_y.read(n_pts*8)))
    z_file = 'interpmesh/z_pts.bin'
    with open(z_file, 'rb') as f_z:
        z_pts = np.asarray(struct.unpack('<'+n_pts*'d',f_z.read(n_pts*8)))

    # Number of points in each coordinate direction
    n_pts_file = 'interpmesh/number_of_points.txt'
    n_r, n_phi, n_z= np.loadtxt(n_pts_file, dtype='int')
    if (n_pts != n_r*n_phi*n_z):
        print('Error: Number of points do not match: {0:d} != {1:d}'.format(n_pts, n_r*n_phi*n_z))
        sys.exit(1)

    n_cross = n_r*n_phi
    r_pts = x_pts[-n_r:]
    z_pts_only = z_pts[0::n_cross]

    # Variable for storing circumferential points
    phi_pts = np.zeros(n_phi)
    # Keep redundant definition of 0°=360° at this position???
    phi_pts = np.linspace(0,2*np.pi,n_phi)
    
    # Mesh generation in polar coordinates
    theta, rho = np.meshgrid(phi_pts, r_pts, indexing='xy')
    # Mesh transformation in cartesian coordinates
    X, Y = my_math.pol2cart(theta, rho)


    fld_3d = np.zeros((n_r, n_phi, n_z, n_fields))           

    fname = indata
    with open(fname, 'rb') as f:
        for i_stat in range(n_fields):
            ## Read in the interpolated values
            #---------------------------------
            fld_ar = np.asarray(struct.unpack('<'+n_pts*'d',f.read(n_pts*8)))
            # Reshape into 3D array 
            for iz in range(n_z):
                for iphi in range(n_phi):
                    fld_3d[:, iphi, iz, i_stat] = fld_ar[iphi*n_r+iz*n_cross:(iphi+1)*n_r+iz*n_cross]
    
    ## Save the data
    #---------------
    np.savez(outdata, fld_3d=fld_3d, r=r_pts, phi=phi_pts, z=z_pts_only)

#    with h5py.File(outdata, "w") as f:
#        f.create_dataset(name="fld_3d", data=fld_3d)
#        f.create_dataset(name="r", data=r_pts)
#        f.create_dataset(name="phi", data=phi_pts)
#        f.create_dataset(name="z", data=z_pts_only)


def n2py_3d_cart(indata, outdata, n_pts, n_fields):
    """
    Convert nek to python format.

    This function reads data generated by Nek5000 and saved in binary format
    and saves them in uncompressed .npz format

    Parameters
    ----------
    indata : string
        Location of the input file(s).
    outdata : string
        Location of the output file(s).
    n_pts : int
        Number of points to be read from the binary file.
    n_fields : int
        Number of fields to convert.

    """

    ## Read mesh parameters
    x_file = 'interpmesh/x_pts.bin'
    with open(x_file, 'rb') as f_x:
        x_pts = np.asarray(struct.unpack('<'+n_pts*'d',f_x.read(n_pts*8)))
    y_file = 'interpmesh/y_pts.bin'
    with open(y_file, 'rb') as f_y:
        y_pts = np.asarray(struct.unpack('<'+n_pts*'d',f_y.read(n_pts*8)))
    z_file = 'interpmesh/z_pts.bin'
    with open(z_file, 'rb') as f_z:
        z_pts = np.asarray(struct.unpack('<'+n_pts*'d',f_z.read(n_pts*8)))

    # Number of points in each coordinate direction
    n_pts_file = 'interpmesh/number_of_points.txt'
    n_x, n_y, n_z = np.loadtxt(n_pts_file, dtype='int')
    if (n_pts != n_x*n_y*n_z):
        print('Error: Number of points do not match: {0:d} != {1:d}'.format(n_pts, n_x*n_y*n_z))
        sys.exit(1)

    x = x_pts[:n_x]
    y = y_pts[:n_x*n_y:n_x]
    z = z_pts[::n_x*n_y]

    fld_3d = np.zeros((n_x, n_y, n_z, n_fields))

    fname = indata
    with open(fname, 'rb') as f:
        for i_stat in range(n_fields):
            ## Read in the interpolated values
            #---------------------------------
            fld_ar = np.asarray(struct.unpack('<'+n_pts*'d',f.read(n_pts*8)))
            # Reshape into 3D array 
            for iz in range(n_z):
                for iy in range(n_y):
                    fld_3d[:, iy, iz, i_stat] = fld_ar[iy*n_x+iz*(n_x*n_y):(iy+1)*n_x+iz*(n_x*n_y)]

    ## Save the data
    #---------------
    np.savez(outdata, fld_3d=fld_3d, x=x, y=y, z=z)
