import numpy as np
import matplotlib.pyplot as plt
from ..misc import my_math
import configparser
import os
#import h5py
import pdb

from . import n2py

def case_params(interp_dir, fl_max):
    """
    Set the timerange and get number of interpolation points.

    Read start and end time of interpolated data and write them to timerange.ini.
    Read number of interpolation points.

    Parameters
    ----------

    interp_dir : String
        Directory of interpolated data
    fl_max : int
        Maximum file number to consider.

    Returns
    -------
    (starttime, endtime, n_pts) : (float, float, int)
        Time of the first snapshot,
        Time of the second snapshot,
        Number of points of interpolated data

    """

    caseinfo = [interp_dir + 'caseinfo{0:05d}.txt'.format(i+1) for i in range(fl_max)]
    with open(caseinfo[0], 'r') as f:
        starttime = float(f.readline().split()[2])
        n_pts = int(f.readline().split()[2])
    with open(caseinfo[-1], 'r') as f:
        endtime = float(f.readline().split()[2])
    
    config = configparser.ConfigParser(allow_no_value=True)
    config['Time'] = {}
    config.set('Time', '# Single or comma-separated list of timeranges')
    config['Time']['t_select'] = '{0:d}-{1:d}'.format(int(starttime), int(endtime))
    with open('timerange.ini', 'w') as configfile:
        config.write(configfile)

    return (starttime, endtime, n_pts)


def stats(nfields=10, fstart=0, fields=0, reorder=True):
    """ 
    Convenience function to call for statistics.
    
    Parameters
    ----------
    n_fields : int, optional 
        Number of fields to convert 
    f_start : int, optional (experimental)
        First field to consider
    fields : tuple
        Fields to process as input, e.g. (1, 2, 4, 5)
    reorder : boolean
        Reorder statistics for 10t setup so that the first 6 are homogeneous
        (only meaningfull for nfields=10)

        Other settings still important for output

    """

    indata = ['ZSTAT/int_fldtt{0:d}'.format(i) for i in range(fstart, nfields)]

    if (reorder == True):
        indata = (
                "ZSTAT/int_fldtt0",
                "ZSTAT/int_fldtt1", 
                "ZSTAT/int_fldtt2", 
                "ZSTAT/int_fldtt5", 
                "ZSTAT/int_fldtt6", 
                "ZSTAT/int_fldtt7", 
                "ZSTAT/int_fldtt3", 
                "ZSTAT/int_fldtt4", 
                "ZSTAT/int_fldtt8", 
                "ZSTAT/int_fldtt9", 
                "ZSTAT/int_fldtv1"
                )
        # Reorder them such that azimuthally non-homogeneous are at the end.
        # and 6t setup can be reused
        outdata = (
                "statistics_matrix_tt1.npz",
                "statistics_matrix_tt2.npz",
                "statistics_matrix_tt3.npz",
                "statistics_matrix_tt4.npz",
                "statistics_matrix_tt5.npz",
                "statistics_matrix_tt6.npz",
                "statistics_matrix_tt0.npz",
                "statistics_matrix_tt7.npz",
                "statistics_matrix_tt8.npz",
                "statistics_matrix_tt9.npz",
                "statistics_matrix_tv1.npz",
                )
    else:
        if (fields == 0):
            indata = ['ZSTAT/int_fldtt{0:d}'.format(i) for i in range(fstart, nfields+fstart)]
            indata.append('ZSTAT/int_fldtv1')
            outdata = ['statistics_matrix_tt{0:d}'.format(i) for i in range(1, nfields+1)]
            outdata.append('statistics_matrix_tv1.npz')
        else:
            indata = ['ZSTAT/int_fldtt{0:d}'.format(i) for i in fields]
            indata.append('ZSTAT/int_fldtv1')
            outdata = ['statistics_matrix_tt{0:d}'.format(i) for i in range(1, nfields+1)]
            outdata.append('statistics_matrix_tv1.npz')




    # Set the timerange according the start and end time in interpolated field
    #-------------------------------------------------------------------------
    with open(indata[0], 'rb') as f:
        header = f.readline()
    
    starttime = float(header.split()[32][:-1].decode('ascii'))
    endtime = float(header.split()[36][:-1].decode('ascii'))
    
    config = configparser.ConfigParser(allow_no_value=True)
    config['Time'] = {}
    config.set('Time', '# Single or comma-separated list of timeranges')
    config['Time']['t_select'] = '{0:d}-{1:d}'.format(int(starttime), int(endtime))
    with open('timerange.ini', 'w') as configfile:
        config.write(configfile)

    # Create folder to store python results:
    #---------------------------------------
    stats_path = 'stats_{0:d}-{1:d}/'.format(int(starttime), int(endtime))
    if not os.path.exists(stats_path):
        os.makedirs(stats_path)
    
    for i in range(len(indata)):
        n2py.n2py_stats(indata[i], stats_path + outdata[i])


def t_3d(n_fields = 1, fl_max = 0):
    """ 
    Convenience function to call for thermal fields in my setup.
    
    Parameters
    ----------

    n_fields : int, optional (>1: experimental)
        Number of fields to convert (>1 for statistics & derivatives)
    fl_max : int, optional 
        Maximum number of fields to process (0: find it)
    
    """

    interp_dir = 'interptemp/'
    interp_dir_python = 'interptemp_python/'
    
    # Loop over all files present to find maximum number
    n_temp = 0
    file_max = 0
    for fl in os.listdir(interp_dir):
        fname = interp_dir + '/' + fl
        if os.path.isfile(fname):
            if (fl[-4:] != '.txt'):
                file_max = max(file_max, int(fl[-5:]))       # maximum file number
                n_temp = max(n_temp, int(fl[1]))             # maximum thermal field number
    # Thermal fields start from 0
    n_temp += 1     
    if fl_max == 0:
        fl_max = file_max
    
    # Construct lists for indata
    indata = []
    # Construct filenames
    f_t_base = ['t{0:d}_pipe0.f'.format(i) for i in range(0,n_temp)]
    for fl_num in range(fl_max):
        for t in range(n_temp):
            f_t_in = interp_dir + f_t_base[t] + '{0:05d}'.format(fl_num+1)
            indata.append(f_t_in)
    
    (starttime, endtime, n_pts) = case_params(interp_dir, fl_max)
    
    out_dir = interp_dir_python + '{0:d}-{1:d}/'.format(int(starttime), int(endtime))
    # Construct lists for outdata
    outdata = []
    for fl_num in range(fl_max):
        for t in range(n_temp):
            f_t_out = out_dir + 't{0:d}_3d_{1:05d}'.format(t, fl_num+1) 
            outdata.append(f_t_out)
    
    # Create folder to store python results:
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    
    # Call actual function for all input data
    for i in range(len(indata)):
        n2py.n2py_3d(indata[i], outdata[i], n_pts, n_fields)




def v_3d(n_fields = 1, fl_max = 0):
    """ 
    Convenience function to call for thermal fields in my setup.
    
    Parameters
    ----------

    n_fields : int, optional (>1, experimental)
        Number of fields to convert (>1 for statistics & derivatives)
    fl_max : int, optional
        Maximum number of fields to process (0: find it)
    
    """

    interp_dir = 'interpvel/'
    interp_dir_python = 'interpvel_python/'
    
    # Loop over all files present to find maximum number (of files per thermal field and thermal field)
    file_max = 0
    for fl in os.listdir(interp_dir):
        fname = interp_dir + '/' + fl
        if os.path.isfile(fname):
            file_max = max(file_max, int(fl[-5:]))       # maximum file number
    if fl_max == 0:
        fl_max = file_max
    
    # Construct lists for indata
    indata = []
    f_vx_base = 'vx_pipe0.f'
    f_vy_base = 'vy_pipe0.f'
    f_vz_base = 'vz_pipe0.f'
    for fl_num in range(fl_max):
        f_vx_in = interp_dir + f_vx_base + '{0:05d}'.format(fl_num+1)
        f_vy_in = interp_dir + f_vy_base + '{0:05d}'.format(fl_num+1)
        f_vz_in = interp_dir + f_vz_base + '{0:05d}'.format(fl_num+1)
        indata.extend((f_vx_in, f_vy_in, f_vz_in))

    (starttime, endtime, n_pts) = case_params(interp_dir, fl_max)

    out_dir = interp_dir_python + '{0:d}-{1:d}/'.format(int(starttime), int(endtime))
    # Construct lists for outdata
    outdata = []
    for fl_num in range(1, fl_max+1):
        f_vx_out = out_dir + 'vx_3d_' + '{0:05d}'.format(fl_num) 
        f_vy_out = out_dir + 'vy_3d_' + '{0:05d}'.format(fl_num)
        f_vz_out = out_dir + 'vz_3d_' + '{0:05d}'.format(fl_num)
        outdata.extend((f_vx_out, f_vy_out, f_vz_out))
    
    # Create folder to store python results:
    #---------------------------------------
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    
    for i in range(len(indata)):
        n2py.np2py_3d(indata[i], outdata[i], n_pts, n_fields)


def stats_3d(n_fields = 2, n_stats = 42, iftemp=True):
    """ 
    Convenience function to call for 3d fields in my setup.

    
    Parameters
    ----------

    n_fields : int, optional 
        Number of fields (e.g. for Pr=0.71 and Pr=0.025) to convert
        Time-averaged and interpolated statistics are named as st?pipe0.f00001.
        The passive scalar *fields* are given by st1, st2, st3, ...
    n_stats : int, optional
        Number of thermal statistics (42 or in previous versions 33)
    iftemp : boolean
        True = evaluate temperature statistics; False = evaluate velocity statistics
    """

    interp_dir_python = 'interpdata_python/'


    if(iftemp):
        interp_dir = 'interptemp/'
        indata = [interp_dir + 't{0:1d}.nekdat'.format(i+1) for i in range(n_fields)]
        avginfo = '../avg/run/avg.info'
    else:
        interp_dir = 'interpvel/'
        indata = [interp_dir + 'v{0:1d}.nekdat'.format(i+1) for i in range(n_fields)]
        avginfo = '../avg/run/avg_v.info'


    caseinfo = interp_dir + 'caseinfo1.txt'
    with open(caseinfo, 'r') as f:
        dummy = float(f.readline().split()[2])
        n_pts = int(f.readline().split()[2])
    with open(avginfo, 'r') as f:
        starttime = float(f.readline().split()[2])
        endtime = float(f.readline().split()[2])
        Re = float(f.readline().split()[2])
    
    config = configparser.ConfigParser(allow_no_value=True)
    config['Time'] = {}
    config.set('Time', '# Single or comma-separated list of timeranges')
    config['Time']['t_select'] = '{0:d}-{1:d}'.format(int(starttime), int(endtime))
    with open('timerange.ini', 'w') as configfile:
        config.write(configfile)


    out_dir = interp_dir_python + '{0:d}-{1:d}/'.format(int(starttime), int(endtime))

    if(iftemp):
        outdata = [out_dir + 't{0:1d}_stats3d'.format(i) for i in range(n_fields)]
    else:
        outdata = [out_dir + 'v{0:1d}_stats3d'.format(i) for i in range(n_fields)]



    # Create folder to store python results:
    #---------------------------------------
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    for i in range(len(indata)):
        n2py.n2py_3d(indata[i], outdata[i], n_pts, n_stats)

def deriv_3d(n_fields = 2, n_derivs = 79, iftemp=True):
    """ 
    Convenience function to call for 3d fields in my setup.

    
    Parameters
    ----------

    n_fields : int, optional 
        Number of fields (e.g. for Pr=0.71 and Pr=0.025) to convert
        Time-averaged and interpolated derivatives are named as dt?.nekdat or dv?.nekdat
    n_derivs : int, optional
        Number of (thermal) derivatives (79, 9)
    iftemp : boolean
        True = evaluate temperature derivatives; False = evaluate velocity derivatives
    """

    interp_dir_python = 'interpdata_python/'

    interp_dir = 'interpderiv/'
    if(iftemp):
        indata = [interp_dir + 'dt{0:d}.nekdat'.format(i+1) for i in range(n_fields)]
        avginfo = '../avg/run/avg.info'
    else:
        indata = [interp_dir + 'dv{0:d}.nekdat'.format(i+1) for i in range(n_fields)]
        avginfo = '../avg/run/avg_v.info'


    caseinfo = interp_dir + 'caseinfo1.txt'
    with open(caseinfo, 'r') as f:
        dummy = float(f.readline().split()[2])
        n_pts = int(f.readline().split()[2])
    with open(avginfo, 'r') as f:
        starttime = float(f.readline().split()[2])
        endtime = float(f.readline().split()[2])
        Re = float(f.readline().split()[2])
    
    config = configparser.ConfigParser(allow_no_value=True)
    config['Time'] = {}
    config.set('Time', '# Single or comma-separated list of timeranges')
    config['Time']['t_select'] = '{0:d}-{1:d}'.format(int(starttime), int(endtime))
    with open('timerange.ini', 'w') as configfile:
        config.write(configfile)


    out_dir = interp_dir_python + '{0:d}-{1:d}/'.format(int(starttime), int(endtime))

    if(iftemp):
        outdata = [out_dir + 't{0:1d}_derivs3d'.format(i) for i in range(n_fields)]
    else:
        outdata = [out_dir + 'v{0:1d}_derivs3d'.format(i) for i in range(n_fields)]



    # Create folder to store python results:
    #---------------------------------------
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    for i in range(len(indata)):
        n2py.n2py_3d(indata[i], outdata[i], n_pts, n_derivs)


def snaps_3d(n_files, n_fields, iftemp=True, ifcart=False):
    """ 
    Convenience function to call for 3d fields in my setup.

    
    Parameters
    ----------

    n_files : int
        Number of files 
    n_fields : int
        Number of fields within each file
    iftemp : boolean
        True = evaluate temperature statistics; False = evaluate velocity statistics
    ifcart : boolean [False]
        Interpolate to a cartesian mesh?
    """

    interp_dir_python = 'interpdata_python/'

    if(iftemp):
        interp_dir = 'interptemp/'
        indata = [interp_dir + 't{0:05d}.nekdat'.format(i) for i in range(1, n_files+1)]
    else:
        interp_dir = 'interpvel/'
        indata = [interp_dir + 'v{0:05d}.nekdat'.format(i) for i in range(1, n_files+1)]


    caseinfo_list = [interp_dir + 'caseinfo{0:05d}.txt'.format(i) for i in range(1, n_files+1)]
    time_list = []
    for caseinfo in caseinfo_list:
        with open(caseinfo, 'r') as f:
            this_time = float(f.readline().split()[2])
            n_pts = int(f.readline().split()[2])
        time_list.append('{0:d}'.format(int(this_time)))

    
    config = configparser.ConfigParser(allow_no_value=True)
    config['Time'] = {}
    config.set('Time', '# Single or comma-separated list of times')
    config['Time']['t_select'] = ",".join(time_list)
    with open('timelist.ini', 'w') as configfile:
        config.write(configfile)

    out_dir = interp_dir_python + '{0:d}-{1:d}/'.format(int(time_list[0]), int(time_list[-1]))

    if(iftemp):
        outdata = [out_dir + 't{0:05d}'.format(i) for i in range(n_files)]
    else:
        outdata = [out_dir + 'v{0:05d}'.format(i) for i in range(n_files)]



    # Create folder to store python results:
    #---------------------------------------
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    for i in range(len(indata)):
        if (ifcart):
            n2py.n2py_3d_cart(indata[i], outdata[i], n_pts, n_fields) 
        else:
            n2py.n2py_3d(indata[i], outdata[i], n_pts, n_fields) 


def vis(t_fld=0, n_snap=1):
    """ 
    Convenience function to Plot cross sections of instantaneous interpolated fields.

    Parameters
    ----------
    
    t_fld : int, optional
        Number of thermal field
    n_snap : int, optional
        Number of snapshot

    """

    plt.close('all')
    
    ## Load data
    #-----------
    # Number of calculated (and interpolated) statistics & derivatives
    n_fields = 1

    # Construct filenames
    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')[0]

    f_t_base = 't{0:d}_3d_'.format(t_fld)
    interp_dir_python = 'interptemp_python/' + timeranges_select + '/'
    fname0 = interp_dir_python + f_t_base + '{0:05d}.npz'.format(n_snap) 

    # Read first snapshot to get dimensions for array
    with np.load(fname0) as data:
        fld = data['fld_3d']
        r = data['r']
        phi = data['phi']
        z = data['z']
    
    
    # Mesh generation in polar coordinates
    theta, rho = np.meshgrid(phi, r, indexing='xy')
    X, Y = my_math.pol2cart(theta, rho)
    
    # Set up an array for y-z plane
    y = np.concatenate((-r[::-1], r))
    zz, yy = np.meshgrid(z, y, indexing='xy')
    nphi = len(phi)
    
    for i_fld in range(n_fields):
        plt.figure()
        plt.contourf(X, Y, fld[:, :, 0, i_fld], 50, cmap='jet')
        plt.xlabel(r'$ x/ D$')
        plt.ylabel(r'$ y/ D$')
        plt.colorbar()
    
        fld_yz = np.concatenate((fld[::-1, int((nphi-1)*3/4), :, i_fld], fld[:, int((nphi-1)*1/4), :, i_fld]))
    
        plt.figure()
        plt.contourf(zz, yy, fld_yz, 50, cmap='jet')
        plt.xlabel(r'$ z/ D$')
        plt.ylabel(r'$ y/ D$')
        plt.axis('equal')
        plt.colorbar()
    
    plt.show()
    
    
def vis_3d(fname, num_stat=0):
    """ 
    Convenience function to Plot 2d plots of interpolated data.

    Parameters
    ----------
    
    fname : string
        Filename
    num_stat : int
        Number of saved statistic 

    """

    plt.close('all')
    
    ## Load data
    #-----------
    with np.load(fname) as data:
        fld = data['fld_3d']
        r = data['r']
        phi = data['phi']
        z = data['z']

#    with h5py.File(fname, 'r') as data:
#        fld = np.asarray(data['fld_3d'])
#        r = np.asarray(data['r'])
#        phi = np.asarray(data['phi'])
#        z = np.asarray(data['z'])

    print('Min, max: {0:f}, {1:f}'.format(np.min(fld[..., num_stat]), np.max(fld[..., num_stat])))

    
    # Mesh generation in polar coordinates
    theta, rho = np.meshgrid(phi, r, indexing='xy')
    X, Y = my_math.pol2cart(theta, rho)
    
    # Set up an array for y-z plane
    y = np.concatenate((-r[::-1], r))
    zz, yy = np.meshgrid(z, y, indexing='xy')
    nphi = len(phi)

    # Set up arrays for wall-plane
    Rphi = r[-1]*phi
    zw, rw = np.meshgrid(z, Rphi, indexing='xy')

    
#    plt.figure()
#    plt.contourf(X, Y, fld[:, :, 0, num_stat])
#    plt.xlabel(r'$ x/ D$')
#    plt.ylabel(r'$ y/ D$')
#    plt.colorbar()
#    plt.title('Cross section at the inlet')

    fld_yz = np.concatenate((fld[::-1, int((nphi-1)*3/4), :, num_stat], fld[:, int((nphi-1)*1/4), :, num_stat]))

    plt.figure()
    plt.contourf(zz, yy, fld_yz)
    plt.xlabel(r'$ z/ D$')
    plt.ylabel(r'$ y/ D$')
    plt.axis('equal')
    plt.colorbar()
    plt.title(r'yz-plane at $\varphi=0$')

    plt.figure()
    plt.contourf(zw, rw, fld[-1, :, :, num_stat])
    plt.xlabel(r'$ z/ D$')
    plt.ylabel(r'$ R \varphi / D$')
    plt.axis('equal')
    plt.colorbar()
    plt.title(r'wall-plane at $r=R$')

    plt.show()
    
 
