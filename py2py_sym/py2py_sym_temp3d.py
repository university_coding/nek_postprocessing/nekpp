#!/usr/bin/env python3.5
#-----------------------
# This script reads previously saved data 
# calculates statistics
# and stores the data again.
# ----------------------------------------------------
# Author:   Steffen Straub
# Date  :   2019/10/07
#-----------------------------------------------------

import numpy as np

from ..misc import my_math
import os

import configparser

import pdb


# Define function
def save_stats(span, n_fields):

    # Load my data
    #-------------
    # 1) Velocity statistics
    indata_v = 'interpdata_python/' + span + '/v0_stats3d.npz'
    with np.load(indata_v) as data:
    
        # Mesh points
        r = data['r']
        phi = data['phi']
        z = data['z']
       
        fld_v = data['fld_3d']


    # 2) Temperature statistics
    indata_t_list = ['interpdata_python/'+span+'/t{0:d}_stats3d.npz'.format(k)
            for k in range(n_fields)]
    outdata_list = ['stats_3d/'+span+'/temp_{0:d}.npz'.format(k)
            for k in range(n_fields)]


    Pr_list = (
            0.71,
            0.025,
            )

    for i in range(len(indata_t_list)):
        indata_t = indata_t_list[i]
        outdata = outdata_list[i]
        Pr = Pr_list[i]
        print('Processing ' + indata_t)

        with np.load(indata_t) as data:
        
            # Mesh points
            r = data['r']
            phi = data['phi']
            z = data['z']
        
            fld_t = data['fld_3d']
             

    
        # Read user defined case-dependent parameters
        config = configparser.ConfigParser()
        config.read('case_params.ini')
        Re_b = float(config['Reynolds']['Re_b'])
        Re_t = float(config['Reynolds']['Re_t'])
        Lz = float(config['Domain']['Lz'])

        ## Calculations
        #-------------
        theta, rho, zz= np.meshgrid(phi, r, z, indexing='xy')
        nu = 1/Re_b
        Pe = Re_b * Pr
        D = 1
        R = D/2
#        u_t = my_math.get_utau(r, phi, deriv_vel, theta, nu)
    
    
        (nr, nphi, nz, n_stats) = np.shape(fld_t)

        UiTheta_cart = np.zeros((nr, nphi, nz, 3))
    
        Theta = fld_t[:, :, :, 0]
        Theta_Theta = fld_t[:, :, :, 1]
        UiTheta_cart[:, :, :, 0:3] = fld_t[:, :, :, 2:5]                # <U_1 Theta>, <U_2 Theta>, <U_3 Theta>
        UiTheta = my_math.transform_vect(theta, UiTheta_cart, 1)

        Ui_cart = fld_v[:, :, :, 0:3]
        theta_theta = Theta_Theta - Theta**2
        
        uitheta_cart = UiTheta_cart[..., 0:3] - np.einsum('...i,...',Ui_cart, Theta)
        uitheta = my_math.transform_vect(theta, uitheta_cart, 1)
    
        # Calculate Nusselt number (notes from 09/10/2019)
        # Use transpose for broadcasting first dimension and not last
        UiTheta_dA =   np.trapz(np.trapz((UiTheta[..., 2].T*r).T, r, axis=0), phi, axis=0)
        A_cross = 0.5**2 * np.pi
        U_b = 1

        Theta_b = UiTheta_dA/(U_b * A_cross)
        Theta_w = Theta[-1, :, :]
        qw = np.pi * np.outer(np.sin(phi), np.exp(-1/2 * ((z - Lz/2)/(Lz/5))**2))
        # lower half is adiabatic
        qw[int((nphi-1)/2):, :] = 0     
        # circumferentially averaged
        qw_z = np.exp(-1/2 * ((z - Lz/2)/(Lz/5))**2)
        Theta_w_z = np.trapz(Theta_w, R*phi, axis=0)/(2*np.pi*R)
        
        Nuss = np.zeros((np.shape(Theta_w)))
        Nuss_z = np.zeros((np.shape(Theta_w_z)))
        # Skip inlet to avoid division by zero
        Nuss[:, 1:] = Pe * qw[:, 1:] /(Theta_w[:, 1:] - Theta_b[1:])
        Nuss_z[1:] = Pe * qw_z[1:]/(Theta_w_z[1:] - Theta_b[1:])


        ## Left - right symmetry
        #-----------------------
        Theta_sym = my_math.left_right_sym(Theta, phi, tensor_order=0)
        theta_theta_sym = my_math.left_right_sym(theta_theta, phi, tensor_order=0)
        uitheta_sym = my_math.left_right_sym(uitheta, phi, tensor_order=1)


        mask_q1 = ( (0 <= phi) & (phi <= np.pi/2) )
        mask_q2 = ( (np.pi/2 <= phi) & (phi <= np.pi) )
        mask_q3 = ( (np.pi <= phi) & (phi <= np.pi*3/2) )
        mask_q4 = ( (np.pi*3/2 <= phi) & (phi <= np.pi*2) )
        phi_q1 = phi[mask_q1]
        phi_q4 = phi[mask_q4]
        phi_right = np.concatenate((phi_q4, phi_q1[1:]))
        phi_sym = np.concatenate((phi_q4-2*np.pi, phi_q1[1:]))
        # Mesh generation in polar coordinates
        theta_sym, rho_sym = np.meshgrid(phi_right, r, indexing='xy')
        # Mesh transformation in cartesian coordinates
        X_sym, Y_sym = my_math.pol2cart(theta_sym, rho_sym)



        stats_1d = np.zeros((nz, 1))
        stats_1d[:, 0] = Nuss_z

        stats_2d = np.zeros((nphi, nz, 1))
        stats_2d[..., 0] = Nuss

        stats_3d = np.zeros((nr, nphi, nz, 5))
        stats_3d[..., 0] = Theta
        stats_3d[..., 1] = theta_theta
        stats_3d[..., 2:5] = uitheta

        stats_3d_sym = np.zeros((nr, int((nphi-1)/2)+1, nz, 5))
        stats_3d_sym[..., 0] = Theta_sym
        stats_3d_sym[..., 1] = theta_theta_sym
        stats_3d_sym[..., 2:5] = uitheta_sym
#        stats_3d[..., 5] = Theta_Theta
#        stats_3d[..., 6:9] = UiTheta_cart
#        stats_3d[..., 9:12] = Ui_cart
    
    
        file_path = outdata
        directory = os.path.dirname(file_path)
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        np.savez(file_path, 
                stats_3d=stats_3d,
                stats_3d_sym=stats_3d_sym,
                stats_2d=stats_2d,
                stats_1d=stats_1d,
                r=r, phi=phi, phi_sym=phi_sym, z=z,
                X_sym=X_sym, Y_sym=Y_sym) 
    
      

def run(n_fields=2):
    """ Calculate temperature statistics. 

    Parameters:
    n_fields : scalar
        Number of fields
    """
    # Call function

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    for span in timeranges_select:
        save_stats(span, n_fields)   
