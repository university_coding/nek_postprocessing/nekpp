#!/usr/bin/env python3.5
#-----------------------
# This script reads previously saved data 
# exploits symmetry and stores the data again.
# ----------------------------------------------------
# Author:   Steffen Straub
# Date  :   2018/04/04
#-----------------------------------------------------

import numpy as np

from ..misc import my_math
import os

import configparser

import pdb


# Define function
def expl_sym(span, nfields, fstart, fields):

    # Load my data
    #-------------
    # 1) Velocity statistics
    indata_v = 'stats_' + span + '/statistics_matrix_tv1.npz'
    with np.load(indata_v) as data:
    
        stats_vel = data['stats_m']
        deriv_vel = data['deriv_m']

    # 2) Temperature statistics
    if isinstance(fields, tuple):
        indata_t_list = ['stats_'+span+'/statistics_matrix_tt{0:d}.npz'.format(k)
                for k in (fields)]
    else:
        indata_t_list = ['stats_'+span+'/statistics_matrix_tt{0:d}.npz'.format(k) 
                for k in range(fstart, nfields+fstart)]
    outdata_list = ['stats_2d_sym/'+span+'/2d_sym_tbudget_tt{0:d}.npz'.format(k) 
            for k in range(fstart, nfields+fstart)]

#    indata_t_list = (
#        'stats_' + span + '/statistics_matrix_tt0.npz', 
#        'stats_' + span + '/statistics_matrix_tt7.npz', 
#        'stats_' + span + '/statistics_matrix_tt8.npz', 
#        'stats_' + span + '/statistics_matrix_tt9.npz', 
#        )
#    outdata_list = (
#       'stats_2d_sym/' + span + '/2d_sym_tbudget_tt1.npz',
#       'stats_2d_sym/' + span + '/2d_sym_tbudget_tt2.npz',
#       'stats_2d_sym/' + span + '/2d_sym_tbudget_tt3.npz',
#       'stats_2d_sym/' + span + '/2d_sym_tbudget_tt4.npz',       
#       )


    print('Note: label_list and Pr_list are hard coded.')
    label = (
        'halfconst071',
        'halfsin071',
        'halfconst0025',       
        'halfsin0025',
        )

    Pr = (
            0.71,
            0.71,
            0.025,
            0.025,
            )


    for i in range(len(indata_t_list)):
        indata_t = indata_t_list[i]
        outdata = outdata_list[i]
        print('Processing ' + indata_t)
        with np.load(indata_t) as data:
        
            # Coordinates
            X = data['X']
            Y = data['Y']
        
            # individual radial and circumferential distribution
            r = data['r']
            phi = data['phi']
        
            # runtime averages and postprocessing derivatives
            stats_temp = data['stats_m']
            deriv_temp = data['deriv_m']
        
        
        # Read user defined case-dependent parameters
        config = configparser.ConfigParser()
        config.read('case_params.ini')
        Re_b = float(config['Reynolds']['Re_b'])

        # Calculations
        #-------------
        Pe = Re_b*Pr[i]
    
        theta, rho= np.meshgrid(phi, r, indexing='xy')
        nu = 1/Re_b
        u_t = my_math.get_utau(r, phi, deriv_vel, theta, nu)
        Re_t = u_t*1/nu
        y = 0.5 - r 
        y_plus = (y*u_t)/nu
     
        
        # Allocate variables for vectors and tensors
        #-------------------------------------------
        # Production variables
        Ui_cart = np.zeros((np.size(r), np.size(phi), 3))
        Theta = np.zeros((np.size(r), np.size(phi)))
        dThetadxi_cart = np.zeros((np.size(r), np.size(phi), 3))
        UiTheta_cart = np.zeros((np.size(r), np.size(phi), 3))
        uitheta_cart = np.zeros((np.size(r), np.size(phi), 3))
        P_full = np.zeros((np.size(r), np.size(phi)))
        
        # Dissipation variables
        dThetadxi_dThetadxi = np.zeros((np.size(r), np.size(phi)))
        eps_full = np.zeros((np.size(r), np.size(phi)))
        
        # Molecular diffusion variables
        d2Thetadxi2 = np.zeros((np.size(r), np.size(phi)))
        d2ThetaThetadxi2 = np.zeros((np.size(r), np.size(phi)))
        d2Theta_Thetadxi2 = np.zeros((np.size(r), np.size(phi)))
        d2thetathetadxi2 = np.zeros((np.size(r), np.size(phi)))
        MD_full = np.zeros((np.size(r), np.size(phi)))
        
        # Turbulent diffusion variables
        dUiThetaThetadxi = np.zeros((np.size(r), np.size(phi)))
        dUi_Theta_Thetadxi = np.zeros((np.size(r), np.size(phi)))
        dThetaThetadxi_cart = np.zeros((np.size(r), np.size(phi), 3))
        dthetathetadxi_cart = np.zeros((np.size(r), np.size(phi), 3))
        dUi_thetathetadxi = np.zeros((np.size(r), np.size(phi)))
        dUiThetadxi = np.zeros((np.size(r), np.size(phi)))
        duithetadxi = np.zeros((np.size(r), np.size(phi)))
        duitheta_Thetadxi = np.zeros((np.size(r), np.size(phi)))
        TD_full = np.zeros((np.size(r), np.size(phi)))
        
        # Source term
        S = np.zeros((np.size(r), np.size(phi)))
    
        ## Production
        #------------
        # P_theta = - < u_i theta > * d <Theta> / d x_i
        # < u_i theta > = < U_i Theta > - <U_i> * <Theta>
        Ui_cart[:, :, 0:3] = stats_vel[:, :, 0:3]                                       # <U_1>, <U_2>, <U_3>
        
        Theta[:, :] = stats_temp[:, :, 0]                                     # <Theta>
        dThetadxi_cart[:, :, 0:2] = deriv_temp[:, :, 0:2]              # d <Theta> / d x_1, d <Theta> / d x_2
        UiTheta_cart[:, :, 0:3] = stats_temp[:, :, 2:5]                # <U_1 Theta>, <U_2 Theta>, <U_3 Theta>
        uitheta_cart[:, :, :] = UiTheta_cart[:, :, 0:3] - np.einsum('...i,...',Ui_cart, Theta[:, :])
        
        P_full[:, :] = - np.einsum('...i,...i', uitheta_cart[:, :, :], dThetadxi_cart[:, :, :])
        
        
        ## Dissipation
        #-------------
        # eps_theta = - k/(rho C_p) < d theta / d x_i * d theta / d x_i >
        # k/(rho C_p) = a = alpha = 1/Pe = 1/(Re * Pr)
        # < d theta / d x_i * d theta / d x_i > = < d Theta / d x_i * d Theta / d x_i > - < d Theta / d x_i > * d Theta / d x_i >
        dThetadxi_dThetadxi[:, :] = stats_temp[:, :, 29]
        
        eps_full[:, :] = - 1/Pe * ( dThetadxi_dThetadxi[:, :] - np.einsum('...i,...i', dThetadxi_cart[:, :, :], dThetadxi_cart[:, :, :]) )
        
        
        ## Molecular diffusion
        #---------------------
        # MD = 1/2 * 1/Pe * d**2 < theta theta> / d x_i d x_i
        # d**2 < theta theta > / d x_i**2 = d**2 <Theta Theta> / d x_i**2 - d**2 <Theta><Theta> / d x_i**2
        # d**2 <Theta><Theta> / d x_i**2 = 2 * d <Theta> / d x_i * d <Theta> / d x_i + 2 * <Theta> * d**2 <Theta> / d x_i**2
        d2Thetadxi2[:, :] = deriv_temp[:, :, 16] + deriv_temp[:, :, 17]         # d**2 <Theta> / d x_1**2 + d**2 <Theta> / d x_2**2
        d2ThetaThetadxi2[:, :] = deriv_temp[:, :, 18] + deriv_temp[:, :,19]     # d**2 <ThetaTheta> / d x_1**2 + d**2 <ThetaTheta> / d x_2**2 
        d2Theta_Thetadxi2[:, :] = 2 * ( np.einsum('...i,...i', dThetadxi_cart[:, :, :], dThetadxi_cart[:, :, :]) 
                + Theta[:, :] * d2Thetadxi2[:, :] )
        d2thetathetadxi2[:, :] = d2ThetaThetadxi2[:, :] - d2Theta_Thetadxi2[:, :]
        
        MD_full[:, :] = 1/2 * 1/Pe * d2thetathetadxi2[:, :]
        
        
        ## Turbulent diffusion
        #---------------------
        # TD = - 1/2 * d < u_i theta theta > / d x_i
        # d < ui theta theta > d x_i = d < U_i Theta Theta > / d x_i - d <U_i> <Theta> <Theta> / d x_i - d <U_i> <theta theta> / d x_i - 2 d <u_i theta> <Theta> / d x_i
        dUiThetaThetadxi[:, :] = deriv_temp[:, :, 10] + deriv_temp[:, :, 13]     # d <U_1 Theta Theta> / d x_1 + d <U_2 Theta Theta> / d x_2
        
        # d <U_i> <Theta> <Theta> = d <U_i> / d x_i <Theta> <Theta> + 2 <U_i> <Theta> d <Theta> / d x_i
        # d <U_i> <Theta> <Theta> = 0                               + 2 <U_i> <Theta> d <Theta> / d x_i
        dUi_Theta_Thetadxi[:, :] = 2 * np.einsum('...i,...i', Ui_cart, dThetadxi_cart[:, :, :]) * Theta[:, :]
        
        # d <U_i> <theta theta> / d x_i = <U_i> d <theta theta> / d x_i
        # d <theta theta> / d x_i = d <Theta Theta> / d x_i - 2 <Theta> d <Theta> / d x_i
        dThetaThetadxi_cart[:, :, 0:2] = deriv_temp[:, :, 2:4]                       # d <Theta Theta> / d x_i
        dthetathetadxi_cart[:, :, :] = dThetaThetadxi_cart[:, :, :] - 2 * np.einsum('...,...i', Theta[:, :], dThetadxi_cart[:, :, :])
        dUi_thetathetadxi[:, :] = np.einsum('...i,...i', Ui_cart, dthetathetadxi_cart[:, :, :])
        
        # d <u_i theta> <Theta> / d x_i = d <u_i theta> / d x_i <Theta> + <u_i theta> d <Theta> / d x_i
        # d <u_i theta> / d x_i = d <U_i Theta> / d x_i - 0 - <U_i> d <Theta> / d x_i
        dUiThetadxi[:, :] = deriv_temp[:, :, 4] + deriv_temp[:, :, 7]             # d <U_1 Theta> / d x_1 + d <U_2 Theta> / d x_2
        duithetadxi[:, :] = dUiThetadxi[:, :] - np.einsum('...i,...i', Ui_cart, dThetadxi_cart[:, :, :])
        
        duitheta_Thetadxi[:, :] = duithetadxi[:, :] * Theta[:, :] + np.einsum('...i,...i', uitheta_cart[:, :, :], dThetadxi_cart[:, :, :])
        
        TD_full[:, :] = - 1/2 * ( 
                + dUiThetaThetadxi[:, :]
                - dUi_Theta_Thetadxi[:, :]
                - dUi_thetathetadxi[:, :]
                - 2*duitheta_Thetadxi[:, :]
                )
        
    
        ## Source term (see Piller)
        #--------------------------
        # S = 4 * < u_z theta >         # for halfconst & halfsin
    
        if ("half" in label[i]):
            S = 4*uitheta_cart[:, :, 2]
        else:
            S = 0
    
           
        # Left - right symmetry
        P_sym = my_math.left_right_sym(P_full, phi, tensor_order=0)
        eps_sym = my_math.left_right_sym(eps_full, phi, tensor_order=0)
        MD_sym = my_math.left_right_sym(MD_full, phi, tensor_order=0)
        TD_sym = my_math.left_right_sym(TD_full, phi, tensor_order=0)
        if ("half" in label[i]):
            S_sym = my_math.left_right_sym(S, phi, tensor_order=0)
        else:
            S_sym = 0
        
    
    
    
        mask_q1 = ( (0 <= phi) & (phi <= np.pi/2) )
        mask_q2 = ( (np.pi/2 <= phi) & (phi <= np.pi) )
        mask_q3 = ( (np.pi <= phi) & (phi <= np.pi*3/2) )
        mask_q4 = ( (np.pi*3/2 <= phi) & (phi <= np.pi*2) )
        phi_q1 = phi[mask_q1]
        phi_q4 = phi[mask_q4]
        phi_right = np.concatenate((phi_q4, phi_q1[1:]))
        phi_sym = np.concatenate((phi_q4-2*np.pi, phi_q1[1:]))
        # Mesh generation in polar coordinates
        theta_sym, rho_sym = np.meshgrid(phi_right, r, indexing='xy')
        # Mesh transformation in cartesian coordinates
        X_sym, Y_sym = my_math.pol2cart(theta_sym, rho_sym)
    
#        # Test without symmetry first
#        P_sym = P_full
#        eps_sym = eps_full
#        MD_sym = MD_full
#        TD_sym = TD_full
#        S_sym = S
#        X_sym = X
#        Y_sym = Y

        
        n_vars = 5
        stats_sym = np.zeros((np.shape(P_sym)[0], np.shape(P_sym)[1], n_vars))
        stats_sym[..., 0] = P_sym * nu
        stats_sym[..., 1] = eps_sym * nu
        stats_sym[..., 2] = MD_sym * nu
        stats_sym[..., 3] = TD_sym * nu
        stats_sym[..., 4] = S_sym * nu
    
    
        file_path = outdata
        directory = os.path.dirname(file_path)
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        np.savez(file_path, 
                stats_sym=stats_sym, r=r, phi=phi, phi_sym=phi_sym,
                X_sym=X_sym, Y_sym=Y_sym) 
    
  

def run(nfields=4, fstart=0, fields=0):
    """ Exploit left-right symmetry for budget of temperature variance.
    
    nfields     Number of fields to interpolate
    fstart      First field starts by, e.g 0, 1
    fields      Tuple of fields to process as input, e.g. (1, 2, 4, 5)
                Other settings still important for output
    """

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    for span in timeranges_select:
        expl_sym(span, nfields, fstart, fields)
