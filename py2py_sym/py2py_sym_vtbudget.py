#!/usr/bin/env python3.5
#-----------------------
# This script reads previously saved data 
# exploits symmetry and stores the data again.
# ----------------------------------------------------
# Author:   Steffen Straub
# Date  :   2018/04/05
#-----------------------------------------------------

import numpy as np

from ..misc import my_math
import os

import configparser

import pdb


# Define function
def expl_sym(span):

    # Load my data
    #-------------
    # 1) Velocity statistics
    indata_v = 'stats_' + span + '/statistics_matrix_tv1.npz'
    with np.load(indata_v) as data:
    
        stats_vel = data['stats_m']
        deriv_vel = data['deriv_m']

    # 2) Temperature statistics
    indata_t_list = (
        'stats_' + span + '/statistics_matrix_tt0.npz', 
        'stats_' + span + '/statistics_matrix_tt7.npz', 
        'stats_' + span + '/statistics_matrix_tt8.npz', 
        'stats_' + span + '/statistics_matrix_tt9.npz', 
        )
    outdata_list = (
       'stats_2d_sym/' + span + '/2d_sym_vtbudget_tt1.npz',
       'stats_2d_sym/' + span + '/2d_sym_vtbudget_tt2.npz',
       'stats_2d_sym/' + span + '/2d_sym_vtbudget_tt3.npz',
       'stats_2d_sym/' + span + '/2d_sym_vtbudget_tt4.npz',       
       )
    label = (
        'halfconst071',
        'halfsin071',
        'halfconst0025',       
        'halfsin0025',
        )

    Pr = (
            0.71,
            0.71,
            0.025,
            0.025,
            )



    for i in range(len(indata_t_list)):
        indata_t = indata_t_list[i]
        outdata = outdata_list[i]
        print('Processing ' + indata_t)
        with np.load(indata_t) as data:
        
            # Coordinates
            X = data['X']
            Y = data['Y']
        
            # individual radial and circumferential distribution
            r = data['r']
            phi = data['phi']
        
            # runtime averages and postprocessing derivatives
            stats_temp = data['stats_m']
            deriv_temp = data['deriv_m']
        
        
        # Read user defined case-dependent parameters
        config = configparser.ConfigParser()
        config.read('case_params.ini')
        Re_b = float(config['Reynolds']['Re_b'])

        # Calculations
        #-------------
        Pe = Re_b*Pr[i]
    
        theta, rho= np.meshgrid(phi, r, indexing='xy')
        nu = 1/Re_b
        u_t = my_math.get_utau(r, phi, deriv_vel, theta, nu)
        Re_t = u_t*1/nu
        y = 0.5 - r 
        y_plus = (y*u_t)/nu
        
        
        
        # Allocate variables for vectors and tensors
        #-------------------------------------------
        # Production variables
        Ui_cart = np.zeros((np.size(r), np.size(phi), 3))
        UiUj_cart = np.zeros((np.size(r), np.size(phi), 3, 3))
        uiuj_cart = np.zeros((np.size(r), np.size(phi), 3, 3))
        dUjdxi_cart= np.zeros((np.size(r), np.size(phi), 3, 3))
        Theta = np.zeros((np.size(r), np.size(phi)))
        dThetadxi_cart = np.zeros((np.size(r), np.size(phi), 3))
        UiTheta_cart = np.zeros((np.size(r), np.size(phi), 3))
        uitheta_cart = np.zeros((np.size(r), np.size(phi), 3))
        
        P_cart = np.zeros((np.size(r), np.size(phi), 3))
        P_cyl = np.zeros((np.size(r), np.size(phi), 3))
        
        
        # Dissipation variables
        dThetadxk_dUidxk_cart = np.zeros((np.size(r), np.size(phi), 3))
        
        eps_cart = np.zeros((np.size(r), np.size(phi), 3))
        eps_cyl = np.zeros((np.size(r), np.size(phi), 3))
        
        
        # Molecular diffusion variables
        d2Ujdxk2_cart = np.zeros((np.size(r), np.size(phi), 3))
        d_ThetadUidxk_dxk_cart = np.zeros((np.size(r), np.size(phi), 3))
        d_thetaduidxk_dxk_cart = np.zeros((np.size(r), np.size(phi), 3))
#        d2_UiTheta_dxk2_cart = np.zeros((np.size(r), np.size(phi), 3))
#        d2_uitheta_dxk2_cart = np.zeros((np.size(r), np.size(phi), 3))

        d2Thetadxi2 = np.zeros((np.size(r), np.size(phi)))
        d_UidThetadxk_dxk_cart = np.zeros((np.size(r), np.size(phi), 3))

        d_uidthetadxk_dxk_cart = np.zeros((np.size(r), np.size(phi), 3))
        
        MD_cart = np.zeros((np.size(r), np.size(phi), 3))
        MD_cyl = np.zeros((np.size(r), np.size(phi), 3))
        
        
        # Turbulent diffusion variables
        dUiUjdxk_cart = np.zeros((np.size(r), np.size(phi), 3, 3, 3))
        duiujdxk_cart = np.zeros((np.size(r), np.size(phi), 3, 3, 3))
        
        dThetaUiUkdxk_cart = np.zeros((np.size(r), np.size(phi), 3))
        d_Theta_Ui_Uk_dxk_cart = np.zeros((np.size(r), np.size(phi), 3))
        d_Theta_uiuk_dxk_cart = np.zeros((np.size(r), np.size(phi), 3))
        dUiThetadxk_cart = np.zeros((np.size(r), np.size(phi), 3, 3))
        duithetadxk_cart = np.zeros((np.size(r), np.size(phi), 3, 3))
        d_thetauk_Ui_dxk_cart = np.zeros((np.size(r), np.size(phi), 3))
        d_thetaui_Uk_dxk_cart = np.zeros((np.size(r), np.size(phi), 3))
        
        TD_cart = np.zeros((np.size(r), np.size(phi), 3))
        TD_cyl = np.zeros((np.size(r), np.size(phi), 3))
        
        
        # Turbulent pressure gradient variables 
        dPThetadxi_cart = np.zeros((np.size(r), np.size(phi), 3))
        PdThetadxi_cart = np.zeros((np.size(r), np.size(phi), 3))
        ThetadPdxi_cart = np.zeros((np.size(r), np.size(phi), 3))
        dPdxi_cart = np.zeros((np.size(r), np.size(phi), 3))
    
        TPG_cart = np.zeros((np.size(r), np.size(phi), 3))
        TPG_cyl = np.zeros((np.size(r), np.size(phi), 3))
    
    
        # Source term
        ThetaUiUj_cart = np.zeros((np.size(r), np.size(phi), 3, 3))
        ThetadUidxj_cart = np.zeros((np.size(r), np.size(phi), 3, 3))
        
    
        ## Production
        #------------
        # P = -<u_i u_k> d <Theta> / d x_k - <u_k theta> d <U_i> / d x_k
        
        Ui_cart[:, :, 0:3] = stats_vel[:, :, 0:3]       # <U_1>, <U_2>, <U_3>
        
        UiUj_cart[:, :, 0, 0] = stats_vel[:,:,4]            # <U_1 U_1>
        UiUj_cart[:, :, 0, 1] = stats_vel[:,:,8]            # <U_1 U_2>
        UiUj_cart[:, :, 0, 2] = stats_vel[:,:,10]           # <U_1 U_3>
        UiUj_cart[:, :, 1, 0] = UiUj_cart[:, :, 0, 1]   # <U_2 U_1>
        UiUj_cart[:, :, 1, 1] = stats_vel[:,:,5]            # <U_2 U_2>
        UiUj_cart[:, :, 1, 2] = stats_vel[:,:,9]            # <U_2 U_3>
        UiUj_cart[:, :, 2, 0] = UiUj_cart[:, :, 0, 2]   # <U_3 U_1>
        UiUj_cart[:, :, 2, 1] = UiUj_cart[:, :, 1, 2]   # <U_3 U_2>
        UiUj_cart[:, :, 2, 2] = stats_vel[:,:,6]            # <U_3 U_3>
        
        uiuj_cart = UiUj_cart - np.einsum('...i,...j', Ui_cart, Ui_cart)
        
        dUjdxi_cart[:, :, 0, 0] = deriv_vel[:,:,0]          # d<U_1> / d x_1
        dUjdxi_cart[:, :, 0, 1] = deriv_vel[:,:,1]          # d<U_1> / d x_2
        dUjdxi_cart[:, :, 0, 2] = 0                     # d<U_1> / d x_3
        dUjdxi_cart[:, :, 1, 0] = deriv_vel[:,:,2]          # d<U_2> / d x_1
        dUjdxi_cart[:, :, 1, 1] = deriv_vel[:,:,3]          # d<U_2> / d x_2
        dUjdxi_cart[:, :, 1, 2] = 0                     # d<U_2> / d x_3
        dUjdxi_cart[:, :, 2, 0] = deriv_vel[:,:,4]          # d<U_3> / d x_1
        dUjdxi_cart[:, :, 2, 1] = deriv_vel[:,:,5]          # d<U_3> / d x_2
        dUjdxi_cart[:, :, 2, 2] = 0                     # d<U_3> / d x_3
        
        
        Theta[:, :] = stats_temp[:, :, 0]                                     # <Theta>
        dThetadxi_cart[:, :, 0:2] = deriv_temp[:, :, 0:2]              # d <Theta> / d x_1, d <Theta> / d x_2
        UiTheta_cart[:, :, 0:3] = stats_temp[:, :, 2:5]                # <U_1 Theta>, <U_2 Theta>, <U_3 Theta>
        uitheta_cart[:, :, :] = UiTheta_cart[:, :, 0:3] - np.einsum('...i,...',Ui_cart, Theta[:, :])
        
        P_cart[:, :, :] = (- np.einsum('...ik,...k', uiuj_cart[:, :, :, :], dThetadxi_cart[:, :, :])
                - np.einsum('...k,...ik', uitheta_cart[:, :, :], dUjdxi_cart[:, :, :, :]))
        
        P_cyl[:, :, :] = my_math.transform_vect(theta, P_cart[:, :, :] , 1)
        
        
        
        
        
        ## Dissipation
        #-------------
        # eps = -a (1+Pr) < d theta / d x_k * d u_i / d x_k >
        #     = -a (1+Pr) ( < d Theta / d x_k * d U_i / d x_k > - d <Theta> / d x_k * d <U_i> / d x_k
        dThetadxk_dUidxk_cart[:, :, 0] = stats_temp[:, :, 30]                     # < d Theta / d x_1 * d U_1 / d x_1 > + < d Theta / d x_2 * d U_1 / d x_2 > + < d Theta / d x_3 * d U_1 / d x_3 >
        dThetadxk_dUidxk_cart[:, :, 1] = stats_temp[:, :, 31]                     # < d Theta / d x_1 * d U_2 / d x_1 > + < d Theta / d x_2 * d U_2 / d x_2 > + < d Theta / d x_3 * d U_2 / d x_3 >
        dThetadxk_dUidxk_cart[:, :, 2] = stats_temp[:, :, 32]                     # < d Theta / d x_1 * d U_3 / d x_1 > + < d Theta / d x_2 * d U_3 / d x_2 > + < d Theta / d x_3 * d U_3 / d x_3 >
        
        eps_cart[:, :, :] = - (1 + Pr[i])/Pe * (dThetadxk_dUidxk_cart[:, :, :] 
                - np.einsum('...k,...ik', dThetadxi_cart[:, :, :], dUjdxi_cart[:, :, :, :])
                )
        eps_cyl[:, :, :] = my_math.transform_vect(theta, eps_cart[:, :, :], 1)
        
        
        
        
        
        
        ## Molecular diffusion
        #---------------------
        # MD = 1/Pe * d / d x_k ( < u_i * d theta / d x_k > + Pr < theta  * d u_i / d x_k > )
        # with  < theta * d u_i / d x_k > = < Theta * d U_i / d x_k > - <Theta> * d <U_i> / d x_k
        # d / d x_k ( < theta * d u_i / d x_k > ) = d / d x_k < Theta * d U_i / d x_k > - d <Theta> / d x_k * d <U_i> / d x_k - <Theta> * d**2 <U_i> / d x_k**2
        # and < ui * d theta / d x_k > = < Ui * d Theta / d x_k > - <U_i> * d <Theta> / d x_k
        # d / d x_k ( < u_i * d theta / d x_k > ) = d / d x_k < U_i * d Theta / d x_k > - d <U_i> / d x_k * d <Theta> / d x_k - <U_i> * d**2 <Theta> / d x_k**2


        
        d2Ujdxk2_cart[:, :, 0] = deriv_vel[:, :, 44] + deriv_vel[:, :, 45]          # d^2 <U_1> / d x_k^2
        d2Ujdxk2_cart[:, :, 1] = deriv_vel[:, :, 46] + deriv_vel[:, :, 47]          # d^2 <U_2> / d x_k^2
        d2Ujdxk2_cart[:, :, 2] = deriv_vel[:, :, 48] + deriv_vel[:, :, 49]          # d^2 <U_3> / d x_k^2
        
        d_ThetadUidxk_dxk_cart[:, :, 0] = deriv_temp[:, :, 26] + deriv_temp[:, :, 27]      # d / d x_1 ( < Theta * d U_1 / d x_1 > ) + d / d x_2 ( < Theta * d U_1 / d x_2 > )
        d_ThetadUidxk_dxk_cart[:, :, 1] = deriv_temp[:, :, 28] + deriv_temp[:, :, 29]      # d / d x_1 ( < Theta * d U_2 / d x_1 > ) + d / d x_2 ( < Theta * d U_2 / d x_2 > )
        d_ThetadUidxk_dxk_cart[:, :, 2] = deriv_temp[:, :, 30] + deriv_temp[:, :, 31]      # d / d x_1 ( < Theta * d U_3 / d x_1 > ) + d / d x_2 ( < Theta * d U_3 / d x_2 > )
        
        d_thetaduidxk_dxk_cart[:, :, :] = (d_ThetadUidxk_dxk_cart[:, :, :] 
            - np.einsum('...k,...ik', dThetadxi_cart[:, :, :], dUjdxi_cart[:, :, :, :])
            - np.einsum('...,...i', Theta[:, :], d2Ujdxk2_cart[:, :, :])
            )

        
        d_UidThetadxk_dxk_cart[:, :, 0] = deriv_temp[:, :, 46] + deriv_temp[:, :, 47]       # d < U_1 d Theta / d x_1 > / d x_1 + d < U_1 d Theta / d x_2 > / d x_2

        d_UidThetadxk_dxk_cart[:, :, 1] = deriv_temp[:, :, 48] + deriv_temp[:, :, 49]       # d < U_2 d Theta / d x_1 > / d x_1 + d < U_2 d Theta / d x_2 > / d x_2

        d_UidThetadxk_dxk_cart[:, :, 2] = deriv_temp[:, :, 50] + deriv_temp[:, :, 51]       # d < U_3 d Theta / d x_1 > / d x_1 + d < U_3 d Theta / d x_2 > / d x_2

        d2Thetadxi2[:, :] = deriv_temp[:, :, 16] + deriv_temp[:, :, 17]         # d**2 <Theta> / d x_1**2 + d**2 <Theta> / d x_2**2

        d_uidthetadxk_dxk_cart[:, :, :] = (d_UidThetadxk_dxk_cart[:, :, :]
                - np.einsum('...ik,...k', dUjdxi_cart[:, :, :, :], dThetadxi_cart[:, :, :])
                - np.einsum('...i,...', Ui_cart[:, :, :], d2Thetadxi2[:, :])
                )


#----------------------------------------------------------------------
# previous version which causes wiggles for MD of streamwise turb. heat flux budget at IFi002 
        # and < u_i * d theta / d x_k > = d < u_i theta > / d x_k - < theta * d u_i / d x_k >

#        d2_UiTheta_dxk2_cart[:, :, 0] = deriv_temp[:, :, 20] + deriv_temp[:, :, 21]             # d2 < U_1 Theta > / d x_1 d x_1 + d2 < U_1 Theta > / d x_2 d x_2
#        d2_UiTheta_dxk2_cart[:, :, 1] = deriv_temp[:, :, 22] + deriv_temp[:, :, 23]             # d2 < U_2 Theta > / d x_1 d x_1 + d2 < U_2 Theta > / d x_2 d x_2
#        d2_UiTheta_dxk2_cart[:, :, 2] = deriv_temp[:, :, 24] + deriv_temp[:, :, 25]             # d2 < U_3 Theta > / d x_1 d x_1 + d2 < U_3 Theta > / d x_2 d x_2
#        
#        d2Thetadxi2[:, :] = deriv_temp[:, :, 16] + deriv_temp[:, :, 17]         # d**2 <Theta> / d x_1**2 + d**2 <Theta> / d x_2**2
#        
#        d2_uitheta_dxk2_cart[:, :, :] = (d2_UiTheta_dxk2_cart[:, :, :] 
#            - np.einsum('...i,...', d2Ujdxk2_cart[:, :, :], Theta[:, :]) 
#            - 2 * np.einsum('...ik,...k', dUjdxi_cart[:, :, :, :], dThetadxi_cart[:, :, :])
#            - np.einsum('...i,...', Ui_cart[:, :, :], d2Thetadxi2[:, :])
#            )
#        
#        d_uidthetadxk_dxk_cart[:, :, :] = d2_uitheta_dxk2_cart[:, :, :] - d_thetaduidxk_dxk_cart[:, :, :]
#----------------------------------------------------------------------

        
        MD_cart[:, :, :] = 1/Pe * d_uidthetadxk_dxk_cart[:, :, :] + 1/Re_b * d_thetaduidxk_dxk_cart[:, :, :]
        
        MD_cyl[:, :, :] = my_math.transform_vect(theta, MD_cart[:, :, :], 1)
        
        
        
        
        ## Turbulent diffusion
        #---------------------
        # TD = - d / d x_k  < theta u_i u_k >
        # d / d x_k < theta u_i u_k > = d / d x_k < Theta U_i U_k > - d / d x_k ( <Theta> <U_i> <U_k> ) - d / d x_k ( <Theta> < u_i u_k > )
        #                               - d / d x_k ( < theta u_k > <U_i> ) - d / d x_k ( < theta u_i > <U_k> )
        
        dUiUjdxk_cart[:, :, 0, 0, 0] = deriv_vel[:, :, 8]                   # d <U_1 U_1> / d x_1
        dUiUjdxk_cart[:, :, 0, 0, 1] = deriv_vel[:, :, 9]                   # d <U_1 U_1> / d x_2
        dUiUjdxk_cart[:, :, 0, 0, 2] = 0                                # d <U_1 U_1> / d x_3
        dUiUjdxk_cart[:, :, 0, 1, 0] = deriv_vel[:, :, 16]                  # d <U_1 U_2> / d x_1
        dUiUjdxk_cart[:, :, 0, 1, 1] = deriv_vel[:, :, 17]                  # d <U_1 U_2> / d x_2
        dUiUjdxk_cart[:, :, 0, 1, 2] = 0                                # d <U_1 U_2> / d x_3
        dUiUjdxk_cart[:, :, 0, 2, 0] = deriv_vel[:, :, 20]                  # d <U_1 U_3> / d x_1
        dUiUjdxk_cart[:, :, 0, 2, 1] = deriv_vel[:, :, 21]                  # d <U_1 U_3> / d x_2
        dUiUjdxk_cart[:, :, 0, 2, 2] = 0                                # d <U_1 U_3> / d x_3
        dUiUjdxk_cart[:, :, 1, 0, 0] = dUiUjdxk_cart[:, :, 0, 1, 0]     # d <U_2 U_1> / d x_1
        dUiUjdxk_cart[:, :, 1, 0, 1] = dUiUjdxk_cart[:, :, 0, 1, 1]     # d <U_2 U_1> / d x_2
        dUiUjdxk_cart[:, :, 1, 0, 2] = dUiUjdxk_cart[:, :, 0, 1, 2]     # d <U_2 U_1> / d x_3
        dUiUjdxk_cart[:, :, 1, 1, 0] = deriv_vel[:, :, 10]                  # d <U_2 U_2> / d x_1
        dUiUjdxk_cart[:, :, 1, 1, 1] = deriv_vel[:, :, 11]                  # d <U_2 U_2> / d x_2
        dUiUjdxk_cart[:, :, 1, 1, 2] = 0                                # d <U_2 U_2> / d x_3
        dUiUjdxk_cart[:, :, 1, 2, 0] = deriv_vel[:, :, 18]                  # d <U_2 U_3> / d x_1
        dUiUjdxk_cart[:, :, 1, 2, 1] = deriv_vel[:, :, 19]                  # d <U_2 U_3> / d x_2
        dUiUjdxk_cart[:, :, 1, 2, 2] = 0                                # d <U_2 U_3> / d x_3
        dUiUjdxk_cart[:, :, 2, 0, 0] = dUiUjdxk_cart[:, :, 0, 2, 0]     # d <U_3 U_1> / d x_1
        dUiUjdxk_cart[:, :, 2, 0, 1] = dUiUjdxk_cart[:, :, 0, 2, 1]     # d <U_3 U_1> / d x_2
        dUiUjdxk_cart[:, :, 2, 0, 2] = dUiUjdxk_cart[:, :, 0, 2, 2]     # d <U_3 U_1> / d x_3
        dUiUjdxk_cart[:, :, 2, 1, 0] = dUiUjdxk_cart[:, :, 1, 2, 0]     # d <U_3 U_2> / d x_1
        dUiUjdxk_cart[:, :, 2, 1, 1] = dUiUjdxk_cart[:, :, 1, 2, 1]     # d <U_3 U_2> / d x_2
        dUiUjdxk_cart[:, :, 2, 1, 2] = dUiUjdxk_cart[:, :, 1, 2, 2]     # d <U_3 U_2> / d x_3
        dUiUjdxk_cart[:, :, 2, 2, 0] = deriv_vel[:, :, 12]                  # d <U_3 U_3> / d x_1
        dUiUjdxk_cart[:, :, 2, 2, 1] = deriv_vel[:, :, 13]                  # d <U_3 U_3> / d x_2
        dUiUjdxk_cart[:, :, 2, 2, 2] = 0                                # d <U_3 U_3> / d x_3
        
        duiujdxk_cart = dUiUjdxk_cart - np.einsum('...ik, ...j', dUjdxi_cart, Ui_cart) - np.einsum('...jk, ...i', dUjdxi_cart, Ui_cart)
        
        dThetaUiUkdxk_cart[:, :, 0] = deriv_temp[:, :, 32] + deriv_temp[:, :, 39]                 # d < Theta U_1 U_1 > d x_1 + d < Theta U_1 U_2 > / d x_2
        dThetaUiUkdxk_cart[:, :, 1] = deriv_temp[:, :, 38] + deriv_temp[:, :, 35]                 # d < Theta U_2 U_1 > d x_1 + d < Theta U_2 U_2 > / d x_2
        dThetaUiUkdxk_cart[:, :, 2] = deriv_temp[:, :, 40] + deriv_temp[:, :, 43]                 # d < Theta U_3 U_1 > d x_1 + d < Theta U_3 U_2 > / d x_2
    
        d_Theta_Ui_Uk_dxk_cart[:, :, :] = ( 
                + np.einsum('...k,...i,...k', dThetadxi_cart[:, :, :], Ui_cart, Ui_cart)
                + np.einsum('...,...ik,...k', Theta[:, :], dUjdxi_cart, Ui_cart)
                    )
        
        d_Theta_uiuk_dxk_cart[:, :, :] = ( 
                + np.einsum('...k, ...ik', dThetadxi_cart[:, :, :], uiuj_cart)          
                + np.einsum('..., ...ikk', Theta[:, :], duiujdxk_cart)                   # dominant term
                )
        
        dUiThetadxk_cart[:, :, 0, 0] = deriv_temp[:, :, 4]             # d <U_1 Theta> / d x_1 
        dUiThetadxk_cart[:, :, 0, 1] = deriv_temp[:, :, 5]             # d <U_1 Theta> / d x_2 
        dUiThetadxk_cart[:, :, 1, 0] = deriv_temp[:, :, 6]             # d <U_2 Theta> / d x_1 
        dUiThetadxk_cart[:, :, 1, 1] = deriv_temp[:, :, 7]             # d <U_2 Theta> / d x_2 
        dUiThetadxk_cart[:, :, 2, 0] = deriv_temp[:, :, 8]             # d <U_3 Theta> / d x_1 
        dUiThetadxk_cart[:, :, 2, 1] = deriv_temp[:, :, 9]             # d <U_3 Theta> / d x_2 
        
        
        duithetadxk_cart[:, :, :, :] = ( dUiThetadxk_cart[:, :, :, :] 
                - np.einsum('...i,...k', Ui_cart, dThetadxi_cart[:, :, :]) 
                - np.einsum('...,...ik', Theta[:, :], dUjdxi_cart)
                )
        
        d_thetauk_Ui_dxk_cart[:, :, :] = ( np.einsum('...kk,...i', duithetadxk_cart[:, :, :, :], Ui_cart)
                + np.einsum('...k,...ik', uitheta_cart[:, :, :], dUjdxi_cart)
                )
        
        d_thetaui_Uk_dxk_cart[:, :, :] = np.einsum('...ik,...k', duithetadxk_cart[:, :, :, :], Ui_cart) + 0
        
        
        TD_cart[:, :, :] = - ( 
                + dThetaUiUkdxk_cart[:, :, :]           
                - d_Theta_Ui_Uk_dxk_cart[:, :, :] 
                - d_Theta_uiuk_dxk_cart[:, :, :]        
                - d_thetauk_Ui_dxk_cart[:, :, :]
                - d_thetaui_Uk_dxk_cart[:, :, :]
                )
        
        TD_cyl[:, :, :] = my_math.transform_vect(theta, TD_cart[:, :, :], 1)
        
        
        
        ## Turbulent pressure gradient
        #---------------------
        # TPG = - 1/rho * < theta * d p' / dx_i > = - 1/rho * ( < Theta * d P / d x_i > - <Theta> * d <P> / d x_i )
    
        dPThetadxi_cart[:, :, 0:2] = deriv_temp[:, :, 44:46]            # d < P * Theta > / d x_1, d < P * Theta > / d x_2
        PdThetadxi_cart[:, :, 0:3] = stats_temp[:, :, 6:9]              # < P * d Theta / d x_1 >, < P * d Theta / d x_2 > , < P * d Theta / d x_3 >
    
        ThetadPdxi_cart =  dPThetadxi_cart - PdThetadxi_cart
        
        dPdxi_cart[:, :, 0:2] = deriv_vel[:, :, 6:8]                    # d <P> / d x_1, d <P> / d x_2
    
        TPG_cart = -1/1 * (
                + ThetadPdxi_cart
                - np.einsum('..., ...i', Theta, dPdxi_cart)
                )
        
        TPG_cyl[:, :, :] = my_math.transform_vect(theta, TPG_cart[:, :, :], 1)
    
        ## Source term (analoguous to Piller for turb. heat flux)
        #--------------------------------------------------------
        # S = 4 * < u_z u_i >       # for for halfconst & halfsin

        uiuj_cyl = my_math.transform_vect(theta, uiuj_cart, 2)
    
        if ("half" in label[i]):
            S = 4 * uiuj_cyl[..., 2, :] 
        else:
            S = 0
    
           
        ## Left - right symmetry
        #-----------------------
        P_sym = my_math.left_right_sym(P_cyl, phi, tensor_order=1)
        eps_sym = my_math.left_right_sym(eps_cyl, phi, tensor_order=1)
        MD_sym = my_math.left_right_sym(MD_cyl, phi, tensor_order=1)
        TD_sym = my_math.left_right_sym(TD_cyl, phi, tensor_order=1)
        TPG_sym = my_math.left_right_sym(TPG_cyl, phi, tensor_order=1)
        if ("half" in label[i]):
            S_sym = my_math.left_right_sym(S, phi, tensor_order=1)
        else:
            S_sym = 0
        
    
    
    
        mask_q1 = ( (0 <= phi) & (phi <= np.pi/2) )
        mask_q2 = ( (np.pi/2 <= phi) & (phi <= np.pi) )
        mask_q3 = ( (np.pi <= phi) & (phi <= np.pi*3/2) )
        mask_q4 = ( (np.pi*3/2 <= phi) & (phi <= np.pi*2) )
        phi_q1 = phi[mask_q1]
        phi_q4 = phi[mask_q4]
        phi_right = np.concatenate((phi_q4, phi_q1[1:]))
        phi_sym = np.concatenate((phi_q4-2*np.pi, phi_q1[1:]))
        # Mesh generation in polar coordinates
        theta_sym, rho_sym = np.meshgrid(phi_right, r, indexing='xy')
        # Mesh transformation in cartesian coordinates
        X_sym, Y_sym = my_math.pol2cart(theta_sym, rho_sym)

        # Test without symmetry first
#        P_sym = P_cyl
#        eps_sym = eps_cyl
#        MD_sym = MD_cyl
#        TD_sym = TD_cyl
#        TPG_sym = TPG_cyl
#        S_sym = S
#        X_sym = X
#        Y_sym = Y
    
        
        n_vars = 6*3
        stats_sym = np.zeros((np.shape(P_sym)[0], np.shape(P_sym)[1], n_vars))
        stats_sym[..., 0:3] = P_sym * nu/u_t**2
        stats_sym[..., 3:6] = eps_sym * nu/u_t**2
        stats_sym[..., 6:9] = MD_sym * nu/u_t**2
        stats_sym[..., 9:12] = TD_sym * nu/u_t**2
        stats_sym[..., 12:15] = TPG_sym * nu/u_t**2
        stats_sym[..., 15:18] = S_sym * nu/u_t**2
    
    
        file_path = outdata
        directory = os.path.dirname(file_path)
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        np.savez(file_path, 
                stats_sym=stats_sym, r=r, phi=phi, phi_sym=phi_sym,
                X_sym=X_sym, Y_sym=Y_sym) 
    
  

def run():
    """ Exploit left-right symmetry for budget of turb. heat flux."""

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    for span in timeranges_select:
        expl_sym(span)
