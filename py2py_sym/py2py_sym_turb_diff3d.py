#!/usr/bin/env python3.5
#-----------------------
# This script reads previously saved data 
# calculates statistics
# and stores the data again.
# ----------------------------------------------------
# Author:   Steffen Straub
# Date  :   2019/10/07
#-----------------------------------------------------

import numpy as np

from ..misc import my_math
import os

import configparser

import pdb


# Define function
def save_stats(span, n_fields):

    # Load my data
    #-------------
    # 1) Velocity statistics
    indata_v = 'interpdata_python/' + span + '/v0_stats3d.npz'
    with np.load(indata_v) as data:
    
        # Mesh points
        r = data['r']
        phi = data['phi']
        z = data['z']
       
        fld_v = data['fld_3d']


    # 2) Temperature statistics
    indata_t_list = ['interpdata_python/'+span+'/t{0:d}_stats3d.npz'.format(k)
            for k in range(n_fields)]
    indata_dt_list = ['interpdata_python/'+span+'/t{0:d}_derivs3d.npz'.format(k)
            for k in range(n_fields)]
    indata_v = 'interpdata_python/' + span + '/v0_stats3d.npz'


    outdata_list = ['stats_3d/'+span+'/turb_diff_{0:d}.npz'.format(k)
            for k in range(n_fields)]


    Pr_list = (
            0.71,
            0.025,
            )

    for i in range(len(indata_t_list)):
        indata_t = indata_t_list[i]
        indata_dt = indata_dt_list[i]
        outdata = outdata_list[i]
        Pr = Pr_list[i]
        print('Processing ' + indata_t)

        with np.load(indata_t) as data:
        
            # Mesh points
            r = data['r']
            phi = data['phi']
            z = data['z']
        
            fld_t = data['fld_3d']
             
        # Derivatives
        with np.load(indata_dt) as data:
            fld_dt = data['fld_3d']

        # Velocity stats
        with np.load(indata_v) as data:
            fld_v = data['fld_3d']


    
        # Read user defined case-dependent parameters
        config = configparser.ConfigParser()
        config.read('case_params.ini')
        Re_b = float(config['Reynolds']['Re_b'])
        Re_t = float(config['Reynolds']['Re_t'])
        Lz = float(config['Domain']['Lz'])

        ## Calculations
        #-------------
        theta, rho, zz= np.meshgrid(phi, r, z, indexing='xy')
        nu = 1/Re_b
        Pe = Re_b * Pr
        D = 1
        R = D/2
#        u_t = my_math.get_utau(r, phi, deriv_vel, theta, nu)
    
    
        (nr, nphi, nz, n_stats) = np.shape(fld_t)
        n_derivs = np.shape(fld_dt)[3]

        UiTheta_cart = np.zeros((nr, nphi, nz, 3))
        dThetadxi_cart = np.zeros((nr, nphi, nz, 3))
        alpha_t = np.zeros((nr, nphi, nz, 3))
        alpha_t_sym = np.zeros((nr, int((nphi-1)/2)+1, nz, 3))


        # Thermal diffusivity
        #--------------------
        # alpha_t = - < u_i theta > / (d <Theta>/dx_i)

        Ui_cart = fld_v[..., 0:3]       
        Theta = fld_t[..., 0]
        UiTheta_cart[..., 0:3] = fld_t[..., 2:5]                # <U_1 Theta>, <U_2 Theta>, <U_3 Theta>
        uitheta_cart = UiTheta_cart - np.einsum('...i,...',Ui_cart, Theta)
        uitheta_cyl = my_math.transform_vect(theta, uitheta_cart, 1)

        dThetadxi_cart = fld_dt[..., 0:3]              # d <Theta> / d x_1, d <Theta> / d x_2, d <Theta> / d x_3
        dThetadxi_cyl = my_math.transform_vect(theta, dThetadxi_cart, 1)

        alpha_t[:, :, 1:-1] = - uitheta_cyl[:, :, 1:-1] / dThetadxi_cyl[:, :, 1:-1]



        ## Left - right symmetry
        #-----------------------
        uitheta_sym = my_math.left_right_sym(uitheta_cyl, phi, tensor_order=1)
        dThetadxi_sym = my_math.left_right_sym(dThetadxi_cyl, phi, tensor_order=1)
        alpha_t_sym[:, :, 1:-1] = -uitheta_sym[:, :, 1:-1] / dThetadxi_sym[:, :, 1:-1]

        mask_q1 = ( (0 <= phi) & (phi <= np.pi/2) )
        mask_q2 = ( (np.pi/2 <= phi) & (phi <= np.pi) )
        mask_q3 = ( (np.pi <= phi) & (phi <= np.pi*3/2) )
        mask_q4 = ( (np.pi*3/2 <= phi) & (phi <= np.pi*2) )
        phi_q1 = phi[mask_q1]
        phi_q4 = phi[mask_q4]
        phi_right = np.concatenate((phi_q4, phi_q1[1:]))
        phi_sym = np.concatenate((phi_q4-2*np.pi, phi_q1[1:]))
        # Mesh generation in polar coordinates
        theta_sym, rho_sym = np.meshgrid(phi_right, r, indexing='xy')
        # Mesh transformation in cartesian coordinates
        X_sym, Y_sym = my_math.pol2cart(theta_sym, rho_sym)

        stats_3d = np.zeros((nr, nphi, nz, 3))
        stats_3d[..., 0:3] = alpha_t * Re_b

        stats_3d_sym = np.zeros((nr, int((nphi-1)/2)+1, nz, 3))
        stats_3d_sym[..., 0:3] = alpha_t_sym * Re_b
    
        file_path = outdata
        directory = os.path.dirname(file_path)
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        np.savez(file_path, 
                stats_3d=stats_3d,
                stats_3d_sym=stats_3d_sym,
                r=r, phi=phi, phi_sym=phi_sym, z=z,
                X_sym=X_sym, Y_sym=Y_sym) 
    
      

def run(n_fields=2):
    """ Calculate temperature statistics. 

    Parameters:
    n_fields : scalar
        Number of fields
    """
    # Call function

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    for span in timeranges_select:
        save_stats(span, n_fields)   
