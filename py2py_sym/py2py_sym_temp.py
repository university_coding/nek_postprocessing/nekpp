#!/usr/bin/env python3.5
#-----------------------
# This script reads previously saved data 
# exploits symmetry and stores the data again.
# ----------------------------------------------------
# Author:   Steffen Straub
# Date  :   2018/03/26
#-----------------------------------------------------

import numpy as np

from ..misc import my_math
import os

import configparser

import pdb


# Define function
def expl_sym(span, t_fld_inhomog):

    # Load my data
    #-------------
    # 1) Velocity statistics
    indata_v = 'stats_' + span + '/statistics_matrix_tv1.npz'
    with np.load(indata_v) as data:
    
        stats_vel = data['stats_m']
        deriv_vel = data['deriv_m']

    # 2) Temperature statistics
    indata_t_list = ['stats_'+span+'/statistics_matrix_tt{0:d}.npz'.format(k)
            for k in t_fld_inhomog]
    outdata_list = ['stats_2d_sym/'+span+'/2d_sym_temp_tt{0:d}.npz'.format(k)
            for k in range(0, len(t_fld_inhomog))]


    print('Note: label_list and Pr_list are hard coded.')
    label = (
        'halfconst071',
        'halfsin071',
        'halfconst0025',       
        'halfsin0025',
        )

    Pr = (
            0.71,
            0.71,
            0.025,
            0.025,
            )

    for i in range(len(indata_t_list)):
        indata_t = indata_t_list[i]
        outdata = outdata_list[i]
        print('Processing ' + indata_t)
        with np.load(indata_t) as data:
        
            # Coordinates
            X = data['X']
            Y = data['Y']
        
            # individual radial and circumferential distribution
            r = data['r']
            phi = data['phi']
        
            # runtime averages and postprocessing derivatives
            stats_temp = data['stats_m']
            deriv_temp = data['deriv_m']

    
        # Read user defined case-dependent parameters
        config = configparser.ConfigParser()
        config.read('case_params.ini')
        Re_b = float(config['Reynolds']['Re_b'])

        ## Calculations
        #-------------
        theta, rho= np.meshgrid(phi, r, indexing='xy')
        nu = 1/Re_b
        u_t = my_math.get_utau(r, phi, deriv_vel, theta, nu)
    
    
        UiTheta_cart = np.zeros((np.size(r), np.size(phi), 3))
    
        Theta = stats_temp[:, :, 0]
        Theta_Theta = stats_temp[:, :, 1]
        UiTheta_cart[:, :, 0:3] = stats_temp[:, :, 2:5]                # <U_1 Theta>, <U_2 Theta>, <U_3 Theta>
        Ui_cart = stats_vel[:, :, 0:3]
    
        theta_theta = Theta_Theta - Theta**2
        
        uitheta_cart = UiTheta_cart[..., 0:3] - np.einsum('...i,...',Ui_cart, Theta)
        uitheta_cyl = my_math.transform_vect(theta, uitheta_cart, 1)
        uitheta = uitheta_cyl
    
    
    
        ## Left - right symmetry
        #-----------------------
        Theta_sym = my_math.left_right_sym(Theta, phi, tensor_order=0)
        theta_theta_sym = my_math.left_right_sym(theta_theta, phi, tensor_order=0)
        uitheta_sym = my_math.left_right_sym(uitheta, phi, tensor_order=1)


        mask_q1 = ( (0 <= phi) & (phi <= np.pi/2) )
        mask_q2 = ( (np.pi/2 <= phi) & (phi <= np.pi) )
        mask_q3 = ( (np.pi <= phi) & (phi <= np.pi*3/2) )
        mask_q4 = ( (np.pi*3/2 <= phi) & (phi <= np.pi*2) )
        phi_q1 = phi[mask_q1]
        phi_q4 = phi[mask_q4]
        phi_right = np.concatenate((phi_q4, phi_q1[1:]))
        phi_sym = np.concatenate((phi_q4-2*np.pi, phi_q1[1:]))
        # Mesh generation in polar coordinates
        theta_sym, rho_sym = np.meshgrid(phi_right, r, indexing='xy')
        # Mesh transformation in cartesian coordinates
        X_sym, Y_sym = my_math.pol2cart(theta_sym, rho_sym)
    
   
        # Test without symmetry first
#        Theta_sym = Theta
#        theta_theta_sym = theta_theta
#        uitheta_sym = uitheta
#        X_sym = X
#        Y_sym = Y


        n_vars = 5
        stats_sym = np.zeros((np.shape(Theta_sym)[0], np.shape(Theta_sym)[1], n_vars))
        stats_sym[..., 0] = Theta_sym * u_t
        stats_sym[..., 1] = theta_theta_sym * u_t**2
        stats_sym[..., 2] = uitheta_sym[...,0]
        stats_sym[..., 3] = uitheta_sym[...,1]
        stats_sym[..., 4] = uitheta_sym[...,2]
    
        file_path = outdata
        directory = os.path.dirname(file_path)
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        np.savez(file_path, 
                stats_sym=stats_sym, r=r, phi=phi, phi_sym=phi_sym,
                X_sym=X_sym, Y_sym=Y_sym) 
    
      

def run(t_fld_inhomog=[0, 7, 8, 9]):
    """ Exploit left-right symmetry for temperature.

    t_fld_inhomog           list of inhomogeneous fields
    """
    # Call function

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    for span in timeranges_select:
        expl_sym(span, t_fld_inhomog)   
