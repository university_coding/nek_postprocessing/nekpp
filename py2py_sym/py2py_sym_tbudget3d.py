#!/usr/bin/env python3.5
#-----------------------
# This script reads previously saved data 
# calculates statistics
# and stores the data again.
# ----------------------------------------------------
# Author:   Steffen Straub
# Date  :   2019/10/07
#-----------------------------------------------------

import numpy as np

from ..misc import my_math
import os

import configparser

import pdb


# Define function
def save_stats(span, n_fields):

    # Load my data
    #-------------
    # 1) Velocity statistics
    indata_v = 'interpdata_python/' + span + '/v0_stats3d.npz'
    with np.load(indata_v) as data:
    
        # Mesh points
        r = data['r']
        phi = data['phi']
        z = data['z']
       
        fld_v = data['fld_3d']


    # 2) Temperature statistics
    indata_t_list = ['interpdata_python/'+span+'/t{0:d}_stats3d.npz'.format(k)
            for k in range(n_fields)]
    indata_dt_list = ['interpdata_python/'+span+'/t{0:d}_derivs3d.npz'.format(k)
            for k in range(n_fields)]

    outdata_list = ['stats_3d/'+span+'/tbudget_{0:d}.npz'.format(k)
            for k in range(n_fields)]


    Pr_list = (
            0.71,
            0.025,
            )

    for i in range(len(indata_t_list)):
        indata_t = indata_t_list[i]
        indata_dt = indata_dt_list[i]
        outdata = outdata_list[i]
        Pr = Pr_list[i]
        print('Processing ' + indata_t)

        with np.load(indata_t) as data:
        
            # Mesh points
            r = data['r']
            phi = data['phi']
            z = data['z']
        
            fld_t = data['fld_3d']
             
        # Derivatives
        with np.load(indata_dt) as data:
            fld_dt = data['fld_3d']

    
        # Read user defined case-dependent parameters
        config = configparser.ConfigParser()
        config.read('case_params.ini')
        Re_b = float(config['Reynolds']['Re_b'])
        Re_t = float(config['Reynolds']['Re_t'])
        Lz = float(config['Domain']['Lz'])

        ## Calculations
        #-------------
        theta, rho, zz= np.meshgrid(phi, r, z, indexing='xy')
        nu = 1/Re_b
        Pe = Re_b * Pr
        D = 1
        R = D/2
#        u_t = my_math.get_utau(r, phi, deriv_vel, theta, nu)
    
    
        (nr, nphi, nz, n_stats) = np.shape(fld_t)
        n_derivs = np.shape(fld_dt)[3]


        # Allocate variables for vectors and tensors
        #-------------------------------------------
        # Production variables
        Ui_cart = np.zeros((np.size(r), np.size(phi), np.size(z), 3))
        Theta = np.zeros((np.size(r), np.size(phi), np.size(z)))
        dThetadxi_cart = np.zeros((np.size(r), np.size(phi), np.size(z), 3))
        UiTheta_cart = np.zeros((np.size(r), np.size(phi), np.size(z), 3))
        uitheta_cart = np.zeros((np.size(r), np.size(phi), np.size(z), 3))
        P_full = np.zeros((np.size(r), np.size(phi), np.size(z)))
        
        # Dissipation variables
        dThetadxi_dThetadxi = np.zeros((np.size(r), np.size(phi), np.size(z)))
        eps_full = np.zeros((np.size(r), np.size(phi), np.size(z)))
        
        # Molecular diffusion variables
        d2Thetadxi2 = np.zeros((np.size(r), np.size(phi), np.size(z)))
        d2ThetaThetadxi2 = np.zeros((np.size(r), np.size(phi), np.size(z)))
        d2Theta_Thetadxi2 = np.zeros((np.size(r), np.size(phi), np.size(z)))
        d2thetathetadxi2 = np.zeros((np.size(r), np.size(phi), np.size(z)))
        MD_full = np.zeros((np.size(r), np.size(phi), np.size(z)))
        
        # Turbulent diffusion variables
        dUiThetaThetadxi = np.zeros((np.size(r), np.size(phi), np.size(z)))
        dUi_Theta_Thetadxi = np.zeros((np.size(r), np.size(phi), np.size(z)))
        dThetaThetadxi_cart = np.zeros((np.size(r), np.size(phi), np.size(z), 3))
        dthetathetadxi_cart = np.zeros((np.size(r), np.size(phi), np.size(z), 3))
        dUi_thetathetadxi = np.zeros((np.size(r), np.size(phi), np.size(z)))
        dUiThetadxi = np.zeros((np.size(r), np.size(phi), np.size(z)))
        duithetadxi = np.zeros((np.size(r), np.size(phi), np.size(z)))
        duitheta_Thetadxi = np.zeros((np.size(r), np.size(phi), np.size(z)))
        TD_full = np.zeros((np.size(r), np.size(phi), np.size(z)))


        ## Production
        #------------
        # P_theta = - < u_i theta > * d <Theta> / d x_i
        # < u_i theta > = < U_i Theta > - <U_i> * <Theta>
        Ui_cart[..., 0:3] = fld_v[..., 0:3]                                       # <U_1>, <U_2>, <U_3>
        
        Theta = fld_t[..., 0]                                     # <Theta>
        dThetadxi_cart[..., 0:3] = fld_dt[..., 0:3]              # d <Theta> / d x_1, d <Theta> / d x_2, d <Theta> / d x_3
        UiTheta_cart[..., 0:3] = fld_t[..., 2:5]                # <U_1 Theta>, <U_2 Theta>, <U_3 Theta>
        uitheta_cart = UiTheta_cart - np.einsum('...i,...',Ui_cart, Theta)
        
        P_full = - np.einsum('...i,...i', uitheta_cart, dThetadxi_cart)


        ## Dissipation
        #-------------
        # eps_theta = - k/(rho C_p) < d theta / d x_i * d theta / d x_i >
        # k/(rho C_p) = a = alpha = 1/Pe = 1/(Re * Pr)
        # < d theta / d x_i * d theta / d x_i > = < d Theta / d x_i * d Theta / d x_i > - < d Theta / d x_i > * d Theta / d x_i >
        dThetadxi_dThetadxi = fld_t[..., 29]
        
        eps_full = - 1/Pe * ( dThetadxi_dThetadxi - np.einsum('...i,...i', dThetadxi_cart, dThetadxi_cart) )
 

        ## Molecular diffusion
        #---------------------
        # MD = 1/2 * 1/Pe * d**2 < theta theta> / d x_i d x_i
        # d**2 < theta theta > / d x_i**2 = d**2 <Theta Theta> / d x_i**2 - d**2 <Theta><Theta> / d x_i**2
        # d**2 <Theta><Theta> / d x_i**2 = 2 * d <Theta> / d x_i * d <Theta> / d x_i + 2 * <Theta> * d**2 <Theta> / d x_i**2
        d2Thetadxi2 = fld_dt[..., 24] + fld_dt[..., 25] + fld_dt[..., 26]         # d**2 <Theta> / d x_1**2 + d**2 <Theta> / d x_2**2 + d**2 <Theta> / d x_3**2
        d2ThetaThetadxi2 = fld_dt[..., 27] + fld_dt[...,28] + fld_dt[..., 29]     # d**2 <ThetaTheta> / d x_1**2 + d**2 <ThetaTheta> / d x_2**2 + d**2 <ThetaTheta> / d x_3**2
        d2Theta_Thetadxi2 = 2 * ( np.einsum('...i,...i', dThetadxi_cart, dThetadxi_cart) 
                + Theta * d2Thetadxi2 )
        d2thetathetadxi2 = d2ThetaThetadxi2 - d2Theta_Thetadxi2
        
        MD_full[:, :] = 1/2 * 1/Pe * d2thetathetadxi2
 

        ## Turbulent diffusion
        #---------------------
        # TD = - 1/2 * d < u_i theta theta > / d x_i
        # d < ui theta theta > d x_i = d < U_i Theta Theta > / d x_i - d <U_i> <Theta> <Theta> / d x_i - d <U_i> <theta theta> / d x_i - 2 d <u_i theta> <Theta> / d x_i
        dUiThetaThetadxi = fld_dt[..., 15] + fld_dt[..., 19] + fld_dt[..., 23]     # d <U_1 Theta Theta> / d x_1 + d <U_2 Theta Theta> / d x_2 + d <U_3 Theta Theta> d x_3
        
        # d <U_i> <Theta> <Theta> = d <U_i> / d x_i <Theta> <Theta> + 2 <U_i> <Theta> d <Theta> / d x_i
        # d <U_i> <Theta> <Theta> = 0                               + 2 <U_i> <Theta> d <Theta> / d x_i
        dUi_Theta_Thetadxi = 2 * np.einsum('...i,...i', Ui_cart, dThetadxi_cart) * Theta
        
        # d <U_i> <theta theta> / d x_i = <U_i> d <theta theta> / d x_i
        # d <theta theta> / d x_i = d <Theta Theta> / d x_i - 2 <Theta> d <Theta> / d x_i
        dThetaThetadxi_cart = fld_dt[..., 3:6]                       # d <Theta Theta> / d x_i
        dthetathetadxi_cart = dThetaThetadxi_cart - 2 * np.einsum('...,...i', Theta, dThetadxi_cart)
        dUi_thetathetadxi = np.einsum('...i,...i', Ui_cart, dthetathetadxi_cart)
        
        # d <u_i theta> <Theta> / d x_i = d <u_i theta> / d x_i <Theta> + <u_i theta> d <Theta> / d x_i
        # d <u_i theta> / d x_i = d <U_i Theta> / d x_i - 0 - <U_i> d <Theta> / d x_i
        dUiThetadxi = fld_dt[..., 6] + fld_dt[..., 10] + fld_dt[..., 14]             # d <U_1 Theta> / d x_1 + d <U_2 Theta> / d x_2 + d <U_3 Theta> / d x_3
        duithetadxi = dUiThetadxi - np.einsum('...i,...i', Ui_cart, dThetadxi_cart)
        
        duitheta_Thetadxi = duithetadxi * Theta + np.einsum('...i,...i', uitheta_cart, dThetadxi_cart)
        
        TD_full = - 1/2 * ( 
                + dUiThetaThetadxi
                - dUi_Theta_Thetadxi
                - dUi_thetathetadxi
                - 2*duitheta_Thetadxi
                )



        ## Left - right symmetry
        #-----------------------
        P_sym = my_math.left_right_sym(P_full, phi, tensor_order=0)
        eps_sym = my_math.left_right_sym(eps_full, phi, tensor_order=0)
        MD_sym = my_math.left_right_sym(MD_full, phi, tensor_order=0)
        TD_sym = my_math.left_right_sym(TD_full, phi, tensor_order=0)

        mask_q1 = ( (0 <= phi) & (phi <= np.pi/2) )
        mask_q2 = ( (np.pi/2 <= phi) & (phi <= np.pi) )
        mask_q3 = ( (np.pi <= phi) & (phi <= np.pi*3/2) )
        mask_q4 = ( (np.pi*3/2 <= phi) & (phi <= np.pi*2) )
        phi_q1 = phi[mask_q1]
        phi_q4 = phi[mask_q4]
        phi_right = np.concatenate((phi_q4, phi_q1[1:]))
        phi_sym = np.concatenate((phi_q4-2*np.pi, phi_q1[1:]))
        # Mesh generation in polar coordinates
        theta_sym, rho_sym = np.meshgrid(phi_right, r, indexing='xy')
        # Mesh transformation in cartesian coordinates
        X_sym, Y_sym = my_math.pol2cart(theta_sym, rho_sym)

        stats_3d = np.zeros((nr, nphi, nz, 4))
        stats_3d[..., 0] = P_full * nu
        stats_3d[..., 1] = eps_full * nu
        stats_3d[..., 2] = MD_full * nu
        stats_3d[..., 3] = TD_full * nu

        stats_3d_sym = np.zeros((nr, int((nphi-1)/2)+1, nz, 4))
        stats_3d_sym[..., 0] = P_sym * nu
        stats_3d_sym[..., 1] = eps_sym * nu
        stats_3d_sym[..., 2] = MD_sym * nu
        stats_3d_sym[..., 3] = TD_sym * nu

    
        file_path = outdata
        directory = os.path.dirname(file_path)
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        np.savez(file_path, 
                stats_3d=stats_3d,
                stats_3d_sym=stats_3d_sym,
                r=r, phi=phi, phi_sym=phi_sym, z=z,
                X_sym=X_sym, Y_sym=Y_sym) 
    
      

def run(n_fields=2):
    """ Calculate temperature statistics. 

    Parameters:
    n_fields : scalar
        Number of fields
    """
    # Call function

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    for span in timeranges_select:
        save_stats(span, n_fields)   
