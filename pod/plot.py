#!/usr/bin/env python3.5
#-----------------------
# My first steps in doing POD
#
#----------------------------------------------------------------------
# Author:   Steffen Straub
# Date:     2019/10/25
#----------------------------------------------------------------------
import numpy as np
import matplotlib.pyplot as plt
from ..misc import my_save_fig as msf
import pdb

def plot_sigma_compare(dat_path, given_labels, iftikz=False):
    plt.ioff()
    plt.close('all')

    # For halfsin071 (fld=4) and halfsin0025 (fld=9)
    flds = ('fld4', 'fld9')
    for p in range(2):
        # Load all data
        n_data = len(dat_path)
        data = []
        fname = 'sing_vals.dat'
        for i in range(n_data):
            this_data = np.loadtxt(dat_path[i] + '/' + flds[p] + '/' + fname)
            data.append(this_data)
    
        marker_list = ('o', 's', '+', 'x')

    
        fig, axs = plt.subplots(nrows=2, sharex=True)
        for i in range(n_data):
            m = np.arange(1, len(data[i])+1)

            w = data[i]
            lam = w**2

            axs[0].semilogx(m[:1000], lam[:1000]/np.sum(lam), marker_list[i], label=given_labels[i],
                    markerfacecolor='none', markersize=4)
            if (p==0):
                axs[0].set_ylabel(r'$\sigma_i^2$')
            axs[0].legend()

            axs[1].semilogx(m[:1000], np.cumsum(lam[:1000])/np.sum(lam), marker_list[i], label=given_labels[i],
                    markerfacecolor='none', markersize=4)
            axs[1].set_ylim(ymin=0)
            axs[1].set_xlabel('mode number')
            if (p==0):
                axs[1].set_ylabel('Cum. sum')

        fname = 'eigenvalues'
        directory = 'plots/compare/' + flds[p] + '/'
        extra_opts = ['scaled y ticks=false', 'y tick label style={/pgf/number format/fixed}']
        msf.save_tikz_png(fname, directory, iftikz, axis_opts=extra_opts)

        plt.close('all')




