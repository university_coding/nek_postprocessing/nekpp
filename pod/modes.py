#!/usr/bin/env python3.5
#-----------------------
# My first steps in doing POD
#
#----------------------------------------------------------------------
# Author:   Steffen Straub
# Date:     2019/10/25
#----------------------------------------------------------------------

import numpy as np
from scipy.interpolate import griddata
import matplotlib.pyplot as plt
import configparser

from ..misc import my_math
from ..misc import my_save_fig as msf

import os
import time

import pdb

plt.close('all')

def load_data():
    """ Load the data """
    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timelist.ini')
    times = str.split(config['Time']['t_select'], ',')
    span = times[0] + '-' + times[-1]
    
    n_files = len(times)
    
    # Load the snapshot(s)
    indata = ['interpdata_python/' + span + '/t{0:05d}.npz'.format(i) for i in range(n_files)]
    
    # Load first dataset (to get the shape)
    with np.load(indata[0]) as data:
        x = data['x']
        y = data['y']
        z = data['z']
        this_fld = data['fld_3d']
    n_x, n_y, n_z, n_fields = np.shape(this_fld)
    flds = np.zeros((n_x, n_y, n_z, n_fields, n_files))
    flds[..., 0] = this_fld
    for ifile in range(1, n_files):
        with np.load(indata[ifile]) as data:
            x = data['x']
            y = data['y']
            z = data['z']
            this_fld = data['fld_3d']
    
        flds[..., ifile] = this_fld

    return x, y, z, flds, span



def plot_z(x, y, Z, fname, directory, figwidth, figheight, ifpdf=False):
    """ Contour plot in z-plane 
    Interpolated cartesian data to cylindrical mesh.

    x : array_like
        x location (1D)
    y : array_like
        y location (1D)
    Z : array_like
        2D data to plot
    fname : string
        filename 
    directory : string
        directory where to save 
    figwidth : scalar
        figure width in cm
    figheight : scalar
        figure height in cm
    ifpdf : boolean
        whether or not to save pdf
    
    """

    width = my_math.cm2in(figwidth)
    height = my_math.cm2in(figheight)

    # Cartesian mesh
    X, Y = np.meshgrid(x, y, indexing='ij')

    # Cylindrical mesh
    R = 0.5
    n_r = 32
    n_phi = 64
    r = np.linspace(0, R, n_r)
    phi = np.linspace(0, 2*np.pi, n_phi)
    rho, theta = np.meshgrid(r, phi, indexing='ij')
    (Xi, Yi) = my_math.pol2cart(theta, rho)

    # Interpolation
    points = np.array([np.ravel(X), np.ravel(Y)]).T
    values = np.ravel(Z)
    xi  = np.array([np.ravel(Xi), np.ravel(Yi)]).T
    interp_data = griddata(points, values, xi, method='cubic')

    Z = np.reshape(interp_data, (n_r, n_phi))
    

    plt.figure(figsize=(width, height))
    cnt = plt.contourf(Xi, Yi, Z)
    cnt0 = plt.contour(Xi, Yi, Z, levels=[0], colors='white', linewidths=1.0)
    # This is the fix for the white lines between contour levels
    for c in cnt.collections:
        c.set_edgecolor("face")
    plt.colorbar(cnt)
    plt.axis('off')
    plt.axis('equal')
    plt.tight_layout()

    msf.save_tikz_png(fname, directory, iftikz=False)
    if (ifpdf):
        plt.savefig(directory + fname + '.pdf')

    plt.close('all')
   

def plot_ampl(ampl, fname, directory, iftikz):
    """ Plot amplitude against time """
    plt.figure()
    plt.plot(ampl)
    plt.xlabel('time (snapshot number)')
    plt.ylabel('amplitude of mode')

    msf.save_tikz_png(fname, directory, iftikz)


def plot_eigval(sigma, fname, directory, iftikz):
    """ Plot eigenvalues 

    sigma : array_like
        singular values, lambda = sigma**2
    fname : string
        filename
    directory : string
        directory
    iftikz : boolean
        whether or not to plot in tikz
    
    """

    m = np.arange(1, len(sigma)+1)

    lam = sigma**2
    fig, axs = plt.subplots(nrows=2, sharex=True)
    axs[0].semilogx(m, lam/np.sum(lam), '.')
    axs[0].set_ylabel(r'$\sigma_i^2$')
    axs[1].semilogx(m, np.cumsum(lam)/np.sum(lam), '.')
    axs[1].set_ylim(ymin=0)
    axs[1].set_ylabel('Cum. sum')
    axs[1].set_xlabel('mode number')

    msf.save_tikz_png(fname, directory, iftikz)

def get_modes_90(sigma):
    """ Find the number of modes to capture 90 % of the energy """

    lam = sigma**2
    mode_90 = np.min(np.argwhere(np.cumsum(lam)/np.sum(lam) > 0.9))
    return mode_90

def get_avg(flds, fld):
    n_r, n_phi, n_z, n_fields, n_files = np.shape(flds)
    # Average
    avg_t = np.mean(flds[..., fld, :], axis=3)    # t-average
    avg_tz = np.mean(avg_t, axis=2) # z-average

    return avg_tz

def get_variance(flds, fld, avg):
    n_r, n_phi, n_z, n_fields, n_files = np.shape(flds)
   
    fluct = (flds[:, :, :, fld, :].T - avg.T).T     # Transpose for broadcasting
    var = fluct*fluct
    var_t = np.mean(var, axis=3)    # t-average
    var_tz = np.mean(var_t, axis=2)     # z-average

    return var_tz

def tot_energy(fld=4):
    """ Calculate total energy (half the temperature variance) in two ways.
    1) Directly evaluate the temperature variance and integrate over the area
    2) Summing all eigenvalues
    """

    # 1)
    # Load data
    (x, y, z, flds, span) = load_data()
    n_x, n_y, n_z, n_fields, n_files = np.shape(flds)

    avg_tz = get_avg(flds, fld)
    var_tz = get_variance(flds, fld, avg_tz)

    integ = np.trapz(np.trapz(var_tz, x), y)
    print("Integrated energy is: {0:f}".format(1/2*integ))


    # 2)
    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timelist.ini')
    times = str.split(config['Time']['t_select'], ',')
    span = times[0] + '-' + times[-1]

    file_path = 'pod_modes/' + span + '/fld{0:d}'.format(fld) + '/sing_vals.dat'
    sigma = np.loadtxt(file_path)
    Nt = n_files*(n_z-1)
    lam = sigma**2/Nt
    print("Summed eigenvalues yield: {0:f}".format(1/2*np.sum(lam)))


    
def datmat(flds, weight, fld=4):
    """ Construct the data matrix 
    flds : array_like
        array containing all the loaded fields
    weight : scalar
        Weight corresponding to flow area (uniform mesh)
    fld : scalar
        number of thermal field to process
    """

    n_x, n_y, n_z, n_fields, n_files = np.shape(flds)
   
    avg_tz = get_avg(flds, fld)

    n_z_unique = n_z-1
    n_cr = n_x*n_y
    datmat = np.zeros((n_cr, n_z_unique*n_files), dtype=np.float64)
    for ifile in range(n_files):
        for iz in range(n_z_unique):     
            fluct = flds[:, :, iz, fld, ifile] - avg_tz[:, :]   
            vec = np.reshape(fluct[:, :], -1)
            datmat[:, iz + ifile*n_z_unique] = vec

    datmat *= np.sqrt(weight)

    return datmat

def datmat3d(flds, wmat, fld=4):
    """ Construct the data matrix for 3D POD
    flds : array_like
        array containing all the loaded fields
    weight : array_like
        Weights corresponding to flow volume
    fld : scalar
        number of thermal field to process
    """

    n_x, n_y, n_z, n_fields, n_files = np.shape(flds)
   
    avg_t = np.mean(flds[..., fld], axis=3) # t average
    n_z_unique = n_z-1
    n_vol = n_x*n_y*n_z_unique
    datmat = np.zeros((n_vol, n_files), dtype=np.float64)
    for ifile in range(n_files):
        fluct = flds[..., :-1, fld, ifile] - avg_t[..., :-1]   
        datmat[:, ifile] = np.ravel(fluct)

    weights = np.ravel(wmat)
    datmat = (np.sqrt(weights) * datmat.T).T

    return datmat




def dpod(A):
    """ Perform direct POD on matrix A 
    and return pod modes and amplitudes.

    A : array_like
        data matrix
    """

    (Nx, Nt) = np.shape(A)
    n_x = int(np.sqrt(Nx))
    n_y = n_x
    (w, v) = np.linalg.eig(1/Nt * A @ A.T)

    # Compare eigenvalues with singular values: lambda = sigma**2/M <-> sqrt(lambda*M) = sigma
    # Sort eigenvalues and eigenvectors in descending order
    ind_sort = np.flip(np.argsort(w))
    lam = w[ind_sort]
    v = v[:, ind_sort]
    # Switch sign of (small) negative values 
    lam[lam<0] *= -1
    sigma= np.sqrt(lam*Nt)

    u = v
    modes = np.reshape(u, (n_x, n_y, -1))

    # Construct amplitudes: V = A.T U Sigma^(-1)
    ATu = A.T @ u
    # Avoid division by zero
    v = np.divide(ATu ,sigma, out=np.zeros_like(ATu), where=(sigma!=0))

    # Amplitude: a = Sigma V.T
    ampl = (sigma*v).T

    # only real part
    ampl = np.real(ampl)
    sigma = np.real(sigma)
    modes = np.real(modes)

    return (modes, sigma, ampl)

def spod(A):
    """ Perform method of snapshots on matrix A 
    and return pod modes and amplitudes.

    A : array_like
        data matrix
    """

    (Nx, Nt) = np.shape(A)
    n_x = int(np.sqrt(Nx))
    n_y = n_x
    (w, v) = np.linalg.eig(1/Nt * A.T @ A)

    # Compare eigenvalues with singular values: lambda = sigma**2/M <-> sqrt(lambda*M) = sigma
    # Sort eigenvalues and eigenvectors in descending order
    ind_sort = np.flip(np.argsort(w))
    lam = w[ind_sort]
    v = v[:, ind_sort]
    # Switch sign of (small) negative values 
    lam[lam<0] *= -1
    sigma = np.sqrt(lam*Nt)

    # Construct spatial modes: U = A V Sigma^(-1)
    Av = A @ v
    # Avoid division by zero
    u = np.divide(Av ,sigma, out=np.zeros_like(Av), where=(sigma!=0))
    modes = np.reshape(u, (n_x, n_y, -1))

    # Amplitude: a = Sigma V.T
    ampl = (sigma*v).T

    # only real part
    ampl = np.real(ampl)
    sigma = np.real(sigma)
    modes = np.real(modes)

    return (modes, sigma, ampl)

def spod3d(A, n_x, n_y, n_z):
    """ Perform method of snapshots on matrix A 
    and return pod modes and amplitudes.

    A : array_like
        data matrix
    """

    (Nx, Nt) = np.shape(A)
    (w, v) = np.linalg.eig(1/Nt * A.T @ A)

    # Compare eigenvalues with singular values: lambda = sigma**2/M <-> sqrt(lambda*M) = sigma
    # Sort eigenvalues and eigenvectors in descending order
    ind_sort = np.flip(np.argsort(w))
    lam = w[ind_sort]
    v = v[:, ind_sort]
    # Switch sign of (small) negative values 
    lam[lam<0] *= -1
    sigma = np.sqrt(lam*Nt)

    # Construct spatial modes: U = A V Sigma^(-1)
    Av = A @ v
    # Avoid division by zero
    u = np.divide(Av ,sigma, out=np.zeros_like(Av), where=(sigma!=0))
    modes = np.reshape(u, (n_x, n_y, (n_z-1), -1))

    # Amplitude: a = Sigma V.T
    ampl = (sigma*v).T

    # only real part
    ampl = np.real(ampl)
    sigma = np.real(sigma)
    modes = np.real(modes)

    return (modes, sigma, ampl)


def svd_pod(A):
    """ Perform singular value decomposition on matrix A 
    and return pod modes and amplitudes.

    A : array_like
        data matrix
    """

    (Nx, Nt) = np.shape(A)
    n_x = int(np.sqrt(Nx))
    n_y = n_x
    (u, s, vh) = np.linalg.svd(A)

    # Compare eigenvalues with singular values: lambda = sigma**2/M <-> sqrt(lambda*M) = sigma
    sigma = s
    lam = sigma**2/Nt

    # Reshape modes and add redundant phi=2 pi data for plotting
    modes = np.reshape(u, (n_x, n_y, -1))
    # Amplitude: Sigma Vh
    ampl = (sigma*vh[:(n_x*n_y), :].T).T

    return (modes, sigma, ampl)



def save_modes(fld=4, n_modes=6, ifverbose=True):
    """ Calculate and save the thermal POD modes 
    
    fld : scalar [4]
        Number of field to process: 4=halfsin071, 9=halfsin0025
    n_modes : scalar [6]
        Number of modes to process
    ifverbose : boolean [True]
        Get some more verbose output?
    """

    # Load data
    t0 = time.time()
    (x, y, z, flds, span) = load_data()
    t1 = time.time()
    if (ifverbose):
        print(">> Loading data takes: {0:f} s".format(t1-t0))
    n_x, n_y, n_z, n_fields, n_files = np.shape(flds)
    if (n_x != n_y):
        print("Error: Only uniform spacing with n_x == n_y is allowed: {0:d} != {1:d}".format(n_x, n_y))

    # Build data matrix
    dx = x[1] - x[0]
    dy = dx
    weight = dx*dy
    A = datmat(flds, weight, fld)


    pref_svd = 10
    if (np.shape(A)[0]*pref_svd < np.shape(A)[1]):
        if (ifverbose):
            print("Using direct POD for A = {0:d}x{1:d}".format(np.shape(A)[0], np.shape(A)[1]))
        t0 = time.time()
        (modes, sigma, ampl) = dpod(A)
        t1 = time.time()
        if (ifverbose):
            print(">> POD takes: {0:f} s".format(t1-t0))
    elif (np.shape(A)[0] > np.shape(A)[1]*pref_svd):
        if (ifverbose):
            print("Using method of snapshots for A = {0:d}x{1:d}".format(np.shape(A)[0], np.shape(A)[1]))
        t0 = time.time()
        (modes, sigma, ampl) = spod(A)
        t1 = time.time()
        if (ifverbose):
            print(">> POD takes: {0:f} s".format(t1-t0))
    else:
        if (ifverbose):
            print("Using singular value decomposition for A = {0:d}x{1:d}".format(np.shape(A)[0], np.shape(A)[1]))
        t0 = time.time()
        (modes, sigma, ampl) = svd_pod(A)
        t1 = time.time()
        if (ifverbose):
            print(">> POD takes: {0:f} s".format(t1-t0))


    # Get number of modes to capture 90 %
    mode_90 = get_modes_90(sigma)

    # Save data
    t0 = time.time()
    file_path = 'pod_modes/' + span + '/fld{0:d}'.format(fld) + '/90_percent.dat'
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)
    with open(file_path, 'w') as f:
        f.write('# Number of modes required to capture 90%\n')
        f.write('{0:d}\n'.format(mode_90))

    file_path = 'pod_modes/' + span + '/fld{0:d}'.format(fld) + '/sing_vals.dat'
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)
    np.savetxt(file_path, 
            np.transpose(
                sigma, 
                ), header='singular values')

    file_path = 'pod_modes/' + span + '/fld{0:d}'.format(fld) + '/amplitudes.dat'
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)
    np.savetxt(file_path, 
                ampl[:n_modes, :], 
                header='Temporal coefficients (amplitudes) of mode 0, 1, 2, ...')

    file_path = 'pod_modes/' + span + '/fld{0:d}'.format(fld) + '/modes.npz'
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)
    np.savez(file_path,
            x=x, y=y, z=z,
            modes=modes[:, :, :n_modes])
    t1 = time.time()
    if (ifverbose):
        print(">> Saving data takes: {0:f} s".format(t1-t0))

def save_modes3d(fld=0, n_modes=6, ifverbose=True):
    """ Calculate and save the thermal POD modes 
    
    fld : scalar [0]
        Number of field to process
    n_modes : scalar [6]
        Number of modes to process
    ifverbose : boolean [True]
        Get some more verbose output?
    """

    # Load data
    t0 = time.time()
    (x, y, z, flds, span) = load_data()
    t1 = time.time()
    if (ifverbose):
        print(">> Loading data takes: {0:f} s".format(t1-t0))
    n_x, n_y, n_z, n_fields, n_files = np.shape(flds)
    if (n_x != n_y):
        print("Error: Only uniform spacing with n_x == n_y is allowed: {0:d} != {1:d}".format(n_x, n_y))

    # Build data matrix
    wmat = np.ones((n_x, n_y, (n_z-1)))
    A = datmat3d(flds, wmat, fld)

    print("Using method of snapshots for A = {0:d}x{1:d}".format(np.shape(A)[0], np.shape(A)[1]))
    t0 = time.time()
    (modes, sigma, ampl) = spod3d(A, n_x, n_y, n_z)
    t1 = time.time()
    if (ifverbose):
        print(">> POD takes: {0:f} s".format(t1-t0))

    # Get number of modes to capture 90 %
    mode_90 = get_modes_90(sigma)

    # Save data
    t0 = time.time()
    file_path = 'pod_modes/' + span + '/fld{0:d}'.format(fld) + '/90_percent.dat'
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)
    with open(file_path, 'w') as f:
        f.write('# Number of modes required to capture 90%\n')
        f.write('{0:d}\n'.format(mode_90))

    file_path = 'pod_modes/' + span + '/fld{0:d}'.format(fld) + '/sing_vals.dat'
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)
    np.savetxt(file_path, 
            np.transpose(
                sigma, 
                ), header='singular values')

    file_path = 'pod_modes/' + span + '/fld{0:d}'.format(fld) + '/amplitudes.dat'
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)
    np.savetxt(file_path, 
                ampl[:n_modes, :], 
                header='Temporal coefficients (amplitudes) of mode 0, 1, 2, ...')

    file_path = 'pod_modes/' + span + '/fld{0:d}'.format(fld) + '/modes.npz'
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)
    np.savez(file_path,
            x=x, y=y, z=z,
            modes=modes[:, :, :, :n_modes])
    t1 = time.time()
    if (ifverbose):
        print(">> Saving data takes: {0:f} s".format(t1-t0))


def plot_modes(fld=4, n_modes=6, iftikz=False, ifverbose=True, ifsym=False):
    """ Plot the thermal POD modes 
    
    fld : scalar [4]
        Number of field to process: 4=halfsin071, 9=halfsin0025
    n_modes : scalar [6]
        Number of modes to process
    iftikz : boolean [False]
        Save line plots as .tex files?
    ifverbose : boolean [True]
        Get some more verbose output?
    ifsym : boolean [False]
        Use left-right symmetry?

    """

    figheight = 6.0
    figwidth = 4.8

    plt.ioff()
    plt.close('all')

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timelist.ini')
    times = str.split(config['Time']['t_select'], ',')
    span = times[0] + '-' + times[-1]

    file_path = 'pod_modes/' + span + '/fld{0:d}'.format(fld) + '/modes.npz'
    t0 = time.time()
    with np.load(file_path) as data:
        x = data['x']
        y = data['y']
        z = data['z']
        modes = data['modes']
    t1 = time.time()
    if (ifverbose):
        print(">> Loading data takes: {0:f} s".format(t1-t0))


    n_x = len(x)
    n_y= len(y)
    n_z = len(z)


    file_path = 'pod_modes/' + span + '/fld{0:d}'.format(fld) + '/sing_vals.dat'
    sigma = np.loadtxt(file_path)
    file_path = 'pod_modes/' + span + '/fld{0:d}'.format(fld) + '/amplitudes.dat'
    ampl = np.loadtxt(file_path)

    directory = 'plots/' + span + '/fld{0:d}/'.format(fld)
    if (ifsym):
        modes_sym = (modes[(x<0), :] + np.flip(modes[(x>0), :], axis=0))/2
        modes_anti = (modes[(x<0), :] - np.flip(modes[(x>0), :], axis=0))/2
        modes_sym_full = np.concatenate((modes_sym, np.flip(modes_sym, axis=0)), axis=0)
        modes_anti_full = np.concatenate((modes_anti, -np.flip(modes_anti, axis=0)), axis=0)
        # sign is undefined, but enforced mode0_sym to be always the same
        if (modes_sym_full[int(n_x/2), int(3/4*n_y), 0] < 0):
            modes_sym_full *= -1

        for i in range(n_modes):
            fname = 'mode_{0:d}_sym'.format(i)
            plot_z(x, y, modes_sym_full[:, :, i], fname, directory, figheight, figwidth, ifpdf=True)
            fname = 'mode_{0:d}_anti'.format(i)
            plot_z(x, y, modes_anti_full[:, :, i], fname, directory, figheight, figwidth, ifpdf=True)
            fname = 'mode_{0:d}'.format(i)
            plot_z(x, y, modes[:, :, i], fname, directory, figheight, figwidth, ifpdf=True)

    else:
        for i in range(n_modes):
            fname = 'mode_{0:d}'.format(i)
            plot_z(x, y, modes[:, :, i], fname, directory, figheight, figwidth, ifpdf=True)

    for i in range(n_modes):
        fname = 'ampl_{0:d}'.format(i)
        plot_ampl(ampl[i, :], fname, directory, iftikz)

    fname = 'eigvals'
    plot_eigval(sigma, fname, directory, iftikz)

def plot_modes3d(fld=0, n_modes=6, iftikz=False, ifverbose=True, ifsym=False):
    """ Plot the thermal POD modes 
    
    fld : scalar [0]
        Number of field to process
    n_modes : scalar [6]
        Number of modes to process
    iftikz : boolean [False]
        Save line plots as .tex files?
    ifverbose : boolean [True]
        Get some more verbose output?
    ifsym : boolean [False]
        Use left-right symmetry?

    """

    figheight = 6.0
    figwidth = 4.8

    plt.ioff()
    plt.close('all')

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timelist.ini')
    times = str.split(config['Time']['t_select'], ',')
    span = times[0] + '-' + times[-1]

    file_path = 'pod_modes/' + span + '/fld{0:d}'.format(fld) + '/modes.npz'
    t0 = time.time()
    with np.load(file_path) as data:
        x = data['x']
        y = data['y']
        z = data['z']
        modes = data['modes']
    t1 = time.time()
    if (ifverbose):
        print(">> Loading data takes: {0:f} s".format(t1-t0))


    n_x = len(x)
    n_y= len(y)
    n_z = len(z)


    file_path = 'pod_modes/' + span + '/fld{0:d}'.format(fld) + '/sing_vals.dat'
    sigma = np.loadtxt(file_path)
    file_path = 'pod_modes/' + span + '/fld{0:d}'.format(fld) + '/amplitudes.dat'
    ampl = np.loadtxt(file_path)

    directory = 'plots/' + span + '/fld{0:d}/'.format(fld)
    if (ifsym):
        modes_sym = (modes[(x<0), ...] + np.flip(modes[(x>0), ...], axis=0))/2
        modes_anti = (modes[(x<0), ...] - np.flip(modes[(x>0), ...], axis=0))/2
        modes_sym_full = np.concatenate((modes_sym, np.flip(modes_sym, axis=0)), axis=0)
        modes_anti_full = np.concatenate((modes_anti, -np.flip(modes_anti, axis=0)), axis=0)
        # sign is undefined, but enforced mode0_sym to be always the same
        if (modes_sym_full[int(n_x/2), int(3/4*n_y), 0] < 0):
            modes_sym_full *= -1

        for i in range(n_modes):
            fname = 'mode_{0:d}_sym'.format(i)
            plot_z(x, y, modes_sym_full[:, :, -1, i], fname, directory, figheight, figwidth, ifpdf=True)
            fname = 'mode_{0:d}_anti'.format(i)
            plot_z(x, y, modes_anti_full[:, :, -1, i], fname, directory, figheight, figwidth, ifpdf=True)
            fname = 'mode_{0:d}'.format(i)
            plot_z(x, y, modes[:, :, -1, i], fname, directory, figheight, figwidth, ifpdf=True)

    else:
        for i in range(n_modes):
            fname = 'mode_{0:d}'.format(i)
            plot_z(x, y, modes[:, :, -1, i], fname, directory, figheight, figwidth, ifpdf=True)

    for i in range(n_modes):
        fname = 'ampl_{0:d}'.format(i)
        plot_ampl(ampl[i, :], fname, directory, iftikz)

    fname = 'eigvals'
    plot_eigval(sigma, fname, directory, iftikz)


def test_ways(fld=4, n_modes=6, iftikz=False, ifverbose=True, ifrec=False):
    """ Test different ways of doing the POD.
    1) direct 
    2) snapshot
    3) SVD

    fld : scalar [4]
        Number of field to process: 4=halfsin071, 9=halfsin0025
    n_modes : scalar [6]
        Number of modes to process
    iftikz : boolean [False]
        Save line plots as .tex files?
    ifverbose : boolean [True]
        Get some more verbose output?
    ifrec : boolean [False]
        Reconstruct matrix A?

    """

    plt.close('all')
    plt.ioff()


    figheight = 6.5
    figwidth = 5

    # Load data
    (x, y, z, flds, span) = load_data()
    n_x, n_y, n_z, n_fields, n_files = np.shape(flds)

    # Build data matrix
    dx = x[1] - x[0]
    dy = dx
    weight = dx*dy
    A = datmat(flds, weight, fld)

    ##############################
    # 1) Direct pod
    if (ifverbose):
        print("Using direct POD for A = {0:d}x{1:d}".format(np.shape(A)[0], np.shape(A)[1]))

    t0 = time.time()
    (modes, sigma, ampl) = dpod(A)
    t1 = time.time()
    if (ifverbose):
        print(">> POD takes: {0:f} s".format(t1-t0))

    u = np.reshape(modes, (n_x*n_y, -1))

    directory = 'plots_test/' + span + '/fld{0:d}/'.format(fld)
    for i in range(n_modes):
        fname = 'mode_{0:d}'.format(i) + '_direct'
        plot_z(x, y, modes[:, :, i], fname, directory, figheight, figwidth, ifpdf=False)
        fname = 'ampl_{0:d}'.format(i) + '_direct'
        plot_ampl(ampl[i, :], fname, directory, iftikz)

    fname = 'eigvals' + '_direct'
    plot_eigval(sigma, fname, directory, iftikz)

    if (ifrec):
        # Reconstruct snapshots from modes and amplitudes X = U Sigma Vh = U ampl
        A_rec = u @ ampl
        print('maximum difference direct: {0:f}'.format(np.max(np.abs(A - A_rec))))

    plt.close('all')

    ##############################
    # 2) Snapshot pod
    if (ifverbose):
        print("Using method of snapshots for A = {0:d}x{1:d}".format(np.shape(A)[0], np.shape(A)[1]))

    t0 = time.time()
    (modes, sigma, ampl) = spod(A)
    t1 = time.time()
    if (ifverbose):
        print(">> POD takes: {0:f} s".format(t1-t0))

    u = np.reshape(modes, (n_x*n_y, -1))

    directory = 'plots_test/' + span + '/fld{0:d}/'.format(fld)
    for i in range(n_modes):
        fname = 'mode_{0:d}'.format(i) + '_snap'
        plot_z(x, y, modes[:, :, i], fname, directory, figheight, figwidth, ifpdf=False)
        fname = 'ampl_{0:d}'.format(i) + '_snap'
        plot_ampl(ampl[i, :], fname, directory, iftikz)

    fname = 'eigvals' + '_snap'
    plot_eigval(sigma, fname, directory, iftikz)

    if (ifrec):
        # Reconstruct snapshots from modes and amplitudes X = U Sigma Vh = U ampl
        A_rec = u @ ampl
        print('maximum difference direct: {0:f}'.format(np.max(np.abs(A - A_rec))))

    plt.close('all')

    ##############################
    # 3) Singular value decomposition
    if (ifverbose):
        print("Using singular value decomposition for A = {0:d}x{1:d}".format(np.shape(A)[0], np.shape(A)[1]))

    t0 = time.time()
    (modes, sigma, ampl) = svd_pod(A)
    t1 = time.time()
    if (ifverbose):
        print(">> POD takes: {0:f} s".format(t1-t0))

    u = np.reshape(modes, (n_x*n_y, n_x*n_y))

    directory = 'plots_test/' + span + '/fld{0:d}/'.format(fld)
    for i in range(n_modes):
        fname = 'mode_{0:d}'.format(i) + '_svd'
        plot_z(x, y, modes[:, :, i], fname, directory, figheight, figwidth, ifpdf=False)
        fname = 'ampl_{0:d}'.format(i) + '_svd'
        plot_ampl(ampl[i, :], fname, directory, iftikz)

    fname = 'eigvals' + '_svd'
    plot_eigval(sigma, fname, directory, iftikz)

    if (ifrec):
        # include zero singular values to build complete matrix for amplitudes (when Nx < Nt)
        (Na, Nt) = np.shape(ampl)
        Nx = np.shape(u)[0]
        ampl_compl = np.zeros((Nx, Nt))
        ampl_compl[:Na, :Nt] = ampl

        # Reconstruct snapshots from modes and amplitudes X = U Sigma Vh = U ampl
        A_rec = u @ ampl_compl
        print('maximum difference direct: {0:f}'.format(np.max(np.abs(A - A_rec))))

    plt.close('all')

