#!/usr/bin/env python3.5
#----------------------------------------------------------------------
# Compare my results to Piller (2005) data
#----------------------------------------------------------------------
# Author:   Steffen Straub
# Date:     2018/10/19
#----------------------------------------------------------------------
import os
import matplotlib.pyplot as plt
import numpy as np
from matplotlib2tikz import save as tikz_save
import timerange


def run_valid(iftikz=False):
    plt.close('all')
    span = timerange.t_select[0]
    
    #
    ## Files to process
    #
    data_dir = 'profiles/' + span + '/'
    data_names = (
            'temp_stats_IT071.dat',
            'temp_stats_MBC071.dat',
            'temp_stats_IF071.dat',
            'temp_stats_IT0025.dat',
            'temp_stats_MBC0025.dat',
            'temp_stats_IF0025.dat',
            )


    piller_dirs = [
            '/net/istmlupus/localhome/hi202/local/data/pipe/2005_Piller_data/Theta/',
            '/net/istmlupus/localhome/hi202/local/data/pipe/2005_Piller_data/theta_rms/',
            '/net/istmlupus/localhome/hi202/local/data/pipe/2005_Piller_data/urtheta/',
            '/net/istmlupus/localhome/hi202/local/data/pipe/2005_Piller_data/uztheta/',
            ]

    antoranz_dirs = [
            '/net/istmlupus/localhome/hi202/local/data/pipe/2015_Antoranz_data/',
            ]
    
    piller_names = [
            'Theta_IT.dat',
            'Theta_MBC.dat',
            'Theta_IF.dat',
            'theta_rms_IT.dat',
            'theta_rms_MBC.dat',
            'theta_rms_IF.dat',
            'urtheta_IT.dat',
            'urtheta_MBC.dat',
            'urtheta_IF.dat',
            'uztheta_IT.dat',
            'uztheta_MBC.dat',
            'uztheta_IF.dat',
            ]

    antoranz_names = (
            'theta_rms.dat',
            'ui_theta.dat',
            )
    

    #
    ## Load and plot
    #
    label_list = (
            'IT',
            'MBC',
            'IF',
            )
    linestyle_list = (
            '-',
            ':',
            '--',
            )
    marker_list = (
            'o',
            's',
            '^',
            )


    fname_list= (
            'valid_thetatheta',
            'valid_urtheta',
            'valid_uztheta',
            )
    ylabel_list = (

            r'$\langle \vartheta \vartheta \rangle^+$',
            r'$\langle u_r \vartheta \rangle^+$',
            r'$\langle u_z \vartheta \rangle^+$',
            )


    # markersize
    ms = 5

    # Theta
    plt.figure()
    for i in range(3):
        data = np.loadtxt(data_dir + data_names[i])
        piller_data = np.loadtxt(piller_dirs[0] + piller_names[i])
        plt.semilogx(data[:, 0], data[:, 1], color='C0', linestyle=linestyle_list[i],
                label=label_list[i])
        plt.semilogx(piller_data[::3, 0], piller_data[::3, 1], marker=marker_list[i], 
                markerfacecolor='none', markersize=ms, linestyle='none',
                color='k')

        plt.xlabel(r'$y^+$')
        plt.ylabel(r'$\langle \Theta \rangle^+$')

        plt.xlim(xmin=1)
        plt.ylim(ymin=0)

        plt.legend()

    #
    ## Save 
    #
    directory = 'plots/valid/'
    fname = 'valid_Theta'
    if not os.path.exists(directory):
        os.makedirs(directory)
    
    if (iftikz):
       tikz_save(directory + fname + '.tex',
               figureheight='\\figureheight',
               figurewidth='\\figurewidth',
               show_info=False,
           )
    
    plt.savefig(directory + fname + '.png')

    # thetatheta
    plt.figure()
    for i in range(3):
        data = np.loadtxt(data_dir + data_names[i])
        piller_data = np.loadtxt(piller_dirs[1] + piller_names[i+3])
        antoranz_data = np.loadtxt(antoranz_dirs[0] + antoranz_names[0])
        plt.semilogx(data[:, 0], data[:, 2], color='C0', linestyle=linestyle_list[i],
                label=label_list[i])
        plt.semilogx(piller_data[::3, 0], piller_data[::3, 1]**2, marker=marker_list[i], 
                markerfacecolor='none', markersize=ms, linestyle='none',
                color='k')
        plt.semilogx(antoranz_data[::3, 0], antoranz_data[::3, 1]**2, marker='p', 
                markerfacecolor='none', markersize=ms, linestyle='none',
                color='k')


        plt.xlabel(r'$y^+$')
        plt.ylabel(r'$\langle \vartheta \vartheta \rangle^+$')

        plt.xlim(xmin=1)
        plt.ylim(ymin=0)

#        plt.legend()

    #
    ## Save 
    #
    directory = 'plots/valid/'
    fname = 'valid_thetatheta'
    if not os.path.exists(directory):
        os.makedirs(directory)
    
    if (iftikz):
       tikz_save(directory + fname + '.tex',
               figureheight='\\figureheight',
               figurewidth='\\figurewidth',
               show_info=False,
           )
    
    plt.savefig(directory + fname + '.png')

    # urtheta
    plt.figure()
    for i in range(3):
        data = np.loadtxt(data_dir + data_names[i])
        piller_data = np.loadtxt(piller_dirs[2] + piller_names[i+6])
        antoranz_data = np.loadtxt(antoranz_dirs[0] + antoranz_names[1])
        plt.semilogx(data[:, 0], data[:, 3], color='C0', linestyle=linestyle_list[i],
                label=label_list[i])
        plt.semilogx(piller_data[::3, 0], piller_data[::3, 1], marker=marker_list[i], 
                markerfacecolor='none', markersize=ms, linestyle='none',
                color='k')
        plt.semilogx(antoranz_data[::3, 0], -antoranz_data[::3, 1], marker='p', 
                markerfacecolor='none', markersize=ms, linestyle='none',
                color='k')


        plt.xlabel(r'$y^+$')
        plt.ylabel(r'$\langle u_r \vartheta \rangle^+$')

        plt.xlim(xmin=1)
        plt.ylim(ymin=0)

#        plt.legend()

    #
    ## Save 
    #
    directory = 'plots/valid/'
    fname = 'valid_urtheta'
    if not os.path.exists(directory):
        os.makedirs(directory)
    
    if (iftikz):
       tikz_save(directory + fname + '.tex',
               figureheight='\\figureheight',
               figurewidth='\\figurewidth',
               show_info=False,
           )
    
    plt.savefig(directory + fname + '.png')

    # uztheta
    plt.figure()
    for i in range(3):
        data = np.loadtxt(data_dir + data_names[i])
        piller_data = np.loadtxt(piller_dirs[3] + piller_names[i+9])
        antoranz_data = np.loadtxt(antoranz_dirs[0] + antoranz_names[1])
        plt.semilogx(data[:, 0], data[:, 5], color='C0', linestyle=linestyle_list[i],
                label=label_list[i])
        plt.semilogx(piller_data[::3, 0], piller_data[::3, 1], marker=marker_list[i], 
                markerfacecolor='none', markersize=ms, linestyle='none',
                color='k')
        plt.semilogx(antoranz_data[::3, 0], -antoranz_data[::3, 3], marker='p', 
                markerfacecolor='none', markersize=ms, linestyle='none',
                color='k')


        plt.xlabel(r'$y^+$')
        plt.ylabel(r'$\langle u_z \vartheta \rangle^+$')

        plt.xlim(xmin=1)
        plt.ylim(ymin=0)

#        plt.legend()

    #
    ## Save 
    #
    directory = 'plots/valid/'
    fname = 'valid_uztheta'
    if not os.path.exists(directory):
        os.makedirs(directory)
    
    if (iftikz):
       tikz_save(directory + fname + '.tex',
               figureheight='\\figureheight',
               figurewidth='\\figurewidth',
               show_info=False,
           )
    
    plt.savefig(directory + fname + '.png')


    
    #
    ## Plot
#    n_terms = 5
#    labels = [r'$P^+$', r'$\tilde{\epsilon}^+$', r'$\Pi^+$', r'$D^+$', r'$T^+$']
#    plt.figure(1)
#    plt.plot(EK_r[:, 1], EK_k[:, 1])
#    plt.plot(EK_r[:, 1], EK_k[:, 6])
#    plt.plot(EK_r[:, 1], EK_k[:, 3] + EK_k[:, 4])
#    plt.plot(EK_r[:, 1], EK_k[:, 5])
#    plt.plot(EK_r[:, 1], EK_k[:, 2])
#    for i in range(n_terms):
#        plt.plot(SS_k[::3, 0], SS_k[::3, i+1], 'o', 
#                markerfacecolor='none', 
#            color='C{0:d}'.format(i), label=labels[i])
#    
#    plt.xlabel(r'$y^+$')    
#    plt.legend()
    
   
