#/usr/bin/env python3.5
#----------------------------------------------------------------------
# Plot profiles of data in .dat files
#----------------------------------------------------------------------
# Date:     2018/03/01
# Author:   Steffen Straub
#----------------------------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt
from ..misc import my_save_fig as msf
import os
import sys

import configparser

import pdb

def plot_individual(span, iftikz):
    plt.ioff()
    plt.close('all')

    print('Note: label_list is hardcoded for now.')
    label = (
        '071',
        '0025',
        )

    n_axpos = 10
    n_probes = 5

    # Load data
    data_dir = 'profiles/' + span + '/' + label[0] + '/z_1/probe_0/'
    data_name = 'temp.dat'
    # Just to get the shape
    data0 = np.loadtxt(data_dir + data_name)
    rows, cols = np.shape(data0)


    data = np.zeros((rows, cols, n_axpos-1, n_probes))

    for i in range(len(label)):

        for iz in range(n_axpos-1):
            for k in range(n_probes):
                data_dir = 'profiles/' + span + '/' + label[i] + '/z_{0:d}/probe_{1:d}/'.format(iz+1, k)
                data_name = 'temp.dat'
                # Just to get the shape
                data_izk = np.loadtxt(data_dir + data_name)
                rows, cols = np.shape(data0)
            
                data[:, :, iz, k] = data_izk

        data_dir = 'profiles/' + span + '/' + label[0] + '/'
        data_name = 'wall_profiles_z.dat'
        data_z = np.loadtxt(data_dir + data_name)


        y_plus = data[:, 0, 0, 0]
        Theta = data[:, 1, :, :]
        theta_theta = data[:, 2, :, :]
        uitheta = data[:, 3:6, :, :]

        z = data_z[:, 0]
        Theta_z = data_z[:, 1]
        theta_theta_z = data_z[:, 2]

        def plot_profiles_r(statistic, fname, ylab):
            for k in range(n_probes):
                fig = plt.figure()
                for iz in range(0, n_axpos-1, 2):
                    if (iz <= (n_axpos-1)/2 - 1):
                        lstyle='-'
                    else:
                        lstyle='--'
    
                    plt.plot(y_plus, statistic[:, iz, k], lstyle)
    
                plt.ylabel(ylab)
                plt.xlabel(r'$y^+$')
        
                directory = 'plots/' + span + '/' + label[i] + '/probe_{0:d}/'.format(k)
                msf.save_tikz_png(fname, directory, iftikz)

                plt.close('all')

        def plot_profiles_z(statistic, fname, ylab):
            fig = plt.figure()
            plt.plot(z[:-1], statistic[:-1])
            plt.ylabel(ylab)
            plt.xlabel(r'z/D')

            directory = 'plots/' + span + '/' + label[i] + '/'
            msf.save_tikz_png(fname, directory, iftikz)
            plt.close('all')


        plot_profiles_r(Theta, 'Theta', r'$\langle \Theta \rangle^+$')
        plot_profiles_r(theta_theta, 'theta_theta', r'$\langle \vartheta \vartheta \rangle^+$')
        plot_profiles_r(uitheta[:, 0], 'ur_theta', r'$\langle u_r \vartheta \rangle^+$')
        plot_profiles_r(uitheta[:, 1], 'uphi_theta', r'$\langle u_\varphi \vartheta \rangle^+$')
        plot_profiles_r(uitheta[:, 2], 'uz_theta', r'$\langle u_z \vartheta \rangle^+$')

        plot_profiles_z(Theta_z, 'Theta_z', r'$\langle \Theta \rangle^+$')
        plot_profiles_z(theta_theta_z, 'theta_theta_z', r'$\langle \vartheta \vartheta \rangle^+$')



def plot_compare(dat_path, given_labels=None, iftikz=False):
    plt.ioff()
    plt.close('all')

    print('Note: label_list is hardcoded for now.')
    label = (
        '071',
        '0025',
        )

    # Read user defined case-dependent parameters
    config = configparser.ConfigParser()
    config.read('case_params.ini')
    Lz = float(config['Domain']['Lz'])

    # which legend labels?
    if (given_labels):
        leg_labels = given_labels
    else:
        leg_labels = dat_path
   

    n_axpos = 10
    n_probes = 5

    # Load data
    n_data = len(dat_path)
    data = []

    if (n_data != 2):
        print("Usage: 2 setups can be compared, but n_data = {0:d}".format(n_data))
        sys.exit()
        

    data_dir = dat_path[0] + '/' + label[0] + '/z_1/probe_0/'
    data_name = 'temp.dat'
    # Just to get the shape
    data0 = np.loadtxt(data_dir + data_name)
    rows, cols = np.shape(data0)

    this_data = np.zeros((rows, cols, n_axpos-1, n_probes))

    for i in range(len(label)):
        
        for idata in range(n_data):
            this_data = np.zeros((rows, cols, n_axpos-1, n_probes))


            for iz in range(n_axpos-1):
                for k in range(n_probes):
                    data_dir = dat_path[idata] + '/' + label[i] + '/z_{0:d}/probe_{1:d}/'.format(iz+1, k)
                    data_name = 'temp.dat'
                    # Just to get the shape
                    data_izk = np.loadtxt(data_dir + data_name)
                
                    this_data[:, :, iz, k] = data_izk
            data.append(this_data)

        y_plus = data[0][:, 0, 0, 0]
        Theta0 = data[0][:, 1, :, :]
        theta_theta0 = data[0][:, 2, :, :]
        uitheta0 = data[0][:, 3:6, :, :]
        Theta_diff = Theta0 - data[1][:, 1, :, :]
        theta_theta_diff = theta_theta0 - data[1][:, 2, :, :]
        uitheta_diff = uitheta0 - data[1][:, 3:6, :, :]


        def plot_diff_z(diff, ref, fname, ylab):
            for k in range(n_probes):
                fig = plt.figure()
                for iz in range(n_axpos-1):
                    if (iz <= (n_axpos-1)/2 - 1):
                        lstyle='-'
                    else:
                        lstyle='--'
    
                    plt.plot(y_plus, diff[:, iz, k]/np.max(np.abs(ref[:, iz, k])), lstyle)
    
                plt.ylabel(ylab)
                plt.xlabel(r'$y^+$')
        
                directory = 'plots/diff/' + label[i] + '/probe_{0:d}/'.format(k)
                msf.save_tikz_png(fname, directory, iftikz)

                plt.close('all')

        def plot_cmp(statistic, ref, label_ax, fname, ylab):
            for k in range(n_probes):
                fig = plt.figure()
                for iz in range(0, n_axpos-1, 2):
                    plt.plot(y_plus, statistic[:, iz, k], label=label_ax[iz])
                    plt.plot(y_plus, ref[:, iz, k],'k:')

                plt.ylabel(ylab)
                plt.xlabel(r'$y^+$')
                plt.legend()
        
                directory = 'plots/diff/' + label[i] + '/probe_{0:d}/'.format(k)
                msf.save_tikz_png(fname, directory, iftikz)

                plt.close('all')


        plot_diff_z(Theta_diff, Theta0, 'Theta_diff', 
                r'$dev(\langle \Theta \rangle^+)$')
        plot_diff_z(theta_theta_diff, theta_theta0, 'theta_theta_diff', 
                r'$dev(\langle \vartheta \vartheta\rangle^+)$')
        plot_diff_z(uitheta_diff[:, 0], uitheta0[:, 0], 'ur_theta_diff', 
            r'$dev(\langle u_r \vartheta \rangle^+)$')
        plot_diff_z(uitheta_diff[:, 1], uitheta0[:, 1], 'uphi_theta_diff', 
            r'$dev(\langle u_\varphi \vartheta \rangle^+)$')
        plot_diff_z(uitheta_diff[:, 2], uitheta0[:, 2], 'uz_theta_diff', 
            r'$dev(\langle u_z \vartheta \rangle^+)$')

        label_ax = np.linspace(0, Lz, 11)[1:-1]
        plot_cmp(data[1][:, 1], Theta0, label_ax, 'Theta_cmp', 
            r'$\langle \Theta \rangle^+$')
        plot_cmp(data[1][:, 2], theta_theta0, label_ax, 'theta_theta_cmp', 
            r'$\langle \vartheta \vartheta \rangle^+$')
        plot_cmp(data[1][:, 3], uitheta0[:, 0], label_ax, 'ur_theta_cmp', 
            r'$\langle u_r \vartheta \rangle^+$')
        plot_cmp(data[1][:, 4], uitheta0[:, 1], label_ax, 'uphi_theta_cmp', 
            r'$\langle u_\varphi \vartheta \rangle^+$')
        plot_cmp(data[1][:, 5], uitheta0[:, 2], label_ax, 'uz_theta_cmp', 
            r'$\langle u_z \vartheta \rangle^+$')


        # reset data for 0025
        data = []

def plot_cmp_wall_profiles(dat_path, Lz_list, label_list, iftikz=None):
    plt.ioff()
    plt.close('all')

    print('Note: label_list is hardcoded for now.')
    label = (
        '071',
        '0025',
        )
    
    # Load data
    n_data = len(dat_path)
    data = []

    for i in range(len(label)):

        for idata in range(n_data):
            data_dir = dat_path[idata] + '/' + label[i] + '/'
            data_name = 'wall_profiles_z.dat'
            this_data = np.loadtxt(data_dir + data_name)
    
            data.append(this_data)


        # z profiles
        fig = plt.figure()
        for idata in range(0, n_data-1):
            plt.plot(data[idata][:-1, 0]/Lz_list[idata], data[idata][:-1, 1], label=label_list[idata])

        plt.plot(data[n_data-1][:-1, 0]/Lz_list[n_data-1], data[n_data-1][:-1, 1], 'k--', label=label_list[n_data-1])

        plt.ylabel(r'$\langle \Theta_w \rangle^+ (\varphi=90^\circ)$')
        plt.xlabel('$z/L_z$')
        plt.legend(loc='lower right', framealpha=0.5)

        fname = 'Theta_w_z'
        directory = 'plots/cmp/' + label[i] + '/'
        msf.save_tikz_png(fname, directory, iftikz)

        fig = plt.figure()
        for idata in range(0, n_data-1):
            plt.plot(data[idata][:-1, 0]/Lz_list[idata], data[idata][:-1, 2], label=label_list[idata])

        plt.plot(data[n_data-1][:-1, 0]/Lz_list[n_data-1], data[n_data-1][:-1, 2], 'k--', label=label_list[n_data-1])

        plt.ylabel(r'$\langle \vartheta \vartheta \rangle^+|_w (\varphi=90^\circ)$')
        plt.xlabel('$z/L_z$')

        fname = 'theta_theta_w_z'
        directory = 'plots/cmp/' + label[i] + '/'
        msf.save_tikz_png(fname, directory, iftikz)


        # reset data for 0025
        data = []

    # Load data for phi plots
    n_data = len(dat_path)
    data = []

    for i in range(len(label)):
        for idata in range(n_data):
            data_dir = dat_path[idata] + '/' + label[i] + '/'
            data_name = 'wall_profiles_phi.dat'
            this_data = np.loadtxt(data_dir + data_name)
            data.append(this_data)

        # z profiles
        fig = plt.figure()
        for idata in range(0, n_data-1):
            plt.plot(data[idata][:-1, 0]*180/np.pi, data[idata][:-1, 1], label=label_list[idata])

        plt.plot(data[n_data-1][:-1, 0]*180/np.pi, data[n_data-1][:-1, 1], 'k--', label=label_list[n_data-1])

        plt.ylabel(r'$\langle \Theta_w \rangle^+ (z=L_z/2)$')
        plt.xlabel(r'$\varphi [^\circ]$')
        plt.legend()
#        plt.legend(loc='lower right', framealpha=0.5)

        fname = 'Theta_w_phi'
        directory = 'plots/cmp/' + label[i] + '/'
        msf.save_tikz_png(fname, directory, iftikz)


        # reset data for 0025
        data = []



def run(iftikz=False):
    """Plot profiles of thermal statistics in 3d case"""

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    # Loop over specified timeranges
    for span in timeranges_select:
        # Call a postprocessing script
        plot_individual(span, iftikz)


