#/usr/bin/env python3.5
#----------------------------------------------------------------------
# Plot profiles of data in .dat files
#----------------------------------------------------------------------
# Date:     2018/12/09
# Author:   Steffen Straub
#----------------------------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt
from ..misc import my_save_fig as msf
import os

import configparser

import pdb


def plot_individual(timerange_select):


    label = (
        'halfconst071',
        'halfsin071',
        'halfconst0025',       
        'halfsin0025',
        )

   
    for i in range(len(label)):
    
        plt.close('all')

        print("Processing " + label[i] + ' at time ' + timerange_select)
        # Load data
        #----------
        data_dir_alphat = 'profiles/' + timerange_select + '/' + label[i] + '/'
        data_name_alphat = (
                'alphat_avg.dat'
                )
        data_dir_nut = 'profiles/' + timerange_select + '/' 
        data_name_nut = (
                'nut.dat'
                )

        alphat = np.loadtxt(data_dir_alphat + data_name_alphat)
        nut = np.loadtxt(data_dir_nut + data_name_nut)

        Prt = np.expand_dims(nut[:, 1], axis=1) / alphat[:, 1:]
    
        # Plot
        #-----
        plt.figure()
        # Skip value in the center because the denominator d<Uz>/dr = 0
        plt.plot(alphat[:-1, 0], alphat[:-1, 1])
        plt.xlabel(r'$y^+$')
        plt.ylabel(r'$\langle \alpha_{t,r} \rangle / \nu $')
    
        fname = 'alphat_r'
        directory = 'plots/' + timerange_select + '/' + label[i] + '/'
        msf.save_tikz_png(fname, directory, False)

        plt.figure()
        # Skip value in the center because the denominator d<Uz>/dr = 0
        plt.plot(alphat[:-1, 0], alphat[:-1, 2])
        plt.xlabel(r'$y^+$')
        plt.ylabel(r'$\langle \alpha_{t,\varphi} \rangle / \nu $')
    
        fname = 'alphat_phi'
        msf.save_tikz_png(fname, directory, False)

        plt.figure()
        # Skip value in the center because the denominator d<Uz>/dr = 0
        plt.plot(alphat[:-1, 0], Prt[:-1, 0])
        plt.xlabel(r'$y^+$')
        plt.ylabel(r'$\langle Pr_{t,r} \rangle^ {\varphi, z, t} $')
    
        fname = 'Prt_r'
        msf.save_tikz_png(fname, directory, False)

        plt.savefig(directory + 'Prt_r.png')

        plt.figure()
        # Skip value in the center because the denominator d<Uz>/dr = 0
        plt.plot(alphat[:-1, 0], Prt[:-1, 1])
        plt.xlabel(r'$y^+$')
        plt.ylabel(r'$\langle Pr_{t,\varphi} \rangle^ {varphi, z, t} $')
    
        fname = 'Prt_phi'
        msf.save_tikz_png(fname, directory, False)



#def plot_timeseries(timeranges):
#
#    plt.close('all')
#
#    print("Processing " + ','.join(timeranges))
#    # Load data
#    #----------
#    label = (
#        'halfconst071',
#        'halfsin071',
#        'sin071',
#        'halfconst0025',       
#        'halfsin0025',
#        'sin0025',
#        )
#
#    data_name = (
#            'alphat_avg.dat'
#            )
#
#    nr = int(np.loadtxt('interpmesh/number_of_points.txt')[0])
#    ncols = 3
#    n_labels = len(label)
#    n_timeranges = len(timeranges)
#
#    data = np.zeros((nr, ncols, n_labels, n_timeranges))
#    for i in range(n_timeranges):
#        for j in range(n_labels):
#            data_dir = 'profiles/' + timeranges[i] + '/' + label[j] + '/'
#            data[:, :, j, i] = np.loadtxt(data_dir + data_name)
#
#
#    # Plot
#    #-----
#    for j in range(n_labels):
#        plt.figure()
#        for i in range(n_timeranges):
#            # Skip value in the center because the denominator d<Uz>/dr = 0
#            plt.plot(data[:-1, 0, j, i], data[:-1, 1, j, i], label=timeranges[i])
#            plt.xlabel(r'$y^+$')
#            plt.ylabel(r'$\langle \alpha_{t,r} \rangle / \nu $')
#            plt.legend()
#    
#        directory = 'plots/' + timeranges[-1] + '/' + label[j] + '/'
#        if not os.path.exists(directory):
#            os.makedirs(directory)
#    
#        plt.savefig(directory + 'alphat_r_timerange.png')
#
#
#        plt.figure()
#        for i in range(n_timeranges):
#            # Skip value in the center because the denominator d<Uz>/dr = 0
#            plt.plot(data[:-1, 0, j, i], data[:-1, 2, j, i], label=timeranges[i])
#            plt.xlabel(r'$y^+$')
#            plt.ylabel(r'$\langle \alpha_{t,\varphi} \rangle / \nu $')
#            plt.legend()
#    
#        directory = 'plots/' + timeranges[-1] + '/' + label[j] + '/'
#        if not os.path.exists(directory):
#            os.makedirs(directory)
#    
#        plt.savefig(directory + 'alphat_phi_timerange.png')


def plot_comparison(dat_path, given_labels, iftikz=False):

    plt.close('all')

    print("Processing " + ','.join(dat_path))
    print('Compare ' + ' '.join(given_labels))
    all_labels = '_'.join(given_labels)
    # Load data
    #----------
    case_label = (
            'halfconst071',
            'halfsin071',
            'halfconst0025',
            'halfsin0025',
            )

    data_name_alphat = (
            'alphat_avg.dat'
            )

    data_name_nut = (
            'nut.dat'
            )

    n_data = len(dat_path)
    n_names = len(case_label)
    alphat = []
    for i in range(n_data):
        pts_file = dat_path[i] + '../../interpmesh/number_of_points.txt'
        nr = int(np.loadtxt(pts_file)[0])
        ncols = 3

        this_data = np.zeros((nr, ncols, n_names))
        for j in range(n_names):
            data_dir = dat_path[i]
            data_file = case_label[j] + '/' + data_name_alphat
            this_data[:, :, j] = np.loadtxt(data_dir + data_file)

        alphat.append(this_data)

    nut = []
    for i in range(n_data):
        pts_file = dat_path[i] + '../../interpmesh/number_of_points.txt'
        nr = int(np.loadtxt(pts_file)[0])
        ncols = 2

        this_data = np.zeros((nr, ncols))
        data_dir = dat_path[i]
        data_file = data_name_nut
        this_data = np.loadtxt(data_dir + data_file)

        nut.append(this_data)
       

    # which labels?
    if (given_labels):
        labels = given_labels
    else:
        labels=data_path


    # Plot
    #-----
    # Loop over case names
    for j in range(n_names):
        plt.figure()
        # Loop over datasets which are compared
        for i in range(n_data):
            # Skip value in the center because the denominator d<Uz>/dr = 0
            plt.plot(alphat[i][:-1, 0, j], nut[i][:-1, 1] / alphat[i][:-1, 1, j], '-', 
                    color='C{0:d}'.format(i), label=labels[i])
            plt.plot(alphat[i][:-1, 0, j], nut[i][:-1, 1] / alphat[i][:-1, 2, j], '--', 
                    color='C{0:d}'.format(i))
        plt.xlabel(r'$y^+$')
        plt.ylabel(r'$\langle Pr_{t} \rangle$')
#        plt.xlim(0, 180)
        plt.xlim(xmin=0)
        plt.ylim(ymin=0)
        plt.legend()

        fname = 'Prt_' + all_labels
        directory = 'plots/compare/Prt/' + case_label[j] + '/'
        msf.save_tikz_png(fname, directory, iftikz)

#        plt.figure()
#        # Loop over datasets which are compared
#        for i in range(n_data):
#            # Skip value in the center because the denominator d<Uz>/dr = 0
#            plt.plot(alphat[i][:-1, 0, j], nut[i][:-1, 1] / alphat[i][:-1, 2, j], label=labels[i])
#        plt.xlabel(r'$y^+$')
#        plt.ylabel(r'$\langle Pr_{t,\varphi} \rangle^{\varphi, z, t} $')
#        plt.xlim(0, 180)
#        plt.ylim(ymin=0)
#        plt.legend()
#
#        fname = 'Prt_phi_' + all_labels
#        directory = 'plots/compare/Prt/' + case_label[j] + '/'
#        msf.save_tikz_png(fname, directory, iftikz)






def run():
    """Plot averaged turbulent thermal diffusivities and turbulent Prandtl number"""

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    # Loop over specified timeranges
    for span in timeranges_select:
        # Call a postprocessing script
        plot_individual(span)


