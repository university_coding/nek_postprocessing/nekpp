#/usr/bin/env python3.5
#----------------------------------------------------------------------
# Plot profiles of data in .dat files
#----------------------------------------------------------------------
# Date:     2018/03/01
# Author:   Steffen Straub
#----------------------------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt
from matplotlib2tikz import save as tikz_save
import os

import configparser


import pdb


def plot_2pc_v(timerange_select, Lz, iftikz):
    """ Plot two-point correlations of velocity. 

    timerange_select            selection of averaged data to process
    Lz                          domain length
    iftikz                      generate tikze plots?
    """

    plt.close('all')

    fname = '2pc_v.dat'
    
    # Load data
    #----------
    data_dir = 'profiles/' + timerange_select + '/' 
    data_name = fname

    print("Processing " + data_name + ' at time ' + timerange_select)           
    # Read header first
    with open(data_dir + data_name) as f:
        header = f.readline()

    y_p = float(header.split()[5][:-1])
    y = float(header.split()[8][:-1])
    data = np.loadtxt(data_dir + data_name)

    z = data[:, 0]
    nz = len(z)
    R_z_varmax = data[:, 1]
    R_z_r01 = data[:, 2]

    # Plot
    #-----
    plt.figure(1)
    plt.plot(z, R_z_varmax/R_z_varmax[0], 
            label=r'$y^+={0:3.1f}$'.format(y_p))
    plt.plot(z, R_z_r01/R_z_r01[0],  
            label=r'$y={0:3.1f} D$'.format(y))
    plt.plot(z[0:nz//2], np.ones(nz//2)*0, 'k--')
    plt.xlabel(r'$\Delta z / D$')
    plt.ylabel(r'$R(\Delta z) / R(0)$')
    plt.xlim([0, Lz/2])
    plt.legend()


    directory = 'plots/' + timerange_select + '/2pc/'
    if not os.path.exists(directory):
        os.makedirs(directory)

    if (iftikz):
        tikz_save(directory + '2pc_v.tex',
                figureheight='\\figureheight',
                figurewidth='\\figurewidth',
                show_info=False,
                )

    plt.savefig(directory + '2pc_v.png')


def plot_2pc_t(timerange_select, Lz, n_temp, t_fld_inhomog, iftikz):
    """ Plot two-point correlations of thermal statistics. 

    timerange_select            selection of averaged data to process
    Lz                          domain length
    n_temp                      number of thermal fields
    iftikz                      generate tikze plots?
    t_fld_inhomog               list of inhomogeneous field numbers
    """

    plt.close('all')

    fname_list = ['2pc_t{0:d}.dat'.format(i) for i in range(n_temp)]
    label_homog_list = (
            'IT071',
            'MBC071',
            'IF071',
            'IT0025',
            'MBC0025',
            'IF0025',
            )
    label_inhomog_list = (
            'halfconst071',
            'halfsin071',
            'halfconst0025',
            'halfsin0025'
            )
    probe_list = (
            '-90',
            '-45',
            '0',
            '45',
            '90',
            )



    directory = 'plots/' + timerange_select + '/2pc/'
    if not os.path.exists(directory):
        os.makedirs(directory)


    i1 = 0
    i2 = 0
    n_probes = len(probe_list)
    for k in range(n_temp):
    
        # Load data
        #----------
        data_dir = 'profiles/' + timerange_select + '/' 
        data_name = fname_list[k]

        print("Processing " + fname_list[k] + ' at time ' + timerange_select)           
        # Read header first
        with open(data_dir + data_name) as f:
            header1 = f.readline()

        data = np.loadtxt(data_dir + data_name)

        z = data[:, 0]
        nz = len(z)

        if k in t_fld_inhomog:

            # Loop over probes
            for l in range(n_probes):
                R_z_varmax = data[:, 1 + l]
                R_z_r01 = data[:, 1 + n_probes + l]
                R_z_wall = data[:, 1 + 2*n_probes + l]
        
                # Plot
                #-----
                plt.figure(10+i1)
                plt.plot(z, R_z_varmax/R_z_varmax[0], 
                        color='C{0:d}'.format(l), label=probe_list[l])
                plt.figure(20+i1)
                plt.plot(z, R_z_r01/R_z_r01[0],  
                        color='C{0:d}'.format(l), label=probe_list[l])
                plt.figure(30+i1)
                plt.plot(z, R_z_wall/R_z_wall[0],  
                        color='C{0:d}'.format(l), label=probe_list[l])



            plt.figure(10+i1)
            plt.plot(z[0:nz//2], np.ones(nz//2)*0, 'k--')
            plt.xlabel(r'$\Delta z / D$')
            plt.ylabel(r'$R(\Delta z) / R(0)$')
            plt.xlim([0, Lz/2])
            plt.legend()
        
            fname = '2pc_' + label_inhomog_list[i1] + '_maxfluct'
            if (iftikz):
                tikz_save(directory + fname + '.tex',
                        figureheight='\\figureheight',
                        figurewidth='\\figurewidth',
                        show_info=False,
                        )
        
            plt.savefig(directory + fname + '.png')
        
        
            plt.figure(20+i1)
            plt.plot(z[0:nz//2], np.ones(nz//2)*0, 'k--')
            plt.xlabel(r'$\Delta z / D$')
            plt.ylabel(r'$R(\Delta z) / R(0)$')
            plt.xlim([0, Lz/2])
            plt.legend()
        
            fname = '2pc_' + label_inhomog_list[i1] + '_center'
            if (iftikz):
                tikz_save(directory + fname + '.tex',
                        figureheight='\\figureheight',
                        figurewidth='\\figurewidth',
                        show_info=False,
                        )
        
            plt.savefig(directory + fname + '.png')
    
            plt.figure(30+i1)
            plt.plot(z[0:nz//2], np.ones(nz//2)*0, 'k--')
            plt.xlabel(r'$\Delta z / D$')
            plt.ylabel(r'$R(\Delta z) / R(0)$')
            plt.xlim([0, Lz/2])
            plt.legend()
        
            fname = '2pc_' + label_inhomog_list[i1] + '_wall'
            if (iftikz):
                tikz_save(directory + fname + '.tex',
                        figureheight='\\figureheight',
                        figurewidth='\\figurewidth',
                        show_info=False,
                        )
        
            plt.savefig(directory + fname + '.png')

            i1 += 1

        else:
            R_z_varmax = data[:, 1]
            R_z_r01 = data[:, 2]
    
            # Plot
            #-----
            plt.figure(1)
            plt.plot(z, R_z_varmax/R_z_varmax[0], 
                    color='C{0:d}'.format(i2), label=label_homog_list[i2])
            plt.figure(2)
            plt.plot(z, R_z_r01/R_z_r01[0],  
                    color='C{0:d}'.format(i2), label=label_homog_list[i2])
            
            i2 += 1
    

    plt.figure(1)
    plt.plot(z[0:nz//2], np.ones(nz//2)*0, 'k--')
    plt.xlabel(r'$\Delta z / D$')
    plt.ylabel(r'$R(\Delta z) / R(0)$')
    plt.xlim([0, Lz/2])
    plt.legend()

    fname = '2pc_homog_maxfluct'
    if (iftikz):
        tikz_save(directory + fname + '.tex',
                figureheight='\\figureheight',
                figurewidth='\\figurewidth',
                show_info=False,
                )

    plt.savefig(directory + fname + '.png')


    plt.figure(2)
    plt.plot(z[0:nz//2], np.ones(nz//2)*0, 'k--')
    plt.xlabel(r'$\Delta z / D$')
    plt.ylabel(r'$R(\Delta z) / R(0)$')
    plt.xlim([0, Lz/2])
    plt.legend()

    fname = '2pc_homog_center'
    if (iftikz):
        tikz_save(directory + fname + '.tex',
                figureheight='\\figureheight',
                figurewidth='\\figurewidth',
                show_info=False,
                )

    plt.savefig(directory + fname + '.png')
    




def run_v(Lz = 12.5, iftikz=False):
    # Call function

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    for span in timeranges_select:
        plot_2pc_v(span, Lz, iftikz)   


def run_t(Lz = 12.5, n_temp=10, iftikz=False):
    # Call function

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    # inhomogeneous TBC in fields:
    t_fld_inhomog = [3, 4, 8, 9]


    for span in timeranges_select:
        plot_2pc_t(span, Lz, n_temp, t_fld_inhomog, iftikz)   
