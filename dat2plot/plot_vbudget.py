#/usr/bin/env python3.5
#----------------------------------------------------------------------
# Plot profiles of data in .dat files
#----------------------------------------------------------------------
# Date:     2018/03/02
# Author:   Steffen Straub
#----------------------------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt
from matplotlib2tikz import save as tikz_save
import os

import configparser

import pdb


def plot_individual(timerange_select):

    plt.close('all')

    print("Processing " + timerange_select)
    # Load data
    #----------
    data_dir = 'profiles/' + timerange_select + '/'
    data_name = (
            'v_budget_k.dat',
            'v_budget_rr.dat',
            'v_budget_tt.dat',
            'v_budget_zz.dat',
            'v_budget_rz.dat',
            )

    data = []
    for name in data_name:
        data.append(np.loadtxt(data_dir + name))
    data = np.asarray(data)




    # Plot
    #-----
    for i in range(len(data)):

        # k
        plt.figure()
        plt.plot(data[i, :, 0], data[i, :, 1], label=r'$P^+$')
        plt.plot(data[i, :, 0], data[i, :, 2], label=r'$-\tilde{\epsilon}^+$')
        plt.plot(data[i, :, 0], data[i, :, 3], label=r'$\Pi^+$')
        plt.plot(data[i, :, 0], data[i, :, 4], label=r'$D^+$')
        plt.plot(data[i, :, 0], data[i, :, 5], label=r'$T^+$')
        plt.plot(data[i, :, 0], np.sum(data[i, :, 1:], axis=1), 'k--', label=r'resid')

        plt.xlabel(r'$y^+$')
        plt.legend()
    
        directory = 'plots/' + timerange_select + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + data_name[i][:-4] + '.png')





def plot_timeseries(timeranges):

    plt.close('all')

    print("Processing " + ','.join(timeranges))
    # Load data
    #----------
    data_name = (
            'v_budget_k.dat',
            'v_budget_rr.dat',
            'v_budget_tt.dat',
            'v_budget_zz.dat',
            'v_budget_rz.dat',
            )

    nr = int(np.loadtxt('interpmesh/number_of_points.txt')[0])
    ncols = 6
    n_names = len(data_name)
    n_timeranges = len(timeranges)

    data = np.zeros((nr, ncols, n_names, n_timeranges))
    for i in range(n_timeranges):
        for j in range(n_names):
            data_dir = 'profiles/' + timeranges[i] + '/'
            data[:, :, j, i] = np.loadtxt(data_dir + data_name[j])


    # Plot
    #-----

    # Production
    for j in range(n_names):
        plt.figure()
        for i in range(n_timeranges):
            plt.plot(data[:, 0, j, i], data[:, 1, j, i], label=timeranges[i])
            plt.xlabel(r'$y^+$')
            plt.ylabel(r'$P^+$')
            plt.legend()

        directory = 'plots/' + timeranges[-1] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + data_name[j][:-4] + '_P_timerange.png')
        
    plt.close('all')
    # Dissipation
    for j in range(n_names):
        plt.figure()
        for i in range(n_timeranges):
            plt.plot(data[:, 0, j, i], data[:, 2, j, i], label=timeranges[i])
            plt.xlabel(r'$y^+$')
            plt.ylabel(r'$-\tilde{\epsilon}^+$')
            plt.legend()

        directory = 'plots/' + timeranges[-1] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + data_name[j][:-4] + '_eps_timerange.png')

    plt.close('all')
    # Pressure-related diffusion
    for j in range(n_names):
        plt.figure()
        for i in range(n_timeranges):
            plt.plot(data[:, 0, j, i], data[:, 3, j, i], label=timeranges[i])
            plt.xlabel(r'$y^+$')
            plt.ylabel(r'$\Pi^+$')
            plt.legend()

        directory = 'plots/' + timeranges[-1] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + data_name[j][:-4] + '_Pi_timerange.png')

    plt.close('all')
    # Viscous diffusion
    for j in range(n_names):
        plt.figure()
        for i in range(n_timeranges):
            plt.plot(data[:, 0, j, i], data[:, 4, j, i], label=timeranges[i])
            plt.xlabel(r'$y^+$')
            plt.ylabel(r'$D^+$')
            plt.legend()

        directory = 'plots/' + timeranges[-1] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + data_name[j][:-4] + '_D_timerange.png')

    plt.close('all')
    # Turbulent velocity related diffusion
    for j in range(n_names):
        plt.figure()
        for i in range(n_timeranges):
            plt.plot(data[:, 0, j, i], data[:, 5, j, i], label=timeranges[i])
            plt.xlabel(r'$y^+$')
            plt.ylabel(r'$T^+$')
            plt.legend()

        directory = 'plots/' + timeranges[-1] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + data_name[j][:-4] + '_T_timerange.png')

    plt.close('all')
    # Residual
    for j in range(n_names):
        plt.figure()
        for i in range(n_timeranges):
            plt.plot(data[:, 0, j, i], np.sum(data[:, 1:, j, i], axis=1), label=timeranges[i])
            plt.xlabel(r'$y^+$')
            plt.ylabel(r'$resid$')
            plt.legend()

        directory = 'plots/' + timeranges[-1] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + data_name[j][:-4] + '_resid_timerange.png')



def plot_max_rms(timeranges):

    plt.close('all')

    print("Processing " + ','.join(timeranges))
    # Load data
    #----------
    data_name = (
            'v_budget_k.dat',
            'v_budget_rr.dat',
            'v_budget_tt.dat',
            'v_budget_zz.dat',
            'v_budget_rz.dat',
            )

    labels = [data_name[i][9:-4] for i in range(len(data_name))]

    n_names = len(data_name)
    ncols = 2
    n_timeranges = len(timeranges)

    data = np.zeros((n_names, ncols, n_timeranges))

    for i in range(n_timeranges):
        data_dir = 'profiles/' + timeranges[i] + '/'
        data[..., i] = np.loadtxt(data_dir + 'resid_vbudget_max_rms.dat')

    # Extract time duration
    delta = []
    for k in range(n_timeranges):
        t1 = int(timeranges[k].split('-')[0])
        t2 = int(timeranges[k].split('-')[1])
        delta.append(t2-t1)


    # Plot
    #-----
    plt.figure()
    for k in range(n_names):
        plt.plot(delta, data[k, 0, :], label=labels[k])
        plt.xlabel('Average time')
        plt.ylabel('max(eps)')
        plt.legend()

        directory = 'plots/' + timeranges[-1] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + 'resid_vbudget_max.png')

    plt.figure()
    for k in range(n_names):
        plt.plot(delta, data[k, 1, :], label=labels[k])
#        plt.plot(delta, np.ones(n_timeranges)*1e-3, 'k--')
        plt.xlabel('Average time')
        plt.ylabel('RMS(eps)')
        plt.legend()

        directory = 'plots/' + timeranges[-1] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + 'resid_vbudget_rms.png')





def plot_comparison(dat_path, given_labels="", iftikz=False):

    plt.close('all')

    print("Processing " + ','.join(dat_path))
    print('Compare ' + ' '.join(given_labels))
    all_labels = '_'.join(given_labels)
    # Load data
    #----------
    data_name = (
            'v_budget_k.dat',
            'v_budget_rr.dat',
            'v_budget_tt.dat',
            'v_budget_zz.dat',
            'v_budget_rz.dat',
            )

    # Save all datasets in one list "data"
    n_data = len(dat_path)
    n_names = len(data_name)
    data = []
    for i in range(n_data):
        pts_file = dat_path[i] + '../../interpmesh/number_of_points.txt'
        nr = int(np.loadtxt(pts_file)[0])
        ncols = 6

        this_data = np.zeros((nr, ncols, n_names))
        for j in range(n_names):
            data_dir = dat_path[i]
            this_data[:, :, j] = np.loadtxt(data_dir + data_name[j])

        data.append(this_data)


    # which labels?
    if (given_labels):
        labels = given_labels
    else:
        labels=data_path


    # Plot
    #-----

    # Production
    for j in range(n_names):
        plt.figure()
        for i in range(n_data):
            plt.semilogx(data[i][:, 0, j], data[i][:, 1, j], label=labels[i])
            plt.xlabel(r'$y^+$')
            plt.ylabel(r'$P^+$')
            plt.legend()

        directory = 'plots/' + 'compare_' + all_labels + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + data_name[j][:-4] + '_P_' + all_labels + '.png')
        
    plt.close('all')
    # Dissipation
    for j in range(n_names):
        plt.figure()
        for i in range(n_data):
            plt.semilogx(data[i][:, 0, j], data[i][:, 2, j], label=labels[i])
            plt.xlabel(r'$y^+$')
            plt.ylabel(r'$-\tilde{\epsilon}^+$')
            plt.legend()

        directory = 'plots/' + 'compare_' + all_labels + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + data_name[j][:-4] + '_eps_' + all_labels + '.png')

    plt.close('all')
    # Pressure-related diffusion
    for j in range(n_names):
        plt.figure()
        for i in range(n_data):
            plt.semilogx(data[i][:, 0, j], data[i][:, 3, j], label=labels[i])
            plt.xlabel(r'$y^+$')
            plt.ylabel(r'$\Pi^+$')
            plt.legend()

        directory = 'plots/' + 'compare_' + all_labels + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + data_name[j][:-4] + '_Pi_' + all_labels + '.png')

    plt.close('all')
    # Viscous diffusion
    for j in range(n_names):
        plt.figure()
        for i in range(n_data):
            plt.semilogx(data[i][:, 0, j], data[i][:, 4, j], label=labels[i])
            plt.xlabel(r'$y^+$')
            plt.ylabel(r'$D^+$')
            plt.legend()

        directory = 'plots/' + 'compare_' + all_labels + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + data_name[j][:-4] + '_D_' + all_labels + '.png')

    plt.close('all')
    # Turbulent velocity related diffusion
    for j in range(n_names):
        plt.figure()
        for i in range(n_data):
            plt.semilogx(data[i][:, 0, j], data[i][:, 5, j], label=labels[i])
            plt.xlabel(r'$y^+$')
            plt.ylabel(r'$T^+$')
            plt.legend()

        directory = 'plots/' + 'compare_' + all_labels + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + data_name[j][:-4] + '_T_' + all_labels + '.png')

    plt.close('all')
    # Residual
    for j in range(n_names):
        plt.figure()
        for i in range(n_data):
            plt.semilogx(data[i][:, 0, j], np.sum(data[i][:, 1:, j], axis=1), label=labels[i])
            plt.xlabel(r'$y^+$')
            plt.ylabel(r'$resid$')
            plt.legend()

        directory = 'plots/' + 'compare_' + all_labels + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + data_name[j][:-4] + '_resid_' + all_labels + '.png')

    # All together
    my_linestyles = ('-', '--', ':', '-.')
    for j in range(n_names):
        plt.figure()
        for i in range(n_data):
            plt.semilogx(data[i][:, 0, j], data[i][:, 1, j], 
                    my_linestyles[i], color='C0', label='$P^+$')
            plt.semilogx(data[i][:, 0, j], data[i][:, 2, j], 
                    my_linestyles[i], color='C1', label=u'$\epsilon^+$')
            plt.semilogx(data[i][:, 0, j], data[i][:, 3, j], 
                    my_linestyles[i], color='C2', label=u'$\Pi^+$')
            plt.semilogx(data[i][:, 0, j], data[i][:, 4, j], 
                    my_linestyles[i], color='C3', label=u'$D^+$')
            plt.semilogx(data[i][:, 0, j], data[i][:, 5, j], 
                    my_linestyles[i], color='C4', label=u'$T^+$')


            plt.xlabel(r'$y^+$')
#            plt.ylabel(r'$P^+$')
            plt.legend()

        directory = 'plots/' + 'compare_' + all_labels + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)

        if (iftikz):
           tikz_save(directory + data_name[j][:-4] + '_all_' + all_labels + '.tex',
                   figureheight='\\figureheight',
                   figurewidth='\\figurewidth',
                   show_info=False,
               )
   
        plt.savefig(directory + data_name[j][:-4] + '_all_' + all_labels + '.png')
        
    plt.close('all')


def run():
    """Plot velocity statitics: budgets."""

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    # Loop over specified timeranges
    for span in timeranges_select:
        # Call a postprocessing script
        plot_individual(span)
