#!/usr/bin/env python3.5
#-----------------------
# Compare DNS and LES-RT results
#----------------------------------------------------------------------
# Author:   Steffen Straub
# Date:     2018/03/15
#----------------------------------------------------------------------

from NekPP.dat2plot import plot_temp

# Location of profiles to compare
data_path = (
        'dns_profiles/900-4500/',
        'profiles/2925-4181/',
        )

# Corresponding labels
labels = (
        'DNS',
        'LES',
        )

plot_temp.plot_comparison(data_path, labels, iftikz=False)


