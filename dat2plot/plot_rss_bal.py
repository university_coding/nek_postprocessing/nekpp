#/usr/bin/env python3.5
#----------------------------------------------------------------------
# Plot profiles of data in .dat files
#----------------------------------------------------------------------
# Date:     2018/03/01
# Author:   Steffen Straub
#----------------------------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt
from ..misc import my_save_fig as msf
import os

import configparser

import pdb


def plot_individual(timerange_select, iftikz):

    plt.close('all')

    print("Processing " + timerange_select)
    # Load data
    #----------
    data_dir = 'profiles/' + timerange_select + '/'
    data_name = (
            'rss_bal.dat'
            )
    data = np.loadtxt(data_dir + data_name)

    # Read user defined case-dependent parameters
    config = configparser.ConfigParser()
    config.read('case_params.ini')
    Re_t = float(config['Reynolds']['Re_t'])


    # Plot
    #-----
    plt.figure(0)
    plt.plot(data[:, 0], data[:, 1], label='visc')
    plt.plot(data[:, 0], data[:, 2], label='turb')
    plt.plot(data[:, 0], data[:, 3], label='total')
    plt.plot(data[:, 0], 1 - data[:, 0]/(Re_t/2), '--', label='analytic')
    plt.xlabel(r'$y^+$')
    plt.ylabel(r'$\tau_w$')
    plt.legend()

    fname = 'rss_bal'
    directory = 'plots/' + timerange_select + '/'
    msf.save_tikz_png(fname, directory, iftikz)
    

    plt.figure(1)
    plt.plot(data[:, 0], data[:, 4])
    plt.xlabel(r'$y^+$')
    plt.ylabel(r'$E_R$')

    fname = 'resid_ER'
    msf.save_tikz_png(fname, directory, iftikz)


    plt.figure(2)
    plt.plot(data[:, 0], data[:, 5])
    plt.xlabel(r'$y^+$')
    plt.ylabel(r'$E_U$')
    
    fname = 'resid_EU'
    msf.save_tikz_png(fname, directory, iftikz)


def run(iftikz=False):
    """Plot RSS balance"""

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    # Loop over specified timeranges
    for span in timeranges_select:
        # Call a postprocessing script
        plot_individual(span, iftikz)
