#!/usr/bin/env python3
#----------------------------------------------------------------------
# Plot the individual contributions to the Nusselt number for
# MBC and IF
#----------------------------------------------------------------------
# Author:   Steffen Straub
# Date:     2019/02/15
#----------------------------------------------------------------------
import numpy as np
import matplotlib.pyplot as plt
from ..misc import my_save_fig as msf

import configparser

import pdb

def plot_individual(data_path):
    """ Plot contributions to the Nusselt number. 
    
    data_path          Files containing data to plot
    """

    plt.close('all')
    # Load data
    n_groups = len(data_path)
    data = np.zeros((n_groups, 3))      
    for i in range(n_groups):
        data[i, :] = np.loadtxt(data_path[i])

    # Data manipulation
    Nu_l = 1/data[:, 0]
    Nu_hf = 1/data[:, 1]
    Nu_rss = 1/data[:, 2]



    # Plot data
    index = np.arange(n_groups)
    bar_width = 0.35
    opacity = 0.4

    fig, ax = plt.subplots()

    rects1 = ax.bar(index, Nu_l/Nu_hf, bar_width)
    ax.set_ylabel(r'$Nu_L / Nu_{HF}$')
    ax.set_xticks(index)
    ax.set_xticklabels(('MBC071', 'IF071', 'MBC0025', 'IF0025'))

    fig.tight_layout()

    plt.show()


def plot_comparison(data_path, Re_list, iftikz):
    """ Compare Nu contributions against Re for different thermal boundary conditions. 

    data_path               list of location of dataset files to compare
    Re_list                 list of corresponding Re numbers
    iftikz                  save plots also in tex format?
    """

    plt.close('all')
    all_labels = '_'.join(Re_list)

    # Load data
    #----------
    # Load data
    n_cases = len(data_path)
    n_groups = len(data_path[0])
    data = np.zeros((n_cases, n_groups, 5))      
    for i in range(n_cases):
        for j in range(n_groups):
            data[i, j, :] = np.loadtxt(data_path[i][j])

    # Data manipulation
    Nu_l = 1/data[..., 0]
    Nu_hfr = 1/data[..., 1]
    Nu_rss = 1/data[..., 2]
    Nu_hfz = 1/data[..., 3]
    Nu = 1/(data[...,0] - data[..., 1] - data[..., 2] + data[..., 3])

    

    # Plot data
    ind1 = np.arange(n_groups)
    ind2 = np.linspace(0, 0.9*ind1[1], n_cases, endpoint=False)
    bar_width = 0.2
    disp = 0.02
    opacity = 0.4

    fig, ax = plt.subplots()
    for i in range(n_groups):
        for j in range(n_cases):

#            summed = (Nu_l[0, 0]/Nu[i, j] 
#                    + Nu_l[0, 0]/Nu_hfr[i, j] 
#                    + Nu_l[0, 0]/Nu_rss[i, j] 
#                    - Nu_l[0,0]/Nu_hfz[i,j]
#                    )
#            print('i={0:d}, j={1:d}, sum={2:f}'.format(i, j, summed))
#            print('i={0:d}, j={1:d}, rss={2:f}'.format(i, j, Nu_l[0, 0]/Nu_rss[i, j]))
#            print('i={0:d}, j={1:d}, hfr={2:f}'.format(i, j, Nu_l[0, 0]/Nu_hfr[i, j]))
#            print('i={0:d}, j={1:d}, hfz={2:f}'.format(i, j, Nu_l[0, 0]/Nu_hfz[i, j]))
            ax.bar(ind1[j]+ind2[i], Nu_l[0, 0]/Nu[i, j], bar_width, 
                    bottom = Nu_l[0, 0]/Nu_rss[i, j] + Nu_l[0, 0]/Nu_hfr[i, j], 
                    align = 'edge',
                    color = '#000000')

            ax.bar(ind1[j]+ind2[i], Nu_l[0, 0]/Nu_hfr[i, j], bar_width, 
                    bottom = Nu_l[0, 0]/Nu_rss[i, j], 
                    align = 'edge',
                    color = '#FF0000')

            ax.bar(ind1[j]+ind2[i], Nu_l[0, 0]/Nu_rss[i, j], bar_width, 
                    bottom = 0, 
                    align = 'edge',
                    color = '#FFCC00')

            ax.bar(ind1[j]+ind2[i], - Nu_l[0, 0]/Nu_hfz[i, j], bar_width, 
                    bottom = 0, 
                    align = 'edge',
                    color = '#3399FF')


            ax.annotate(Re_list[i], [ind1[j]+ind2[i]+bar_width/2, 0.05], 
                    rotation=90, verticalalignment='bottom',
                    horizontalalignment='center',
                    size=20,
                    )

#    ax.set_ylabel(r'$Nu_L / Nu$')
#    ax.set_ylim(bottom=0, top=1)
    ax.set_xticks(ind1 + (ind2[1]+bar_width+ind2[2])/2 )
    ax.set_xticklabels(('MBC071', 'IF071', 'MBC0025', 'IF0025'))

    fname = 'Nu_contributions'
    directory = 'plots/' + 'compare_'+all_labels + '/'
    msf.save_tikz_png(fname, directory, iftikz)

def run():

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    # Possibly loop over all timeranges
    for time in timeranges_select:

        # Loop over all MBC / IF fields
        data_path = []
        for tfield in (2, 3, 5, 6):
            data_path.append('nusselt/'+time+'/nuss_contrib{0:d}.dat'.format(tfield))

        plot_individual(data_path)
