#/usr/bin/env python3.5
#----------------------------------------------------------------------
# Plot profiles of data in .dat files
#----------------------------------------------------------------------
# Date:     2018/03/01
# Author:   Steffen Straub
#----------------------------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt
from matplotlib2tikz import save as tikz_save
from ..misc import my_save_fig as msf
from ..misc import correlations

import os

import configparser

import pdb



def plot_individual(timerange_select):

    plt.close('all')

    print("Processing " + timerange_select)
    # Load data
    #----------
    data_dir = 'profiles/' + timerange_select + '/'
    data_name = (
            'heat_bal_IT071.dat',
            'heat_bal_MBC071.dat',
            'heat_bal_IF071.dat',
            'heat_bal_IT0025.dat',
            'heat_bal_MBC0025.dat',
            'heat_bal_IF0025.dat',
            )

    data = []
    for name in data_name:
        data.append(np.loadtxt(data_dir + name))
    data = np.asarray(data)

    # Plot
    #-----
    for i in range(len(data)):

        plt.figure()
        plt.semilogx(data[i, :, 0], data[i, :, 1], label='turb')
        plt.semilogx(data[i, :, 0], data[i, :, 2], label='mol')
        plt.semilogx(data[i, :, 0], data[i, :, 3], label='advec')
        plt.semilogx(data[i, :, 0], data[i, :, 4], '--k', label='total')
        plt.xlabel(r'$y^+$')
        plt.ylabel(r'$\langle q_w \rangle$')
        plt.legend()
    
        directory = 'plots/' + timerange_select + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + 'heat_bal_' + data_name[i][9:-4] + '.png')


def run():
    """Plot heat flux balance"""


    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    # Loop over specified timeranges
    for span in timeranges_select:
        plot_individual(span)

