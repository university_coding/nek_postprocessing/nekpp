#!/usr/bin/env python3.5
#-----------------------
# Compare datasets
#----------------------------------------------------------------------
# Author:   Steffen Straub
# Date:     2018/03/15
#----------------------------------------------------------------------

from NekPP.dat2plot import plot_nuss

# Location of profiles to compare
data_path = (
        'nusselt_360/',
        'nusselt_720/',
        'nusselt_1100/',
        'nusselt/1-7',
        )

# Corresponding Re numbers
Re_list = (
        5300, 
        11700, 
        19000, 
        37700,
        )




plot_nuss.plot_comparison(data_path, Re_list, iftikz=True)


