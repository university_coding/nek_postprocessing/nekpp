#!/usr/bin/env python3.5
#----------------------------------------------------------------------
# Calculate Kolmogorov length
#----------------------------------------------------------------------
# Author:   Steffen Straub
# Date:     2019/02/01
#----------------------------------------------------------------------
import numpy as np
import matplotlib.pyplot as plt
from ..misc import my_save_fig as msf
import os
import pdb

import configparser


def plot_individual(timerange_select, iftikz, ifgrid):
    """ Calculate Kolmogorov and Corrsin scales and plot them together 
    with smallest and largest grid spacing."""


    plt.close('all')
    print("Processing " + timerange_select)
    # Load data
    #----------
    data_dir = 'profiles/' + timerange_select + '/'
    data_name = (
            'v_budget_k.dat',
            )
    data = np.loadtxt(data_dir + data_name[0])
    y_p = data[:, 0]
    eps = -data[:, 2]

    # Read user defined case-dependent parameters
    config = configparser.ConfigParser()
    config.read('case_params.ini')
    Re_b = float(config['Reynolds']['Re_b'])
    Re_t = float(config['Reynolds']['Re_t'])
    
    # Variable assignments
    #---------------------
    D = 1
    Pr = np.asarray([0.71, 0.025])

    # Dissipation (normalized in viscous scales)
    # See calculations from 10.07.19
    eta = eps**(-1/4)

    # Plot
    #-----
    f, ax1 = plt.subplots(1, sharex=True)
    ax1.plot(y_p, eta, label=r'$\eta^+$')
#    ax1.plot(y_p, 3*eta, '--k', label=r'$3 \times \eta^+$')
    ax1.plot(y_p, eta*Pr[0]**(-3/4), label=r'$\eta_T^+ (0.71)$')
    ax1.plot(y_p, eta*Pr[1]**(-3/4), label=r'$\eta_T^+ (0.025)$')

    # Grid 
    if (ifgrid):
        r_grid = np.loadtxt('./r_grid.dat')
        y_grid = D/2 - r_grid
        dy_grid = y_grid[:-1] - y_grid[1:]
        
        ax1.plot(y_grid[:-1]*Re_t, dy_grid*Re_t, 'k', label='grid')
    else:
        # Example resolution for DNS and LES
        r_p_min = 0.37
        r_p_max = 4.5
        r_p_min_les = 0.52
        r_p_max_les = 10.3
        ax1.plot(y_p[-1], r_p_max, 'ro', label='DNS')
        ax1.plot(y_p[0], r_p_min, 'r+')
        ax1.plot(y_p[-1], r_p_max_les, 'k+', label='LES')
        ax1.plot(y_p[0], r_p_min_les, 'k+', )
   
    
    
    
    
    ax1.set_xlabel(r'$y^+$')
    ax1.set_ylabel(r'$\Delta r^+$')
    ax1.legend()

    fname = 'kolmogorov'
    directory = 'plots/' + timerange_select + '/'
    msf.save_tikz_png(fname, directory, iftikz)


def run(iftikz=False, ifgrid=False):
    """Plot Kolmogorov and Corrsin scales"""

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    # Loop over specified timeranges
    for span in timeranges_select:
        # Call a postprocessing script
        plot_individual(span, iftikz, ifgrid)



