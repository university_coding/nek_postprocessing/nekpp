#!/usr/bin/env python3.5
#----------------------------------------------------------------------
# Compare my results to El Khoury (2013) data
#----------------------------------------------------------------------
# Author:   Steffen Straub
# Date:     2018/10/11
#----------------------------------------------------------------------
import os
import matplotlib.pyplot as plt
import numpy as np
from matplotlib2tikz import save as tikz_save

import configparser



def valid(data_dirs, data_names, iftikz=False):
    plt.close('all')
    
    SS_k = np.loadtxt(data_dirs[0] + data_names[0])
    EK_r = np.loadtxt(data_dirs[1] + data_names[1], skiprows=21)
    EK_t = np.loadtxt(data_dirs[1] + data_names[2], skiprows=21)
    EK_z = np.loadtxt(data_dirs[1] + data_names[3], skiprows=21)
    
    
    EK_k = 1/2 * (EK_r[:, 1:] + EK_t[:, 1:] + EK_z[:, 1:])
    
    #
    ## Plot
    n_terms = 5
    every = 20
    labels = [r'$P^+$', r'$-\tilde{\epsilon}^+$', r'$\Pi^+$', r'$D^+$', r'$T^+$']
    plt.figure(1)
    plt.plot(EK_r[::every, 1], EK_k[::every, 1], 'x', markerfacecolor='none')
    plt.plot(EK_r[::every, 1], EK_k[::every, 6], 'x', markerfacecolor='none')
    plt.plot(EK_r[::every, 1], EK_k[::every, 3] + EK_k[::every, 4], 'x', markerfacecolor='none')
    plt.plot(EK_r[::every, 1], EK_k[::every, 5], 'x', markerfacecolor='none')
    plt.plot(EK_r[::every, 1], EK_k[::every, 2], 'x', markerfacecolor='none')
    for i in range(n_terms):
        plt.plot(SS_k[:, 0], SS_k[:, i+1], color='C{0:d}'.format(i), label=labels[i])
    
    plt.xlabel(r'$y^+$')    
    plt.ylabel('Loss / Gain')
    plt.xlim([0, 180])
    plt.legend()
    
    #
    ## Save 
    directory = 'plots/valid/'
    fname = 'valid_k'
    if not os.path.exists(directory):
        os.makedirs(directory)
    
    if (iftikz):
       tikz_save(directory + fname + '.tex',
               figureheight='\\figureheight',
               figurewidth='\\figurewidth',
               show_info=False,
           )
    
    plt.savefig(directory + fname + '.png')
    


def run_valid(iftikz=False):
    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    span = timeranges_select[0]
    
    #
    ## Load
    data_dirs = [
            'profiles/' + span + '/',
            '/net/istmlupus/localhome/hi202/local/data/pipe/2013_El_Khoury/',
            ]
    
    data_names = [
            'v_budget_k.dat',
            '1000_RR_Budget.dat',
            '1000_TT_Budget.dat',
            '1000_ZZ_Budget.dat',
            ]

    valid(data_dirs, data_names, iftikz)
