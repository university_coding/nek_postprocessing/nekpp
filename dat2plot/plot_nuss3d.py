#/usr/bin/env python3.5
#----------------------------------------------------------------------
# Plot profiles of data in .dat files
#----------------------------------------------------------------------
# Date:     2018/03/01
# Author:   Steffen Straub
#----------------------------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt
from ..misc import my_save_fig as msf
import os

import configparser

import pdb

def plot_individual(span, iftikz):
    plt.ioff()
    plt.close('all')

    print('Note: label_list is hardcoded for now.')
    label = (
        '071',
        '0025',
        )

    # Load data
    for i in range(len(label)):
        data_dir = 'profiles/' + span + '/' + label[i] + '/'
        data_name = 'Nusselt.dat'
        data = np.loadtxt(data_dir + data_name)

        z = data[:, 0]
        Nuss_z = data[:, 1]
        Nuss_max = data[:, 2]

        fig = plt.figure()
        plt.plot(z, Nuss_z, label=r'$\langle Nu \rangle^\varphi$')
        plt.plot(z, Nuss_max, label=r'$Nu(\varphi=\pi/2)$')
        plt.ylabel('$Nu$')
        plt.xlabel('$z/D$')
        plt.legend()

        fname = 'Nusselt'
        directory = 'plots/' + span + '/' + label[i] + '/'
        msf.save_tikz_png(fname, directory, iftikz)


def plot_diff(dat_path, given_labels=None, iftikz=None):
    plt.ioff()
    plt.close('all')

    print('Note: label_list is hardcoded for now.')
    label = (
        '071',
        '0025',
        )
    
    # which legend labels?
    if (given_labels):
        leg_labels = given_labels


    # Load data
    n_data = len(dat_path)
    data = []

    if (n_data != 2):
        print("Usage: 2 setups can be compared, but n_data = {0:d}".format(n_data))
        sys.exit()

    for i in range(len(label)):

        for idata in range(n_data):
            data_dir = dat_path[idata] + '/' + label[i] + '/'
            data_name = 'Nusselt.dat'
            this_data = np.loadtxt(data_dir + data_name)
    
            data.append(this_data)

        z = data[0][:, 0]

        fig = plt.figure()
        plt.plot(z, data[0][:, 1], label=r'$\langle Nu \rangle^\varphi$')
        plt.plot(z, data[1][:, 1], 'k:')
        plt.plot(z, data[0][:, 2], label=r'$Nu(\varphi=90^\circ)$')
        plt.plot(z, data[1][:, 2], 'k:')

        plt.ylabel(r'$Nu$')
        plt.xlabel('$z/D$')
        plt.legend()

        fname = 'Nusselt'
        directory = 'plots/diff/' + label[i] + '/'
        msf.save_tikz_png(fname, directory, iftikz)

        # reset data for 0025
        data = []


def plot_compare(dat_path, Lz_list, iftikz=None):
    plt.ioff()
    plt.close('all')

    print('Note: label_list is hardcoded for now.')
    label = (
        '071',
        '0025',
        )
    
    # Load data
    n_data = len(dat_path)
    data = []

    for i in range(len(label)):

        for idata in range(n_data):
            data_dir = dat_path[idata] + '/' + label[i] + '/'
            data_name = 'Nusselt.dat'
            this_data = np.loadtxt(data_dir + data_name)
    
            data.append(this_data)

        z = data[0][:, 0]

        fig = plt.figure()
        plt.plot(data[0][:, 0]/Lz_list[0], data[0][:, 1], 
                color='C0', label=r'$\langle Nu \rangle^\varphi$')
        plt.plot(data[0][:, 0]/Lz_list[0], data[0][:, 2], 
                color='C1', linestyle='--', label=r'$Nu(\varphi=90^\circ)$')
        for idata in range(1, n_data-1):
            plt.plot(data[idata][:, 0]/Lz_list[idata], data[idata][:, 1], color='C0')
            plt.plot(data[idata][:, 0]/Lz_list[idata], data[idata][:, 2], color='C1', linestyle='--')
        # Last dataset is 25
        plt.plot(data[n_data-1][:, 0]/Lz_list[n_data-1], data[n_data-1][:, 1], color='k')
        plt.plot(data[n_data-1][:, 0]/Lz_list[n_data-1], data[n_data-1][:, 2], color='k', linestyle='--')


        plt.ylabel(r'$Nu$')
        plt.xlabel('$z/L_z$')
        plt.legend()

        fname = 'Nusselt'
        directory = 'plots/cmp/' + label[i] + '/'
        msf.save_tikz_png(fname, directory, iftikz)

        # reset data for 0025
        data = []




def run(iftikz=False):
    """Plot profiles of Nusselt number of 3d case"""

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    # Loop over specified timeranges
    for span in timeranges_select:
        # Call a postprocessing script
        plot_individual(span, iftikz)

