#!/usr/bin/env python3.5
#----------------------------------------------------------------------
# Compare my results to El Khoury (2013) data
#----------------------------------------------------------------------
# Author:   Steffen Straub
# Date:     2018/10/11
#----------------------------------------------------------------------
import os
import matplotlib.pyplot as plt
import numpy as np
from matplotlib2tikz import save as tikz_save
from ..misc import my_save_fig as msf

import configparser



def valid(data_dirs, data_names, iftikz=False):
    plt.close('all')
    
    SS = np.loadtxt(data_dirs[0] + data_names[0])
    EK = np.loadtxt(data_dirs[1] + data_names[1], skiprows=24)
    
    every = 20
    ## Plot
    plt.figure()
    plt.semilogx(EK[::every, 1], EK[::every, 2], 'x', markerfacecolor='none')
    plt.semilogx(SS[:, 0], SS[:, 1])
    plt.xlabel(r'$y^+$')    
    plt.ylabel(r'$\langle U_z \rangle^+$')
    directory = 'plots/valid/'
    fname = 'valid_' + 'Uz'
    msf.save_tikz_png(fname, directory, iftikz)
    
    plt.figure()
    plt.semilogx(EK[::every, 1], EK[::every, 4]**2, 'x', markerfacecolor='none')
    plt.semilogx(SS[:, 0], SS[:, 2])
    plt.xlabel(r'$y^+$')    
    plt.ylabel(r'$\langle u_r u_r \rangle^+$')
    directory = 'plots/valid/'
    fname = 'valid_' + 'urur'
    msf.save_tikz_png(fname, directory, iftikz)

    plt.figure()
    plt.semilogx(EK[::every, 1], EK[::every, 5]**2, 'x', markerfacecolor='none')
    plt.semilogx(SS[:, 0], SS[:, 3])
    plt.xlabel(r'$y^+$')    
    plt.ylabel(r'$\langle u_\varphi u_\varphi \rangle^+$')
    directory = 'plots/valid/'
    fname = 'valid_' + 'utut'
    msf.save_tikz_png(fname, directory, iftikz)

    plt.figure()
    plt.semilogx(EK[::every, 1], EK[::every, 6]**2, 'x', markerfacecolor='none')
    plt.semilogx(SS[:, 0], SS[:, 4])
    plt.xlabel(r'$y^+$')    
    plt.ylabel(r'$\langle u_z u_z \rangle^+$')
    directory = 'plots/valid/'
    fname = 'valid_' + 'uzuz'
    msf.save_tikz_png(fname, directory, iftikz)

    plt.figure()
    plt.semilogx(EK[::every, 1], EK[::every, 7], 'x', markerfacecolor='none')
    plt.semilogx(SS[:, 0], SS[:, 5])
    plt.xlabel(r'$y^+$')    
    plt.ylabel(r'$\langle u_r u_z \rangle^+$')
    directory = 'plots/valid/'
    fname = 'valid_' + 'uruz'
    msf.save_tikz_png(fname, directory, iftikz)





def run_valid(iftikz=False):
    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    span = timeranges_select[0]
    
    #
    ## Load
    data_dirs = [
            'profiles/' + span + '/',
            '/net/istmlupus/localhome/hi202/local/data/pipe/2013_El_Khoury/',
            ]
    
    data_names = [
            'v_stats.dat',
            '1000_Re_1.dat',
            ]

    valid(data_dirs, data_names, iftikz)
