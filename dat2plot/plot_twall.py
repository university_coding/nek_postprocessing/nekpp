#/usr/bin/env python3.5
#----------------------------------------------------------------------
# Plot profiles of data in .dat files
#----------------------------------------------------------------------
# Date:     2018/03/01
# Author:   Steffen Straub
#----------------------------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt
from matplotlib2tikz import save as tikz_save
from ..misc import my_save_fig as msf
import os

import configparser

import pdb


def plot_individual(timerange_select, iftikz):

    label = (
            'halfconst071',
            'halfsin071',
            'halfconst0025',
            'halfsin0025',
            )
    
    data_name = 'Theta_wall.dat'

    for k in range(len(label)):

        plt.close('all')
    
        # Load data
        #----------
        data_dir = 'profiles/' + timerange_select + '/' + label[k] + '/'

        print("Processing " + label[k] + ' at time ' + timerange_select)           
        data = np.loadtxt(data_dir + data_name)

        # Plot
        #-----

        # Theta_wall
        plt.figure()
        plt.plot(data[:, 0], data[:, 2])
        plt.xlabel(r'$\varphi$')
        plt.ylabel(r'$\langle \Theta_w \rangle (\varphi)$')
    
        fname = 'Theta_wall'
        directory = 'plots/' + timerange_select + '/' + label[k] + '/'
        msf.save_tikz_png(fname, directory, iftikz)


        # local Nusselt
        plt.figure()
        plt.plot(data[:, 0], data[:, 4])
        plt.xlabel(r'$\varphi$')
        plt.ylabel(r'$\langle Nu \rangle (\varphi)$')
    
        fname = 'Nu_local'
        directory = 'plots/' + timerange_select + '/' + label[k] + '/'
        msf.save_tikz_png(fname, directory, iftikz)






def plot_comparison(dat_path, given_labels, iftikz=False):

    plt.close('all')

    print("Processing " + ','.join(dat_path))
    print('Compare ' + ' '.join(given_labels))
    all_labels = '_'.join(given_labels)
    # Load data
    #----------
    data_name = (
            'Theta_wall.dat'
            )

    # Save all datasets in one list "data"
    n_data = len(dat_path)
    data = []
    for i in range(n_data):
        pts_file = dat_path[i] + '../../../interpmesh/number_of_points.txt'
        nphi = int(np.loadtxt(pts_file)[1])
        nphi_sym = int((nphi+1)/2)
        ncols = 5

        this_data = np.zeros((nphi_sym, ncols))
        data_dir = dat_path[i]
        this_data[:, :] = np.loadtxt(data_dir + data_name)

        data.append(this_data)



    # which labels?
    if (given_labels):
        labels = given_labels
    else:
        labels=data_path

    # Get Prandtl from dat_path
    if ('071' in dat_path[0]):
        Pr = '071'
    elif ('0025' in dat_path[0]):
        Pr = '0025'
    else:
        Pr = 'xxx'


    

    # Plot
    #-----
    # Theta_wall
    plt.figure()
    for i in range(n_data):
        plt.plot(data[i][:, 0], data[i][:, 3], label=labels[i])
    plt.xlabel(r'$\varphi$')
    plt.ylabel(r'$\langle \Theta_w ^+ \rangle^{z, t} (\varphi)$')
    plt.xlim(-np.pi/2, np.pi/2)
    plt.legend()

    plt.grid(which='both')

    fname = 'Theta_wall' + Pr + '_' + all_labels
    directory = 'plots/compare/Nuss_azimuth/'
    msf.save_tikz_png(fname, directory, iftikz)

 
    # Nusselt
    plt.figure()
    for i in range(n_data):
        plt.plot(data[i][:, 0], data[i][:, 4],
                color='C{0:d}'.format(i), label=labels[i])

    plt.xlabel(r'$\varphi$')
    plt.ylabel(r'$Nu (\varphi)$')
    plt.xlim(0, np.pi/2)
    if (Pr=='071'):
        plt.ylim(ymax=150)
    elif (Pr=='0025'):
        plt.ylim(ymax=20)
    plt.ylim(ymin=0)
    plt.legend()

    plt.grid(which='both')


    fname = 'Nuss_phi' + Pr + '_' + all_labels
    directory = 'plots/compare/Nuss_azimuth/'
    msf.save_tikz_png(fname, directory, iftikz)

    directory = 'plots/compare/Nuss_azimuth/'
    if not os.path.exists(directory):
        os.makedirs(directory)



def plot_comparison_paper(dat_path, given_labels, iftikz=False):

    plt.close('all')

    print("Processing " + ','.join(dat_path))
    print('Compare ' + ' '.join(given_labels))
    all_labels = '_'.join(given_labels)
    # Load data
    #----------
    data_name = (
            'Theta_wall.dat'
            )

    az_names = (
            'halfsin071',
            'halfconst071',
            'halfsin0025',
            'halfconst0025',
            )

    # Save all datasets in one list "data"
    n_data = len(dat_path) * len(az_names)
    data = []
    for i in range(len(dat_path)):
        pts_file = dat_path[i] + '../../interpmesh/number_of_points.txt'
        nphi = int(np.loadtxt(pts_file)[1])
        nphi_sym = int((nphi+1)/2)
        ncols = 5

        for j in range(len(az_names)):
            this_data = np.zeros((nphi_sym, ncols))
            data_dir = dat_path[i] + '/' + az_names[j] + '/'
            this_data[:, :] = np.loadtxt(data_dir + data_name)
    
            data.append(this_data)



    # which labels?
    if (given_labels):
        labels = given_labels
    else:
        labels=data_path


    
    Pr = (
            '071',
            '0025',
            )


    # Plot
    #-----

    for p in range(2):
        # Theta_wall
        plt.figure()
        k = 0
        for i in range(0+2*p, n_data, 4):
            plt.plot(data[i][:, 0]*180/np.pi, data[i][:, 3], 
                    color='C{0:d}'.format(k),
                    linestyle='-', label=labels[k])
            k += 1
        k = 0
        for i in range(1+2*p, n_data, 4):
            plt.plot(data[i][:, 0]*180/np.pi, data[i][:, 3], 
                    color='C{0:d}'.format(k),
                    linestyle='--')
            k += 1

        plt.xlabel(r'$\varphi$')
        plt.ylabel(r'$\langle \Theta_w^+\rangle^{z, t} (\varphi)$')
        plt.xlim(-90, 90)
        plt.legend()
    
        plt.grid(which='both')
    
        fname = 'Theta_wall' + Pr[p] + '_' + all_labels
        directory = 'plots/compare_paper/Nuss_azimuth/'
        msf.save_tikz_png(fname, directory, iftikz)

 
        # Nusselt
        plt.figure()
        k = 0
        for i in range(0+2*p, n_data, 4):
            plt.plot(data[i][:, 0]*180/np.pi, data[i][:, 4], 
                    color='C{0:d}'.format(k),
                    linestyle='-', label=labels[k])
            k += 1
        k = 0
        for i in range(1+2*p, n_data, 4):
            plt.plot(data[i][:, 0]*180/np.pi, data[i][:, 4], 
                    color='C{0:d}'.format(k),
                    linestyle='--')
            k +=1
    
        plt.xlabel(r'$\varphi [^\circ]$')
        plt.ylabel(r'$Nu (\varphi)$')
        plt.xlim(0, 90)
        if (Pr[p]=='071'):
            plt.ylim(ymax=150)
        elif (Pr[p]=='0025'):
            plt.ylim(ymax=20)
        plt.ylim(ymin=0)
        plt.legend()
    
#        plt.grid(which='both')
    
    
        fname = 'Nuss_phi' + Pr[p] + '_' + all_labels
        directory = 'plots/compare_paper/Nuss_azimuth/'
        msf.save_tikz_png(fname, directory, iftikz)
    
        directory = 'plots/compare/Nuss_azimuth/'
        if not os.path.exists(directory):
            os.makedirs(directory)





def run(iftikz=False):
    """Plot local Nusselt number for azimuthally inhomogeneous TBC. 
    
    iftikz      Save as tikz
    
    """

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    # Loop over specified timeranges
    for span in timeranges_select:
        # Call a postprocessing script
        plot_individual(span, iftikz)

