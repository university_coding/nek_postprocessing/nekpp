#/usr/bin/env python3.5
#----------------------------------------------------------------------
# Plot profiles of data in .dat files
#----------------------------------------------------------------------
# Date:     2018/03/01
# Author:   Steffen Straub
#----------------------------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt
from matplotlib2tikz import save as tikz_save
from ..misc import my_save_fig as msf

import os


import configparser

import pdb



def plot_individual(timerange_select):

    plt.close('all')

    print("Processing " + timerange_select)
    # Load data
    #----------
    data_dir = 'profiles/' + timerange_select + '/'
    data_name = (
            't_balance_IT071.dat',
            't_balance_MBC071.dat',
            't_balance_IF071.dat',
            't_balance_IT0025.dat',
            't_balance_MBC0025.dat',
            't_balance_IF0025.dat',
            )


    # Plot
    #-----
    directory = 'plots/' + timerange_select + '/'
    if not os.path.exists(directory):
        os.makedirs(directory)

    for i in range(len(data_name)):
        # Read in data one by one because of different number of columns for IT
        data = np.loadtxt(data_dir + data_name[i])

        # Theta
        # All contributions to the budget
        plt.figure()
        plt.plot(data[:, 0], data[:, 1], label='rad. h.f.')
        plt.plot(data[:, 0], data[:, 2], label='mean adv. h.f.')
        if ("IT" in data_name[i]):
            plt.plot(data[:, 0], data[:, 3], label='turb. ax. adv. h.f.')
            plt.plot(data[:, 0], data[:, 4], label='mean streamw. th. diff.')
        plt.plot(data[:, 0], np.sum(data[:, 1:], axis=1), label='sum')
        plt.xlabel(r'$r/D$')
        plt.ylabel(r'$\langle \Theta \rangle (r)$')
        plt.legend()

        plt.savefig(directory + 't_balance_' + data_name[i][10:-4] + '.png')


def plot_paper(timerange_select, iftikz):

    plt.close('all')

    print("Processing " + timerange_select)
    # Load data
    #----------
    data_dir = 'profiles/' + timerange_select + '/'
    data_name = (
            't_balance_IT071.dat',
            't_balance_MBC071.dat',
            't_balance_IF071.dat',
            't_balance_IT0025.dat',
            't_balance_MBC0025.dat',
            't_balance_IF0025.dat',
            )


    # Plot
    #-----
    directory = 'plots/' + timerange_select + '/'
    if not os.path.exists(directory):
        os.makedirs(directory)

    

    def twoplot(i1, i2, fname):
        plt.figure()
        data = np.loadtxt(data_dir + data_name[i1])
        plt.plot(data[:, 0], data[:, 1], color='C4', label='rad.')
        plt.plot(data[:, 0], data[:, 2], color='C5', label='mean adv.')
        plt.plot(data[:, 0], data[:, 3], color='C6', label='turb. adv.')
        plt.plot(data[:, 0], data[:, 4], color='C8', label='therm. diff.')
        plt.plot(data[:, 0], np.sum(data[:, 1:], axis=1), 'k', label='sum')
        data = np.loadtxt(data_dir + data_name[i2])   
        plt.plot(data[:, 0], data[:, 1], ':', color='C4')
        plt.plot(data[:, 0], data[:, 2], ':', color='C5')
        plt.plot(data[:, 0], np.sum(data[:, 1:], axis=1), ':k')
        plt.xlabel(r'$y/D$')
        plt.ylabel(r'$\langle \Theta \rangle$')
        plt.xlim(left=0)
#        plt.legend()
    
        msf.save_tikz_png(fname, directory, iftikz)

    twoplot(0, 1, 't_balance_ITMBC071')
    twoplot(3, 4, 't_balance_ITMBC0025')







def run():
    """Plot balance for mean temperature"""

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    # Loop over specified timeranges
    for span in timeranges_select:
        # Call a postprocessing script
        plot_individual(span)


def run_paper(iftikz=False):
    """Plot balance for mean temperature in a paper format.
    
    iftikz      Save picture also in tex format
    """

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    # Loop over specified timeranges
    for span in timeranges_select:
        # Call a postprocessing script
        plot_paper(span, iftikz)


