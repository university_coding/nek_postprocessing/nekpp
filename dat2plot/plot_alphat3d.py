#/usr/bin/env python3.5
#----------------------------------------------------------------------
# Plot profiles of data in .dat files
#----------------------------------------------------------------------
# Date:     2018/03/01
# Author:   Steffen Straub
#----------------------------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt
from ..misc import my_save_fig as msf
import os
import sys

import configparser

import pdb

def plot_individual(span, iftikz):
    plt.ioff()
    plt.close('all')

    print('Note: label_list is hardcoded for now.')
    label = (
        '071',
        '0025',
        )

    n_axpos = 10
    n_probes = 5

    # Load data
    data_dir_nut = 'profiles/' + span + '/' 
    data_name_nut = 'nut.dat'
    nu_t = np.loadtxt(data_dir_nut + data_name_nut)[:, 1]

    data_dir = 'profiles/' + span + '/' + label[0] + '/z_1/probe_0/'
    data_name = 'turb_diff.dat'
    # Just to get the shape
    data0 = np.loadtxt(data_dir + data_name)
    rows, cols = np.shape(data0)


    data = np.zeros((rows, cols, n_axpos-1, n_probes))

    for i in range(len(label)):

        for iz in range(n_axpos-1):
            for k in range(n_probes):
                data_dir = 'profiles/' + span + '/' + label[i] + '/z_{0:d}/probe_{1:d}/'.format(iz+1, k)
                data_name = 'turb_diff.dat'
                # Just to get the shape
                data_izk = np.loadtxt(data_dir + data_name)
                rows, cols = np.shape(data0)
            
                data[:, :, iz, k] = data_izk


        y_plus = data[:, 0, 0, 0]

        for iz in range(0, n_axpos-1, 2):
            for k in range(n_probes):
                fig = plt.figure()
                plt.plot(y_plus[1:-1], nu_t[1:-1]/data[1:-1, 1, iz, k], label=r'$Pr_{t,r}$')
                plt.plot(y_plus[1:-1], nu_t[1:-1]/data[1:-1, 2, iz, k], '--', label=r'$Pr_{t,\varphi}$')
                plt.plot(y_plus[1:-1], nu_t[1:-1]/data[1:-1, 3, iz, k], ':', label=r'$Pr_{t,z}$')

                plt.ylabel(r'$Pr_t$')
                plt.xlabel(r'$y^+$')
                plt.legend()
        
                directory = 'plots/' + span + '/' + label[i] + '/z_{0:d}/probe_{1:d}/'.format(iz, k)
                fname = 'Prt'
                msf.save_tikz_png(fname, directory, iftikz)
    
                plt.close('all')






def run(iftikz=False):
    """Plot profiles of turbulent Prandtl number in 3d case"""

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    # Loop over specified timeranges
    for span in timeranges_select:
        # Call a postprocessing script
        plot_individual(span, iftikz)


