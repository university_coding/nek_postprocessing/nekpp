#/usr/bin/env python3.5
#----------------------------------------------------------------------
# Plot profiles of data in .dat files
#----------------------------------------------------------------------
# Date:     2018/03/01
# Author:   Steffen Straub
#----------------------------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt
from ..misc import my_save_fig as msf
import os
import sys

import configparser

import pdb

def plot_individual(span, iftikz):
    plt.ioff()
    plt.close('all')

    print('Note: label_list is hardcoded for now.')
    label = (
        '071',
        '0025',
        )

    n_axpos = 10
    n_probes = 5

    # Load data
    data_dir = 'profiles/' + span + '/' + label[0] + '/z_1/probe_0/'
    data_name = 'tbudget.dat'
    # Just to get the shape
    data0 = np.loadtxt(data_dir + data_name)
    rows, cols = np.shape(data0)


    data = np.zeros((rows, cols, n_axpos-1, n_probes))

    for i in range(len(label)):

        for iz in range(n_axpos-1):
            for k in range(n_probes):
                data_dir = 'profiles/' + span + '/' + label[i] + '/z_{0:d}/probe_{1:d}/'.format(iz+1, k)
                data_name = 'tbudget.dat'
                # Just to get the shape
                data_izk = np.loadtxt(data_dir + data_name)
                rows, cols = np.shape(data0)
            
                data[:, :, iz, k] = data_izk


        y_plus = data[:, 0, 0, 0]
        P = data[:, 1, :, :]
        eps = data[:, 2, :, :]
        MD = data[:, 3, :, :]
        TD = data[:, 4, :, :]

        for iz in range(0, n_axpos-1, 2):
            for k in range(n_probes):
                fig = plt.figure()
                plt.plot(y_plus, data[:, 1, iz, k], label=r'$P^+$')
                plt.plot(y_plus, data[:, 2, iz, k], label=r'$\epsilon^+$')
                plt.plot(y_plus, data[:, 3, iz, k], label=r'$MD^+$')
                plt.plot(y_plus, data[:, 4, iz, k], label=r'$TD^+$')
#                plt.plot(y_plus, np.sum(data[:, 1:, iz, k], axis=1), 'k--', label='resid')

                plt.ylabel('Loss / Gain')
                plt.xlabel(r'$y^+$')
                plt.legend()
        
                directory = 'plots/' + span + '/' + label[i] + '/z_{0:d}/probe_{1:d}/'.format(iz, k)
                fname = 'tbudget'
                msf.save_tikz_png(fname, directory, iftikz)
    
                plt.close('all')






def run(iftikz=False):
    """Plot profiles of budget of temperature variance in 3d case"""

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    # Loop over specified timeranges
    for span in timeranges_select:
        # Call a postprocessing script
        plot_individual(span, iftikz)


