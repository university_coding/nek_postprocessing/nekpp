#!/usr/bin/env python3.5
#-----------------------
# Compare 
#----------------------------------------------------------------------
# Author:   Steffen Straub
# Date:     2019/03/01
#----------------------------------------------------------------------

from NekPP.dat2plot import plot_nuss_decomposition


    
Re_list = ('5300', '11700', '19000', '37700')
# Cases
data_path5300 = 'nusselt_180/9182-17182/'
data_path11700 = 'nusselt_360/563-2163/'
data_path19000 = 'nusselt_550/303-1178/'
data_path37700 = 'nusselt_1000/194-256/'
case_paths = (data_path5300, data_path11700,
        data_path19000, data_path37700)

data_path = []
# Loop over all cases:
for case in case_paths:

    # Loop over all MBC / IF fields
    contrib_path = []
    for tfield in (2, 3, 5, 6):
        contrib_path.append(case + 'nuss_contrib{0:d}.dat'.format(tfield))
    data_path.append(contrib_path)

plot_nuss_decomposition.plot_comparison(data_path, Re_list, False)

