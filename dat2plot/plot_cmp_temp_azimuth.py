#!/usr/bin/env python3.5
#-----------------------
# Compare DNS and LES-RT results
#----------------------------------------------------------------------
# Author:   Steffen Straub
# Date:     2018/03/15
#----------------------------------------------------------------------

from NekPP.dat2plot import plot_temp

# Location of profiles to compare
data_path = (
        'L12-5_profiles/7182-10182/',
        'profiles/200-1200/',
        )

# Corresponding labels
labels = (
        'L12-5',
        'L50',
        )

plot_temp.plot_azimuth_comparison(data_path, labels, iftikz=False)


