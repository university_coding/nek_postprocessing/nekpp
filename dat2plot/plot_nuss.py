#/usr/bin/env python3.5
#----------------------------------------------------------------------
# Plot profiles of data in .dat files
#----------------------------------------------------------------------
# Date:     2018/03/01
# Author:   Steffen Straub
#----------------------------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt
from matplotlib2tikz import save as tikz_save
from ..misc import correlations
import os

import configparser

import pdb

def plot_comparison(data_path, Re_list, iftikz):
    """ Compare Nu against Re for different thermal boundary conditions. 
    data_path               list of location of dataset files to compare
    Re_list                 list of corresponding Re numbers
    iftikz                  save plots also in tex format?


    """


    plt.close('all')

    # Load data
    #----------
    fname = 'nuss.dat'
    data_name = [data_path[i] + '/' + fname for i in range(len(data_path))]

    data = np.zeros((6, len(data_name)))
    for i in range(len(data_name)):
        data[:, i] = np.loadtxt(data_name[i])

    label_list= (
            'IT',
            'MBC',
            'IF',
            )

    # Calculate correlation results
    D = 1
    Lz = 999999                 # fully-developed flow

    Re_arr = np.linspace(5000, 40000, 100) 
    Pr = np.array([0.71, 0.025])
    Pe0025 = Re_arr * Pr[1]

    Nu_gniel = correlations.gnielinski(Re_arr, Pr[0], D, Lz)
    Nu_lub = correlations.lubarsky(Pe0025)
    Nu_skup = correlations.skupinski(Pe0025)
    Nu_tric = correlations.tricoli(Pe0025)
    Nu_pacio = correlations.it_best_fit(Pe0025)
    
    


    # Plot
    #-----
    marker_list = ('o', '+', 's', '*')
    plt.figure(1)
    for i in range(3):
        plt.plot(Re_list, data[i, :],
                linestyle='none',
                marker=marker_list[i], markerfacecolor='none',
                label=label_list[i])
    plt.plot(Re_arr, Nu_gniel, 'k--', label='Gnielinski') 

    plt.xlabel(r'$Re_b$')
    plt.ylabel(r'$Nu$')
    plt.legend()

    fname = 'Nu071'
    directory = 'plots/' + 'compare' + '/'
    if not os.path.exists(directory):
        os.makedirs(directory)
    if (iftikz):
        tikz_save(directory + fname + '.tex',
                figureheight='\\figureheight',
                figurewidth='\\figurewidth',
                show_info=False,
                extra_axis_parameters={'scaled x ticks = false',
                    'scaled y ticks = false'}
                )
    plt.savefig(directory + fname + '.png')


    plt.figure(2)
    for i in range(3, 6):
        plt.plot(Re_list, data[i, :],
                linestyle='none',
                marker=marker_list[i-3], markerfacecolor='none',
                label=label_list[i-3])
    plt.plot(Re_arr, Nu_lub, 'k--', label='Lubarsky')
    plt.plot(Re_arr, Nu_skup, 'k:', label='Skupinski')
    plt.plot(Re_arr, Nu_pacio, 'k', label='Pacio')
    # Use Tricoli's relation on actual data of IF to estimate IT
    plt.plot(Re_list, data[5, :] * np.pi**2/12, 'xk', label='Tricoli')
    plt.xlabel(r'$Re_b$')
    plt.ylabel(r'$Nu$')
    plt.legend(loc='lower right')

    fname = 'Nu0025'
    directory = 'plots/' + 'compare' + '/'
    if not os.path.exists(directory):
        os.makedirs(directory)
    if (iftikz):
        tikz_save(directory + fname + '.tex',
                figureheight='\\figureheight',
                figurewidth='\\figurewidth',
                show_info=False,
                extra_axis_parameters={'scaled x ticks = false',
                    'scaled y ticks = false',
                    } 
                )

    plt.savefig(directory + fname + '.png')

    

def plot_comparison_azimuth(data_path, Re_list, iftikz):
    """ Compare Nu against Re for different thermal boundary conditions. 
    data_path               list of location of dataset files to compare
    Re_list                 list of corresponding Re numbers
    iftikz                  save plots also in tex format?


    """


    plt.close('all')

    # Load data
    #----------
    fname = 'nuss.dat'
    fname_az = 'nuss_azimuth.dat'
    data_name = [data_path[i] + '/' + fname for i in range(len(data_path))]
    data_name_az = [data_path[i] + '/' + fname_az for i in range(len(data_path))]

    nuss_hom = np.zeros((6, len(data_name)))
    for i in range(len(data_name)):
        data = np.loadtxt(data_name[i])
        nuss_hom[:, i] = data

    nuss_az = np.zeros((4, len(data_name_az)))
    for i in range(len(data_name_az)):
        data = np.loadtxt(data_name_az[i])
        nuss_az[:, i] = data[:, 0]

    nuss = np.concatenate((nuss_hom, nuss_az))


    label_list= (
            'IT',
            'MBC',
            'homog',
            'halfconst',
            'halfsin',
            )

    # Calculate correlation results
    D = 1
    Lz = 999999                 # fully-developed flow

    Re_arr = np.linspace(5000, 40000, 100) 
    Pr = np.array([0.71, 0.025])
    Pe0025 = Re_arr * Pr[1]

    Nu_gniel = correlations.gnielinski(Re_arr, Pr[0], D, Lz)
    Nu_lub = correlations.lubarsky(Pe0025)
    Nu_skup = correlations.skupinski(Pe0025)
    
    


    # Plot
    #-----
    marker_list = ('o', '+', 's', '*', '^', 'p')
    plt.figure(1)
    for i in range(2, 3):
        plt.plot(Re_list, nuss[i, :],
                linestyle='none',
                marker=marker_list[i], markerfacecolor='none',
                label=label_list[i])
    plt.plot(Re_list, nuss[6, :],
            linestyle='none',
            marker=marker_list[3], markerfacecolor='none',
            label=label_list[3])
    plt.plot(Re_list, nuss[7, :],
            linestyle='none',
            marker=marker_list[4], markerfacecolor='none',
            label=label_list[4])



    plt.plot(Re_arr, Nu_gniel, 'k--', label='Gnielinski') 

    plt.xlabel(r'$Re_b$')
    plt.ylabel(r'$Nu^G$')
    plt.legend()

    fname = 'Nu071_azimuth'
    directory = 'plots/' + 'compare' + '/'
    if not os.path.exists(directory):
        os.makedirs(directory)
    if (iftikz):
        tikz_save(directory + fname + '.tex',
                figureheight='\\figureheight',
                figurewidth='\\figurewidth',
                show_info=False,
                extra_axis_parameters={'scaled x ticks = false',
                    'scaled y ticks = false'}
                )
    plt.savefig(directory + fname + '.png')


    plt.figure(2)
    for i in range(5, 6):
        plt.plot(Re_list, nuss[i, :],
                linestyle='none',
                marker=marker_list[i-3], markerfacecolor='none',
                label=label_list[i-3])
    plt.plot(Re_list, nuss[8, :],
            linestyle='none',
            marker=marker_list[3], markerfacecolor='none',
            label=label_list[3])
    plt.plot(Re_list, nuss[9, :],
            linestyle='none',
            marker=marker_list[4], markerfacecolor='none',
            label=label_list[4])

    plt.plot(Re_arr, Nu_lub, 'k--', label='Lubarsky')
    plt.plot(Re_arr, Nu_skup, 'k:', label='Skupinski')
    plt.xlabel(r'$Re_b$')
    plt.ylabel(r'$Nu^G$')
    plt.legend(loc='lower right')

    fname = 'Nu0025_azimuth'
    directory = 'plots/' + 'compare' + '/'
    if not os.path.exists(directory):
        os.makedirs(directory)
    if (iftikz):
        tikz_save(directory + fname + '.tex',
                figureheight='\\figureheight',
                figurewidth='\\figurewidth',
                show_info=False,
                extra_axis_parameters={'scaled x ticks = false',
                    'scaled y ticks = false',
                    } 
                )

    plt.savefig(directory + fname + '.png')

 



