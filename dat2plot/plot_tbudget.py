#/usr/bin/env python3.5
#----------------------------------------------------------------------
# Plot profiles of data in .dat files
#----------------------------------------------------------------------
# Date:     2018/03/02
# Author:   Steffen Straub
#----------------------------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt
from matplotlib2tikz import save as tikz_save
from ..misc import my_save_fig as msf
import os

import configparser

import pdb


def plot_individual(timerange_select):

    plt.close('all')

    print("Processing " + timerange_select)
    # Load data
    #----------
    data_dir = 'profiles/' + timerange_select + '/'
    data_name = (
            '/temp_budget_IT071.dat',
            '/temp_budget_MBC071.dat',
            '/temp_budget_IF071.dat',
            '/temp_budget_IT0025.dat',       
            '/temp_budget_MBC0025.dat',
            '/temp_budget_IF0025.dat',
            )

    data = []
    for name in data_name:
        data.append(np.loadtxt(data_dir + name))
    data = np.asarray(data)



    # Plot
    #-----
    for i in range(len(data)):

        # k
        plt.figure()
        plt.plot(data[i, :, 0], data[i, :, 1], label=r'$P^+$')
        plt.plot(data[i, :, 0], data[i, :, 2], label=r'$\tilde{\epsilon}^+$')
        plt.plot(data[i, :, 0], data[i, :, 3], label=r'$MD^+$')
        plt.plot(data[i, :, 0], data[i, :, 4], label=r'$TD^+$')
        plt.plot(data[i, :, 0], data[i, :, 5], label=r'$S^+$')
        plt.plot(data[i, :, 0], np.sum(data[i, :, 1:], axis=1), 'k--', label=r'resid')

        plt.xlabel(r'$y^+$')
        plt.legend()
    
        directory = 'plots/' + timerange_select + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + data_name[i][:-4] + '.png')




def plot_timeseries(timeranges):

    plt.close('all')

    print("Processing " + ','.join(timeranges))
    # Load data
    #----------
    data_name = (
            '/temp_budget_IT071.dat',
            '/temp_budget_MBC071.dat',
            '/temp_budget_IF071.dat',
            '/temp_budget_IT0025.dat',       
            '/temp_budget_MBC0025.dat',
            '/temp_budget_IF0025.dat',
            )

    nr = int(np.loadtxt('interpmesh/number_of_points.txt')[0])
    ncols = 6
    n_names = len(data_name)
    n_timeranges = len(timeranges)

    data = np.zeros((nr, ncols, n_names, n_timeranges))
    for i in range(n_timeranges):
        for j in range(n_names):
            data_dir = 'profiles/' + timeranges[i] + '/'
            data[:, :, j, i] = np.loadtxt(data_dir + data_name[j])


    # Plot
    #-----

    # Production
    for j in range(n_names):
        plt.figure()
        for i in range(n_timeranges):
            plt.plot(data[:, 0, j, i], data[:, 1, j, i], label=timeranges[i])
            plt.xlabel(r'$y^+$')
            plt.ylabel(r'$P^+$')
            plt.legend()

        directory = 'plots/' + timeranges[-1] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + data_name[j][:-4] + '_P_timerange.png')
        
    plt.close('all')
    # Dissipation
    for j in range(n_names):
        plt.figure()
        for i in range(n_timeranges):
            plt.plot(data[:, 0, j, i], data[:, 2, j, i], label=timeranges[i])
            plt.xlabel(r'$y^+$')
            plt.ylabel(r'$\tilde{\epsilon}^+$')
            plt.legend()

        directory = 'plots/' + timeranges[-1] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + data_name[j][:-4] + '_eps_timerange.png')

    plt.close('all')
    # Molecular diffusion
    for j in range(n_names):
        plt.figure()
        for i in range(n_timeranges):
            plt.plot(data[:, 0, j, i], data[:, 3, j, i], label=timeranges[i])
            plt.xlabel(r'$y^+$')
            plt.ylabel(r'$MD^+$')
            plt.legend()

        directory = 'plots/' + timeranges[-1] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + data_name[j][:-4] + '_MD_timerange.png')

    plt.close('all')
    # Turbulent diffusion
    for j in range(n_names):
        plt.figure()
        for i in range(n_timeranges):
            plt.plot(data[:, 0, j, i], data[:, 4, j, i], label=timeranges[i])
            plt.xlabel(r'$y^+$')
            plt.ylabel(r'$TD^+$')
            plt.legend()

        directory = 'plots/' + timeranges[-1] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + data_name[j][:-4] + '_TD_timerange.png')

    plt.close('all')
    # Source term contribution
    for j in range(n_names):
        plt.figure()
        for i in range(n_timeranges):
            plt.plot(data[:, 0, j, i], data[:, 5, j, i], label=timeranges[i])
            plt.xlabel(r'$y^+$')
            plt.ylabel(r'$S^+$')
            plt.legend()

        directory = 'plots/' + timeranges[-1] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + data_name[j][:-4] + '_S_timerange.png')

    plt.close('all')
    # Residual
    for j in range(n_names):
        plt.figure()
        for i in range(n_timeranges):
            plt.plot(data[:, 0, j, i], np.sum(data[:, 1:, j, i], axis=1), label=timeranges[i])
            plt.xlabel(r'$y^+$')
            plt.ylabel(r'$resid$')
            plt.legend()

        directory = 'plots/' + timeranges[-1] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + data_name[j][:-4] + '_resid_timerange.png')



def plot_max_rms(timeranges):

    plt.close('all')

    print("Processing " + ','.join(timeranges))
    # Load data
    #----------
    data_name = (
            '/temp_budget_IT071.dat',
            '/temp_budget_MBC071.dat',
            '/temp_budget_IF071.dat',
            '/temp_budget_IT0025.dat',       
            '/temp_budget_MBC0025.dat',
            '/temp_budget_IF0025.dat',
            )

    labels = [data_name[i][13:-4] for i in range(len(data_name))]

    n_names = len(data_name)
    ncols = 2
    n_timeranges = len(timeranges)

    data = np.zeros((n_names, ncols, n_timeranges))

    for i in range(n_timeranges):
        data_dir = 'profiles/' + timeranges[i] + '/'
        data[..., i] = np.loadtxt(data_dir + 'resid_tbudget_max_rms.dat')

    # Extract time duration
    delta = []
    for k in range(n_timeranges):
        t1 = int(timeranges[k].split('-')[0])
        t2 = int(timeranges[k].split('-')[1])
        delta.append(t2-t1)


    # Plot
    #-----
    plt.figure()
    for k in range(n_names):
        plt.plot(delta, data[k, 0, :], label=labels[k])
        plt.xlabel('Average time')
        plt.ylabel('max(eps)')
        plt.legend()

        directory = 'plots/' + timeranges[-1] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + 'resid_tbudget_max.png')

    plt.figure()
    for k in range(n_names):
        plt.plot(delta, data[k, 1, :], label=labels[k])
#        plt.plot(delta, np.ones(n_timeranges)*1e-3, 'k--')
        plt.xlabel('Average time')
        plt.ylabel('RMS(eps)')
        plt.legend()

        directory = 'plots/' + timeranges[-1] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + 'resid_tbudget_rms.png')




def plot_comparison(dat_path, given_labels, iftikz=False):

    plt.close('all')

    print("Processing " + ','.join(dat_path))
    print('Compare ' + ' '.join(given_labels))
    all_labels = '_'.join(given_labels)
    # Load data
    #----------
    data_name = (
            '/temp_budget_IT071.dat',
            '/temp_budget_MBC071.dat',
            '/temp_budget_IF071.dat',
            '/temp_budget_IT0025.dat',       
            '/temp_budget_MBC0025.dat',
            '/temp_budget_IF0025.dat',
            )

    # Save all datasets in one list "data"
    n_data = len(dat_path)
    n_names = len(data_name)
    data = []
    for i in range(n_data):
        pts_file = dat_path[i] + '../../interpmesh/number_of_points.txt'
        nr = int(np.loadtxt(pts_file)[0])
        ncols = 6

        this_data = np.zeros((nr, ncols, n_names))
        for j in range(n_names):
            data_dir = dat_path[i]
            this_data[:, :, j] = np.loadtxt(data_dir + data_name[j])

        data.append(this_data)

    # which labels?
    if (given_labels):
        labels = given_labels
    else:
        labels=data_path


    # Plot
    #-----

    # Production
    for j in range(n_names):
        plt.figure()
        for i in range(n_data):
            plt.semilogx(data[i][:, 0, j], data[i][:, 1, j], label=labels[i])
            plt.xlabel(r'$y^+$')
            plt.ylabel(r'$P^+$')
            plt.legend()

        directory = 'plots/' + 'compare_' + all_labels + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + data_name[j][:-4] + '_P_' + all_labels + '.png')
        
    plt.close('all')
    # Dissipation
    for j in range(n_names):
        plt.figure()
        for i in range(n_data):
            plt.semilogx(data[i][:, 0, j], data[i][:, 2, j], label=labels[i])
            plt.xlabel(r'$y^+$')
            plt.ylabel(r'$\tilde{\epsilon}^+$')
            plt.legend()

        directory = 'plots/' + 'compare_' + all_labels + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + data_name[j][:-4] + '_eps_' + all_labels + '.png')

    plt.close('all')
    # Molecular diffusion
    for j in range(n_names):
        plt.figure()
        for i in range(n_data):
            plt.semilogx(data[i][:, 0, j], data[i][:, 3, j], label=labels[i])
            plt.xlabel(r'$y^+$')
            plt.ylabel(r'$MD^+$')
            plt.legend()

        directory = 'plots/' + 'compare_' + all_labels + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + data_name[j][:-4] + '_MD_' + all_labels + '.png')

    plt.close('all')
    # Turbulent diffusion
    for j in range(n_names):
        plt.figure()
        for i in range(n_data):
            plt.semilogx(data[i][:, 0, j], data[i][:, 4, j], label=labels[i])
            plt.xlabel(r'$y^+$')
            plt.ylabel(r'$TD^+$')
            plt.legend()

        directory = 'plots/' + 'compare_' + all_labels + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + data_name[j][:-4] + '_TD_' + all_labels + '.png')

    plt.close('all')
    # Source term contribution
    for j in range(n_names):
        plt.figure()
        for i in range(n_data):
            plt.semilogx(data[i][:, 0, j], data[i][:, 5, j], label=labels[i])
            plt.xlabel(r'$y^+$')
            plt.ylabel(r'$S^+$')
            plt.legend()

        directory = 'plots/' + 'compare_' + all_labels + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + data_name[j][:-4] + '_S_' + all_labels + '.png')

    plt.close('all')
    # Residual
    for j in range(n_names):
        plt.figure()
        for i in range(n_data):
            plt.semilogx(data[i][:, 0, j], np.sum(data[i][:, 1:, j], axis=1), label=labels[i])
            plt.xlabel(r'$y^+$')
            plt.ylabel(r'$resid$')
            plt.legend()

        directory = 'plots/' + 'compare_' + all_labels + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + data_name[j][:-4] + '_resid_' + all_labels + '.png')


    # All together
    my_linestyles = ('-', '--', ':', '-.')
    for j in range(n_names):
        plt.figure()
        for i in range(n_data):
            plt.semilogx(data[i][:, 0, j], data[i][:, 1, j], 
                    my_linestyles[i], color='C0', label='$P^+$')
            plt.semilogx(data[i][:, 0, j], data[i][:, 2, j], 
                    my_linestyles[i], color='C1', label=u'$\epsilon^+$')
            plt.semilogx(data[i][:, 0, j], data[i][:, 3, j], 
                    my_linestyles[i], color='C2', label=u'$MD^+$')
            plt.semilogx(data[i][:, 0, j], data[i][:, 4, j], 
                    my_linestyles[i], color='C3', label=u'$TD^+$')
            plt.semilogx(data[i][:, 0, j], data[i][:, 5, j], 
                    my_linestyles[i], color='C4', label=u'$S^+$')


            plt.xlabel(r'$y^+$')
#            plt.ylabel(r'$P^+$')
            plt.legend()

        directory = 'plots/' + 'compare_' + all_labels + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        if (iftikz):
            tikz_save(directory + data_name[j][:-4] + '_all_' + all_labels + '.tex',
                    figureheight='\\figureheight',
                    figurewidth='\\figurewidth',
                    show_info=False,
                    )

        plt.savefig(directory + data_name[j][:-4] + '_all_' + all_labels + '.png')
        
    plt.close('all')



def plot_azimuth_individual(timerange_select):

    label = (
            'halfconst071',
            'halfsin071',
            'halfconst0025',
            'halfsin0025',
            )
    
    probe = (
            '-90',
            '-45',
            '0',
            '45',
            '90',
            )

    data_name = (
            'probe_0_tbudget.dat',
            'probe_1_tbudget.dat',
            'probe_2_tbudget.dat',
            'probe_3_tbudget.dat',
            'probe_4_tbudget.dat',
            )

    for k in range(len(label)):

        plt.close('all')
    
        # Load data
        #----------
        data_dir = 'profiles/' + timerange_select + '/'

        print("Processing " + label[k] + ' at time ' + timerange_select)           
        data = []
        for name in data_name:
            data.append(np.loadtxt(data_dir + label[k] + '/' + name))
        data = np.asarray(data)

        # Plot
        #-----

        for i in range(len(data)):
            plt.figure()
            plt.plot(data[i, :, 0], data[i, :, 1], label=r'$P^+$')
            plt.plot(data[i, :, 0], data[i, :, 2], label=r'$\tilde{\epsilon}^+$')
            plt.plot(data[i, :, 0], data[i, :, 3], label=r'$MD^+$')
            plt.plot(data[i, :, 0], data[i, :, 4], label=r'$TD^+$')
            plt.plot(data[i, :, 0], data[i, :, 5], label=r'$S^+$')
            plt.plot(data[i, :, 0], np.sum(data[i, :, 1:], axis=1), 'k--', label=r'resid')

            plt.xlabel(r'$y^+$')
            plt.legend()


    
            directory = 'plots/' + timerange_select + '/' + label[k] + '/'
            if not os.path.exists(directory):
                os.makedirs(directory)
        
            plt.savefig(directory + 'tbudget_probe_{0:d}'.format(i) + '.png')


def plot_comparison_paper(dat_path, iftikz=False):

    plt.close('all')

    print("Processing " + ','.join(dat_path))
    # Load data
    #----------
    data_name = (
            '/temp_budget_IT071.dat',
            '/temp_budget_MBC071.dat',
            '/temp_budget_IF071.dat',
            '/temp_budget_IT0025.dat',       
            '/temp_budget_MBC0025.dat',
            '/temp_budget_IF0025.dat',
            )

    # Save all datasets in one list "data"
    n_data = len(dat_path)
    n_names = len(data_name)
    data = []
    for i in range(n_data):
        pts_file = dat_path[i] + '../../interpmesh/number_of_points.txt'
        nr = int(np.loadtxt(pts_file)[0])
        ncols = 6

        this_data = np.zeros((nr, ncols, n_names))
        for j in range(n_names):
            data_dir = dat_path[i]
            this_data[:, :, j] = np.loadtxt(data_dir + data_name[j])

        data.append(this_data)

    label_list = (
            'IT',
            'MBC',
            'IF',
            )

    linestyle_list = (
            '-',
            ':',
            '--',
            )

    # Plot
    #-----
    string_list = ('P', 'eps', 'MD', 'TD', 'S')
    string_latex_list = (r'$P^+$', r'$\epsilon^+$', r'$MD$', r'$TD$', r'$S$')
    # Loop over all terms
    for l in range(len(string_list)):   
        plt.figure()
        i = 0
        # Loop over TBCs
        for j in range(3):
            plt.semilogx(data[i][:, 0, j], data[i][:, 1+l, j], 
                    color='C{0:d}'.format(i), linestyle=linestyle_list[j],
                    label=label_list[j])
        # Loop over datasets    
        for i in range(1, len(dat_path)):
            # Loop over TBCs
            for j in range(3):
                plt.semilogx(data[i][:, 0, j], data[i][:, 1+l, j], 
                        color='C{0:d}'.format(i), linestyle=linestyle_list[j],
                        )
        plt.xlabel(r'$y^+$')
        plt.ylabel(string_latex_list[l])
#        plt.legend()
    
        fname = 'tbudget_' + string_list[l] + '_071'
        directory = 'plots/compare/'
        msf.save_tikz_png(fname, directory, iftikz)
    
        plt.close('all')
    
        plt.figure()
        i = 0
        for j in range(3, 6):
            plt.semilogx(data[i][:, 0, j], data[i][:, 1+l, j], 
                    color='C{0:d}'.format(i), linestyle=linestyle_list[j-3],
                    label=label_list[j-3])
        for i in range(1, len(dat_path)):
            for j in range(3, 6):
                plt.semilogx(data[i][:, 0, j], data[i][:, 1+l, j], 
                        color='C{0:d}'.format(i), linestyle=linestyle_list[j-3],
                        )
        plt.xlabel(r'$y^+$')
        plt.ylabel(string_latex_list[l])
#        plt.legend()
    
        fname = 'tbudget_' + string_list[l] + '_0025'
        directory = 'plots/compare/'
        msf.save_tikz_png(fname, directory, iftikz)
    
        plt.close('all')




def plot_azimuth_comparison(dat_path, given_labels, iftikz=False):

    print("Processing " + ','.join(dat_path))
    print('Compare ' + ' '.join(given_labels))
    all_labels = '_'.join(given_labels)


    # Load data:
    #-----------
    case_label = (
            'halfconst071',
            'halfsin071',
            'halfconst0025',
            'halfsin0025',
            )
    
    probe = (
            '-90',
            '-45',
            '0',
            '45',
            '90',
            )

    data_name = (
            'probe_0_tbudget.dat',
            'probe_1_tbudget.dat',
            'probe_2_tbudget.dat',
            'probe_3_tbudget.dat',
            'probe_4_tbudget.dat',
            )

    # Save all datasets in one list "data"
    n_data = len(dat_path)
    n_names = len(case_label)
    n_probes = len(probe)
    data = []
    for i in range(n_data):
        pts_file = dat_path[i] + '../../interpmesh/number_of_points.txt'
        nr = int(np.loadtxt(pts_file)[0])
        ncols = 6

        this_data = np.zeros((nr, ncols, n_names, n_probes))
        for j in range(n_names):
            for k in range(n_probes):
                data_dir = dat_path[i]
                data_file = case_label[j] + '/' + data_name[k]
                this_data[:, :, j, k] = np.loadtxt(data_dir + data_file)

        data.append(this_data)



    # which labels?
    if (given_labels):
        labels = given_labels
    else:
        labels=data_path

    # Plot
    #-----
    my_linestyles = ('-', '--', ':')


    stat_names = ['P', 'D', 'MD', 'TD', 'S']
    tex_names = ['P', r'\tilde{\epsilon}', 'MD', 'TD', 'S'] 
    # Loop over statistics contained in file
    for stat_num in range(len(stat_names)):

        # Loop over case names
        for j in range(n_names):
            plt.figure()
            # Loop over datasets which are compared
            for i in range(n_data):
                # Loop over probes
                for k in range(n_probes):
                    plt.semilogx(data[i][:, 0, j, k], data[i][:, stat_num+1, j, k], 
                            my_linestyles[i], color='C{0:d}'.format(k), label = labels[i] + ' ' + probe[k])
                    plt.xlabel(r'$y^+$')
                    plt.ylabel(r'$\langle ' + tex_names[stat_num] + r' \rangle^+$')
                    plt.legend()
            
            directory = 'plots/' + 'compare_' + all_labels + '/' + case_label[j] + '/'
            if not os.path.exists(directory):
                os.makedirs(directory)
     
            if (iftikz):
                tikz_save(directory + 'tbudget_' + stat_names[stat_num] + '_' + all_labels + '.tex',
                        figureheight='\\figureheight',
                        figurewidth='\\figurewidth',
                        show_info=False,
                        )   
    
            plt.savefig(directory + 'tbudget_' + stat_names[stat_num] + '_' + all_labels + '.png')
        
            plt.close('all')

    # Residual
    # Loop over case names
    for j in range(n_names):
        plt.figure()
        # Loop over datasets which are compared
        for i in range(n_data):
            # Loop over probes
            for k in range(n_probes):
                plt.semilogx(data[i][:, 0, j, k], np.sum(data[i][:, 1:, j, k], axis=1), 
                        my_linestyles[i], color='C{0:d}'.format(k), label = labels[i] + ' ' + probe[k])
                plt.xlabel(r'$y^+$')
                plt.ylabel(r'$\langle ' + 'Resid' + r' \rangle^+$')
                plt.legend()
        
        directory = 'plots/' + 'compare_' + all_labels + '/' + case_label[j] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
 
        if (iftikz):
            tikz_save(directory + 'tbudget_' + 'resid' + '_' + all_labels + '.tex',
                    figureheight='\\figureheight',
                    figurewidth='\\figurewidth',
                    show_info=False,
                    )   

        plt.savefig(directory + 'tbudget_' + 'resid' + '_' + all_labels + '.png')
    
        plt.close('all')





def run():
    """Plot temperature statitics: budgets of temp. variance."""

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    # Loop over specified timeranges
    for span in timeranges_select:
        # Call a postprocessing script
        plot_individual(span)

def run_azimuth():
    """Plot temperature statistics: budgets of temp. variance."""

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    # Loop over specified timeranges
    for span in timeranges_select:
        # Call a postprocessing script
        plot_azimuth_individual(span)

