#/usr/bin/env python3.5
#----------------------------------------------------------------------
# Plot profiles of data in .dat files
#----------------------------------------------------------------------
# Date:     2018/03/01
# Author:   Steffen Straub
#----------------------------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt
from matplotlib2tikz import save as tikz_save
import os

import configparser

import pdb


def plot_individual(timerange_select):

    plt.close('all')

    print("Processing " + timerange_select)
    # Load data
    #----------
    data_dir = 'profiles/' + timerange_select + '/'
    data_name = (
            'v_stats.dat'
            )

    data = np.loadtxt(data_dir + data_name)

    # Plot
    #-----
    plt.figure()
    plt.semilogx(data[:, 0], data[:, 1])
    plt.xlabel(r'$y^+$')
    plt.ylabel(r'$\langle U_z \rangle^+$')

    directory = 'plots/' + timerange_select + '/'
    if not os.path.exists(directory):
        os.makedirs(directory)

    plt.savefig(directory + 'Uz.png')

    plt.figure()
    plt.semilogx(data[:, 0], data[:, 2], label=r'$r$')
    plt.semilogx(data[:, 0], data[:, 3], label=r'$\varphi$')
    plt.semilogx(data[:, 0], data[:, 4], label=r'$z$')
    plt.semilogx(data[:, 0], data[:, 5], label=r'$rz$')
    plt.xlabel(r'$y^+$')
    plt.ylabel(r'$\langle u_i u_j \rangle^+$')
    plt.legend()

    directory = 'plots/' + timerange_select + '/'
    if not os.path.exists(directory):
        os.makedirs(directory)

    plt.savefig(directory + 'uiuj.png')




def plot_timeseries(timeranges, iftikz=False):

    plt.close('all')

    print("Processing " + ','.join(timeranges))
    # Load data
    #----------
    data_name = (
            'v_stats.dat'
            )

    nr = int(np.loadtxt('interpmesh/number_of_points.txt')[0])
    ncols = 6
    n_timeranges = len(timeranges)

    data = np.zeros((nr, ncols, n_timeranges))
    for i in range(n_timeranges):
        data_dir = 'profiles/' + timeranges[i] + '/'
        data[:, :, i] = np.loadtxt(data_dir + data_name)


    # Plot
    #-----
    # Uz
    plt.figure()
    for i in range(n_timeranges):
        plt.semilogx(data[:, 0, i], data[:, 1, i], label=timeranges[i])
        plt.xlabel(r'$y^+$')
        plt.ylabel(r'$\langle U_z \rangle^+$')

    plt.xlim(xmin=1)
    plt.ylim(ymin=0)
    plt.legend()

    directory = 'plots/' + timeranges[-1] + '/'
    if not os.path.exists(directory):
        os.makedirs(directory)

    if (iftikz): 
        tikz_save(directory + 'Uz_timerange.tex',
                figureheight='\\figureheight',
                figurewidth='\\figurewidth',
                show_info=False,
                )

    plt.savefig(directory + 'Uz_timerange.png')

    # urur
    plt.figure()
    for i in range(n_timeranges):
        plt.semilogx(data[:, 0, i], data[:, 2, i], label=timeranges[i])
        plt.xlabel(r'$y^+$')
        plt.ylabel(r'$\langle u_r u_r \rangle^+$')

    plt.xlim(xmin=0.1)
    plt.ylim(ymin=0)
    plt.legend()


    directory = 'plots/' + timeranges[-1] + '/'
    if not os.path.exists(directory):
        os.makedirs(directory)

    if (iftikz):
        tikz_save(directory + 'urur_timerange.tex',
                figureheight='\\figureheight',
                figurewidth='\\figurewidth',
                show_info=False,
                )

    plt.savefig(directory + 'urur_timerange.png')

    # utut
    plt.figure()
    for i in range(n_timeranges):
        plt.semilogx(data[:, 0, i], data[:, 3, i], label=timeranges[i])
        plt.xlabel(r'$y^+$')
        plt.ylabel(r'$\langle u_\varphi u_\varphi \rangle^+$')

    plt.xlim(xmin=0.1)
    plt.ylim(ymin=0)
    plt.legend()


    directory = 'plots/' + timeranges[-1] + '/'
    if not os.path.exists(directory):
        os.makedirs(directory)

    if (iftikz):
        tikz_save(directory + 'utut_timerange.tex',
                figureheight='\\figureheight',
                figurewidth='\\figurewidth',
                show_info=False,
                )

    plt.savefig(directory + 'utut_timerange.png')

    # uzuz
    plt.figure()
    for i in range(n_timeranges):
        plt.semilogx(data[:, 0, i], data[:, 4, i], label=timeranges[i])
        plt.xlabel(r'$y^+$')
        plt.ylabel(r'$\langle u_z u_z \rangle^+$')
        plt.legend()

    directory = 'plots/' + timeranges[-1] + '/'
    if not os.path.exists(directory):
        os.makedirs(directory)

    if (iftikz):
        tikz_save(directory + 'uzuz_timerange.tex',
                figureheight='\\figureheight',
                figurewidth='\\figurewidth',
                show_info=False,
                )

    plt.savefig(directory + 'uzuz_timerange.png')

    # uruz
    plt.figure()
    for i in range(n_timeranges):
        plt.semilogx(data[:, 0, i], data[:, 5, i], label=timeranges[i])
        plt.xlabel(r'$y^+$')
        plt.ylabel(r'$\langle u_r u_z \rangle^+$')
        plt.legend()

    directory = 'plots/' + timeranges[-1] + '/'
    if not os.path.exists(directory):
        os.makedirs(directory)

    if (iftikz):
        tikz_save(directory + 'uruz_timerange.tex',
                figureheight='\\figureheight',
                figurewidth='\\figurewidth',
                show_info=False,
                )

    plt.savefig(directory + 'uruz_timerange.png')



def plot_comparison(dat_path, given_labels="", iftikz=False):

    plt.close('all')

    print("Processing " + ','.join(dat_path))
    print('Compare ' + ' '.join(given_labels))
    all_labels = '_'.join(given_labels)
    # Load data
    #----------
    data_name = (
            'v_stats.dat'
            )

    # Save all datasets in one list "data"
    n_data = len(dat_path)
    data = []
    for i in range(n_data):
        pts_file = dat_path[i] + '../../interpmesh/number_of_points.txt'
        nr = int(np.loadtxt(pts_file)[0])
        ncols = 6

        this_data = np.zeros((nr, ncols))
        data_dir = dat_path[i]
        this_data = np.loadtxt(data_dir + data_name)
        data.append(this_data)


    # which labels?
    if (given_labels):
        labels = given_labels
    else:
        labels=data_path

#    pdb.set_trace()

    # Plot
    #-----
    # Uz
    plt.figure()
    for i in range(n_data):
        plt.semilogx(data[i][:, 0], data[i][:, 1], label=labels[i])
        plt.xlabel(r'$y^+$')
        plt.ylabel(r'$\langle U_z \rangle^+$')
        plt.legend()


    directory = 'plots/' + 'compare_' + all_labels + '/'
    if not os.path.exists(directory):
        os.makedirs(directory)

    if (iftikz):
        tikz_save(directory + 'Uz_' + all_labels + '.tex',
                figureheight='\\figureheight',
                figurewidth='\\figurewidth',
                show_info=False,
                )

    plt.savefig(directory + 'Uz_' + all_labels + '.png')

    # urur
    plt.figure()
    for i in range(n_data):
        plt.semilogx(data[i][:, 0], data[i][:, 2], label=labels[i])
        plt.xlabel(r'$y^+$')
        plt.ylabel(r'$\langle u_r u_r \rangle^+$')
        plt.legend()

    directory = 'plots/' + 'compare_' + all_labels + '/'
    if not os.path.exists(directory):
        os.makedirs(directory)

    if (iftikz):
        tikz_save(directory + 'urur_' + all_labels + '.tex',
                figureheight='\\figureheight',
                figurewidth='\\figurewidth',
                show_info=False,
                )

    plt.savefig(directory + 'urur_' + all_labels + '.png')

    # utut
    plt.figure()
    for i in range(n_data):
        plt.semilogx(data[i][:, 0], data[i][:, 3], label=labels[i])
        plt.xlabel(r'$y^+$')
        plt.ylabel(r'$\langle u_\varphi u_\varphi \rangle^+$')
        plt.legend()

    directory = 'plots/' + 'compare_' + all_labels + '/'
    if not os.path.exists(directory):
        os.makedirs(directory)

    if (iftikz):
        tikz_save(directory + 'utut_' + all_labels + '.tex',
                figureheight='\\figureheight',
                figurewidth='\\figurewidth',
                show_info=False,
                )

    plt.savefig(directory + 'utut_' + all_labels + '.png')

    # uzuz
    plt.figure()
    for i in range(n_data):
        plt.semilogx(data[i][:, 0], data[i][:, 4], label=labels[i])
        plt.xlabel(r'$y^+$')
        plt.ylabel(r'$\langle u_z u_z \rangle^+$')
        plt.legend()

    directory = 'plots/' + 'compare_' + all_labels + '/'
    if not os.path.exists(directory):
        os.makedirs(directory)

    if (iftikz):
        tikz_save(directory + 'uzuz_' + all_labels + '.tex',
                figureheight='\\figureheight',
                figurewidth='\\figurewidth',
                show_info=False,
                )

    plt.savefig(directory + 'uzuz_' + all_labels + '.png')

    # uruz
    plt.figure()
    for i in range(n_data):
        plt.semilogx(data[i][:, 0], data[i][:, 5], label=labels[i])
        plt.xlabel(r'$y^+$')
        plt.ylabel(r'$\langle u_r u_z \rangle^+$')
        plt.legend()

    directory = 'plots/' + 'compare_' + all_labels + '/'
    if not os.path.exists(directory):
        os.makedirs(directory)

    if (iftikz):
        tikz_save(directory + 'uruz_' + all_labels + '.tex',
                figureheight='\\figureheight',
                figurewidth='\\figurewidth',
                show_info=False,
                )

    plt.savefig(directory + 'uruz_' + all_labels + '.png')


def run():
    """Plot velocity statistics: 1st and 2nd order"""

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    # Loop over specified timeranges
    for span in timeranges_select:
        # Call a postprocessing script
        plot_individual(span)
