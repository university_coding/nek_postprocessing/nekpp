#/usr/bin/env python3.5
#----------------------------------------------------------------------
# Plot profiles of data in .dat files
#----------------------------------------------------------------------
# Date:     2018/03/01
# Author:   Steffen Straub
#----------------------------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt
from matplotlib2tikz import save as tikz_save
from ..misc import my_save_fig as msf
from ..misc import correlations

import os

import configparser

import pdb



def plot_individual(timerange_select):

    plt.close('all')

    print("Processing " + timerange_select)
    # Load data
    #----------
    data_dir = 'profiles/' + timerange_select + '/'
    data_name = (
            'temp_stats_IT071.dat',
            'temp_stats_MBC071.dat',
            'temp_stats_IF071.dat',
            'temp_stats_IT0025.dat',
            'temp_stats_MBC0025.dat',
            'temp_stats_IF0025.dat',
            )

    data = []
    for name in data_name:
        data.append(np.loadtxt(data_dir + name))
    data = np.asarray(data)

    # Plot
    #-----
    for i in range(len(data)):

        # Theta
        plt.figure()
        plt.semilogx(data[i, :, 0], data[i, :, 1])
        plt.xlabel(r'$y^+$')
        plt.ylabel(r'$\langle \Theta \rangle^+$')
    
        directory = 'plots/' + timerange_select + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + 'Theta_' + data_name[i][11:-4] + '.png')


        # theta theta
        plt.figure()
        plt.semilogx(data[i, :, 0], data[i, :, 2])
        plt.xlabel(r'$y^+$')
        plt.ylabel(r'$\langle \vartheta \vartheta \rangle^+$')
    
        directory = 'plots/' + timerange_select + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + 'thetatheta_' + data_name[i][11:-4] + '.png')


        # u_i theta
        plt.figure()
        plt.semilogx(data[i, :, 0], data[i, :, 3], label=r'$u_r \vartheta$')
#        plt.semilogx(data[i, :, 0], data[i, :, 4], label=r'$u_t \vartheta$')
        plt.semilogx(data[i, :, 0], data[i, :, 5], label=r'$u_z \vartheta$')
        plt.xlabel(r'$y^+$')
        plt.ylabel(r'$\langle u_i \vartheta \rangle^+$')
        plt.legend()
    
        directory = 'plots/' + timerange_select + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + 'uitheta_' + data_name[i][11:-4] + '.png')




def plot_timeseries(timeranges, iftikz=False):

    plt.close('all')

    print("Processing " + ','.join(timeranges))
    # Load data
    #----------
    data_name = (
            'temp_stats_IT071.dat',
            'temp_stats_MBC071.dat',
            'temp_stats_IF071.dat',
            'temp_stats_IT0025.dat',
            'temp_stats_MBC0025.dat',
            'temp_stats_IF0025.dat',
            )

    nr = int(np.loadtxt('interpmesh/number_of_points.txt')[0])
    ncols = 7
    n_names = len(data_name)
    n_timeranges = len(timeranges)

    data = np.zeros((nr, ncols, n_names, n_timeranges))
    for i in range(n_timeranges):
        for j in range(n_names):
            data_dir = 'profiles/' + timeranges[i] + '/'
            data[:, :, j, i] = np.loadtxt(data_dir + data_name[j])


    # Plot
    #-----
    # Theta
    for j in range(n_names):
        plt.figure()
        for i in range(n_timeranges):
            plt.semilogx(data[:, 0, j, i], data[:, 1, j, i], label=timeranges[i])

        plt.xlim(xmin=1)
        plt.ylim(ymin=0)
        plt.xlabel(r'$y^+$')
        plt.ylabel(r'$\langle \Theta \rangle^+$')
        plt.legend()
    
        directory = 'plots/' + timeranges[-1] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        if (iftikz):
            tikz_save(directory + 'Theta_' + data_name[j][11:-4] + '_timerange.tex',
                    figureheight='\\figureheight',
                    figurewidth='\\figurewidth',
                    show_info=False,
                    )

        plt.savefig(directory + 'Theta_' + data_name[j][11:-4] + '_timerange.png')

    plt.close('all')

    # theta theta
    for j in range(n_names):
        plt.figure()
        for i in range(n_timeranges):
            plt.semilogx(data[:, 0, j, i], data[:, 2, j, i], label=timeranges[i])

        plt.xlim(xmin=1)
        plt.ylim(ymin=0)
        plt.xlabel(r'$y^+$')
        plt.ylabel(r'$\langle \vartheta \vartheta \rangle^+$')
        plt.legend()

    
        directory = 'plots/' + timeranges[-1] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)

        if (iftikz):
            tikz_save(directory + 'thetatheta_' + data_name[j][11:-4] + '_timerange.tex',
                    figureheight='\\figureheight',
                    figurewidth='\\figurewidth',
                    show_info=False,
                    )
       
        plt.savefig(directory + 'thetatheta_' + data_name[j][11:-4] + '_timerange.png')

    plt.close('all')

    # ur theta
    for j in range(n_names):
        plt.figure()
        for i in range(n_timeranges):
            plt.semilogx(data[:, 0, j, i], data[:, 3, j, i], label=timeranges[i])

        plt.xlim(xmin=1)
        plt.ylim(ymin=0)
        plt.xlabel(r'$y^+$')
        plt.ylabel(r'$\langle u_r \vartheta \rangle^+$')
        plt.legend()
    
        directory = 'plots/' + timeranges[-1] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        if (iftikz):
            tikz_save(directory + 'urtheta_' + data_name[j][11:-4] + '_timerange.tex',
                    figureheight='\\figureheight',
                    figurewidth='\\figurewidth',
                    show_info=False,
                    )

        plt.savefig(directory + 'urtheta_' + data_name[j][11:-4] + '_timerange.png')

    plt.close('all')

    # uz theta
    for j in range(n_names):
        plt.figure()
        for i in range(n_timeranges):
            plt.semilogx(data[:, 0, j, i], data[:, 5, j, i], label=timeranges[i])

        plt.xlim(xmin=1)
        plt.ylim(ymin=0)
        plt.xlabel(r'$y^+$')
        plt.ylabel(r'$\langle u_z \vartheta \rangle^+$')
        plt.legend()

        directory = 'plots/' + timeranges[-1] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)

        if (iftikz):
            tikz_save(directory + 'uztheta_' + data_name[j][11:-4] + '_timerange.tex',
                    figureheight='\\figureheight',
                    figurewidth='\\figurewidth',
                    show_info=False,
                    )
   

        plt.savefig(directory + 'uztheta_' + data_name[j][11:-4] + '_timerange.png')


def plot_comparison(dat_path, given_labels, iftikz=False):

    plt.close('all')

    print("Processing " + ','.join(dat_path))
    print('Compare ' + ' '.join(given_labels))
    all_labels = '_'.join(given_labels)
    # Load data
    #----------
    data_name = (
            'temp_stats_IT071.dat',
            'temp_stats_MBC071.dat',
            'temp_stats_IF071.dat',
            'temp_stats_IT0025.dat',
            'temp_stats_MBC0025.dat',
            'temp_stats_IF0025.dat',
            )
    
    # Save all datasets in one list "data"
    n_data = len(dat_path)
    n_names = len(data_name)
    data = []
    for i in range(n_data):
        pts_file = dat_path[i] + '../../interpmesh/number_of_points.txt'
        nr = int(np.loadtxt(pts_file)[0])
        ncols = 6

        this_data = np.zeros((nr, ncols, n_names))
        for j in range(n_names):
            data_dir = dat_path[i]
            # skip last column that contains Theta in another scaling
            this_data[:, :, j] = np.loadtxt(data_dir + data_name[j], usecols=(0,1,2,3,4,5))

        data.append(this_data)



    # which labels?
    if (given_labels):
        labels = given_labels
    else:
        labels=data_path


    # Plot
    #-----
    # Theta
    for j in range(n_names):
        plt.figure()
        for i in range(n_data):
            plt.semilogx(data[i][:, 0, j], data[i][:, 1, j], label=labels[i])
            plt.xlabel(r'$y^+$')
            plt.ylabel(r'$\langle \Theta \rangle^+$')
            plt.legend()
    
        directory = 'plots/' + 'compare_' + all_labels + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
 
        if (iftikz):
            tikz_save(directory + 'Theta_' + data_name[j][11:-4] + '_' + all_labels + '.tex',
                    figureheight='\\figureheight',
                    figurewidth='\\figurewidth',
                    show_info=False,
                    )   

        plt.savefig(directory + 'Theta_' + data_name[j][11:-4] + '_' + all_labels + '.png')

    # theta theta
    for j in range(n_names):
        plt.figure()
        for i in range(n_data):
            plt.semilogx(data[i][:, 0, j], data[i][:, 2, j], label=labels[i])
            plt.xlabel(r'$y^+$')
            plt.ylabel(r'$\langle \vartheta \vartheta \rangle^+$')
            plt.legend()
    
        directory = 'plots/' + 'compare_' + all_labels + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
  
        if (iftikz):
            tikz_save(directory + 'thetatheta_' + data_name[j][11:-4] + '_' + all_labels + '.tex',
                    figureheight='\\figureheight',
                    figurewidth='\\figurewidth',
                    show_info=False,
                    )   
   
        plt.savefig(directory + 'thetatheta_' + data_name[j][11:-4] + '_' + all_labels + '.png')

    plt.close('all')

    # ur theta
    for j in range(n_names):
        plt.figure()
        for i in range(n_data):
            plt.semilogx(data[i][:, 0, j], data[i][:, 3, j], label=labels[i])
            plt.xlabel(r'$y^+$')
            plt.ylabel(r'$\langle u_r \vartheta \rangle^+$')
            plt.legend()
    
        directory = 'plots/' + 'compare_' + all_labels + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
  
        if (iftikz):
            tikz_save(directory + 'urtheta_' + data_name[j][11:-4] + '_' + all_labels + '.tex',
                    figureheight='\\figureheight',
                    figurewidth='\\figurewidth',
                    show_info=False,
                    )   
   
        plt.savefig(directory + 'urtheta_' + data_name[j][11:-4] + '_' + all_labels + '.png')

    plt.close('all')

    # uz theta
    for j in range(n_names):
        plt.figure()
        for i in range(n_data):
            plt.semilogx(data[i][:, 0, j], data[i][:, 5, j], label=labels[i])
            plt.xlabel(r'$y^+$')
            plt.ylabel(r'$\langle u_z \vartheta \rangle^+$')
            plt.legend()
    
        directory = 'plots/' + 'compare_' + all_labels + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
  
        if (iftikz):
            tikz_save(directory + 'uztheta_' + data_name[j][11:-4] + '_' + all_labels + '.tex',
                    figureheight='\\figureheight',
                    figurewidth='\\figurewidth',
                    show_info=False,
                    )   
   
        plt.savefig(directory + 'uztheta_' + data_name[j][11:-4] + '_' + all_labels + '.png')

def plot_comparison_paper(dat_path, iftikz=False):

    plt.close('all')

    print("Processing " + ','.join(dat_path))
    # Load data
    #----------
    data_name = (
            'temp_stats_IT071.dat',
            'temp_stats_MBC071.dat',
            'temp_stats_IF071.dat',
            'temp_stats_IT0025.dat',
            'temp_stats_MBC0025.dat',
            'temp_stats_IF0025.dat',
            )
    
    # Save all datasets in one list "data"
    n_data = len(dat_path)
    n_names = len(data_name)
    data = []
    for i in range(n_data):
        pts_file = dat_path[i] + '../../interpmesh/number_of_points.txt'
        nr = int(np.loadtxt(pts_file)[0])
        ncols = 7

        this_data = np.zeros((nr, ncols, n_names))
        for j in range(n_names):
            data_dir = dat_path[i]
            this_data[:, :, j] = np.loadtxt(data_dir + data_name[j])

        data.append(this_data)

    Ret_list = (
            360,
#            720,
#            1100,
            2000,
            )

    label_list = (
            'IT',
            'MBC',
            'IF',
            )

    linestyle_list = (
            '-',
            ':',
            '--',
            )

    # Plot
    #-----
    string_list = ('Theta', 'thetatheta', 'urtheta', 'uphitheta', 'uztheta')
    string_latex_list = (r'$\langle \Theta \rangle^+$', r'$\langle \vartheta \vartheta \rangle^+$', 
            r'$\langle u_r \vartheta \rangle^+$', r'$\langle u_\varphi \vartheta \rangle^+$', 
            r'$\langle u_z \vartheta \rangle^+$')
    # Loop over all terms
    for l in range(len(string_list)):
        plt.figure()
        i = 0
        # Loop over TBC
        for j in range(3):
            plt.semilogx(data[i][:, 0, j], data[i][:, 1+l, j], 
                    color='C{0:d}'.format(i), linestyle=linestyle_list[j],
                    label=label_list[j])
        # Loop over datasets
        for i in range(1, len(dat_path)):
            # Loop over TBC
            for j in range(3):
                plt.semilogx(data[i][:, 0, j], data[i][:, 1+l, j], 
                        color='C{0:d}'.format(i), linestyle=linestyle_list[j],
                        )
        if (string_list[l] == 'Theta'):
            R = 0.5
            Pr = 0.71
            i = len(dat_path)-1     # only for highest Re
            yp = data[i][:, 0, 0]
            y = yp/Ret_list[i]
            kader = correlations.kader(R, Pr, y, yp)
            plt.semilogx(yp, kader, 'k-.')

        plt.xlabel(r'$y^+$')
        plt.ylabel(string_latex_list[l])
    #    plt.legend()
    
        fname = string_list[l] + '_all071'
        directory = 'plots/' + 'compare/'
        msf.save_tikz_png(fname, directory, iftikz)
    
    
        plt.figure()
        i= 0
        for j in range(3, 6):
            plt.semilogx(data[i][:, 0, j], data[i][:, 1+l, j], 
                    color='C{0:d}'.format(i), linestyle=linestyle_list[j-3],
                    label=label_list[j-3])
        for i in range(1, len(dat_path)):
            for j in range(3, 6):
                plt.semilogx(data[i][:, 0, j], data[i][:, 1+l, j], 
                        color='C{0:d}'.format(i), linestyle=linestyle_list[j-3],
                        )
        if (string_list[l] == 'Theta'):
            R = 0.5
            Pr = 0.025
            i = len(dat_path)-1     # only for highest Re
            yp = data[i][:, 0, 0]
            y = yp/Ret_list[i]
            kader = correlations.kader(R, Pr, y, yp)
            plt.semilogx(yp, kader, 'k-.') 

        plt.xlabel(r'$y^+$')
        plt.ylabel(string_latex_list[l])
    #    plt.legend()
    
        fname = string_list[l] + '_all0025'
        directory = 'plots/' + 'compare/'
        msf.save_tikz_png(fname, directory, iftikz)

        plt.close('all')


def plot_azimuth_individual(timerange_select):

    label = (
            'halfconst071',
            'halfsin071',
            'halfconst0025',
            'halfsin0025',
            )
    
    probe = (
            '-90',
            '-45',
            '0',
            '45',
            '90',
            )

    data_name = (
            'probe_0_temp.dat',
            'probe_1_temp.dat',
            'probe_2_temp.dat',
            'probe_3_temp.dat',
            'probe_4_temp.dat',
            )

    for k in range(len(label)):

        plt.close('all')
    
        # Load data
        #----------
        data_dir = 'profiles/' + timerange_select + '/'

        print("Processing " + label[k] + ' at time ' + timerange_select)           
        data = []
        for name in data_name:
            data.append(np.loadtxt(data_dir + label[k] + '/' + name))
        data = np.asarray(data)

        # Plot
        #-----

        # Theta
        plt.figure()
        for i in range(len(data)):
            plt.semilogx(data[i, :, 0], data[i, :, 1], label=probe[i])
        plt.xlabel(r'$y^+$')
        plt.ylabel(r'$\langle \Theta \rangle^+$')
        plt.legend()
    
        directory = 'plots/' + timerange_select + '/' + label[k] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + 'Theta' + '.png')


        # theta theta
        plt.figure()
        for i in range(len(data)):
            plt.semilogx(data[i, :, 0], data[i, :, 2], label=probe[i])
        plt.xlabel(r'$y^+$')
        plt.ylabel(r'$\langle \vartheta \vartheta \rangle^+$')
        plt.legend()
   
        directory = 'plots/' + timerange_select + '/' + label[k] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + 'thetatheta' + '.png')


        # u_r theta
        plt.figure()
        for i in range(len(data)):
            plt.semilogx(data[i, :, 0], data[i, :, 3], label=probe[i])
        plt.xlabel(r'$y^+$')
        plt.ylabel(r'$\langle u_r \vartheta \rangle^+$')
        plt.legend()
    
        directory = 'plots/' + timerange_select + '/' + label[k] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + 'urtheta' + '.png')


        # u_t theta
        plt.figure()
        for i in range(len(data)):
            plt.semilogx(data[i, :, 0], data[i, :, 4], label=probe[i])
        plt.xlabel(r'$y^+$')
        plt.ylabel(r'$\langle u_\varphi \vartheta \rangle^+$')
        plt.legend()
    
        directory = 'plots/' + timerange_select + '/' + label[k] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + 'uphitheta' + '.png')


        # u_z theta
        plt.figure()
        for i in range(len(data)):
            plt.semilogx(data[i, :, 0], data[i, :, 5], label=probe[i])
        plt.xlabel(r'$y^+$')
        plt.ylabel(r'$\langle u_z \vartheta \rangle^+$')
        plt.legend()
    
        directory = 'plots/' + timerange_select + '/' + label[k] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + 'uztheta' + '.png')

def plot_azimuth_timeseries(timeranges):

    plt.close('all')

    print("Processing " + ','.join(timeranges))
    # Load data
    #----------
    label = (
            'halfconst071',
            'halfsin071',
            'halfconst0025',
            'halfsin0025',
            )

    probe_name = (
            'probe_0_temp.dat',
            'probe_1_temp.dat',
            'probe_2_temp.dat',
            'probe_3_temp.dat',
            'probe_4_temp.dat',
            )

    for k in range(len(label)):
        for m in range(len(probe_name)):

            nr = int(np.loadtxt('interpmesh/number_of_points.txt')[0])
            ncols = 6
            n_timeranges = len(timeranges)

            data = np.zeros((nr, ncols, n_timeranges))
            for i in range(n_timeranges):
                data_dir = 'profiles/' + timeranges[i] + '/' + label[k] + '/'
                data[:, :, i] = np.loadtxt(data_dir + probe_name[m])


            # Plot
            #-----
            # Theta
            plt.figure()
            for i in range(n_timeranges):
                plt.semilogx(data[:, 0, i], data[:, 1, i], label=timeranges[i])
                plt.xlabel(r'$y^+$')
                plt.ylabel(r'$\langle Theta \rangle^+$')
                plt.legend()
        
            directory = 'plots/' + timeranges[-1] + '/' + label[k] + '/'
            if not os.path.exists(directory):
                os.makedirs(directory)
        
            plt.savefig(directory + 'Theta_probe_{0:d}'.format(m) + '_timerange.png')
    
            plt.close('all')
    

            # thetatheta
            plt.figure()
            for i in range(n_timeranges):
                plt.semilogx(data[:, 0, i], data[:, 2, i], label=timeranges[i])
                plt.xlabel(r'$y^+$')
                plt.ylabel(r'$\langle \vartheta \vartheta \rangle^+$')
                plt.legend()
        
            directory = 'plots/' + timeranges[-1] + '/' + label[k] + '/'
            if not os.path.exists(directory):
                os.makedirs(directory)
        
            plt.savefig(directory + 'thetatheta_probe_{0:d}'.format(m) + '_timerange.png')
    
            plt.close('all')

            # urtheta
            plt.figure()
            for i in range(n_timeranges):
                plt.semilogx(data[:, 0, i], data[:, 3, i], label=timeranges[i])
                plt.xlabel(r'$y^+$')
                plt.ylabel(r'$\langle u_r \vartheta \rangle^+$')
                plt.legend()
        
            directory = 'plots/' + timeranges[-1] + '/' + label[k] + '/'
            if not os.path.exists(directory):
                os.makedirs(directory)
        
            plt.savefig(directory + 'urtheta_probe_{0:d}'.format(m) + '_timerange.png')
    
            plt.close('all')
 
            # uphitheta
            plt.figure()
            for i in range(n_timeranges):
                plt.semilogx(data[:, 0, i], data[:, 4, i], label=timeranges[i])
                plt.xlabel(r'$y^+$')
                plt.ylabel(r'$\langle u_r \vartheta \rangle^+$')
                plt.legend()
        
            directory = 'plots/' + timeranges[-1] + '/' + label[k] + '/'
            if not os.path.exists(directory):
                os.makedirs(directory)
        
            plt.savefig(directory + 'uphitheta_probe_{0:d}'.format(m) + '_timerange.png')
    
            plt.close('all')
   
            # uztheta
            plt.figure()
            for i in range(n_timeranges):
                plt.semilogx(data[:, 0, i], data[:, 5, i], label=timeranges[i])
                plt.xlabel(r'$y^+$')
                plt.ylabel(r'$\langle u_z \vartheta \rangle^+$')
                plt.legend()
        
            directory = 'plots/' + timeranges[-1] + '/' + label[k] + '/'
            if not os.path.exists(directory):
                os.makedirs(directory)
        
            plt.savefig(directory + 'uztheta_probe_{0:d}'.format(m) + '_timerange.png')
    
            plt.close('all')
 

def plot_azimuth_comparison(dat_path, given_labels, iftikz=False):

    print("Processing " + ','.join(dat_path))
    print('Compare ' + ' '.join(given_labels))
    all_labels = '_'.join(given_labels)


    # Load data:
    #-----------
    case_label = (
            'halfconst071',
            'halfsin071',
            'halfconst0025',
            'halfsin0025',
            )
    
    probe = (
            '-90',
            '-45',
            '0',
            '45',
            '90',
            )

    data_name = (
            'probe_0_temp.dat',
            'probe_1_temp.dat',
            'probe_2_temp.dat',
            'probe_3_temp.dat',
            'probe_4_temp.dat',
            )

    # Save all datasets in one list "data"
    n_data = len(dat_path)
    n_names = len(case_label)
    n_probes = len(probe)
    data = []
    for i in range(n_data):
        pts_file = dat_path[i] + '../../interpmesh/number_of_points.txt'
        nr = int(np.loadtxt(pts_file)[0])
        ncols = 6

        this_data = np.zeros((nr, ncols, n_names, n_probes))
        for j in range(n_names):
            for k in range(n_probes):
                data_dir = dat_path[i]
                data_file = case_label[j] + '/' + data_name[k]
                this_data[:, :, j, k] = np.loadtxt(data_dir + data_file)

        data.append(this_data)



    # which labels?
    if (given_labels):
        labels = given_labels
    else:
        labels=data_path

    # Plot
    #-----
    my_linestyles = ('-', '--', ':', '-.')


    stat_names = ['Theta', 'thetatheta', 'ur_theta', 'ut_theta', 'uz_theta']
    tex_names = [r'\Theta', r'\vartheta \vartheta', 
            r'u_r \vartheta', r'u_\varphi \vartheta', r'u_z \vartheta']
    # Loop over statistics contained in file
    for stat_num in range(len(stat_names)):

        # Loop over case names
        for j in range(n_names):
            plt.figure()
            # Loop over datasets which are compared
            for i in range(n_data):
                # Loop over probes
                for k in range(n_probes):
                    plt.semilogx(data[i][:, 0, j, k], data[i][:, stat_num+1, j, k], 
                            my_linestyles[i], color='C{0:d}'.format(k), label = labels[i] + ' ' + probe[k])
                    plt.xlabel(r'$y^+$')
                    plt.ylabel(r'$\langle ' + tex_names[stat_num] + r' \rangle^+$')
                    plt.legend()
            
            directory = 'plots/' + 'compare_' + all_labels + '/' + case_label[j] + '/'
            if not os.path.exists(directory):
                os.makedirs(directory)
     
            if (iftikz):
                tikz_save(directory + stat_names[stat_num] + '_' + all_labels + '.tex',
                        figureheight='\\figureheight',
                        figurewidth='\\figurewidth',
                        show_info=False,
                        )   
    
            plt.savefig(directory + stat_names[stat_num] + '_' + all_labels + '.png')
        
        plt.close('all')


def plot_azimuth_comparison_paper(dat_path, given_labels, iftikz=False):

    print("Processing " + ','.join(dat_path))
    print('Compare ' + ' '.join(given_labels))
    all_labels = '_'.join(given_labels)


    # Load data:
    #-----------
    case_label = (
            'halfconst071',
            'halfsin071',
            'halfconst0025',
            'halfsin0025',
            )
    
    probe = (
            '-90',
            '-45',
            '0',
            '45',
            '90',
            )

    data_name = (
            'probe_0_temp.dat',
            'probe_1_temp.dat',
            'probe_2_temp.dat',
            'probe_3_temp.dat',
            'probe_4_temp.dat',
            )
    

    # Save all datasets in one list "data"
    n_data = len(dat_path)
    n_names = len(case_label)
    n_probes = len(probe)
    data = []
    data_h = []
    for i in range(n_data):
        pts_file = dat_path[i] + '../../interpmesh/number_of_points.txt'
        nr = int(np.loadtxt(pts_file)[0])
        ncols = 6

        this_data = np.zeros((nr, ncols, n_names, n_probes))
        for j in range(n_names):
            for k in range(n_probes):
                data_dir = dat_path[i]
                data_file = case_label[j] + '/' + data_name[k]
                this_data[:, :, j, k] = np.loadtxt(data_dir + data_file)

        data.append(this_data)



    # which labels?
    if (given_labels):
        labels = given_labels
    else:
        labels=data_path

    # Plot
    #-----
    my_linestyles = ('-', '--', ':', '-.')


    stat_names = ['Theta', 'thetatheta', 'ur_theta', 'ut_theta', 'uz_theta']
    tex_names = [r'\Theta', r'\vartheta \vartheta', 
            r'u_r \vartheta', r'u_\varphi \vartheta', r'u_z \vartheta']
    # Loop over statistics contained in file
    for stat_num in range(len(stat_names)):

        # Loop over case names
        for j in range(n_names):
            plt.figure()
            # Loop over datasets which are compared
            for i in range(n_data):
                # Loop over probes
                for k in range(n_probes):
                    plt.semilogx(data[i][:, 0, j, k], data[i][:, stat_num+1, j, k], 
                            my_linestyles[i], color='C{0:d}'.format(k), label = labels[i] + ' ' + probe[k])
                    plt.xlabel(r'$y^+$')
                    plt.ylabel(r'$\langle ' + tex_names[stat_num] + r' \rangle^+$')
#                    plt.legend()

            directory = 'plots/' + 'compare_' + all_labels + '/' + case_label[j] + '/'
            if not os.path.exists(directory):
                os.makedirs(directory)
     
            if (iftikz):
                tikz_save(directory + stat_names[stat_num] + '_' + all_labels + '.tex',
                        figureheight='\\figureheight',
                        figurewidth='\\figurewidth',
                        show_info=False,
                        )   
    
            plt.savefig(directory + stat_names[stat_num] + '_' + all_labels + '.png')
        
        plt.close('all')




def run():
    """Plot temperature statistics: 1st and 2nd order"""


    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    # Loop over specified timeranges
    for span in timeranges_select:
        # Call a postprocessing script
        plot_individual(span)

def run_azimuth():
    """Plot temperature statistics: 1st and 2nd order"""


    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    # Loop over specified timeranges
    for span in timeranges_select:
        # Call a postprocessing script
        plot_azimuth_individual(span)



