#!/usr/bin/env python3.5
#-----------------------
# This script reads previously saved data 
# and saves  profiles 
# of the Reynolds shear stress balance
# in a simple text file.
# ----------------------------------------------------
# Author:   Steffen Straub
# Date  :   2019/07/02
#-----------------------------------------------------

import numpy as np
from ..misc import my_math
import matplotlib.pyplot as plt
from scipy import integrate as itg
from ..misc import my_save_fig as msf
import os

import configparser

import pdb

# Define function
def save_rss_bal(timerange_select):
    indata_v = 'stats_' + timerange_select + '/statistics_matrix_tv1.npz'
    outdata = (
            'profiles/' + timerange_select + '/rss_bal.dat'
             )
    ## Load my data
    #--------------
    # Velocity statistics
    with np.load(indata_v) as data:
            
        # Coordinates
        X = data['X']
        Y = data['Y']
    
        # individual radial and circumferential distribution
        r = data['r']
        phi = data['phi']
 
        stats_vel = data['stats_m']
        deriv_vel = data['deriv_m']
 
    ## Allocate variables
    #--------------------
    # Mean velocity vector
    Ui_cart = np.zeros((np.size(r), np.size(phi), 3))
    Ui_cyl = np.zeros((np.size(r), np.size(phi), 3))
    Ui = np.zeros((np.size(r), 3))
        
    # Mean velocity correlation tensor
    UiUj_cart = np.zeros((np.size(r), np.size(phi), 3, 3))
    UiUj_cyl = np.zeros((np.size(r), np.size(phi), 3, 3))
    UiUj = np.zeros((np.size(r), 3, 3))
    
    # Reynolds stress tensor
    uiuj_cart = np.zeros((np.size(r), np.size(phi), 3, 3))
    uiuj_cyl = np.zeros((np.size(r), np.size(phi), 3, 3))
    uiuj = np.zeros((np.size(r), 3, 3))
    
    # Gradient of the mean velocity
    dUjdxi_cart= np.zeros((np.size(r), np.size(phi), 3, 3))
    dUjdxi_cyl= np.zeros((np.size(r), np.size(phi), 3, 3))
    dUjdxi= np.zeros((np.size(r), 3, 3))
 
 
    # Read user defined case-dependent parameters
    config = configparser.ConfigParser()
    config.read('case_params.ini')
    Re_b = float(config['Reynolds']['Re_b'])
 
    ## Calculations
    #-------------
    D = 1
    R = D/2
    nu = 1/Re_b
    y = R - r 
    
    theta, rho= np.meshgrid(phi, r, indexing='xy')
    u_t = my_math.get_utau(r, phi, deriv_vel, theta, nu)
    Re_t = u_t*D/nu
    y_plus = (y*u_t)/nu
    y_plus_flip = np.flipud(y_plus)
    
    # Mean velocities <U_i>
    #----------------------
    Ui_cart = stats_vel[:, :, 0:3]
    Ui_cyl = my_math.transform_vect(theta, Ui_cart, 1)
    Ui = np.mean(Ui_cyl, axis=1)
    
    # <U_i U_j>
    #----------
    UiUj_cart[:, :, 0, 0] = stats_vel[:,:,4]
    UiUj_cart[:, :, 0, 1] = stats_vel[:,:,8]
    UiUj_cart[:, :, 0, 2] = stats_vel[:,:,10]
    UiUj_cart[:, :, 1, 0] = UiUj_cart[:, :, 0, 1]
    UiUj_cart[:, :, 1, 1] = stats_vel[:,:,5]
    UiUj_cart[:, :, 1, 2] = stats_vel[:,:,9]
    UiUj_cart[:, :, 2, 0] = UiUj_cart[:, :, 0, 2]
    UiUj_cart[:, :, 2, 1] = UiUj_cart[:, :, 1, 2]
    UiUj_cart[:, :, 2, 2] = stats_vel[:,:,6]
    UiUj_cyl = my_math.transform_vect(theta, UiUj_cart, 2)
    UiUj = np.mean(UiUj_cyl, axis=1)
    
    # <u_i u_j> Reynolds stress
    #--------------------------
    uiuj_cart = UiUj_cart - np.einsum('...i,...j', Ui_cart, Ui_cart)
    uiuj_cyl = my_math.transform_vect(theta, uiuj_cart, 2)
    uiuj = np.mean(uiuj_cyl, axis=1)
    
    # Derivatives
    #------------
    # d <U_j> / d x_i
    #----------------
    dUjdxi_cart[:, :, 0, 0] = deriv_vel[:,:,0] # d<U_1> / d x_1
    dUjdxi_cart[:, :, 0, 1] = deriv_vel[:,:,1] # d<U_1> / d x_2
    dUjdxi_cart[:, :, 0, 2] = 0            # d<U_1> / d x_3
    dUjdxi_cart[:, :, 1, 0] = deriv_vel[:,:,2] # d<U_2> / d x_1
    dUjdxi_cart[:, :, 1, 1] = deriv_vel[:,:,3] # d<U_2> / d x_2
    dUjdxi_cart[:, :, 1, 2] = 0            # d<U_2> / d x_3
    dUjdxi_cart[:, :, 2, 0] = deriv_vel[:,:,4] # d<U_3> / d x_1
    dUjdxi_cart[:, :, 2, 1] = deriv_vel[:,:,5] # d<U_3> / d x_2
    dUjdxi_cart[:, :, 2, 2] = 0            # d<U_3> / d x_3
    dUjdxi_cyl = my_math.transform_vect(theta, dUjdxi_cart, 2)
    dUjdxi = np.mean(dUjdxi_cyl, axis=1)
    
    # Viscous shear stress 
    #---------------
    # \rho \nu d <U_z> / dy = -\rho \nu d <U_z> / d r
    tau_visc = - nu * dUjdxi[:, 2, 0]
    tau_visc_plus = tau_visc / u_t**2
    
    # Turbulent shear stress
    #-----------------------
    # - \rho < u v > = + \rho < u_z u_r >
    tau_turb = uiuj[:, 0, 2]
    tau_turb_plus = tau_turb / u_t**2
    
    # Total shear stress
    #-------------------
    # tau = tau_visc + tau_turb
    tau_total = tau_visc + tau_turb
    tau_total_plus = tau_total / u_t**2
    
    # Residual
    #---------
    # Following the definition of Thompson et al. (2016) 
    # and my own notes from 18/02/09
    #----------------------------------------------------------------------
    E_R = -1 + y_plus / (Re_t/2) + tau_total_plus
    E_R_mean = 1/y_plus_flip * itg.cumtrapz(E_R, y_plus_flip, initial=0)
    E_U = - np.flip(y_plus_flip * E_R_mean)


    

        
    file_path = outdata
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)
    
    np.savetxt(file_path, 
            np.transpose([
                np.flipud(y_plus), 
                np.flipud(tau_visc_plus),
                np.flipud(tau_turb_plus),
                np.flipud(tau_total_plus),
                np.flipud(E_R),
                np.flipud(E_U),
                ]), header="y_plus\ttau_visc_plus\ttau_turb_plus\ttau_total_plus\tE_r\tE_U")
    
def run():
    # Call function

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    for span in timeranges_select:
        save_rss_bal(span)
