#!/usr/bin/env python3.5
#-----------------------
# This script reads previously saved data 
# and saves  profiles of
# the turbulent thermal diffusivity alpha_t (averaged over 
# a section where singularities do not affect it)
# in a simple text file.
# ----------------------------------------------------
# Author:   Steffen Straub
# Date  :   2018/11/09
#-----------------------------------------------------

import numpy as np
from ..misc import my_math
import os

import configparser

import pdb


# Define function
def save_alphat(timerange_select, nfields, fstart):

    # Load data
    #----------
    data_dir = 'stats_2d_sym/' + timerange_select + '/'
    data_name = ['/2d_sym_turb_diff_tt{0:d}.npz'.format(k) for k in range(fstart, nfields+fstart)]

    print('Note: label_list is hardcoded for now.')
    label = (
        'halfconst071',
        'halfsin071',
        'halfconst0025',       
        'halfsin0025',
        )

   
    for i in range(len(data_name)):
    
        print("Processing " + label[i] + ' at time ' + timerange_select)

        with np.load(data_dir + data_name[i]) as data:
        
            # Coordinates
            X = data['X_sym']
            Y = data['Y_sym']
       
            # individual radial and circumferential distribution
            r = data['r']
            phi = data['phi']
        
            # data averaged over time, streamwise direction, and exploited symmetry
            stats_sym = data['stats_sym']


        # 2) Velocity statistics
        indata_v = 'stats_' + timerange_select + '/statistics_matrix_tv1.npz'
        with np.load(indata_v) as data:
                    
            stats_vel = data['stats_m']
            deriv_vel = data['deriv_m']
    

        # Read user defined case-dependent parameters
        config = configparser.ConfigParser()
        config.read('case_params.ini')
        Re_b = float(config['Reynolds']['Re_b'])
       
        ## Calculations
        #-------------
        theta, rho= np.meshgrid(phi, r, indexing='xy')
        nu = 1/Re_b
        u_t = my_math.get_utau(r, phi, deriv_vel, theta, nu)
        Re_t = u_t*1/nu
        y = 0.5 - r 
        y_plus = (y*u_t)/nu

        alpha_t = stats_sym[..., 0:3]

        # Average alpha_t_r in section [pi/4, pi/2]
        n_phi_max = np.max(np.shape(alpha_t)[1])-1
        # number of slices for 45° change
        d_45 = int(n_phi_max/4)
        
        n_phi = np.shape(alpha_t)[1]

#        pdb.set_trace()
        alpha_t_r_avg = np.mean(alpha_t[:, 3*d_45:4*d_45, 0], axis=1)
        alpha_t_phi_avg = np.mean(alpha_t[:, 2*d_45:3*d_45, 1], axis=1)

        file_path = 'profiles/' + timerange_select + '/' + label[i] + '/alphat_avg.dat'
        directory = os.path.dirname(file_path)
        if not os.path.exists(directory):
            os.makedirs(directory)
        
        np.savetxt(file_path, 
                np.transpose([
                    np.flipud(y_plus), 
                    np.flipud(alpha_t_r_avg),
                    np.flipud(alpha_t_phi_avg),
                    ]), header='y_plus\t<alpha_t_r>/nu\t<alpha_t_phi>/nu')


def run(nfields=4, fstart=0):
    """
    Write averaged turbulent thermal diffusivities in text files.

    nfields     Number of fields to interpolate
    fstart      First field starts by, e.g (0, 1)
    """

    # Call function

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    for span in timeranges_select:
        save_alphat(span, nfields, fstart)
