#!/usr/bin/env python3.5
#-----------------------
# This script reads previously saved data 
# and saves  profiles 
# of the heat flux balance
# in a simple text file.
# ----------------------------------------------------
# Author:   Steffen Straub
# Date  :   2018/01/14
#-----------------------------------------------------

import numpy as np
from ..misc import my_math
import os

import configparser

import pdb

# Define function
def save_heat_bal(timerange_select):

    indata_v = 'stats_' + timerange_select + '/statistics_matrix_tv1.npz'
    indata_t_list = (
            'stats_' + timerange_select + '/statistics_matrix_tt1.npz', 
            'stats_' + timerange_select + '/statistics_matrix_tt2.npz', 
            'stats_' + timerange_select + '/statistics_matrix_tt3.npz', 
            'stats_' + timerange_select + '/statistics_matrix_tt4.npz', 
            'stats_' + timerange_select + '/statistics_matrix_tt5.npz', 
            'stats_' + timerange_select + '/statistics_matrix_tt6.npz', 
            )
    Pr_list = (
            0.71,
            0.71,
            0.71,
            0.025,
            0.025,
            0.025,
            )
    outdata_list = (
            'profiles/' + timerange_select + '/heat_bal_IT071.dat',
            'profiles/' + timerange_select + '/heat_bal_MBC071.dat',
            'profiles/' + timerange_select + '/heat_bal_IF071.dat',
            'profiles/' + timerange_select + '/heat_bal_IT0025.dat',       
            'profiles/' + timerange_select + '/heat_bal_MBC0025.dat',
            'profiles/' + timerange_select + '/heat_bal_IF0025.dat',
            )

    for i in range(len(indata_t_list)):
        indata_t = indata_t_list[i]
        Pr = Pr_list[i]
        outdata = outdata_list[i]

        print("Processing ", indata_t)
        ## Load my data
        #--------------
        # 1) Temperature statistics
        with np.load(indata_t) as data:
        
            # Coordinates
            X = data['X']
            Y = data['Y']
        
            # individual radial and circumferential distribution
            r = data['r']
            phi = data['phi']
        
            # runtime averages and postprocessing derivatives
            stats_temp = data['stats_m']
            deriv_temp = data['deriv_m']
        
        # 2) Velocity statistics
        with np.load(indata_v) as data:
                
            stats_vel = data['stats_m']
            deriv_vel = data['deriv_m']

        # Read user defined case-dependent parameters
        config = configparser.ConfigParser()
        config.read('case_params.ini')
        Re_b = float(config['Reynolds']['Re_b'])

        ## Calculations
        #-------------
        theta, rho= np.meshgrid(phi, r, indexing='xy')
        nr = len(r)
        r_flip = np.flip(r)

        nu = 1/Re_b
        Pe = Re_b * Pr
        u_t = my_math.get_utau(r, phi, deriv_vel, theta, nu)
        q_w = my_math.get_qw(r, phi, deriv_temp, theta, Pe)
        Re_t = u_t*1/nu
        y = 0.5 - r 
        y_plus = (y*u_t)/nu
        D = 1
        
        UiTheta_cart = np.zeros((np.size(r), np.size(phi), 3))
        
        Theta = stats_temp[:, :, 0]
        UiTheta_cart[:, :, 0:3] = stats_temp[:, :, 2:5]                # <U_1 Theta>, <U_2 Theta>, <U_3 Theta>
        Ui_cart = stats_vel[:, :, 0:3]
        Ui_cyl = my_math.transform_vect(theta, Ui_cart, 1)
        Ui = np.mean(Ui_cyl, axis=1)
        
        uitheta_cart = UiTheta_cart[..., 0:3] - np.einsum('...i,...',Ui_cart, Theta)
        uitheta_cyl = my_math.transform(theta, uitheta_cart, 1)
        uitheta = np.mean(uitheta_cyl, axis=1)
        
        
        dThetadxi_cart = np.zeros((np.size(r), np.size(phi), 3))
        dThetadxi_cart[:, :, 0:2] = deriv_temp[:, :, 0:2]              # d <Theta> / d x_1, d <Theta> / d x_2
        dThetadxi_cyl = my_math.transform(theta, dThetadxi_cart, 1)
        dThetadxi = np.mean(dThetadxi_cyl, axis=1)

        # Read the previously calculated Nusselt number to determine an averaged ahat
        try:
            Nu = np.loadtxt('./nusselt/' + timerange_select + '/nuss.dat')
        except OSError:
            print('Error: Nusselt numbers are needed for ahat (source term)!')
            raise

        
        # Turbulent heat flux contribution
        q_turb = - 2 * r * uitheta[:, 0]
        
        # Molecular heat flux contribution on a tube with radius r (see notes from 21.12.2017)
        # See Nusselt decomposition (B.15) (02.07.2019)
        q_mol = 2/Pe * r * dThetadxi[:, 0]
        
        # Fractional flow rate (B.16) (02.07.2019
        # Contribution due to source term (i.e. streamwise temperature gradient)
        ffr = np.zeros(nr)
        for i in range(nr):
            ffr[i] = 8 * np.trapz(np.flip(Ui[:, 2])[:nr-i]*r_flip[:nr-i], r_flip[:nr-i])

         # IT
        if ("IT" in outdata):
            if ("071" in outdata):
                ahat = Nu[0]/Pe * 4/D
            elif ("0025" in outdata):
                ahat = Nu[3]/Pe * 4/D
            else:
                print('Error with ahat for the IT cases.')
                sys.exit(1)
            # Mean advective heat flux
            mean_adv = np.mean(ahat * Theta * Ui_cyl[:, :, 2], axis=1)
            # Turbulent axial advective heat flux
            turb_ax  = np.mean(ahat * uitheta_cyl[:, :, 2], axis=1)
            # Mean streamwise thermal diffusion
            mean_diff = np.mean(ahat**2 / Pe * Theta, axis=1)

            mean_adv_int = np.zeros(nr)
            for i in range(nr):
                mean_adv_int[i] = 2 * np.trapz(np.flip(mean_adv+turb_ax+mean_diff)[:nr-i]*r_flip[:nr-i], 
                        r_flip[:nr-i])

        # MBC and IF
        else:
            # Mean advection term
            mean_adv_int = ffr

        resid = q_turb + q_mol + 1 + mean_adv_int 
        print('Maximum residual: {0:10.5e} '.format(np.max(np.abs(resid))))
            
        file_path = outdata
        directory = os.path.dirname(file_path)
        if not os.path.exists(directory):
            os.makedirs(directory)
        
        np.savetxt(file_path, 
                np.transpose([
                    np.flipud(y_plus), 
                    np.flipud(q_turb),
                    np.flipud(q_mol),
                    np.flipud(mean_adv_int),
                    np.flipud(resid),
                    ]), header="y_plus\tq_turb\tq_mol\tffr\tresid")
    
def run():
    # Call function

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    for span in timeranges_select:
        save_heat_bal(span)
