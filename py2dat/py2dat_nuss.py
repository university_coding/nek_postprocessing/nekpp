#/usr/bin/env python3.5
#-----------------------
# Calculate the Nusselt number
# for homogeneous TBC
# ----------------------------------------------------
# Author:   Steffen Straub
# Date  :   2018/02/27
#-----------------------------------------------------

import numpy as np
from ..misc import my_math
import os

import configparser


# Define function
def save_nuss(timerange_select):

    indata_v = 'stats_' + timerange_select + '/statistics_matrix_tv1.npz'
    indata_t_list = (
            'stats_' + timerange_select + '/statistics_matrix_tt1.npz', 
            'stats_' + timerange_select + '/statistics_matrix_tt2.npz', 
            'stats_' + timerange_select + '/statistics_matrix_tt3.npz', 
            'stats_' + timerange_select + '/statistics_matrix_tt4.npz', 
            'stats_' + timerange_select + '/statistics_matrix_tt5.npz', 
            'stats_' + timerange_select + '/statistics_matrix_tt6.npz', 
            )
    Pr_list = (
            0.71,
            0.71,
            0.71,
            0.025,
            0.025,
            0.025,
            )
    outdata = 'nusselt/' + timerange_select + '/nuss.dat'

    nuss = []
    for i in range(len(indata_t_list)):
        indata_t = indata_t_list[i]
        Pr = Pr_list[i]

        print("Processing ", indata_t)
        ## Load my data
        #--------------
        # 1) Temperature statistics
        with np.load(indata_t) as data:
        
            # Coordinates
            X = data['X']
            Y = data['Y']
        
            # individual radial and circumferential distribution
            r = data['r']
            phi = data['phi']
        
            # runtime averages and postprocessing derivatives
            stats_temp = data['stats_m']
            deriv_temp = data['deriv_m']
        
        # 2) Velocity statistics
            with np.load(indata_v) as data:
                    
                stats_vel = data['stats_m']
                deriv_vel = data['deriv_m']
        
        
        # Read user defined case-dependent parameters
        config = configparser.ConfigParser()
        config.read('case_params.ini')
        Re_b = float(config['Reynolds']['Re_b'])

        ## Calculations
        #-------------
        theta, rho= np.meshgrid(phi, r, indexing='xy')
        nu = 1/Re_b
        Pe = Re_b * Pr
        u_t = my_math.get_utau(r, phi, deriv_vel, theta, nu)
        Re_t = u_t*1/nu
        y = 0.5 - r 
        y_plus = (y*u_t)/nu
        
        
        UiTheta_cart = np.zeros((np.size(r), np.size(phi), 3))
        
        UiTheta_cart[:, :, 0:3] = stats_temp[:, :, 2:5]                # <U_1 Theta>, <U_2 Theta>, <U_3 Theta>
        UiTheta_cyl = my_math.transform_vect(theta, UiTheta_cart, 1)
        UiTheta = np.mean(UiTheta_cyl, axis=1)
        
        # Integrate over r
        UiTheta_dA =  2*np.pi * np.trapz(UiTheta[..., 2]*r, r)
        
        
        A_cross = 0.5**2 * np.pi
        U_b = 1
        
        Nu = A_cross * Pe * U_b / UiTheta_dA
        nuss.append(Nu)
    
    file_path = outdata
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)
    
    label = (
            'IT071',
            'MBC071',
            'IF071',
            'IT0025',
            'MBC0025',
            'IF0025',
            )
    
     
    np.savetxt(file_path, 
       nuss,
       header = " ".join(label))
    

    
def run():
    # Call function

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    for span in timeranges_select:
        save_nuss(span)
 

