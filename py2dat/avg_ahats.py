#!/usr/bin/env python3.5
#
# Time averaged values of ahat for homogeneous isothermal pipe flow according to Piller (2005)

# These are averaged over the time period 900 - 2300 and 3300 - 4500 
# since ahat was set constant in between.
a1 = 0.018686053002074984
a2 = 0.1562455428477553

## These are averaged over the time period 900 - 2300
#a1 = 0.018676011101158848
#a2 = 0.1561449663310991
 
## These are averaged over the time period 1100 - 1700
#a1 = 0.018676517596833333
#a2 = 0.156154175555 
