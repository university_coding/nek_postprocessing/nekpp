#/usr/bin/env python3.5
#-----------------------
# Calculate the Nusselt number
# for homogeneous TBC
# ----------------------------------------------------
# Author:   Steffen Straub
# Date  :   2018/02/27
#-----------------------------------------------------

import numpy as np
from ..misc import my_math
import os

import configparser


# Define function
def save_nuss(timerange_select):

    indata_v = 'stats_' + timerange_select + '/statistics_matrix_tv1.npz'
    outdata = 'Ret/' + timerange_select + '/Ret.dat'


    ## Load my data
    #--------------
    with np.load(indata_v) as data:
        # Coordinates
        X = data['X']
        Y = data['Y']
    
        # individual radial and circumferential distribution
        r = data['r']
        phi = data['phi']
    
        # runtime averages and postprocessing derivatives
        stats_vel = data['stats_m']
        deriv_vel = data['deriv_m']

    
    # Read user defined case-dependent parameters
    config = configparser.ConfigParser()
    config.read('case_params.ini')
    Re_b = float(config['Reynolds']['Re_b'])

    ## Calculations
    #-------------
    theta, rho= np.meshgrid(phi, r, indexing='xy')
    nu = 1/Re_b
    u_t = my_math.get_utau(r, phi, deriv_vel, theta, nu)
    Re_t = u_t*1/nu
    
    file_path = outdata
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)
    
    
     
    np.savetxt(file_path, 
       [Re_t],
       header = "Re_tau = u_tau * D / nu")
    

    
def run():
    # Call function

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')
    
    for span in timeranges_select:
        save_nuss(span)
 

