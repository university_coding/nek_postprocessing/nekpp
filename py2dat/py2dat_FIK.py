#!/usr/bin/env python3
#----------------------------------------------------------------------
# Calculate the laminar and turbulent contributions to the Nusselt 
# number for MBC/IF
#----------------------------------------------------------------------
# Author:   Steffen Straub
# Date:     2019/02/15
#----------------------------------------------------------------------
import numpy as np
from scipy import integrate as itg
from ..misc import my_math
from ..misc import save_results
import matplotlib.pyplot as plt
plt.close('all')
import pdb

import configparser


# Define functions
def get_contributions(indata_v):
    """
    Calculate the individual contributions

    indata_v      velocity statistics
    """

    ## Load my data
    #--------------
    # Velocity statistics
    with np.load(indata_v) as data:
        # Coordinates
        X = data['X']
        Y = data['Y']
    
        # individual radial and circumferential distribution
        r = data['r']
        phi = data['phi']

        stats_vel = data['stats_m']
        deriv_vel = data['deriv_m']
        
    ## Allocate variables
    #--------------------
    # Mean velocity vector
    Ui_cart = np.zeros((np.size(r), np.size(phi), 3))
    Ui_cyl = np.zeros((np.size(r), np.size(phi), 3))
    Ui = np.zeros((np.size(r), 3))
        
    # Mean velocity correlation tensor
    UiUj_cart = np.zeros((np.size(r), np.size(phi), 3, 3))
    UiUj_cyl = np.zeros((np.size(r), np.size(phi), 3, 3))
    UiUj = np.zeros((np.size(r), 3, 3))
    
    # Reynolds stress tensor
    uiuj_cart = np.zeros((np.size(r), np.size(phi), 3, 3))
    uiuj_cyl = np.zeros((np.size(r), np.size(phi), 3, 3))
    uiuj = np.zeros((np.size(r), 3, 3))
    
    # Gradient of the mean velocity
    dUjdxi_cart= np.zeros((np.size(r), np.size(phi), 3, 3))
    dUjdxi_cyl= np.zeros((np.size(r), np.size(phi), 3, 3))
    dUjdxi= np.zeros((np.size(r), 3, 3))


    # Read user defined case-dependent parameters
    config = configparser.ConfigParser()
    config.read('case_params.ini')
    Re_b = float(config['Reynolds']['Re_b'])

    ## Calculations
    #--------------
    theta, rho= np.meshgrid(phi, r, indexing='xy')
    nr = len(r)
    r_flip = np.flip(r)


    # Mean velocities <U_i>
    #----------------------
    Ui_cart = stats_vel[:, :, 0:3]
    Ui_cyl = my_math.transform_vect(theta, Ui_cart, 1)
    Ui = np.mean(Ui_cyl, axis=1)
    
    # <U_i U_j>
    #----------
    UiUj_cart[:, :, 0, 0] = stats_vel[:,:,4]
    UiUj_cart[:, :, 0, 1] = stats_vel[:,:,8]
    UiUj_cart[:, :, 0, 2] = stats_vel[:,:,10]
    UiUj_cart[:, :, 1, 0] = UiUj_cart[:, :, 0, 1]
    UiUj_cart[:, :, 1, 1] = stats_vel[:,:,5]
    UiUj_cart[:, :, 1, 2] = stats_vel[:,:,9]
    UiUj_cart[:, :, 2, 0] = UiUj_cart[:, :, 0, 2]
    UiUj_cart[:, :, 2, 1] = UiUj_cart[:, :, 1, 2]
    UiUj_cart[:, :, 2, 2] = stats_vel[:,:,6]
    UiUj_cyl = my_math.transform_vect(theta, UiUj_cart, 2)
    UiUj = np.mean(UiUj_cyl, axis=1)
    
    # <u_i u_j> Reynolds stress
    #--------------------------
    uiuj_cart = UiUj_cart - np.einsum('...i,...j', Ui_cart, Ui_cart)
    uiuj_cyl = my_math.transform_vect(theta, uiuj_cart, 2)
    uiuj = np.mean(uiuj_cyl, axis=1)
    
    # Derivatives
    #------------
    # d <U_j> / d x_i
    #----------------
    dUjdxi_cart[:, :, 0, 0] = deriv_vel[:,:,0] # d<U_1> / d x_1
    dUjdxi_cart[:, :, 0, 1] = deriv_vel[:,:,1] # d<U_1> / d x_2
    dUjdxi_cart[:, :, 0, 2] = 0            # d<U_1> / d x_3
    dUjdxi_cart[:, :, 1, 0] = deriv_vel[:,:,2] # d<U_2> / d x_1
    dUjdxi_cart[:, :, 1, 1] = deriv_vel[:,:,3] # d<U_2> / d x_2
    dUjdxi_cart[:, :, 1, 2] = 0            # d<U_2> / d x_3
    dUjdxi_cart[:, :, 2, 0] = deriv_vel[:,:,4] # d<U_3> / d x_1
    dUjdxi_cart[:, :, 2, 1] = deriv_vel[:,:,5] # d<U_3> / d x_2
    dUjdxi_cart[:, :, 2, 2] = 0            # d<U_3> / d x_3
    dUjdxi_cyl = my_math.transform_vect(theta, dUjdxi_cart, 2)
    dUjdxi = np.mean(dUjdxi_cyl, axis=1)
    

    # FIK decomposition in my outer units (see calculations from 03.07.2019)
    # 1) Laminar contribution
    laminar = 16/Re_b

    # 2) Turbulent contribution
    turb = 32 * np.trapz( 2 * r * uiuj[:, 0, 2] * r, r )

    sum_Cf = laminar + turb
    
    # C_f directly through wall shear stress
    tau_w = 1/Re_b * dUjdxi[-1, 2, 0]
    C_f = -2 * tau_w

    pdb

    return (laminar, turb, sum_Cf, C_f)



def run():
    """ Calculate individual contributions to the Skin friction coefficient. 
    """

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    # for output
    description = "Rows: laminar, turbulent, sum_Cf, C_f"

    # Possibly loop over all timeranges
    for time in timeranges_select:
        indata_v = 'stats_'+time+'/statistics_matrix_tv1.npz'

        contributions = get_contributions(indata_v)
 
        outdata = 'Cf/'+time+'/FIK.dat'
        save_results.save_to_dat(contributions, description, outdata)

