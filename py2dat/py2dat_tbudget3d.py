#!/usr/bin/env python3.5
#-----------------------
# This script reads previously saved data 
# and saves  profiles 
# of first and second order temperature statistics
# in a simple text file.
# ----------------------------------------------------
# Author:   Steffen Straub
# Date  :   2018/03/27
#-----------------------------------------------------

import numpy as np
from ..misc import my_math
import os

import configparser


import pdb

# Define function
def save_temp3d(timerange_select, nfields, fstart):
    
    # Load data
    #----------
    data_dir = 'stats_3d/' + timerange_select + '/'
    data_name = ['tbudget_{0:d}.npz'.format(k) for k in range(fstart, nfields+fstart)]


    print('Note: label_list is hardcoded for now.')
    label = (
        '071',
        '0025',
        )

   
    for i in range(len(data_name)):
    
        print("Processing " + label[i] + ' at time ' + timerange_select)

        with np.load(data_dir + data_name[i]) as data:
        
            # Coordinates
            X = data['X_sym']
            Y = data['Y_sym']
       
            # individual radial and circumferential distribution
            r = data['r']
            phi = data['phi']
            phi_sym = data['phi_sym']
            z = data['z']
        
            # data averaged over time and exploited symmetry
            stats_3d = data['stats_3d_sym']


    
#        # 2) Velocity statistics
#        indata_v = 'stats_' + timerange_select + '/statistics_matrix_tv1.npz'
#        with np.load(indata_v) as data:
#                    
#            stats_vel = data['stats_m']
#            deriv_vel = data['deriv_m']
    
        
        # Read user defined case-dependent parameters
        config = configparser.ConfigParser()
        config.read('case_params.ini')
        Re_b = float(config['Reynolds']['Re_b'])
        Re_t = float(config['Reynolds']['Re_t'])


        ## Calculations
        #-------------
        theta, rho= np.meshgrid(phi, r, indexing='xy')
        nu = 1/Re_b
#        u_t = my_math.get_utau(r, phi, deriv_vel, theta, nu)
#        Re_t = u_t*1/nu
        u_t = Re_t/Re_b
        y = 0.5 - r 
        y_plus = (y*u_t)/nu
        
    
        P = stats_3d[..., 0]
        eps = stats_3d[..., 1]
        MD = stats_3d[..., 2]
        TD = stats_3d[..., 3]


        # Save 5 profiles at phi =
        # -pi/2
        # -pi/4
        # 0
        # pi/4
        # pi/2
        n_probes = 5
        probes_sep = int( (len(phi_sym)-1) / (n_probes-1))

        n_axpos = 10
        ax_sep = int( len(z) / (n_axpos) )     # z=0 does not need to be considered

        
        # Thermal statistics
        for iz in range(1, n_axpos):
            for k in range(n_probes):
                file_path = 'profiles/' + timerange_select + '/' + label[i] + '/z_{0:d}/probe_{1:d}/'.format(iz, k) + 'tbudget.dat'
                directory = os.path.dirname(file_path)
                if not os.path.exists(directory):
                    os.makedirs(directory)
    
                np.savetxt(file_path, 
                        np.transpose([
                            np.flipud(y_plus), 
                            np.flipud(P[:, probes_sep*k, ax_sep*iz]),
                            np.flipud(eps[:, probes_sep*k, ax_sep*iz]),
                            np.flipud(MD[:, probes_sep*k, ax_sep*iz]),
                            np.flipud(TD[:, probes_sep*k, ax_sep*iz]),
                            ]), header='y_plus\t<P>+\t<eps>+\t<MD>+\t<TD>+')
    
   



def run(nfields=2, fstart=0):
    """
    Write budget of temperature variance at 5 probes in text files.

    nfields     Number of fields to interpolate
    fstart      First field starts by, e.g (0, 1)
    """

    # Call function

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    for span in timeranges_select:
        save_temp3d(span, nfields, fstart)
