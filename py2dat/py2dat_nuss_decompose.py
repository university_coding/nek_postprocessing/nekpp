#!/usr/bin/env python3
#----------------------------------------------------------------------
# Calculate the laminar and turbulent contributions to the Nusselt 
# number for MBC/IF
#----------------------------------------------------------------------
# Author:   Steffen Straub
# Date:     2019/02/15
#----------------------------------------------------------------------
import numpy as np
from scipy import integrate as itg
from ..misc import my_math
from ..misc import save_results
import matplotlib.pyplot as plt
plt.close('all')
import pdb

import configparser


# Define functions
def get_contributions(indata_v, indata_t, Pr):
    """
    Calculate the individual contributions

    indata_v      velocity statistics
    indata_t      thermal statistics
    Pr            Prandtl number
    """

    ## Load my data
    #--------------
    # 1) Temperature statistics
    with np.load(indata_t) as data:
        # Coordinates
        X = data['X']
        Y = data['Y']
        # individual radial and circumferential distribution
        r = data['r']
        phi = data['phi']
        # runtime averages and postprocessing derivatives
        stats_temp = data['stats_m']
        deriv_temp = data['deriv_m']
    
    # 2) Velocity statistics
    with np.load(indata_v) as data:
        stats_vel = data['stats_m']
        deriv_vel = data['deriv_m']
        

    # Read user defined case-dependent parameters
    config = configparser.ConfigParser()
    config.read('case_params.ini')
    Re_b = float(config['Reynolds']['Re_b'])
    Re_t = float(config['Reynolds']['Re_t'])
    Pe = Re_b*Pr


    ## Calculations
    #--------------
    theta, rho= np.meshgrid(phi, r, indexing='xy')
    nr = len(r)
    r_flip = np.flip(r)
    
    UiTheta_cart = np.zeros((np.size(r), np.size(phi), 3))

    Theta = stats_temp[:, :, 0]
    Ui_cart = stats_vel[:, :, 0:3]
    Ui_cyl = my_math.transform_vect(theta, Ui_cart, 1)
    Ui = np.mean(Ui_cyl, axis=1)
    UiTheta_cart[:, :, 0:3] = stats_temp[:, :, 2:5]
    uitheta_cart = UiTheta_cart[..., 0:3] - np.einsum('...i,...',Ui_cart, Theta)
    uitheta_cyl = my_math.transform_vect(theta, uitheta_cart, 1)
    uitheta = np.mean(uitheta_cyl, axis=1)

    # Turbulent velocity profile
    U_L = 2 * (1 - (2*r)**2)
    U_T = Ui[:, 2] - U_L
    # fractional flow rate
    ffr = np.flip(8 * itg.cumtrapz(Ui[::-1, 2]*r[::-1], r[::-1],  initial=0))
    ffr_T = np.flip(8 * itg.cumtrapz(U_T[::-1]*r[::-1], r[::-1], initial=0))

    # 1) Laminar contribution
    laminar = 11/48

    # 2) Contribution due to radial turbulent heat flux
#    gain = 1.013
    gain = 1
    turb_hf = np.trapz( (1+ffr) * gain * uitheta[:, 0], r)

#    y = 0.5 - r
#    y_plus = y * Re_t
#    plt.close('all')
#    plt.semilogx(y_plus, (1+ffr), label=r'1+$\Phi$')
#    plt.semilogx(y_plus, uitheta[:, 0], label=r'$\langle u_r \theta \rangle$')
#    plt.semilogx(y_plus, gain*uitheta[:, 0], '--k', label=r'${0:4.3f} \cdot \langle u_r \theta \rangle$'.format(gain))
#    plt.xlabel(r'$y^+$')
#    plt.legend()
#    plt.show()

    # 3) Contribution due to turbulent velocity field
    # for r=0, ffr_T(r=0) = 0, so we avoid the division by zero
    turb_v = 1/2 * np.trapz( 1/r[1:] * ((2*r[1:]**4 - r[1:]**2) 
        * 16 * ffr_T[1:] - ffr_T[1:]**2), r[1:])

    # 4) Additional enthalpy flux due to the streamwise turbulent heat flux
    turb_hfz = 8/Pe * np.trapz( uitheta[:, 2]*r, r)

    # Nusselt number from all contributions
    Nu = 1/(laminar - turb_hf - turb_v + turb_hfz)

    return (laminar, turb_hf, turb_v, turb_hfz, Nu)



def get_contributions_khoury(indata_v, indata_t, Pr):
    """
    Calculate the individual contributions using velocity statistics from El Khoury

    indata_v      velocity statistics
    indata_t      thermal statistics
    Pr            Prandtl number
    """

    ## Load my data
    #--------------
    # 1) Temperature statistics
    with np.load(indata_t) as data:
        # Coordinates
        X = data['X']
        Y = data['Y']
        # individual radial and circumferential distribution
        r = data['r']
        phi = data['phi']
        # runtime averages and postprocessing derivatives
        stats_temp = data['stats_m']
        deriv_temp = data['deriv_m']
    
    # 2) Velocity statistics
        with np.load(indata_v) as data:
            stats_vel = data['stats_m']
            deriv_vel = data['deriv_m']
    def get_khoury_data():
        # Load data
        data_dir = '/net/istmlupus/localhome/hi202/local/data/pipe/2013_El_Khoury/'
        data_name = '1000_Re_1.dat'
        data = np.loadtxt(data_dir + data_name, skiprows=24)
        
        # Calculations
        y = data[:, 0]
        Uz = data[:, 3]
        r = 1 - y
        nr = len(r)
        r_flip = np.flip(r)
        
        # Note that scaling factors are adjusted
        # Turbulent velocity profile
        U_L = 2 * (1 - (r)**2)
        U_T = Uz - U_L
        
        # fractional flow rate
        ffr = np.flip(2 * itg.cumtrapz(Uz[::-1]*r[::-1], r[::-1],  initial=0))
        ffr_T = np.flip(2 * itg.cumtrapz(U_T[::-1]*r[::-1], r[::-1], initial=0))

        # Contribution due to turbulent velocity field
        # for r=0, ffr_T(r=0) = 0, so we avoid the division by zero
        turb_v = 1/2 * np.trapz( 1/r[1:] * ((2*r[1:]**4 - 4 * r[1:]**2) 
            * ffr_T[1:] - ffr_T[1:]**2), r[1:])

        return (r, ffr, turb_v)
    (EK_r, EK_ffr, EK_turb_v) = get_khoury_data()
        
    # Read user defined case-dependent parameters
    config = configparser.ConfigParser()
    config.read('case_params.ini')
    Re_b = float(config['Reynolds']['Re_b'])
    Pe = Re_b*Pr


    ## Calculations
    #--------------
    theta, rho= np.meshgrid(phi, r, indexing='xy')
    nr = len(r)
    r_flip = np.flip(r)
    
    UiTheta_cart = np.zeros((np.size(r), np.size(phi), 3))

    Theta = stats_temp[:, :, 0]
    Ui_cart = stats_vel[:, :, 0:3]
    Ui_cyl = my_math.transform_vect(theta, Ui_cart, 1)
    Ui = np.mean(Ui_cyl, axis=1)
    UiTheta_cart[:, :, 0:3] = stats_temp[:, :, 2:5]
    uitheta_cart = UiTheta_cart[..., 0:3] - np.einsum('...i,...',Ui_cart, Theta)
    uitheta_cyl = my_math.transform_vect(theta, uitheta_cart, 1)
    uitheta = np.mean(uitheta_cyl, axis=1)

    # Turbulent velocity profile
    U_L = 2 * (1 - (2*r)**2)
    U_T = Ui[:, 2] - U_L
    # fractional flow rate
    ffr = np.zeros(nr)
    ffr_T = np.zeros(nr)
    for i in range(nr):
        ffr[i] = 8 * np.trapz(np.flip(Ui[:, 2])[:nr-i]*r_flip[:nr-i], r_flip[:nr-i])
        ffr_T[i] = 8 * np.trapz(np.flip(U_T)[:nr-i]*r_flip[:nr-i], r_flip[:nr-i])

    # interpolate El Khoury's ffr onto my mesh
    ffr = np.interp(r, EK_r/2, EK_ffr)

    # 1) Laminar contribution
    laminar = 11/48

    
    # 2) Contribution due to radial turbulent heat flux
    turb_hf = np.trapz( (1+ffr)*uitheta[:, 0], r)

    # 3) Contribution due to turbulent velocity field
    turb_v = EK_turb_v

    # 4) Additional enthalpy flux due to the streamwise turbulent heat flux
    turb_hfz = 8/Pe * np.trapz( uitheta[:, 2]*r, r)

    # Nusselt number from all contributions
    Nu = 1/(laminar - turb_hf - turb_v + turb_hfz)

    return (laminar, turb_hf, turb_v, turb_hfz, Nu)




def run(tfields = (2, 3, 5, 6), ifkhoury=False):
    """ Calculate individual contributions to the Nusselt number. 
    Typically, I put MBC and IF in fields 2, 3, 5, 6.
    """

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    # for output
    description = "Rows: laminar, turbulent (heat flux), turbulent (velocity), turbulent (streamwise heat flux), Nu"

    Pr_list = (
           0.71,
           0.71,
           0.71,
           0.025,
           0.025,
           0.025,
           )
    TBC_list = (
            'IT071',
            'MBC071',
            'IF071',
            'IT0025',
            'MBC0025',
            'IF0025',
            )

    # Possibly loop over all timeranges
    for time in timeranges_select:
        indata_v = 'stats_'+time+'/statistics_matrix_tv1.npz'

        # Loop over all MBC / IF fields
        for tfield in tfields:
            indata_t = 'stats_'+time+'/statistics_matrix_tt{0:d}.npz'.format(tfield)
            Pr = Pr_list[tfield-1]
            if (ifkhoury):
                contributions = get_contributions_khoury(indata_v, indata_t, Pr)
            else:
                contributions = get_contributions(indata_v, indata_t, Pr)

            outdata = 'nusselt/'+time+'/nuss_contrib_' + TBC_list[tfield-1] + '.dat'
            save_results.save_to_dat(contributions, description, outdata)

