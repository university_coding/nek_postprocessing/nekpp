#!/usr/bin/env python3.5
#-----------------------
# This script reads previously saved data 
# and saves  profiles 
# of the temperature budgets
# in a simple text file.
# ----------------------------------------------------
# Author:   Steffen Straub
# Date  :   2018/02/28
#-----------------------------------------------------

import numpy as np
from ..misc import my_math
import os

import configparser


# Define function
def save_tbudget_azimuth(timerange_select, nfields, fstart):

    # Load data
    #----------
    data_dir = 'stats_2d_sym/' + timerange_select + '/'
    data_name = ['2d_sym_tbudget_tt{0:d}.npz'.format(k) for k in range(fstart, nfields+fstart)]

#    data_name = (
#            '/2d_sym_tbudget_tt1.npz',
#            '/2d_sym_tbudget_tt2.npz',
#            '/2d_sym_tbudget_tt3.npz',
#            '/2d_sym_tbudget_tt4.npz',
#            )

    print('Note: label_list is hardcoded for now.')
    label = (
        'halfconst071',
        'halfsin071',
        'halfconst0025',       
        'halfsin0025',
        )

   
    for i in range(len(data_name)):
    
        print("Processing " + label[i] + ' at time ' + timerange_select)

        with np.load(data_dir + data_name[i]) as data:
        
            # Coordinates
            X = data['X_sym']
            Y = data['Y_sym']
       
            # individual radial and circumferential distribution
            r = data['r']
            phi = data['phi']
        
            # data averaged over time, streamwise direction, and exploited symmetry
            stats_sym = data['stats_sym']

    
        # 2) Velocity statistics
        indata_v = 'stats_' + timerange_select + '/statistics_matrix_tv1.npz'
        with np.load(indata_v) as data:
                    
            stats_vel = data['stats_m']
            deriv_vel = data['deriv_m']

        # Read user defined case-dependent parameters
        config = configparser.ConfigParser()
        config.read('case_params.ini')
        Re_b = float(config['Reynolds']['Re_b'])

        ## Calculations
        #-------------
        theta, rho= np.meshgrid(phi, r, indexing='xy')
        nu = 1/Re_b
        u_t = my_math.get_utau(r, phi, deriv_vel, theta, nu)
        Re_t = u_t*1/nu
        y = 0.5 - r 
        y_plus = (y*u_t)/nu
        
    
        # Save 5 profiles at phi =
        # -pi/2
        # -pi/4
        # 0
        # pi/4
        # pi/2
        n_probes = 5
        probes_sep = int( (np.shape(stats_sym[..., 0])[1]-1) / (n_probes-1))
    
        for k in range(n_probes):
            file_path = 'profiles/' + timerange_select + '/' + label[i] + '/probe_{0:d}'.format(k) + '_tbudget.dat'
            directory = os.path.dirname(file_path)
            if not os.path.exists(directory):
                os.makedirs(directory)
            
        
            np.savetxt(file_path, 
                    np.transpose([
                        np.flipud(y_plus), 
                        np.flipud(stats_sym[:, probes_sep*k, 0]),
                        np.flipud(stats_sym[:, probes_sep*k, 1]),
                        np.flipud(stats_sym[:, probes_sep*k, 2]),
                        np.flipud(stats_sym[:, probes_sep*k, 3]),
                        np.flipud(stats_sym[:, probes_sep*k, 4]),
                        ]), header='y_plus\tProduction <P>+\tDissipation <eps>+\t Molecular diffusion<MD>+\tTurbulent diffusion <TD>+\tSource term contribution <S>+')


                    
def run(nfields=4, fstart=0):
    """
    Write budget terms for temperature variance at 5 probes in text files.

    nfields     Number of fields to interpolate
    fstart      First field starts by, e.g (0, 1)
    """
   
    # Call function

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    for span in timeranges_select:
        save_tbudget_azimuth(span, nfields, fstart)
