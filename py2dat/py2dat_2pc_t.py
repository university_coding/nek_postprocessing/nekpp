#!/usr/bin/env python3.5
#-----------------------
# This script reads previously saved data 
# and saves  
# two-point correlations at different radial locations
# in a simple text file.
# ----------------------------------------------------
# Author:   Steffen Straub
# Date  :   2018/06/28
#-----------------------------------------------------

import numpy as np
from ..misc import my_math
import os

import configparser

import pdb

# Define function
def save(timerange_select, n_temp, t_fld_inhomog):

    indata = 'results/' + timerange_select + '/power_spectra_t.npz'
    outdata_list = ['profiles/' + timerange_select + '/2pc_t{0:d}.dat'.format(i) for i in range(n_temp)]

    print("Processing ", indata)

    label_homog = (
        'IT071',
        'MBC071',
        'IF071',       
        'IT0025',
        'MBC0025',
        'IF0025',
        )
    label_inhomog = (
        'halfconst071',
        'halfsin071',
        'halfconst0025',       
        'halfsin0025',
        )


    ## Load data
    #-----------
    with np.load(indata) as data:
        E = data['E']
        r = data['r']
        phi = data['phi']
        z = data['z']

    # Some pre-calculations and definitions
    nr = len(r)
    nphi = len(phi)
    nz = len(z)
    dphi = phi[1]
    dz = z[1]
    R = 0.5
    y = R - r

    # Read user defined case-dependent parameters
    config = configparser.ConfigParser()
    config.read('case_params.ini')
    Re_t = float(config['Reynolds']['Re_t'])
    
    # Conversion in plus units
    y_p = y * Re_t
    
    i1 = 0
    i2 = 0
  
    for t_fld in range(n_temp):

        if t_fld in t_fld_inhomog:
            ## Declare arrays
            R_z = np.zeros((nr, nphi, nz, len(t_fld_inhomog)))
            t2 = np.zeros((nr, int(nphi/2+0.5), len(t_fld_inhomog)))
    
            # Correlation function R(s) (see Pope, p. 686, eq. E.25)
            R_z[..., i1] = nz * np.real(np.fft.ifft(E[..., t_fld]))
            R_z_sym = my_math.left_right_sym(R_z, phi, tensor_order=0)
            
            # Variance
            t2[..., i1] = R_z_sym[:, :, 0, i1]


            # Save 5 profiles at phi =
            # -pi/2
            # -pi/4
            # 0
            # pi/4
            # pi/2
            n_probes = 5
            probes_sep = int( (np.shape(R_z_sym)[1]-1) / (n_probes-1))

            ## Save 
            #------
            file_path = outdata_list[t_fld]
            directory = os.path.dirname(file_path)
            if not os.path.exists(directory):
                os.makedirs(directory)
        
            i_01 = np.argmin(np.abs(r-0.1))
            R_z_max_list = []
            R_z_center_list = []
            R_z_wall_list = []
            header_list = []
            for k in range(n_probes):
                imax = np.argmax(t2[:, k*probes_sep, i1])
                R_z_max_list.append(R_z_sym[imax, k*probes_sep, :, i1])
                R_z_center_list.append(R_z_sym[i_01, k*probes_sep, :, i1])
                R_z_wall_list.append(R_z_sym[-1, k*probes_sep, :, i1])


            header_full = ('case: ' + label_inhomog[i1] + '\n' + 'cols:\tz, ' 
                    + 'R_z(y_p[imax]) for each probe, '
                    + 'R_z(y=0.4) for each probe, '
                    + 'R_z(wall) for each probe'
                    )

            save_list = []
            save_list.append(z[:])
            save_list.extend(R_z_max_list)
            save_list.extend(R_z_center_list)
            save_list.extend(R_z_wall_list)
                    

            np.savetxt(file_path, 
                    np.transpose(
                        save_list
                        ), 
                    header= header_full)
    
            i1 += 1
        else:
            
            # Sum over modes in homogeneous direction for keeping only dependency on r and either z or phi
            E_z =  np.sum(E, axis=1)
            E_t =  np.sum(E, axis=2)
            
            # Declare arrays
            R_z = np.zeros((nr, nz, n_temp-len(t_fld_inhomog)))
            R_t = np.zeros((nr, nphi, n_temp-len(t_fld_inhomog)))
            t2 = np.zeros((nr, n_temp-len(t_fld_inhomog)))
 
            # Correlation function R(s) (see Pope, p. 686, eq. E.25)
            R_z[..., i2] = nz * np.real(np.fft.ifft(E_z[..., t_fld]))
            R_t[..., i2] = nphi * np.real(np.fft.ifft(E_t[..., t_fld]))
            
            # Variance
            t2[..., i2] = R_z[:, 0, i2]
    

            ## Save 
            #------
            file_path = outdata_list[t_fld]
            directory = os.path.dirname(file_path)
            if not os.path.exists(directory):
                os.makedirs(directory)
        
            imax = np.argmax(t2[..., i2])
            i_01 = np.argmin(np.abs(r-0.1))

            header_full = ('case: ' + label_homog[i2] + '\n'
                    + 'cols:\tz\tR_z(y_p[{0:d}] = {1:3.1f})\tR_z(y[{2:d}] = {3:3.1f})'.format(
                        imax, y_p[imax], i_01, y[i_01]))
            np.savetxt(file_path, 
                    np.transpose([
                        z[:],
                        R_z[imax, :, i2],
                        R_z[i_01, :, i2],
                        ]),
                    header=header_full)

            i2 += 1

        

def run(n_temp = 10):
    # Call function

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    # inhomogeneous TBC in fields:
    t_fld_inhomog = [3, 4, 8, 9]

    for span in timeranges_select:
        save(span, n_temp, t_fld_inhomog)   
