#!/usr/bin/env python3.5
#-----------------------
# Calculate the Nusselt number
# for azimuthaly non-homogeneous TBC
# ----------------------------------------------------
# Author:   Steffen Straub
# Date  :   2018/03/15
#-----------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt
from ..misc import my_math
import os

import configparser

import pdb




# Define function
def save_nuss_azimuth(timerange_select, t_fld_inhomog):

    # Load data
    #----------
    indata_v = 'stats_' + timerange_select + '/statistics_matrix_tv1.npz'
    data_dir = 'stats_2d_sym/' + timerange_select + '/'
    data_name = ['2d_sym_temp_tt{0:d}.npz'.format(k) for k in range(0, len(t_fld_inhomog))]


    Pr_list = (
            0.71,
            0.71,
            0.025,
            0.025,
            )
    
    label_list = (
            'halfconst071',
            'halfsin071',
            'halfconst0025',
            'halfsin0025',
            )
    
    
    outdata = 'nusselt/' + timerange_select + '/nuss_azimuth.dat'
    outdata_local = 'nusselt/' + timerange_select + '/' 

    nuss = []
    for i in range(len(data_name)):
        indata_t = data_dir + data_name[i]
        Pr = Pr_list[i]
        label = label_list[i]



        print("Processing ", indata_t)
        ## Load my data
        #--------------
        # 1) Temperature statistics
        with np.load(indata_t) as data:
        
            # Coordinates
            X = data['X_sym']
            Y = data['Y_sym']
       
            # individual radial and circumferential distribution
            r = data['r']
            phi = data['phi']
            phi_sym = data['phi_sym']
        
            # data averaged over time, streamwise direction, and exploited symmetry
            stats_sym = data['stats_sym']

        
        # 2) Velocity statistics
            with np.load(indata_v) as data:
                    
                stats_vel = data['stats_m']
                deriv_vel = data['deriv_m']
        
        
        # Read user defined case-dependent parameters
        config = configparser.ConfigParser()
        config.read('case_params.ini')
        Re_b = float(config['Reynolds']['Re_b'])

        ## Calculations
        #-------------
        theta, rho= np.meshgrid(phi, r, indexing='xy')
        nu = 1/Re_b
        Pe = Re_b * Pr
        u_t = my_math.get_utau(r, phi, deriv_vel, theta, nu)
        Re_t = u_t*1/nu
        y = 0.5 - r 
        y_plus = (y*u_t)/nu
        
        # Determine the Nusselt number from 
        # Nu = - Pe / Theta_w 
        # See notes from 02/20/18
       
        nphi = len(phi_sym)
    
        Theta_sym = stats_sym[:, :, 0] / u_t
        Theta_wall = np.mean(Theta_sym[-1, :])

     
       
        Nu_G = - Pe / Theta_wall
    
    
        # Azimuthally averaged local Nusselt number
        q_w_phi = np.zeros((nphi))
        if ("halfconst" in label):      
            q_w_phi[int(nphi/2):] = 2
        elif ("halfsin" in label):
            q_w_phi[int(nphi/2):] = np.pi * np.sin(phi_sym[int(nphi/2):])

    
        Nu_local = - Pe / Theta_sym[-1, :] * q_w_phi

        # Save local Nu
        file_path = 'profiles/' + timerange_select + '/' + label_list[i] + '/Theta_wall.dat'
        directory = os.path.dirname(file_path)
        if not os.path.exists(directory):
            os.makedirs(directory)
         
        np.savetxt(file_path, 
                np.transpose([
                    np.flipud(phi_sym),
                    np.flipud(q_w_phi),
                    np.flipud(Theta_sym[-1, :]),      
                    np.flipud(Theta_sym[-1, :]*u_t),
                    np.flipud(Nu_local),
                    ]),
           header = "phi_sym\tq_w(phi)\tTheta_wall\tTheta_wall^+\tNu_local")


        Nu_avg = np.mean(Nu_local)
    
        nuss.append((Nu_G, Nu_avg, Nu_local[-1]))
    
    file_path = outdata
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)
     
    np.savetxt(file_path, 
       nuss,
       header = "rows: " + " ".join(label_list) + "\ncols: Nu_G\tNu_avg\tNu_(phi=pi/2)")

 

def run(t_fld_inhomog=[0, 7, 8, 9]):
    """ Calculate global and averaged Nusselt number for azimuthally inhomogeneous TBCs
    and save them as a text file.

    t_fld_inhomog           list of inhomogeneous fields

    """
    # Call function

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    for span in timeranges_select:
        save_nuss_azimuth(span, t_fld_inhomog)   
