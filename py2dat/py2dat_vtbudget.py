#!/usr/bin/env python3.5
#----------------------------------------------------------------
# This script reads previously saved data 
# and saves  profiles 
# of the turbulent heat flux budgets
# in a simple text file.
# ----------------------------------------------------
# Author:   Steffen Straub
# Date  :   2018/02/28
#-----------------------------------------------------

# Load modules
#-------------
import numpy as np
from ..misc import my_math
import os

import configparser

import pdb


# Define function
def save_vtbudget(timerange_select, tempstats):

    indata_v = 'stats_' + timerange_select + '/statistics_matrix_tv1.npz'
    indata_t_list = (
            'stats_' + timerange_select + '/statistics_matrix_tt1.npz', 
            'stats_' + timerange_select + '/statistics_matrix_tt2.npz', 
            'stats_' + timerange_select + '/statistics_matrix_tt3.npz', 
            'stats_' + timerange_select + '/statistics_matrix_tt4.npz', 
            'stats_' + timerange_select + '/statistics_matrix_tt5.npz', 
            'stats_' + timerange_select + '/statistics_matrix_tt6.npz', 
            )
    Pr_list = (
            0.71,
            0.71,
            0.71,
            0.025,
            0.025,
            0.025,
            )
    data_name = (
            '/vtemp_budget_IT071.dat',
            '/vtemp_budget_MBC071.dat',
            '/vtemp_budget_IF071.dat',
            '/vtemp_budget_IT0025.dat',       
            '/vtemp_budget_MBC0025.dat',
            '/vtemp_budget_IF0025.dat',
            )
    outdata_list = ['profiles/' + timerange_select + '/' + name for name in data_name]

    data_collect_rtheta = []
    data_collect_ztheta = []
    for i in range(len(indata_t_list)):
        indata_t = indata_t_list[i]
        Pr = Pr_list[i]
        outdata = outdata_list[i]
        outdata = outdata[:-4]      # skip .dat extension
    
        print("Processing ", indata_t)
        # Load my data
        #-------------
        # 1) Temperature statistics
        with np.load(indata_t) as data:
        
            # Coordinates
            X = data['X']
            Y = data['Y']
        
            # individual radial and circumferential distribution
            r = data['r']
            phi = data['phi']
        
            # runtime averages and postprocessing derivatives
            stats_temp = data['stats_m']
            deriv_temp = data['deriv_m']
        
        # 2) Velocity statistics
        with np.load(indata_v) as data:
        
            stats_vel = data['stats_m']
            deriv_vel = data['deriv_m']
        
            
        # Read user defined case-dependent parameters
        config = configparser.ConfigParser()
        config.read('case_params.ini')
        Re_b = float(config['Reynolds']['Re_b'])

        # Calculations
        #-------------
        Pe = Re_b*Pr
    
        theta, rho= np.meshgrid(phi, r, indexing='xy')
        nu = 1/Re_b
        u_t = my_math.get_utau(r, phi, deriv_vel, theta, nu)
        Re_t = u_t*1/nu
        y = 0.5 - r 
        y_plus = (y*u_t)/nu
        D = 1
        
        
        
        # Allocate variables for vectors and tensors
        #-------------------------------------------
        # Production variables
        Ui_cart = np.zeros((np.size(r), np.size(phi), 3))
        UiUj_cart = np.zeros((np.size(r), np.size(phi), 3, 3))
        uiuj_cart = np.zeros((np.size(r), np.size(phi), 3, 3))
        dUjdxi_cart= np.zeros((np.size(r), np.size(phi), 3, 3))
        Theta = np.zeros((np.size(r), np.size(phi)))
        dThetadxi_cart = np.zeros((np.size(r), np.size(phi), 3))
        UiTheta_cart = np.zeros((np.size(r), np.size(phi), 3))
        uitheta_cart = np.zeros((np.size(r), np.size(phi), 3))
        
        P_cart = np.zeros((np.size(r), np.size(phi), 3))
        P_cyl = np.zeros((np.size(r), np.size(phi), 3))
        P = np.zeros((np.size(r), 3))
        
        
        # Dissipation variables
        dThetadxk_dUidxk_cart = np.zeros((np.size(r), np.size(phi), 3))
        
        eps_cart = np.zeros((np.size(r), np.size(phi), 3))
        eps_cyl = np.zeros((np.size(r), np.size(phi), 3))
        eps = np.zeros((np.size(r), 3))
        
        
        # Molecular diffusion variables
        d2Ujdxk2_cart = np.zeros((np.size(r), np.size(phi), 3))
        d_ThetadUidxk_dxk_cart = np.zeros((np.size(r), np.size(phi), 3))
        d_thetaduidxk_dxk_cart = np.zeros((np.size(r), np.size(phi), 3))
        if (tempstats==33):
            d2_UiTheta_dxk2_cart = np.zeros((np.size(r), np.size(phi), 3))
            d2_uitheta_dxk2_cart = np.zeros((np.size(r), np.size(phi), 3))

        d2Thetadxi2 = np.zeros((np.size(r), np.size(phi)))
        d_UidThetadxk_dxk_cart = np.zeros((np.size(r), np.size(phi), 3))

        d_uidthetadxk_dxk_cart = np.zeros((np.size(r), np.size(phi), 3))
        
        MD_cart = np.zeros((np.size(r), np.size(phi), 3))
        MD_cyl = np.zeros((np.size(r), np.size(phi), 3))
        MD = np.zeros((np.size(r), 3))
        
        
        # Turbulent diffusion variables
        dUiUjdxk_cart = np.zeros((np.size(r), np.size(phi), 3, 3, 3))
        duiujdxk_cart = np.zeros((np.size(r), np.size(phi), 3, 3, 3))
        
        dThetaUiUkdxk_cart = np.zeros((np.size(r), np.size(phi), 3))
        d_Theta_Ui_Uk_dxk_cart = np.zeros((np.size(r), np.size(phi), 3))
        d_Theta_uiuk_dxk_cart = np.zeros((np.size(r), np.size(phi), 3))
        dUiThetadxk_cart = np.zeros((np.size(r), np.size(phi), 3, 3))
        duithetadxk_cart = np.zeros((np.size(r), np.size(phi), 3, 3))
        d_thetauk_Ui_dxk_cart = np.zeros((np.size(r), np.size(phi), 3))
        d_thetaui_Uk_dxk_cart = np.zeros((np.size(r), np.size(phi), 3))
        
        TD_cart = np.zeros((np.size(r), np.size(phi), 3))
        TD_cyl = np.zeros((np.size(r), np.size(phi), 3))
        TD = np.zeros((np.size(r), 3))
        
        
        # Temperature pressure gradient variables 
        dPThetadxi_cart = np.zeros((np.size(r), np.size(phi), 3))
        PdThetadxi_cart = np.zeros((np.size(r), np.size(phi), 3))
        ThetadPdxi_cart = np.zeros((np.size(r), np.size(phi), 3))
        dPdxi_cart = np.zeros((np.size(r), np.size(phi), 3))
    
        TPG_cart = np.zeros((np.size(r), np.size(phi), 3))
        TPG_cyl = np.zeros((np.size(r), np.size(phi), 3))
        TPG = np.zeros((np.size(r), 3))
    
    
        # Source term
        ThetaUiUj_cart = np.zeros((np.size(r), np.size(phi), 3, 3))
        ThetadUidxj_cart = np.zeros((np.size(r), np.size(phi), 3, 3))
        
    
        ## Production
        #------------
        # P = -<u_i u_k> d <Theta> / d x_k - <u_k theta> d <U_i> / d x_k
        
        Ui_cart[:, :, 0:3] = stats_vel[:, :, 0:3]       # <U_1>, <U_2>, <U_3>
        
        UiUj_cart[:, :, 0, 0] = stats_vel[:,:,4]            # <U_1 U_1>
        UiUj_cart[:, :, 0, 1] = stats_vel[:,:,8]            # <U_1 U_2>
        UiUj_cart[:, :, 0, 2] = stats_vel[:,:,10]           # <U_1 U_3>
        UiUj_cart[:, :, 1, 0] = UiUj_cart[:, :, 0, 1]   # <U_2 U_1>
        UiUj_cart[:, :, 1, 1] = stats_vel[:,:,5]            # <U_2 U_2>
        UiUj_cart[:, :, 1, 2] = stats_vel[:,:,9]            # <U_2 U_3>
        UiUj_cart[:, :, 2, 0] = UiUj_cart[:, :, 0, 2]   # <U_3 U_1>
        UiUj_cart[:, :, 2, 1] = UiUj_cart[:, :, 1, 2]   # <U_3 U_2>
        UiUj_cart[:, :, 2, 2] = stats_vel[:,:,6]            # <U_3 U_3>
        
        uiuj_cart = UiUj_cart - np.einsum('...i,...j', Ui_cart, Ui_cart)
        
        dUjdxi_cart[:, :, 0, 0] = deriv_vel[:,:,0]          # d<U_1> / d x_1
        dUjdxi_cart[:, :, 0, 1] = deriv_vel[:,:,1]          # d<U_1> / d x_2
        dUjdxi_cart[:, :, 0, 2] = 0                     # d<U_1> / d x_3
        dUjdxi_cart[:, :, 1, 0] = deriv_vel[:,:,2]          # d<U_2> / d x_1
        dUjdxi_cart[:, :, 1, 1] = deriv_vel[:,:,3]          # d<U_2> / d x_2
        dUjdxi_cart[:, :, 1, 2] = 0                     # d<U_2> / d x_3
        dUjdxi_cart[:, :, 2, 0] = deriv_vel[:,:,4]          # d<U_3> / d x_1
        dUjdxi_cart[:, :, 2, 1] = deriv_vel[:,:,5]          # d<U_3> / d x_2
        dUjdxi_cart[:, :, 2, 2] = 0                     # d<U_3> / d x_3
        
        
        Theta[:, :] = stats_temp[:, :, 0]                                     # <Theta>
        dThetadxi_cart[:, :, 0:2] = deriv_temp[:, :, 0:2]              # d <Theta> / d x_1, d <Theta> / d x_2
        UiTheta_cart[:, :, 0:3] = stats_temp[:, :, 2:5]                # <U_1 Theta>, <U_2 Theta>, <U_3 Theta>
        uitheta_cart[:, :, :] = UiTheta_cart[:, :, 0:3] - np.einsum('...i,...',Ui_cart, Theta[:, :])
        
        P_cart[:, :, :] = (- np.einsum('...ik,...k', uiuj_cart[:, :, :, :], dThetadxi_cart[:, :, :])
                - np.einsum('...k,...ik', uitheta_cart[:, :, :], dUjdxi_cart[:, :, :, :]))
        
        P_cyl[:, :, :] = my_math.transform_vect(theta, P_cart[:, :, :] , 1)
        
        # Average in circumferential direction:
        P[:, :] = np.mean(P_cyl[:, :, :], axis=1)
        
        
        
        
        ## Dissipation
        #-------------
        # eps = -a (1+Pr) < d theta / d x_k * d u_i / d x_k >
        #     = -a (1+Pr) ( < d Theta / d x_k * d U_i / d x_k > - d <Theta> / d x_k * d <U_i> / d x_k
        dThetadxk_dUidxk_cart[:, :, 0] = stats_temp[:, :, 30]                     # < d Theta / d x_1 * d U_1 / d x_1 > + < d Theta / d x_2 * d U_1 / d x_2 > + < d Theta / d x_3 * d U_1 / d x_3 >
        dThetadxk_dUidxk_cart[:, :, 1] = stats_temp[:, :, 31]                     # < d Theta / d x_1 * d U_2 / d x_1 > + < d Theta / d x_2 * d U_2 / d x_2 > + < d Theta / d x_3 * d U_2 / d x_3 >
        dThetadxk_dUidxk_cart[:, :, 2] = stats_temp[:, :, 32]                     # < d Theta / d x_1 * d U_3 / d x_1 > + < d Theta / d x_2 * d U_3 / d x_2 > + < d Theta / d x_3 * d U_3 / d x_3 >
        
        eps_cart[:, :, :] = - (1 + Pr)/Pe * (dThetadxk_dUidxk_cart[:, :, :] 
                - np.einsum('...k,...ik', dThetadxi_cart[:, :, :], dUjdxi_cart[:, :, :, :])
                )
        eps_cyl[:, :, :] = my_math.transform_vect(theta, eps_cart[:, :, :], 1)
        eps[:, :] = np.mean(eps_cyl[:, :, :], axis=1)
        
        
        
        
        
        
        ## Molecular diffusion
        #---------------------
        # MD = 1/Pe * d / d x_k ( < u_i * d theta / d x_k > + Pr < theta  * d u_i / d x_k > )
        # with  < theta * d u_i / d x_k > = < Theta * d U_i / d x_k > - <Theta> * d <U_i> / d x_k
        # d / d x_k ( < theta * d u_i / d x_k > ) = d / d x_k < Theta * d U_i / d x_k > - d <Theta> / d x_k * d <U_i> / d x_k - <Theta> * d**2 <U_i> / d x_k**2
        # and < ui * d theta / d x_k > = < Ui * d Theta / d x_k > - <U_i> * d <Theta> / d x_k
        # d / d x_k ( < u_i * d theta / d x_k > ) = d / d x_k < U_i * d Theta / d x_k > - d <U_i> / d x_k * d <Theta> / d x_k - <U_i> * d**2 <Theta> / d x_k**2
        # ...

        d2Ujdxk2_cart[:, :, 0] = deriv_vel[:, :, 44] + deriv_vel[:, :, 45]          # d^2 <U_1> / d x_k^2
        d2Ujdxk2_cart[:, :, 1] = deriv_vel[:, :, 46] + deriv_vel[:, :, 47]          # d^2 <U_2> / d x_k^2
        d2Ujdxk2_cart[:, :, 2] = deriv_vel[:, :, 48] + deriv_vel[:, :, 49]          # d^2 <U_3> / d x_k^2
        
        d_ThetadUidxk_dxk_cart[:, :, 0] = deriv_temp[:, :, 26] + deriv_temp[:, :, 27]      # d / d x_1 ( < Theta * d U_1 / d x_1 > ) + d / d x_2 ( < Theta * d U_1 / d x_2 > )
        d_ThetadUidxk_dxk_cart[:, :, 1] = deriv_temp[:, :, 28] + deriv_temp[:, :, 29]      # d / d x_1 ( < Theta * d U_2 / d x_1 > ) + d / d x_2 ( < Theta * d U_2 / d x_2 > )
        d_ThetadUidxk_dxk_cart[:, :, 2] = deriv_temp[:, :, 30] + deriv_temp[:, :, 31]      # d / d x_1 ( < Theta * d U_3 / d x_1 > ) + d / d x_2 ( < Theta * d U_3 / d x_2 > )
        d_thetaduidxk_dxk_cart[:, :, :] = (d_ThetadUidxk_dxk_cart[:, :, :] 
            - np.einsum('...k,...ik', dThetadxi_cart[:, :, :], dUjdxi_cart[:, :, :, :])
            - np.einsum('...,...i', Theta[:, :], d2Ujdxk2_cart[:, :, :])
            )
            
        # previous version which causes wiggles for MD of streamwise turb. heat flux budget at IF002 
        if (tempstats==33):
        # ... 
        # and < u_i * d theta / d x_k > = d < u_i theta > / d x_k - < theta * d u_i / d x_k >
            d2_UiTheta_dxk2_cart[:, :, 0] = deriv_temp[:, :, 20] + deriv_temp[:, :, 21]             # d2 < U_1 Theta > / d x_1 d x_1 + d2 < U_1 Theta > / d x_2 d x_2
            d2_UiTheta_dxk2_cart[:, :, 1] = deriv_temp[:, :, 22] + deriv_temp[:, :, 23]             # d2 < U_2 Theta > / d x_1 d x_1 + d2 < U_2 Theta > / d x_2 d x_2
            d2_UiTheta_dxk2_cart[:, :, 2] = deriv_temp[:, :, 24] + deriv_temp[:, :, 25]             # d2 < U_3 Theta > / d x_1 d x_1 + d2 < U_3 Theta > / d x_2 d x_2
            
            d2Thetadxi2[:, :] = deriv_temp[:, :, 16] + deriv_temp[:, :, 17]         # d**2 <Theta> / d x_1**2 + d**2 <Theta> / d x_2**2
            
            d2_uitheta_dxk2_cart[:, :, :] = (d2_UiTheta_dxk2_cart[:, :, :] 
                - np.einsum('...i,...', d2Ujdxk2_cart[:, :, :], Theta[:, :]) 
                - 2 * np.einsum('...ik,...k', dUjdxi_cart[:, :, :, :], dThetadxi_cart[:, :, :])
                - np.einsum('...i,...', Ui_cart[:, :, :], d2Thetadxi2[:, :])
                )
            
            d_uidthetadxk_dxk_cart[:, :, :] = d2_uitheta_dxk2_cart[:, :, :] - d_thetaduidxk_dxk_cart[:, :, :]
        # Use 42 temperature statistics and 52 derivatives
        else:
            d_UidThetadxk_dxk_cart[:, :, 0] = deriv_temp[:, :, 46] + deriv_temp[:, :, 47]       # d < U_1 d Theta / d x_1 > / d x_1 + d < U_1 d Theta / d x_2 > / d x_2
    
            d_UidThetadxk_dxk_cart[:, :, 1] = deriv_temp[:, :, 48] + deriv_temp[:, :, 49]       # d < U_2 d Theta / d x_1 > / d x_1 + d < U_2 d Theta / d x_2 > / d x_2
    
            d_UidThetadxk_dxk_cart[:, :, 2] = deriv_temp[:, :, 50] + deriv_temp[:, :, 51]       # d < U_3 d Theta / d x_1 > / d x_1 + d < U_3 d Theta / d x_2 > / d x_2
    
            d2Thetadxi2[:, :] = deriv_temp[:, :, 16] + deriv_temp[:, :, 17]         # d**2 <Theta> / d x_1**2 + d**2 <Theta> / d x_2**2
    
            d_uidthetadxk_dxk_cart[:, :, :] = (d_UidThetadxk_dxk_cart[:, :, :]
                    - np.einsum('...ik,...k', dUjdxi_cart[:, :, :, :], dThetadxi_cart[:, :, :])
                    - np.einsum('...i,...', Ui_cart[:, :, :], d2Thetadxi2[:, :])
                    )

        
        MD_cart[:, :, :] = 1/Pe * d_uidthetadxk_dxk_cart[:, :, :] + 1/Re_b * d_thetaduidxk_dxk_cart[:, :, :]
        
        MD_cyl[:, :, :] = my_math.transform_vect(theta, MD_cart[:, :, :], 1)
        MD[:, :] = np.mean(MD_cyl[:, :, :], axis=1)
        
        
        
        
        ## Turbulent diffusion
        #---------------------
        # TD = - d / d x_k  < theta u_i u_k >
        # d / d x_k < theta u_i u_k > = d / d x_k < Theta U_i U_k > - d / d x_k ( <Theta> <U_i> <U_k> ) - d / d x_k ( <Theta> < u_i u_k > )
        #                               - d / d x_k ( < theta u_k > <U_i> ) - d / d x_k ( < theta u_i > <U_k> )
        
        dUiUjdxk_cart[:, :, 0, 0, 0] = deriv_vel[:, :, 8]                   # d <U_1 U_1> / d x_1
        dUiUjdxk_cart[:, :, 0, 0, 1] = deriv_vel[:, :, 9]                   # d <U_1 U_1> / d x_2
        dUiUjdxk_cart[:, :, 0, 0, 2] = 0                                # d <U_1 U_1> / d x_3
        dUiUjdxk_cart[:, :, 0, 1, 0] = deriv_vel[:, :, 16]                  # d <U_1 U_2> / d x_1
        dUiUjdxk_cart[:, :, 0, 1, 1] = deriv_vel[:, :, 17]                  # d <U_1 U_2> / d x_2
        dUiUjdxk_cart[:, :, 0, 1, 2] = 0                                # d <U_1 U_2> / d x_3
        dUiUjdxk_cart[:, :, 0, 2, 0] = deriv_vel[:, :, 20]                  # d <U_1 U_3> / d x_1
        dUiUjdxk_cart[:, :, 0, 2, 1] = deriv_vel[:, :, 21]                  # d <U_1 U_3> / d x_2
        dUiUjdxk_cart[:, :, 0, 2, 2] = 0                                # d <U_1 U_3> / d x_3
        dUiUjdxk_cart[:, :, 1, 0, 0] = dUiUjdxk_cart[:, :, 0, 1, 0]     # d <U_2 U_1> / d x_1
        dUiUjdxk_cart[:, :, 1, 0, 1] = dUiUjdxk_cart[:, :, 0, 1, 1]     # d <U_2 U_1> / d x_2
        dUiUjdxk_cart[:, :, 1, 0, 2] = dUiUjdxk_cart[:, :, 0, 1, 2]     # d <U_2 U_1> / d x_3
        dUiUjdxk_cart[:, :, 1, 1, 0] = deriv_vel[:, :, 10]                  # d <U_2 U_2> / d x_1
        dUiUjdxk_cart[:, :, 1, 1, 1] = deriv_vel[:, :, 11]                  # d <U_2 U_2> / d x_2
        dUiUjdxk_cart[:, :, 1, 1, 2] = 0                                # d <U_2 U_2> / d x_3
        dUiUjdxk_cart[:, :, 1, 2, 0] = deriv_vel[:, :, 18]                  # d <U_2 U_3> / d x_1
        dUiUjdxk_cart[:, :, 1, 2, 1] = deriv_vel[:, :, 19]                  # d <U_2 U_3> / d x_2
        dUiUjdxk_cart[:, :, 1, 2, 2] = 0                                # d <U_2 U_3> / d x_3
        dUiUjdxk_cart[:, :, 2, 0, 0] = dUiUjdxk_cart[:, :, 0, 2, 0]     # d <U_3 U_1> / d x_1
        dUiUjdxk_cart[:, :, 2, 0, 1] = dUiUjdxk_cart[:, :, 0, 2, 1]     # d <U_3 U_1> / d x_2
        dUiUjdxk_cart[:, :, 2, 0, 2] = dUiUjdxk_cart[:, :, 0, 2, 2]     # d <U_3 U_1> / d x_3
        dUiUjdxk_cart[:, :, 2, 1, 0] = dUiUjdxk_cart[:, :, 1, 2, 0]     # d <U_3 U_2> / d x_1
        dUiUjdxk_cart[:, :, 2, 1, 1] = dUiUjdxk_cart[:, :, 1, 2, 1]     # d <U_3 U_2> / d x_2
        dUiUjdxk_cart[:, :, 2, 1, 2] = dUiUjdxk_cart[:, :, 1, 2, 2]     # d <U_3 U_2> / d x_3
        dUiUjdxk_cart[:, :, 2, 2, 0] = deriv_vel[:, :, 12]                  # d <U_3 U_3> / d x_1
        dUiUjdxk_cart[:, :, 2, 2, 1] = deriv_vel[:, :, 13]                  # d <U_3 U_3> / d x_2
        dUiUjdxk_cart[:, :, 2, 2, 2] = 0                                # d <U_3 U_3> / d x_3
        
        duiujdxk_cart = dUiUjdxk_cart - np.einsum('...ik, ...j', dUjdxi_cart, Ui_cart) - np.einsum('...jk, ...i', dUjdxi_cart, Ui_cart)
        
        dThetaUiUkdxk_cart[:, :, 0] = deriv_temp[:, :, 32] + deriv_temp[:, :, 39]                 # d < Theta U_1 U_1 > d x_1 + d < Theta U_1 U_2 > / d x_2
        dThetaUiUkdxk_cart[:, :, 1] = deriv_temp[:, :, 38] + deriv_temp[:, :, 35]                 # d < Theta U_2 U_1 > d x_1 + d < Theta U_2 U_2 > / d x_2
        dThetaUiUkdxk_cart[:, :, 2] = deriv_temp[:, :, 40] + deriv_temp[:, :, 43]                 # d < Theta U_3 U_1 > d x_1 + d < Theta U_3 U_2 > / d x_2
    
        d_Theta_Ui_Uk_dxk_cart[:, :, :] = ( 
                + np.einsum('...k,...i,...k', dThetadxi_cart[:, :, :], Ui_cart, Ui_cart)
                + np.einsum('...,...ik,...k', Theta[:, :], dUjdxi_cart, Ui_cart)
                    )
        
        d_Theta_uiuk_dxk_cart[:, :, :] = ( 
                + np.einsum('...k, ...ik', dThetadxi_cart[:, :, :], uiuj_cart)          
                + np.einsum('..., ...ikk', Theta[:, :], duiujdxk_cart)                   # dominant term
                )
        
        dUiThetadxk_cart[:, :, 0, 0] = deriv_temp[:, :, 4]             # d <U_1 Theta> / d x_1 
        dUiThetadxk_cart[:, :, 0, 1] = deriv_temp[:, :, 5]             # d <U_1 Theta> / d x_2 
        dUiThetadxk_cart[:, :, 1, 0] = deriv_temp[:, :, 6]             # d <U_2 Theta> / d x_1 
        dUiThetadxk_cart[:, :, 1, 1] = deriv_temp[:, :, 7]             # d <U_2 Theta> / d x_2 
        dUiThetadxk_cart[:, :, 2, 0] = deriv_temp[:, :, 8]             # d <U_3 Theta> / d x_1 
        dUiThetadxk_cart[:, :, 2, 1] = deriv_temp[:, :, 9]             # d <U_3 Theta> / d x_2 
        
        
        duithetadxk_cart[:, :, :, :] = ( dUiThetadxk_cart[:, :, :, :] 
                - np.einsum('...i,...k', Ui_cart, dThetadxi_cart[:, :, :]) 
                - np.einsum('...,...ik', Theta[:, :], dUjdxi_cart)
                )
        
        d_thetauk_Ui_dxk_cart[:, :, :] = ( np.einsum('...kk,...i', duithetadxk_cart[:, :, :, :], Ui_cart)
                + np.einsum('...k,...ik', uitheta_cart[:, :, :], dUjdxi_cart)
                )
        
        d_thetaui_Uk_dxk_cart[:, :, :] = np.einsum('...ik,...k', duithetadxk_cart[:, :, :, :], Ui_cart) + 0
        
        
        TD_cart[:, :, :] = - ( 
                + dThetaUiUkdxk_cart[:, :, :]           
                - d_Theta_Ui_Uk_dxk_cart[:, :, :] 
                - d_Theta_uiuk_dxk_cart[:, :, :]        
                - d_thetauk_Ui_dxk_cart[:, :, :]
                - d_thetaui_Uk_dxk_cart[:, :, :]
                )
        
        TD_cyl[:, :, :] = my_math.transform_vect(theta, TD_cart[:, :, :], 1)
        TD[:, :] = np.mean(TD_cyl[:, :, :], axis=1)
        
        
        
        ## Temperature pressure gradient
        #---------------------
        # TPG = - 1/rho * < theta * d p' / dx_i > = - 1/rho * ( < Theta * d P / d x_i > - <Theta> * d <P> / d x_i )
    
        dPThetadxi_cart[:, :, 0:2] = deriv_temp[:, :, 44:46]            # d < P * Theta > / d x_1, d < P * Theta > / d x_2
        PdThetadxi_cart[:, :, 0:3] = stats_temp[:, :, 6:9]              # < P * d Theta / d x_1 >, < P * d Theta / d x_2 > , < P * d Theta / d x_3 >
    
        ThetadPdxi_cart =  dPThetadxi_cart - PdThetadxi_cart
        
        dPdxi_cart[:, :, 0:2] = deriv_vel[:, :, 6:8]                    # d <P> / d x_1, d <P> / d x_2
    
        TPG_cart = -1/1 * (
                + ThetadPdxi_cart
                - np.einsum('..., ...i', Theta, dPdxi_cart)
                )
        
        TPG_cyl[:, :, :] = my_math.transform_vect(theta, TPG_cart[:, :, :], 1)
        TPG[:, :] = np.mean(TPG_cyl[:, :, :], axis=1)
    
        ## Source term (analoguous to Piller for turb. heat flux)
        #--------------------------------------------------------
        # S = 4 * < u_z u_i >       # for IF-i & IF-m
        # S = ahat * ( <Theta> * < u_z * u_i > + <U_z> * < theta * u_i > + < theta * u_i * u_z > )
        #      - 1/Pe * ( 2 * ahat * < d theta / d x_3 * u_i > - ahat**2 * < theta * u_i > )
        # < d theta / d x_3 * u_i > = d < theta * u_i > / d x_3 - < theta d u_i / d x_3 >
        #                           =           0               - < theta d u_i / d x_3 >
    
        uiuj_cyl = my_math.transform_vect(theta, uiuj_cart, 2)
        uitheta_cyl = my_math.transform_vect(theta, uitheta_cart, 1)
    
        ThetaUiUj_cart[..., 0, 0] = stats_temp[..., 22]
        ThetaUiUj_cart[..., 1, 1] = stats_temp[..., 23]
        ThetaUiUj_cart[..., 2, 2] = stats_temp[..., 24]
        ThetaUiUj_cart[..., 0, 1] = stats_temp[..., 25]
        ThetaUiUj_cart[..., 0, 2] = stats_temp[..., 26]
        ThetaUiUj_cart[..., 1, 2] = stats_temp[..., 27]
        
        thetauiuj_cart = (ThetaUiUj_cart 
                - np.einsum('..., ...i, ...j', Theta, Ui_cart, Ui_cart)
                - np.einsum('..., ...ij', Theta, uiuj_cart)
                - np.einsum('...i, ...j', Ui_cart, uitheta_cart)
                - np.einsum('...i, ...j', uitheta_cart, Ui_cart)
                )
    
        thetauiuj_cyl = my_math.transform_vect(theta, thetauiuj_cart, 2)
    
        ThetadUidxj_cart[:, :, 0, 0] = stats_temp[..., 9]
        ThetadUidxj_cart[:, :, 0, 1] = stats_temp[..., 10]
        ThetadUidxj_cart[:, :, 0, 2] = stats_temp[..., 11]
        ThetadUidxj_cart[:, :, 1, 0] = stats_temp[..., 12]
        ThetadUidxj_cart[:, :, 1, 1] = stats_temp[..., 13]
        ThetadUidxj_cart[:, :, 1, 2] = stats_temp[..., 14]
        ThetadUidxj_cart[:, :, 2, 0] = stats_temp[..., 15]
        ThetadUidxj_cart[:, :, 2, 1] = stats_temp[..., 16]
        ThetadUidxj_cart[:, :, 2, 2] = stats_temp[..., 17]
    
        thetaduidxj_cart = (ThetadUidxj_cart
                - np.einsum('..., ...ij', Theta, dUjdxi_cart)
                )
    
        thetaduidxj_cyl = my_math.transform_vect(theta, thetaduidxj_cart, 2)
    
        
        def source_term(ahat):
            """ This function calculates the additional term arising in the budget because of the
            source term introduced in the energy equation.
    
            ahat:   parameter in source term (see Piller 2005)
            """
            Source = ahat * ( 
                    + np.einsum('..., ...i', Theta, uiuj_cyl[..., 2])
                    + np.einsum('..., ...i', Ui_cart[..., 2], uitheta_cyl)
                    + thetauiuj_cyl[..., 2]
                    ) - 1/Pe * (
                            2 * ahat * ( -thetaduidxj_cyl[..., 2] )
                            - ahat**2 * uitheta_cyl
                            )
            return Source
    
        # Read the previously calculated Nusselt number to determine an averaged ahat
        try:
            Nu = np.loadtxt('./nusselt/' + timerange_select + '/nuss.dat')
        except OSError:
            print('Error: Nusselt numbers are needed for ahat (source term)!')
            raise
        
        if ("IT071" in outdata):
            ahat1 = Nu[0]/Pe * 4/D
            Source = source_term(ahat1)
            S = np.mean(Source, axis=1)
                
        elif ("IT0025" in outdata):
            ahat2 = Nu[3]/Pe * 4/D
            Source = source_term(ahat2)
            S = np.mean(Source, axis=1)
    
        else:
            Source = 4 * uiuj_cyl[..., 2, :] 
            S = np.mean(Source, axis = 1)
    
    
        # Turbulent heat flux: <u_r theta>
        file_path = outdata + '_rtheta.dat'
        directory = os.path.dirname(file_path)
        if not os.path.exists(directory):
            os.makedirs(directory)
    
    
        np.savetxt(file_path, 
                np.transpose([
                    np.flipud(y_plus), 
                    np.flipud(P[:, 0]*nu/u_t**2),
                    np.flipud(eps[:, 0]*nu/u_t**2),
                    np.flipud(MD[:, 0]*nu/u_t**2),
                    np.flipud(TD[:, 0]*nu/u_t**2),
                    np.flipud(TPG[:, 0]*nu/u_t**2),
                    np.flipud(S[:, 0]*nu/u_t**2),
                    ]), header='y_plus\tProduction <P>+\tDissipation <eps>+\t Molecular diffusion<MD>+\tTurbulent diffusion <TD>+\tTemperature pressure gradient <TPG>+\tSource term contribution <S>+')
    
        # Turbulent heat flux: <u_z theta>
        file_path = outdata + '_ztheta.dat'
        directory = os.path.dirname(file_path)
        if not os.path.exists(directory):
            os.makedirs(directory)
    
    
        np.savetxt(file_path, 
                np.transpose([
                    np.flipud(y_plus), 
                    np.flipud(P[:, 2]*nu/u_t**2),
                    np.flipud(eps[:, 2]*nu/u_t**2),
                    np.flipud(MD[:, 2]*nu/u_t**2),
                    np.flipud(TD[:, 2]*nu/u_t**2),
                    np.flipud(TPG[:, 2]*nu/u_t**2),
                    np.flipud(S[:, 2]*nu/u_t**2),
                    ]), header='y_plus\tProduction <P>+\tDissipation <eps>+\t Molecular diffusion<MD>+\tTurbulent diffusion <TD>+\tTemperature pressure gradient <TPG>+\tSource term contribution <S>+')

        data_collect_rtheta.append((
                np.transpose([
                    np.flipud(y_plus), 
                    np.flipud(P[:, 0]*nu/u_t**2),
                    np.flipud(eps[:, 0]*nu/u_t**2),
                    np.flipud(MD[:, 0]*nu/u_t**2),
                    np.flipud(TD[:, 0]*nu/u_t**2),
                    np.flipud(TPG[:, 0]*nu/u_t**2),
                    np.flipud(S[:, 0]*nu/u_t**2),
                    ])
                    ))

        data_collect_ztheta.append((
                np.transpose([
                    np.flipud(y_plus), 
                    np.flipud(P[:, 2]*nu/u_t**2),
                    np.flipud(eps[:, 2]*nu/u_t**2),
                    np.flipud(MD[:, 2]*nu/u_t**2),
                    np.flipud(TD[:, 2]*nu/u_t**2),
                    np.flipud(TPG[:, 2]*nu/u_t**2),
                    np.flipud(S[:, 2]*nu/u_t**2),
                    ])
                    ))


    # Write maximum and RMS residual to a separate file
    #--------------------------------------------------
    data_collect_rtheta = np.asarray(data_collect_rtheta)
    y_plus_flip_rtheta = data_collect_rtheta[:, :, 0]    
    residual_flip_rtheta = np.sum(data_collect_rtheta[:, :, 1:], axis=2)
    eps_max_rtheta = np.max(np.abs(residual_flip_rtheta), axis=1)
    eps_rms_rtheta = (1/y_plus_flip_rtheta[:, -1] * np.trapz(residual_flip_rtheta**2, y_plus_flip_rtheta))**0.5

    data_collect_ztheta = np.asarray(data_collect_ztheta)
    y_plus_flip_ztheta = data_collect_ztheta[:, :, 0]    
    residual_flip_ztheta = np.sum(data_collect_ztheta[:, :, 1:], axis=2)
    eps_max_ztheta = np.max(np.abs(residual_flip_ztheta), axis=1)
    eps_rms_ztheta = (1/y_plus_flip_ztheta[:, -1] * np.trapz(residual_flip_ztheta**2, y_plus_flip_ztheta))**0.5


    file_path = 'profiles/' + timerange_select + '/resid_vtbudget_max_rms.dat'
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)

    labels = (
            'IT071_rtheta',
            'MBC071_rtheta',
            'IF071_rtheta',
            'IT0025_rtheta',       
            'MBC0025_rtheta',
            'IF0025_rtheta',
            'IT071_ztheta',
            'MBC071_ztheta',
            'IF071_ztheta',
            'IT0025_ztheta',       
            'MBC0025_ztheta',
            'IF0025_ztheta',
            )

    header1 = ' '.join(labels)

    np.savetxt(file_path,
            np.transpose([
                np.concatenate((eps_max_rtheta, eps_max_ztheta)), 
                np.concatenate((eps_rms_rtheta, eps_rms_ztheta))
                ]),
            header='Rows: ' + header1 + '\nCols: eps_max\teps_rms')


 # Call function
def run(tempstats=42):
    """ Convert npz data to text files.

    tempstats : Number of collected temperature statistics
        old version (33), new version (42)
    
    """

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    for span in timeranges_select:
        save_vtbudget(span, tempstats)
