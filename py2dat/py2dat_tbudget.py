#!/usr/bin/env python3.5
#-----------------------
# This script reads previously saved data 
# and saves  profiles 
# of the temperature budgets
# in a simple text file.
# ----------------------------------------------------
# Author:   Steffen Straub
# Date  :   2018/02/28
#-----------------------------------------------------

import numpy as np
from ..misc import my_math
import os

import configparser

import pdb

# Define function
def save_tbudget(timerange_select):

    indata_v = 'stats_' + timerange_select + '/statistics_matrix_tv1.npz'
    indata_t_list = (
            'stats_' + timerange_select + '/statistics_matrix_tt1.npz', 
            'stats_' + timerange_select + '/statistics_matrix_tt2.npz', 
            'stats_' + timerange_select + '/statistics_matrix_tt3.npz', 
            'stats_' + timerange_select + '/statistics_matrix_tt4.npz', 
            'stats_' + timerange_select + '/statistics_matrix_tt5.npz', 
            'stats_' + timerange_select + '/statistics_matrix_tt6.npz', 
            )
    Pr_list = (
            0.71,
            0.71,
            0.71,
            0.025,
            0.025,
            0.025,
            )
    data_name = (
            '/temp_budget_IT071.dat',
            '/temp_budget_MBC071.dat',
            '/temp_budget_IF071.dat',
            '/temp_budget_IT0025.dat',       
            '/temp_budget_MBC0025.dat',
            '/temp_budget_IF0025.dat',
            )
    outdata_list = ['profiles/' + timerange_select + '/' + name for name in data_name]

    data_collect = []
    for i in range(len(indata_t_list)):
        indata_t = indata_t_list[i]
        Pr = Pr_list[i]
        outdata = outdata_list[i]

        print("Processing ", indata_t)
        ## Load my data
        #--------------
        # 1) Temperature statistics
        with np.load(indata_t) as data:
        
            # Coordinates
            X = data['X']
            Y = data['Y']
        
            # individual radial and circumferential distribution
            r = data['r']
            phi = data['phi']
        
            # runtime averages and postprocessing derivatives
            stats_temp = data['stats_m']
            deriv_temp = data['deriv_m']
        
        # 2) Velocity statistics
            with np.load(indata_v) as data:
                    
                stats_vel = data['stats_m']
                deriv_vel = data['deriv_m']
    
    
        # Read user defined case-dependent parameters
        config = configparser.ConfigParser()
        config.read('case_params.ini')
        Re_b = float(config['Reynolds']['Re_b'])

        # Calculations
        #-------------
        Pe = Re_b*Pr
    
        theta, rho= np.meshgrid(phi, r, indexing='xy')
        nu = 1/Re_b
        u_t = my_math.get_utau(r, phi, deriv_vel, theta, nu)
        Re_t = u_t*1/nu
        y = 0.5 - r 
        y_plus = (y*u_t)/nu
        D = 1
     
        
        # Allocate variables for vectors and tensors
        #-------------------------------------------
        # Production variables
        Ui_cart = np.zeros((np.size(r), np.size(phi), 3))
        Theta = np.zeros((np.size(r), np.size(phi)))
        dThetadxi_cart = np.zeros((np.size(r), np.size(phi), 3))
        UiTheta_cart = np.zeros((np.size(r), np.size(phi), 3))
        uitheta_cart = np.zeros((np.size(r), np.size(phi), 3))
        P_full = np.zeros((np.size(r), np.size(phi)))
        P = np.zeros((np.size(r)))
        
        # Dissipation variables
        dThetadxi_dThetadxi = np.zeros((np.size(r), np.size(phi)))
        eps_full = np.zeros((np.size(r), np.size(phi)))
        eps = np.zeros((np.size(r)))
        
        # Molecular diffusion variables
        d2Thetadxi2 = np.zeros((np.size(r), np.size(phi)))
        d2ThetaThetadxi2 = np.zeros((np.size(r), np.size(phi)))
        d2Theta_Thetadxi2 = np.zeros((np.size(r), np.size(phi)))
        d2thetathetadxi2 = np.zeros((np.size(r), np.size(phi)))
        MD_full = np.zeros((np.size(r), np.size(phi)))
        MD = np.zeros((np.size(r)))
        
        # Turbulent diffusion variables
        dUiThetaThetadxi = np.zeros((np.size(r), np.size(phi)))
        dUi_Theta_Thetadxi = np.zeros((np.size(r), np.size(phi)))
        dThetaThetadxi_cart = np.zeros((np.size(r), np.size(phi), 3))
        dthetathetadxi_cart = np.zeros((np.size(r), np.size(phi), 3))
        dUi_thetathetadxi = np.zeros((np.size(r), np.size(phi)))
        dUiThetadxi = np.zeros((np.size(r), np.size(phi)))
        duithetadxi = np.zeros((np.size(r), np.size(phi)))
        duitheta_Thetadxi = np.zeros((np.size(r), np.size(phi)))
        TD_full = np.zeros((np.size(r), np.size(phi)))
        TD = np.zeros((np.size(r)))
        
        # Source term
        S = np.zeros((np.size(r)))
    
        ## Production
        #------------
        # P_theta = - < u_i theta > * d <Theta> / d x_i
        # < u_i theta > = < U_i Theta > - <U_i> * <Theta>
        Ui_cart[:, :, 0:3] = stats_vel[:, :, 0:3]                                       # <U_1>, <U_2>, <U_3>
        
        Theta[:, :] = stats_temp[:, :, 0]                                     # <Theta>
        dThetadxi_cart[:, :, 0:2] = deriv_temp[:, :, 0:2]              # d <Theta> / d x_1, d <Theta> / d x_2
        UiTheta_cart[:, :, 0:3] = stats_temp[:, :, 2:5]                # <U_1 Theta>, <U_2 Theta>, <U_3 Theta>
        uitheta_cart[:, :, :] = UiTheta_cart[:, :, 0:3] - np.einsum('...i,...',Ui_cart, Theta[:, :])
        
        P_full[:, :] = - np.einsum('...i,...i', uitheta_cart[:, :, :], dThetadxi_cart[:, :, :])
        P = np.mean(P_full[:, :], axis=1)
        
        
        ## Dissipation
        #-------------
        # eps_theta = - k/(rho C_p) < d theta / d x_i * d theta / d x_i >
        # k/(rho C_p) = a = alpha = 1/Pe = 1/(Re * Pr)
        # < d theta / d x_i * d theta / d x_i > = < d Theta / d x_i * d Theta / d x_i > - < d Theta / d x_i > * d Theta / d x_i >
        dThetadxi_dThetadxi[:, :] = stats_temp[:, :, 29]
        
        eps_full[:, :] = - 1/Pe * ( dThetadxi_dThetadxi[:, :] - np.einsum('...i,...i', dThetadxi_cart[:, :, :], dThetadxi_cart[:, :, :]) )
        eps = np.mean(eps_full[:, :], axis=1)
        
        
        ## Molecular diffusion
        #---------------------
        # MD = 1/2 * 1/Pe * d**2 < theta theta> / d x_i d x_i
        # d**2 < theta theta > / d x_i**2 = d**2 <Theta Theta> / d x_i**2 - d**2 <Theta><Theta> / d x_i**2
        # d**2 <Theta><Theta> / d x_i**2 = 2 * d <Theta> / d x_i * d <Theta> / d x_i + 2 * <Theta> * d**2 <Theta> / d x_i**2
        d2Thetadxi2[:, :] = deriv_temp[:, :, 16] + deriv_temp[:, :, 17]         # d**2 <Theta> / d x_1**2 + d**2 <Theta> / d x_2**2
        d2ThetaThetadxi2[:, :] = deriv_temp[:, :, 18] + deriv_temp[:, :,19]     # d**2 <ThetaTheta> / d x_1**2 + d**2 <ThetaTheta> / d x_2**2 
        d2Theta_Thetadxi2[:, :] = 2 * ( np.einsum('...i,...i', dThetadxi_cart[:, :, :], dThetadxi_cart[:, :, :]) 
                + Theta[:, :] * d2Thetadxi2[:, :] )
        d2thetathetadxi2[:, :] = d2ThetaThetadxi2[:, :] - d2Theta_Thetadxi2[:, :]
        
        MD_full[:, :] = 1/2 * 1/Pe * d2thetathetadxi2[:, :]
        MD = np.mean(MD_full[:, :], axis=1)
        
        
        ## Turbulent diffusion
        #---------------------
        # TD = - 1/2 * d < u_i theta theta > / d x_i
        # d < ui theta theta > d x_i = d < U_i Theta Theta > / d x_i - d <U_i> <Theta> <Theta> / d x_i - d <U_i> <theta theta> / d x_i - 2 d <u_i theta> <Theta> / d x_i
        dUiThetaThetadxi[:, :] = deriv_temp[:, :, 10] + deriv_temp[:, :, 13]     # d <U_1 Theta Theta> / d x_1 + d <U_2 Theta Theta> / d x_2
        
        # d <U_i> <Theta> <Theta> = d <U_i> / d x_i <Theta> <Theta> + 2 <U_i> <Theta> d <Theta> / d x_i
        # d <U_i> <Theta> <Theta> = 0                               + 2 <U_i> <Theta> d <Theta> / d x_i
        dUi_Theta_Thetadxi[:, :] = 2 * np.einsum('...i,...i', Ui_cart, dThetadxi_cart[:, :, :]) * Theta[:, :]
        
        # d <U_i> <theta theta> / d x_i = <U_i> d <theta theta> / d x_i
        # d <theta theta> / d x_i = d <Theta Theta> / d x_i - 2 <Theta> d <Theta> / d x_i
        dThetaThetadxi_cart[:, :, 0:2] = deriv_temp[:, :, 2:4]                       # d <Theta Theta> / d x_i
        dthetathetadxi_cart[:, :, :] = dThetaThetadxi_cart[:, :, :] - 2 * np.einsum('...,...i', Theta[:, :], dThetadxi_cart[:, :, :])
        dUi_thetathetadxi[:, :] = np.einsum('...i,...i', Ui_cart, dthetathetadxi_cart[:, :, :])
        
        # d <u_i theta> <Theta> / d x_i = d <u_i theta> / d x_i <Theta> + <u_i theta> d <Theta> / d x_i
        # d <u_i theta> / d x_i = d <U_i Theta> / d x_i - 0 - <U_i> d <Theta> / d x_i
        dUiThetadxi[:, :] = deriv_temp[:, :, 4] + deriv_temp[:, :, 7]             # d <U_1 Theta> / d x_1 + d <U_2 Theta> / d x_2
        duithetadxi[:, :] = dUiThetadxi[:, :] - np.einsum('...i,...i', Ui_cart, dThetadxi_cart[:, :, :])
        
        duitheta_Thetadxi[:, :] = duithetadxi[:, :] * Theta[:, :] + np.einsum('...i,...i', uitheta_cart[:, :, :], dThetadxi_cart[:, :, :])
        
        TD_full[:, :] = - 1/2 * ( 
                + dUiThetaThetadxi[:, :]
                - dUi_Theta_Thetadxi[:, :]
                - dUi_thetathetadxi[:, :]
                - 2*duitheta_Thetadxi[:, :]
                )
        
        TD = np.mean(TD_full[:, :], axis=1)
    
        ## Source term (see Piller)
        #--------------------------
        # S = 4 * < u_z theta >         # for IF-i & IF-m
        # S = ahat * ( 2 <U_z> * k_theta + <Theta> * < u_z * theta > + < u_z * theta * theta > )
        #       - 2/Pe * d k_theta / d x_3 + 2/Pe * ahat * k_theta )
        ThetaTheta = stats_temp[..., 1]                                     # <Theta>
        thetatheta = ThetaTheta - Theta**2
        UzThetaTheta_cart = stats_temp[..., 21]
        uzthetatheta_cart = (UzThetaTheta_cart
                - Ui_cart[..., 2]*Theta*Theta 
                - Ui_cart[..., 2]*thetatheta 
                - Theta*2*uitheta_cart[..., 2]
                )
    
        def source_term(ahat):
            """ This function calculates the additional term arising in the budget because of the
            source term introduced in the energy equation.
    
            ahat:   parameter in source term (see Piller 2005)
            """
            Source = ahat * ( Theta*uitheta_cart[..., 2] 
                    + Ui_cart[..., 2]*thetatheta
                    + uzthetatheta_cart
                    ) - 1/Pe * (
                            ahat*dthetathetadxi_cart[..., 2]
                            - ahat**2 * thetatheta
                            )
            return Source
    
        # Read the previously calculated Nusselt number to determine an averaged ahat
        try:
            Nu = np.loadtxt('./nusselt/' + timerange_select + '/nuss.dat')
        except OSError:
            print('Error: Nusselt numbers are needed for ahat (source term)!')
            raise

        if ("IT071" in outdata):        # IT
            ahat1 = Nu[0]/Pe * 4/D
            Source = source_term(ahat1)
            S = np.mean(Source, axis=1)
                
        elif ("IT0025" in outdata):
            ahat2 = Nu[3]/Pe * 4/D
            Source = source_term(ahat2)
            S = np.mean(Source, axis=1)
    
        else:
            S = np.mean(4*uitheta_cart[:, :, 2], axis=1)
       
    
        file_path = outdata
        directory = os.path.dirname(file_path)
        if not os.path.exists(directory):
            os.makedirs(directory)
        
        np.savetxt(file_path, 
                np.transpose([
                    np.flipud(y_plus), 
                    np.flipud(P*nu),
                    np.flipud(eps*nu),
                    np.flipud(MD*nu),
                    np.flipud(TD*nu),
                    np.flipud(S*nu),
                    ]), header='y_plus\tProduction <P>+\tDissipation <eps>+\t Molecular diffusion<MD>+\tTurbulent diffusion <TD>+\tSource term contribution <S>+')

        data_collect.append((
                np.transpose([
                    np.flipud(y_plus), 
                    np.flipud(P*nu),
                    np.flipud(eps*nu),
                    np.flipud(MD*nu),
                    np.flipud(TD*nu),
                    np.flipud(S*nu),
                    ])
                    ))

    # Write maximum and RMS residual to a separate file
    #--------------------------------------------------
    data_collect = np.asarray(data_collect)

    y_plus_flip = data_collect[:, :, 0]    
    residual_flip = np.sum(data_collect[:, :, 1:], axis=2)
    eps_max = np.max(np.abs(residual_flip), axis=1)

    eps_rms = (1/y_plus_flip[:, -1] * np.trapz(residual_flip**2, y_plus_flip))**0.5

    file_path = 'profiles/' + timerange_select + '/resid_tbudget_max_rms.dat'
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)

    labels = [ data_name[i][13:-4] for i in range(len(data_name))]
    header1 = ' '.join(labels)

    np.savetxt(file_path,
            np.transpose([eps_max, 
                eps_rms]),
            header='Rows: ' + header1 + '\nCols: eps_max\teps_rms')



# Call function
def run():
    """ Convert npz data to text files."""

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    for span in timeranges_select:
        save_tbudget(span)
