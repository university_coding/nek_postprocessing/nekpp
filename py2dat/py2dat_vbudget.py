#!/usr/bin/env python3.5
#-----------------------
# This script reads previously saved data 
# and saves  profiles 
# of velocity budgets (turbulent kinetic energy and Reynolds stresses)
# in a simple text file.
# ----------------------------------------------------
# Author:   Steffen Straub
# Date  :   2018/02/28
#-----------------------------------------------------

import numpy as np
from ..misc import my_math
import os

import configparser

import pdb


# Define function
def save_vbudget(timerange_select):

    indata_v = 'stats_' + timerange_select + '/statistics_matrix_tv1.npz'
    data_name = (
            'v_budget_k.dat',
            'v_budget_rr.dat',
            'v_budget_tt.dat',
            'v_budget_zz.dat',
            'v_budget_rz.dat',
            )
    outdata = ['profiles/' + timerange_select + '/' + name for name in data_name]
#    outdata = (
#            'profiles/' + timerange_select + '/v_budget_k.dat',
#            'profiles/' + timerange_select + '/v_budget_rr.dat',
#            'profiles/' + timerange_select + '/v_budget_tt.dat',
#            'profiles/' + timerange_select + '/v_budget_zz.dat',
#            'profiles/' + timerange_select + '/v_budget_rz.dat',
#            )

    print("Processing ", indata_v)
    # Load my data
    #-------------
    with np.load(indata_v) as data:
    
        # Coordinates
        X = data['X']
        Y = data['Y']
    
        # individual radial and circumferential distribution
        r = data['r']
        phi = data['phi']
    
        # runtime averages and postprocessing derivatives
        stats_vel = data['stats_m']
        deriv_vel = data['deriv_m']
    
    # Read user defined case-dependent parameters
    config = configparser.ConfigParser()
    config.read('case_params.ini')
    Re_b = float(config['Reynolds']['Re_b'])
   
    ## Calculations
    #-------------
    theta, rho= np.meshgrid(phi, r, indexing='xy')
    nu = 1/Re_b
    u_t = my_math.get_utau(r, phi, deriv_vel, theta, nu)
    Re_t = u_t*1/nu
    y = 0.5 - r 
    y_plus = (y*u_t)/nu
    
   
    # Allocate variables for vectors and 2nd order tensors
    #-----------------------------------------------------
    # Mean velocity vector
    Ui_cart = np.zeros((np.size(r), np.size(phi), 3))
    Ui_cyl = np.zeros((np.size(r), np.size(phi), 3))
    Ui = np.zeros((np.size(r), 3))
    
    # Mean pressure (scalar)
    p_full = np.zeros((np.size(r), np.size(phi)))
    p = np.zeros(np.size(r))
    
    # Mean velocity correlation tensor
    UiUj_cart = np.zeros((np.size(r), np.size(phi), 3, 3))
    UiUj_cyl = np.zeros((np.size(r), np.size(phi), 3, 3))
    UiUj = np.zeros((np.size(r), 3, 3))
    # Reynolds stress tensor
    uiuj_cart = np.zeros((np.size(r), np.size(phi), 3, 3))
    uiuj_cyl = np.zeros((np.size(r), np.size(phi), 3, 3))
    uiuj = np.zeros((np.size(r), 3, 3))
    
    # Gradient of the mean velocity
    dUjdxi_cart= np.zeros((np.size(r), np.size(phi), 3, 3))
    dUjdxi_cyl= np.zeros((np.size(r), np.size(phi), 3, 3))
    dUjdxi= np.zeros((np.size(r), 3, 3))
    
    # Production tensor
    P_cart = np.zeros((np.size(r), np.size(phi), 3, 3))
    P_cyl = np.zeros((np.size(r), np.size(phi), 3, 3))
    P = np.zeros((np.size(r), np.size(phi), 3))
    
    # Dissipation tensor
    eps_cart = np.zeros((np.size(r), np.size(phi), 3, 3))
    eps_cyl = np.zeros((np.size(r), np.size(phi), 3, 3))
    eps = np.zeros((np.size(r), np.size(phi), 3))
    
    # Velocity-pressure gradient tensor
    dpuidxj_cart = np.zeros((np.size(r), np.size(phi), 3, 3))
    pduidxj_cart = np.zeros((np.size(r), np.size(phi), 3, 3))
    Pi_cart = np.zeros((np.size(r), np.size(phi), 3, 3))
    Pi_cyl = np.zeros((np.size(r), np.size(phi), 3, 3))
    Pi = np.zeros((np.size(r), np.size(phi), 3))
    
    # Viscous diffusion tensor
    d2UiUjdxk2_cart = np.zeros((np.size(r), np.size(phi), 3, 3))
    d2Ujdxk2_cart = np.zeros((np.size(r), np.size(phi), 3))
    d2uiujdxk2_cart = np.zeros((np.size(r), np.size(phi), 3, 3))
    D_cart = np.zeros((np.size(r), np.size(phi), 3, 3))
    D_cyl = np.zeros((np.size(r), np.size(phi), 3, 3))
    D = np.zeros((np.size(r), np.size(phi), 3))
    
    # Turbulent velocity related diffusion tensor
    dUiUjUkdxk_cart = np.zeros((np.size(r), np.size(phi), 3, 3))
    dUiUjdxk_cart = np.zeros((np.size(r), np.size(phi), 3, 3, 3))
    duiujdxk_cart = np.zeros((np.size(r), np.size(phi), 3, 3, 3))
    duiujukdxk_cart = np.zeros((np.size(r), np.size(phi), 3, 3))
    T_cart = np.zeros((np.size(r), np.size(phi), 3, 3))
    T_cyl = np.zeros((np.size(r), np.size(phi), 3, 3))
    T = np.zeros((np.size(r), np.size(phi), 3))
    
    
    # Mean velocities <U_i>
    #----------------------
    Ui_cart = stats_vel[:, :, 0:3]
    Ui_cyl = my_math.transform_vect(theta, Ui_cart, 1)
                
    # Average in circumferential direction:
    Ui = np.mean(Ui_cyl, axis=1)
    
    
    # Mean pressure <p>
    #------------------
    p_full = stats_vel[:, :, 3]
    p = np.mean(stats_vel[:,:,3], axis=1)
    
    
    # <U_i U_j>
    #----------
    UiUj_cart[:, :, 0, 0] = stats_vel[:,:,4]
    UiUj_cart[:, :, 0, 1] = stats_vel[:,:,8]
    UiUj_cart[:, :, 0, 2] = stats_vel[:,:,10]
    UiUj_cart[:, :, 1, 0] = UiUj_cart[:, :, 0, 1]
    UiUj_cart[:, :, 1, 1] = stats_vel[:,:,5]
    UiUj_cart[:, :, 1, 2] = stats_vel[:,:,9]
    UiUj_cart[:, :, 2, 0] = UiUj_cart[:, :, 0, 2]
    UiUj_cart[:, :, 2, 1] = UiUj_cart[:, :, 1, 2]
    UiUj_cart[:, :, 2, 2] = stats_vel[:,:,6]
    
    UiUj_cyl = my_math.transform_vect(theta, UiUj_cart, 2)
    
    # Average in circumferential direction:
    UiUj = np.mean(UiUj_cyl, axis=1)
    
    
    # <u_i u_j> Reynolds stress
    #--------------------------
    uiuj_cart = UiUj_cart - np.einsum('...i,...j', Ui_cart, Ui_cart)
    
    uiuj_cyl = my_math.transform_vect(theta, uiuj_cart, 2)
    
    # Average in circumferential direction:
    uiuj = np.mean(uiuj_cyl, axis=1)
    
    
    
    # Derivatives
    #------------
    # d <U_j> / d x_i
    #----------------
    dUjdxi_cart[:, :, 0, 0] = deriv_vel[:,:,0] # d<U_1> / d x_1
    dUjdxi_cart[:, :, 0, 1] = deriv_vel[:,:,1] # d<U_1> / d x_2
    dUjdxi_cart[:, :, 0, 2] = 0            # d<U_1> / d x_3
    dUjdxi_cart[:, :, 1, 0] = deriv_vel[:,:,2] # d<U_2> / d x_1
    dUjdxi_cart[:, :, 1, 1] = deriv_vel[:,:,3] # d<U_2> / d x_2
    dUjdxi_cart[:, :, 1, 2] = 0            # d<U_2> / d x_3
    dUjdxi_cart[:, :, 2, 0] = deriv_vel[:,:,4] # d<U_3> / d x_1
    dUjdxi_cart[:, :, 2, 1] = deriv_vel[:,:,5] # d<U_3> / d x_2
    dUjdxi_cart[:, :, 2, 2] = 0            # d<U_3> / d x_3
    
    dUjdxi_cyl = my_math.transform_vect(theta, dUjdxi_cart, 2)
    
    # Average in circumferential direction:
    dUjdxi = np.mean(dUjdxi_cyl, axis=1)
    
    # Production tensor (Pope, eq. 7.179)
    #------------------------------------
    # P_ij = -<u_i u_k> * d_part <U_j> / d_part x_k  -  <u_j u_k> d_part <U_i> / d_part x_k
    # Note: For brevity d_part = d
    # P_ij = -<u_i u_k> * d <U_j> / d x_k  -  <u_j u_k> d <U_i> / d x_k
    P_cart = - np.einsum('...ik,...jk', uiuj_cart, dUjdxi_cart) - np.einsum('...jk,...ik', uiuj_cart, dUjdxi_cart)
    
    P_cyl = my_math.transform_vect(theta, P_cart, 2)
    
    # Average in circumferential direction:
    P = np.mean(P_cyl, axis=1)
    
    
    # Dissipation tensor (Pope, eq. 7.181)
    #------------------------------------
    # eps_ij = 2 nu < du_i/dx_k * du_j/dx_k >
    # < du_i/dx_j * du_i/dx_j >  = < dU_i/dx_j * dU_i/dx_j > - (d<U_i>/dx_j)**2         , d (.) / dx_3 = 0
    # eps_11 = <(dU_1/dx_1)**2 + (dU_1/dx_2)**2 + (dU_1/dx_3)**2> - (d<U_1>/dx_1)**2 - (d<U_1>/dx_2)**2
    eps_cart[:, :, 0, 0] = 2*nu*(stats_vel[:, :, 38] - (dUjdxi_cart[:, :, 0, 0])**2 - (dUjdxi_cart[:, :, 0, 1])**2)
    # eps_22 = <(dU_2/dx_1)**2 + (dU_2/dx_2)**2 + (dU_2/dx_3)**2> - (d<U_2>/dx_1)**2 - (d<U_2>/dx_2)**2
    eps_cart[:, :, 1, 1] = 2*nu*(stats_vel[:, :, 39] - (dUjdxi_cart[:, :, 1, 0])**2 - (dUjdxi_cart[:, :, 1, 1])**2)
    # eps_33 = <(dU_3/dx_1)**2 + (dU_3/dx_2)**2 + (dU_3/dx_3)**2> - (d<U_3>/dx_1)**2 - (d<U_3>/dx_2)**2
    eps_cart[:, :, 2, 2] = 2*nu*(stats_vel[:, :, 40] - (dUjdxi_cart[:, :, 2, 0])**2 - (dUjdxi_cart[:, :, 2, 1])**2)
    # eps_12 = <dU_1/dx_1 * dU_2/dx_1 + dU_1/dx_2 * dU_2/dx_2 + dU_1/dx_3 * dU_2/dx_3> - d<U_1>/dx_1 * d<U_2>/dx_1 - d<U_1>/dx_2 * d<U_2>/dx_2
    eps_cart[:, :, 0, 1] = 2*nu*(stats_vel[:, :, 41] - dUjdxi_cart[:, :, 0, 0]*dUjdxi_cart[:, :, 1, 0] - dUjdxi_cart[:, :, 0, 1]*dUjdxi_cart[:, :, 1, 1])
    # eps_13 = <dU_1/dx_1 * dU_3/dx_1 + dU_1/dx_2 * dU_3/dx_2 + dU_1/dx_3 * dU_3/dx_3> - d<U_1>/dx_1 * d<U_3>/dx_1 - d<U_1>/dx_2 * d<U_3>/dx_2
    eps_cart[:, :, 0, 2] = 2*nu*(stats_vel[:, :, 42] - dUjdxi_cart[:, :, 0, 0]*dUjdxi_cart[:, :, 2, 0] - dUjdxi_cart[:, :, 0, 1]*dUjdxi_cart[:, :, 2, 1])
    # eps_23 = <dU_2/dx_1 * dU_3/dx_1 + dU_2/dx_2 * dU_3/dx_2 + dU_2/dx_3 * dU_3/dx_3> - d<U_2>/dx_1 * d<U_3>/dx_1 - d<U_2>/dx_2 * d<U_3>/dx_2
    eps_cart[:, :, 1, 2] = 2*nu*(stats_vel[:, :, 43] - dUjdxi_cart[:, :, 1, 0]*dUjdxi_cart[:, :, 2, 0] - dUjdxi_cart[:, :, 1, 1]*dUjdxi_cart[:, :, 2, 1])
    # eps_21 = eps_12
    eps_cart[:, :, 1, 0] = eps_cart[:, :, 0, 1]
    # eps_31 = eps_13
    eps_cart[:, :, 2, 0] = eps_cart[:, :, 0, 2]
    # eps_32 = eps_23
    eps_cart[:, :, 2, 1] = eps_cart[:, :, 1, 2]
    
    
    eps_cyl = my_math.transform_vect(theta, eps_cart, 2)
    
    # Average in circumferential direction:
    eps = np.mean(eps_cyl, axis=1)
    
    
    # Velocity-pressure gradient tensor (Pope, eq. 7.180)
    #----------------------------------------------------
    # Pi_ij = - 1/rho < u_i d p' / d x_j  +  u_j d p' / d x_i >
    #       = - 1/rho (< u_i d p' / d x_j> + < u_j d p' / d x_i >)
    # < u_i d p' / d x_j > = d < p' u_i > / d x_j - < p' d u_i / d x_j >
    
    # d <p' u_i> / d x_j = d <P U_i> / d x_j - <U_i> * d <P> / d x_j - <P> d <U_i> / d x_j
    dpuidxj_cart[:, :, 0, 0] = deriv_vel[:, :, 62] - Ui_cart[:, :, 0]*deriv_vel[:, :, 6] - p_full*dUjdxi_cart[:, :, 0, 0]    # d <p' u_1> / d x_1
    dpuidxj_cart[:, :, 0, 1] = deriv_vel[:, :, 63] - Ui_cart[:, :, 0]*deriv_vel[:, :, 7] - p_full*dUjdxi_cart[:, :, 0, 1]    # d <p' u_1> / d x_2
    dpuidxj_cart[:, :, 0, 2] = 0                                                                                    # d <p' u_1> / d x_3
    dpuidxj_cart[:, :, 1, 0] = deriv_vel[:, :, 64] - Ui_cart[:, :, 1]*deriv_vel[:, :, 6] - p_full*dUjdxi_cart[:, :, 1, 0]    # d <p' u_2> / d x_1
    dpuidxj_cart[:, :, 1, 1] = deriv_vel[:, :, 65] - Ui_cart[:, :, 1]*deriv_vel[:, :, 7] - p_full*dUjdxi_cart[:, :, 1, 1]    # d <p' u_2> / d x_2
    dpuidxj_cart[:, :, 1, 2] = 0                                                                                    # d <p' u_2> / d x_3
    dpuidxj_cart[:, :, 2, 0] = deriv_vel[:, :, 66] - Ui_cart[:, :, 2]*deriv_vel[:, :, 6] - p_full*dUjdxi_cart[:, :, 2, 0]    # d <p' u_3> / d x_1
    dpuidxj_cart[:, :, 2, 1] = deriv_vel[:, :, 67] - Ui_cart[:, :, 2]*deriv_vel[:, :, 7] - p_full*dUjdxi_cart[:, :, 2, 1]    # d <p' u_3> / d x_2
    dpuidxj_cart[:, :, 2, 2] = 0                                                                                    # d <p' u_3> / d x_3
    
    # < p' d u_i / d x_j > = < P d U_i / d x_j > - <P> * d <U_i> / d x_j
    pduidxj_cart[:, :, 0, 0] = stats_vel[:, :, 14] - p_full*dUjdxi_cart[:, :, 0, 0]       # <p' d u_1 / d x_1>
    pduidxj_cart[:, :, 0, 1] = stats_vel[:, :, 15] - p_full*dUjdxi_cart[:, :, 0, 1]       # <p' d u_1 / d x_2>
    pduidxj_cart[:, :, 0, 2] = stats_vel[:, :, 16] - p_full*dUjdxi_cart[:, :, 0, 2]       # <p' d u_1 / d x_3>
    pduidxj_cart[:, :, 1, 0] = stats_vel[:, :, 17] - p_full*dUjdxi_cart[:, :, 1, 0]       # <p' d u_2 / d x_1>
    pduidxj_cart[:, :, 1, 1] = stats_vel[:, :, 18] - p_full*dUjdxi_cart[:, :, 1, 1]       # <p' d u_2 / d x_2>
    pduidxj_cart[:, :, 1, 2] = stats_vel[:, :, 19] - p_full*dUjdxi_cart[:, :, 1, 2]       # <p' d u_2 / d x_3>
    pduidxj_cart[:, :, 2, 0] = stats_vel[:, :, 20] - p_full*dUjdxi_cart[:, :, 2, 0]       # <p' d u_3 / d x_1>
    pduidxj_cart[:, :, 2, 1] = stats_vel[:, :, 21] - p_full*dUjdxi_cart[:, :, 2, 1]       # <p' d u_3 / d x_2>
    pduidxj_cart[:, :, 2, 2] = stats_vel[:, :, 22] - p_full*dUjdxi_cart[:, :, 2, 2]       # <p' d u_3 / d x_3>
    
    # Pi_ij
    Pi_cart = - ( (dpuidxj_cart - pduidxj_cart) + np.transpose(dpuidxj_cart - pduidxj_cart, axes=(0, 1, 3, 2)) )
    
    Pi_cyl = my_math.transform_vect(theta, Pi_cart, 2)
    
    # Average in circumferential direction:
    Pi = np.mean(Pi_cyl, axis=1)
    
    
    # Viscous diffusion tensor
    # D_ij = nu d^2 < u_i u_j > / d x_k^2
    # d^2 < u_i u_j > / d x_k^2 = d^2 < U_i U_j > / d x_k^2 - d^2 (<U_i> <U_j>) / d x_k^2
    # d^2 (<U_i> <U_j>) / d x_k^2 = <U_i> d^2 <U_j> / d x_k^2 + 2 d <U_i> / d x_k * d <U_j> / d x_k + <U_i> d^2 <U_j> / d x_k^2
    d2UiUjdxk2_cart[:, :, 0, 0] =  deriv_vel[:, :, 50] + deriv_vel[:, :, 51]    # d^2 <U_1 U_1> / d x_k^2
    d2UiUjdxk2_cart[:, :, 0, 1] =  deriv_vel[:, :, 56] + deriv_vel[:, :, 57]    # d^2 <U_1 U_2> / d x_k^2
    d2UiUjdxk2_cart[:, :, 0, 2] =  deriv_vel[:, :, 58] + deriv_vel[:, :, 59]    # d^2 <U_1 U_3> / d x_k^2
    d2UiUjdxk2_cart[:, :, 1, 0] =  d2UiUjdxk2_cart[:, :, 0, 1]          # d^2 <U_2 U_1> / d x_k^2
    d2UiUjdxk2_cart[:, :, 1, 1] =  deriv_vel[:, :, 52] + deriv_vel[:, :, 53]    # d^2 <U_2 U_2> / d x_k^2
    d2UiUjdxk2_cart[:, :, 1, 2] =  deriv_vel[:, :, 60] + deriv_vel[:, :, 61]    # d^2 <U_2 U_3> / d x_k^3
    d2UiUjdxk2_cart[:, :, 2, 0] =  d2UiUjdxk2_cart[:, :, 0, 2]          # d^2 <U_3 U_1> / d x_k^2 
    d2UiUjdxk2_cart[:, :, 2, 1] =  d2UiUjdxk2_cart[:, :, 1, 2]          # d^2 <U_3 U_2> / d x_k^2
    d2UiUjdxk2_cart[:, :, 2, 2] =  deriv_vel[:, :, 54] + deriv_vel[:, :, 55]    # d^2 <U_3 U_3> / d x_k^2
    
    d2Ujdxk2_cart[:, :, 0] = deriv_vel[:, :, 44] + deriv_vel[:, :, 45]          # d^2 <U_1> / d x_k^2
    d2Ujdxk2_cart[:, :, 1] = deriv_vel[:, :, 46] + deriv_vel[:, :, 47]          # d^2 <U_2> / d x_k^2
    d2Ujdxk2_cart[:, :, 2] = deriv_vel[:, :, 48] + deriv_vel[:, :, 49]          # d^2 <U_3> / d x_k^2
    
    
    d2uiujdxk2_cart = d2UiUjdxk2_cart - (
            np.einsum('...i,...j',Ui_cart, d2Ujdxk2_cart) 
            + 2*np.einsum('...ik,...jk', dUjdxi_cart, dUjdxi_cart) 
            + np.einsum('...i,...j', Ui_cart, d2Ujdxk2_cart)
            )
    
    D_cart = nu * d2uiujdxk2_cart
    
    D_cyl = my_math.transform_vect(theta, D_cart, 2)
    
    # Average in circumferential direction:
    D = np.mean(D_cyl, axis=1)
    
    
    # Turbulent velocity related diffusion tensor
    # T_ij = - d < u_i u_j u_k> / d x_k
    # d < u_i u_j u_k > / d x_k =  d <U_i U_j U_k> / d x_k - d <U_i> / d x_k * <U_j><U_k>
    #   - <U_i><U_k> * d <U_j> / d x_k - <U_i><U_j> d <U_k> / d x_k
    #   - d <u_i u_j> / d x_k * <U_k> - <u_i u_j> * d <U_k> / d x_k
    #   - d <u_j u_k> / d x_k * <U_i> - <u_j u_k> * d <U_i> / d x_k
    #   - d <u_i u_k> / d x_k * <U_j> - <u_i u_k> * d <U_j> / d x_k
    dUiUjUkdxk_cart[:, :, 0, 0] = deriv_vel[:, :, 22] + deriv_vel[:, :, 31]     # d <U_1 U_1 U_k> d x_k
    dUiUjUkdxk_cart[:, :, 0, 1] = deriv_vel[:, :, 30] + deriv_vel[:, :, 35]     # d <U_1 U_2 U_k> d x_k 
    dUiUjUkdxk_cart[:, :, 0, 2] = deriv_vel[:, :, 32] + deriv_vel[:, :, 43]     # d <U_1 U_3 U_k> d x_k 
    dUiUjUkdxk_cart[:, :, 1, 0] = dUiUjUkdxk_cart[:, :, 0, 1]           # d <U_2 U_1 U_k> d x_k 
    dUiUjUkdxk_cart[:, :, 1, 1] = deriv_vel[:, :, 34] + deriv_vel[:, :, 25]     # d <U_2 U_2 U_k> d x_k 
    dUiUjUkdxk_cart[:, :, 1, 2] = deriv_vel[:, :, 42] + deriv_vel[:, :, 37]     # d <U_2 U_3 U_k> d x_k 
    dUiUjUkdxk_cart[:, :, 2, 0] = dUiUjUkdxk_cart[:, :, 0, 2]           # d <U_3 U_1 U_k> d x_k
    dUiUjUkdxk_cart[:, :, 2, 1] = dUiUjUkdxk_cart[:, :, 1, 2]           # d <U_3 U_2 U_k> d x_k
    dUiUjUkdxk_cart[:, :, 2, 2] = deriv_vel[:, :, 38] + deriv_vel[:, :, 41]     # d <U_3 U_3 U_k> d x_k
    
    dUiUjdxk_cart[:, :, 0, 0, 0] = deriv_vel[:, :, 8]                   # d <U_1 U_1> / d x_1
    dUiUjdxk_cart[:, :, 0, 0, 1] = deriv_vel[:, :, 9]                   # d <U_1 U_1> / d x_2
    dUiUjdxk_cart[:, :, 0, 0, 2] = 0                                # d <U_1 U_1> / d x_3
    dUiUjdxk_cart[:, :, 0, 1, 0] = deriv_vel[:, :, 16]                  # d <U_1 U_2> / d x_1
    dUiUjdxk_cart[:, :, 0, 1, 1] = deriv_vel[:, :, 17]                  # d <U_1 U_2> / d x_2
    dUiUjdxk_cart[:, :, 0, 1, 2] = 0                                # d <U_1 U_2> / d x_3
    dUiUjdxk_cart[:, :, 0, 2, 0] = deriv_vel[:, :, 20]                  # d <U_1 U_3> / d x_1
    dUiUjdxk_cart[:, :, 0, 2, 1] = deriv_vel[:, :, 21]                  # d <U_1 U_3> / d x_2
    dUiUjdxk_cart[:, :, 0, 2, 2] = 0                                # d <U_1 U_3> / d x_3
    dUiUjdxk_cart[:, :, 1, 0, 0] = dUiUjdxk_cart[:, :, 0, 1, 0]     # d <U_2 U_1> / d x_1
    dUiUjdxk_cart[:, :, 1, 0, 1] = dUiUjdxk_cart[:, :, 0, 1, 1]     # d <U_2 U_1> / d x_2
    dUiUjdxk_cart[:, :, 1, 0, 2] = dUiUjdxk_cart[:, :, 0, 1, 2]     # d <U_2 U_1> / d x_3
    dUiUjdxk_cart[:, :, 1, 1, 0] = deriv_vel[:, :, 10]                  # d <U_2 U_2> / d x_1
    dUiUjdxk_cart[:, :, 1, 1, 1] = deriv_vel[:, :, 11]                  # d <U_2 U_2> / d x_2
    dUiUjdxk_cart[:, :, 1, 1, 2] = 0                                # d <U_2 U_2> / d x_3
    dUiUjdxk_cart[:, :, 1, 2, 0] = deriv_vel[:, :, 18]                  # d <U_2 U_3> / d x_1
    dUiUjdxk_cart[:, :, 1, 2, 1] = deriv_vel[:, :, 19]                  # d <U_2 U_3> / d x_2
    dUiUjdxk_cart[:, :, 1, 2, 2] = 0                                # d <U_2 U_3> / d x_3
    dUiUjdxk_cart[:, :, 2, 0, 0] = dUiUjdxk_cart[:, :, 0, 2, 0]     # d <U_3 U_1> / d x_1
    dUiUjdxk_cart[:, :, 2, 0, 1] = dUiUjdxk_cart[:, :, 0, 2, 1]     # d <U_3 U_1> / d x_2
    dUiUjdxk_cart[:, :, 2, 0, 2] = dUiUjdxk_cart[:, :, 0, 2, 2]     # d <U_3 U_1> / d x_3
    dUiUjdxk_cart[:, :, 2, 1, 0] = dUiUjdxk_cart[:, :, 1, 2, 0]     # d <U_3 U_2> / d x_1
    dUiUjdxk_cart[:, :, 2, 1, 1] = dUiUjdxk_cart[:, :, 1, 2, 1]     # d <U_3 U_2> / d x_2
    dUiUjdxk_cart[:, :, 2, 1, 2] = dUiUjdxk_cart[:, :, 1, 2, 2]     # d <U_3 U_2> / d x_3
    dUiUjdxk_cart[:, :, 2, 2, 0] = deriv_vel[:, :, 12]                  # d <U_3 U_3> / d x_1
    dUiUjdxk_cart[:, :, 2, 2, 1] = deriv_vel[:, :, 13]                  # d <U_3 U_3> / d x_2
    dUiUjdxk_cart[:, :, 2, 2, 2] = 0                                # d <U_3 U_3> / d x_3
    
    duiujdxk_cart = dUiUjdxk_cart - np.einsum('...ik,...j', dUjdxi_cart, Ui_cart) - np.einsum('...jk,...i', dUjdxi_cart, Ui_cart)
    
    duiujukdxk_cart = dUiUjUkdxk_cart - (
            np.einsum('...ik,...j,...k', dUjdxi_cart, Ui_cart, Ui_cart)
            + np.einsum('...jk,...i,...k', dUjdxi_cart, Ui_cart, Ui_cart)
            + 0
            + np.einsum('...ijk,...k', duiujdxk_cart, Ui_cart)
            + 0
            + np.einsum('...jkk,...i', duiujdxk_cart, Ui_cart)
            + np.einsum('...jk,...ik', uiuj_cart, dUjdxi_cart)
            + np.einsum('...ikk,...j', duiujdxk_cart, Ui_cart)
            + np.einsum('...ik,...jk', uiuj_cart, dUjdxi_cart)
            )
    
    T_cart = - duiujukdxk_cart
    
    T_cyl = my_math.transform_vect(theta, T_cart, 2)
    
    # Average in circumferential direction:
    T = np.mean(T_cyl, axis=1)
    
    
    # The budget of the turbulent kinetic energy can be written like (see Pope eq. 5.164 and El Khoury (2013))
    # D_k / D_ t = 0 + 0 = P^k + \tilde{\epsilon} + \Pi^k + D^k + T^k, 
    # Where D_ / D_ t denotes the mean substantial derivative and 
    # D_ k / D_ t is zero because the flow is in steady state and it is fully developed.
    #
    # P^k:              production
    # - <u_i u_j> d <U_j> / d x_i             d is the partial derivative
    #
    # \tilde{\epsilon}: pseudo-disspiation
    # - \nu < d u_i / d x_j * d u_i / d x_j >
    #
    # \Pi^k:            pressure-related diffusion
    # - 1 / \rho d <u_i p'> / d x_i 
    #
    # D^k:              viscous diffusion
    # \nu / 2 d^2 k / d x_j^2
    #
    # T^k:              turbulent velocity related diffusion
    # - 1/2 d <u_i u_j u_j> / d x_i
    
    # Note: half the trace of the budget for the Reynolds stresses is the budget of the kinetic energy
    
    # Production
    P_k = 1/2 * np.einsum('...ii', P)
    
    # Pseudo-dissipation
    eps_tilde = 1/2 * np.einsum('...ii', eps)
    
    # Pressure related diffusion
    Pi_k = 1/2 * np.einsum('...ii', Pi)
    
    # Viscous diffusion
    D_k = 1/2 * np.einsum('...ii', D)
    
    # Turbulent velocity related diffusion
    T_k = 1/2 * np.einsum('...ii', T)


    # Turbulent kinetic energy k = 1/2 <u_i u_i>
    file_path = outdata[0]
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)
    
    np.savetxt(file_path, 
            np.transpose([
                np.flipud(y_plus), 
                np.flipud(P_k/(u_t**4/nu)),
                np.flipud(-eps_tilde/(u_t**4/nu)),
                np.flipud(Pi_k/(u_t**4/nu)),
                np.flipud(D_k/(u_t**4/nu)),
                np.flipud(T_k/(u_t**4/nu)),
                ]), header='y_plus\tProduction <P>+\t-Pseudo dissipation <eps>+\tPressure related diffusion<Pi>+\tViscous diffusion <D>+\tTurbulent velocity related diffusion <T>+')


    # Reynolds stress: <u_r u_r>
    file_path = outdata[1]
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)
    
    np.savetxt(file_path, 
            np.transpose([
                np.flipud(y_plus), 
                np.flipud(P[:, 0, 0]/(u_t**4/nu)),
                np.flipud(-eps[:, 0, 0]/(u_t**4/nu)),
                np.flipud(Pi[:, 0, 0]/(u_t**4/nu)),
                np.flipud(D[:, 0, 0]/(u_t**4/nu)),
                np.flipud(T[:, 0, 0]/(u_t**4/nu)),
                ]), header='y_plus\tProduction <P>+\t-Pseudo dissipation <eps>+\tPressure related diffusion<Pi>+\tViscous diffusion <D>+\tTurbulent velocity related diffusion <T>+')

    # Reynolds stress: <u_phi u_phi>
    file_path = outdata[2]
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)
    
    np.savetxt(file_path, 
            np.transpose([
                np.flipud(y_plus), 
                np.flipud(P[:, 1, 1]/(u_t**4/nu)),
                np.flipud(-eps[:, 1, 1]/(u_t**4/nu)),
                np.flipud(Pi[:, 1, 1]/(u_t**4/nu)),
                np.flipud(D[:, 1, 1]/(u_t**4/nu)),
                np.flipud(T[:, 1, 1]/(u_t**4/nu)),
                ]), header='y_plus\tProduction <P>+\t-Pseudo dissipation <eps>+\tPressure related diffusion<Pi>+\tViscous diffusion <D>+\tTurbulent velocity related diffusion <T>+')

    # Reynolds stress: <u_z u_z>
    file_path = outdata[3]
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)
    
    np.savetxt(file_path, 
            np.transpose([
                np.flipud(y_plus), 
                np.flipud(P[:, 2, 2]/(u_t**4/nu)),
                np.flipud(-eps[:, 2, 2]/(u_t**4/nu)),
                np.flipud(Pi[:, 2, 2]/(u_t**4/nu)),
                np.flipud(D[:, 2, 2]/(u_t**4/nu)),
                np.flipud(T[:, 2, 2]/(u_t**4/nu)),
                ]), header='y_plus\tProduction <P>+\t-Pseudo dissipation <eps>+\tPressure related diffusion<Pi>+\tViscous diffusion <D>+\tTurbulent velocity related diffusion <T>+')

    # Reynolds stress: <u_r u_z>
    file_path = outdata[4]
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)
    
    np.savetxt(file_path, 
            np.transpose([
                np.flipud(y_plus), 
                np.flipud(P[:, 0, 2]/(u_t**4/nu)),
                np.flipud(-eps[:, 0, 2]/(u_t**4/nu)),
                np.flipud(Pi[:, 0, 2]/(u_t**4/nu)),
                np.flipud(D[:, 0, 2]/(u_t**4/nu)),
                np.flipud(T[:, 0, 2]/(u_t**4/nu)),
                ]), header='y_plus\tProduction <P>+\t-Pseudo dissipation <eps>+\tPressure related diffusion<Pi>+\tViscous diffusion <D>+\tTurbulent velocity related diffusion <T>+')


    # Write maximum and RMS residual to a file
    n_budgets = 5
    y_plus_flip = np.flipud(y_plus) * np.ones((n_budgets, np.size(r)))
    data_collect = np.zeros((n_budgets, np.size(r), 3, 3))
    data_collect[0, ...] = P/(u_t**4/nu)
    data_collect[1, ...] = -eps/(u_t**4/nu)
    data_collect[2, ...] = Pi/(u_t**4/nu)
    data_collect[3, ...] = D/(u_t**4/nu)
    data_collect[4, ...] = T/(u_t**4/nu)


    residual = np.zeros((n_budgets, np.size(r)))
    residual[0, :] = (P_k - eps_tilde + Pi_k +  D_k + T_k)/(u_t**4/nu)
    residual[1, :] = np.sum(data_collect[:, :, 0, 0], axis=0)/(u_t**4/nu)
    residual[2, :] = np.sum(data_collect[:, :, 1, 1], axis=0)/(u_t**4/nu)
    residual[3, :] = np.sum(data_collect[:, :, 2, 2], axis=0)/(u_t**4/nu)
    residual[4, :] = np.sum(data_collect[:, :, 0, 2], axis=0)/(u_t**4/nu)

    eps_max = np.max(np.abs(residual[:, :]), axis=1)
    eps_rms = (1/y_plus_flip[:, -1] * np.trapz(residual**2, y_plus_flip))**0.5


    file_path = 'profiles/' + timerange_select + '/resid_vbudget_max_rms.dat'
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)

    labels = [ data_name[i][9:-4] for i in range(len(data_name))]
    header1 = ' '.join(labels)

    np.savetxt(file_path,
            np.transpose([eps_max, 
                eps_rms]),
            header='Rows: ' + header1 + '\nCols: eps_max\teps_rms')


# Call function
def run():
    """ Convert npz data to text files."""

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    for span in timeranges_select:
        save_vbudget(span)
