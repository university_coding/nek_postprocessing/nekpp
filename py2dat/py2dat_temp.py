#!/usr/bin/env python3.5
#-----------------------
# This script reads previously saved data 
# and saves  profiles 
# of first and second order temperature statistics
# in a simple text file.
# ----------------------------------------------------
# Author:   Steffen Straub
# Date  :   2018/02/27
#-----------------------------------------------------

import numpy as np
from ..misc import my_math
import os

import configparser

import pdb


# Define function
def save_temp(timerange_select):

    indata_v = 'stats_' + timerange_select + '/statistics_matrix_tv1.npz'
    indata_t_list = (
            'stats_' + timerange_select + '/statistics_matrix_tt1.npz', 
            'stats_' + timerange_select + '/statistics_matrix_tt2.npz', 
            'stats_' + timerange_select + '/statistics_matrix_tt3.npz', 
            'stats_' + timerange_select + '/statistics_matrix_tt4.npz', 
            'stats_' + timerange_select + '/statistics_matrix_tt5.npz', 
            'stats_' + timerange_select + '/statistics_matrix_tt6.npz', 
            )
    Pr_list = (
            0.71,
            0.71,
            0.71,
            0.025,
            0.025,
            0.025,
            )
    outdata_list = (
            'profiles/' + timerange_select + '/temp_stats_IT071.dat',
            'profiles/' + timerange_select + '/temp_stats_MBC071.dat',
            'profiles/' + timerange_select + '/temp_stats_IF071.dat',
            'profiles/' + timerange_select + '/temp_stats_IT0025.dat',       
            'profiles/' + timerange_select + '/temp_stats_MBC0025.dat',
            'profiles/' + timerange_select + '/temp_stats_IF0025.dat',
            )

    for i in range(len(indata_t_list)):
        indata_t = indata_t_list[i]
        Pr = Pr_list[i]
        outdata = outdata_list[i]
    
        print("Processing ", indata_t)
        ## Load my data
        #--------------
        # 1) Temperature statistics
        with np.load(indata_t) as data:
        
            # Coordinates
            X = data['X']
            Y = data['Y']
        
            # individual radial and circumferential distribution
            r = data['r']
            phi = data['phi']
        
            # runtime averages and postprocessing derivatives
            stats_temp = data['stats_m']
            deriv_temp = data['deriv_m']
        
        # 2) Velocity statistics
            with np.load(indata_v) as data:
                    
                stats_vel = data['stats_m']
                deriv_vel = data['deriv_m']
    
        # Read user defined case-dependent parameters
        config = configparser.ConfigParser()
        config.read('case_params.ini')
        Re_b = float(config['Reynolds']['Re_b'])
       
        ## Calculations
        #-------------
        theta, rho= np.meshgrid(phi, r, indexing='xy')
        nu = 1/Re_b
        Pe = Re_b * Pr
        u_t = my_math.get_utau(r, phi, deriv_vel, theta, nu)
        Re_t = u_t*1/nu
        y = 0.5 - r 
        y_plus = (y*u_t)/nu
        
    
        UiTheta_cart = np.zeros((np.size(r), np.size(phi), 3))
    
        Theta = stats_temp[:, :, 0]
        Theta_Theta = stats_temp[:, :, 1]
        UiTheta_cart[:, :, 0:3] = stats_temp[:, :, 2:5]                # <U_1 Theta>, <U_2 Theta>, <U_3 Theta>
        UiTheta_cyl = my_math.transform_vect(theta, UiTheta_cart, 1)
        UiTheta = np.mean(UiTheta_cyl, axis=1)
        Ui_cart = stats_vel[:, :, 0:3]
    
        Theta_mean = np.mean(Theta, axis=1)
        Theta_Theta_mean = np.mean(Theta_Theta, axis=1)
        theta_theta = Theta_Theta_mean - Theta_mean**2
        
        uitheta_cart = UiTheta_cart[..., 0:3] - np.einsum('...i,...',Ui_cart, Theta)
        uitheta_cyl = my_math.transform_vect(theta, uitheta_cart, 1)
        uitheta = np.mean(uitheta_cyl, axis=1)

        # Convert to Theta_inhomogeneous = <Tb> - T / T_ref
        #-------- 
        # Integrate over r
        UiTheta_dA =  2*np.pi * np.trapz(UiTheta[..., 2]*r, r)
        A_cross = 0.5**2 * np.pi
        U_b = 1

        Theta_inhomog = Theta_mean - UiTheta_dA / (U_b*A_cross)
    
        file_path = outdata
        directory = os.path.dirname(file_path)
        if not os.path.exists(directory):
            os.makedirs(directory)
        
        np.savetxt(file_path, 
                np.transpose([
                    np.flipud(y_plus), 
                    np.flipud(Theta_mean * u_t),
                    np.flipud(theta_theta * u_t**2),
                    np.flipud(uitheta[..., 0]),
                    np.flipud(uitheta[..., 1]),
                    np.flipud(uitheta[..., 2]),
                    np.flipud(Theta_inhomog * u_t),
                    ]), header='y_plus\t<Theta>+\t<theta theta>+\t<u_r theta>+\t<u_t theta>+\t<u_z theta>+\t<Theta_inhomog>+')



def run():
    # Call function

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    for span in timeranges_select:
        save_temp(span)
