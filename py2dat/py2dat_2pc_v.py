#!/usr/bin/env python3.5
#-----------------------
# This script reads previously saved data 
# and saves  
# two-point correlations at different radial locations
# in a simple text file.
# ----------------------------------------------------
# Author:   Steffen Straub
# Date  :   2018/06/28
#-----------------------------------------------------

import numpy as np
from ..misc import my_math
import os

import configparser

import pdb

# Define function
def save(timerange_select):

    indata_v = 'results/' + timerange_select + '/power_spectra_v.npz'
    outdata = 'profiles/' + timerange_select + '/2pc_v.dat'

    print("Processing ", indata_v)

    # Load my data
    #-------------
    with np.load(indata_v) as data:

        E_2d_rr = data['E_2d_rr']
        E_2d_tt = data['E_2d_tt']
        E_2d_zz = data['E_2d_zz']
        E_2d_rz = data['E_2d_rz']
    
        r = data['r']
        phi = data['phi']
        z = data['z']

    # Some pre-calculations and definitions
    nr = len(r)
    nphi = len(phi)
    nz = len(z)
    dphi = phi[1]
    dz = z[1]
    R = 0.5
    y = R - r
    
    # Read user defined case-dependent parameters
    config = configparser.ConfigParser()
    config.read('case_params.ini')
    Re_t = float(config['Reynolds']['Re_t'])

    # Conversion in plus units
    y_p = y * Re_t
    
    # Sum over modes in homogeneous direction for keeping only dependency on r and either z or phi
    E_uu_z = np.sum(E_2d_zz, axis=1)
    
    # Correlation function R(s) (see Pope, p. 686, eq. E.25)
    R_uu_z = nz * np.real(np.fft.ifft(E_uu_z))
    
    # Variance
    uzuz = R_uu_z[:, 0]



    file_path = outdata
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)
    

    imax = np.argmax(uzuz)
    i_01 = np.argmin(np.abs(r-0.1))
    np.savetxt(file_path, 
            np.transpose([
                z,
                R_uu_z[imax],
                R_uu_z[i_01],
                ]), header='cols:\tz\tR_uu_z(y_p[{0:d}] = {1:3.1f})\tR_uu_z(y[{2:d}] = {3:3.1f})'.format(imax, y_p[imax], i_01, y[i_01]))


def run():
    # Call function

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    for span in timeranges_select:
        save(span)   
