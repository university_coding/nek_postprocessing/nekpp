#!/usr/bin/env python3.5
#-----------------------
# This script reads previously saved data 
# and saves  profiles 
# of first and second order temperature statistics
# in a simple text file.
# ----------------------------------------------------
# Author:   Steffen Straub
# Date  :   2018/03/27
#-----------------------------------------------------

import numpy as np
from ..misc import my_math
import os

import configparser


import pdb

# Define function
def save_temp3d(timerange_select, nfields, fstart):
    
    # Load data
    #----------
    data_dir = 'stats_3d/' + timerange_select + '/'
    data_name = ['temp_{0:d}.npz'.format(k) for k in range(fstart, nfields+fstart)]


    print('Note: label_list is hardcoded for now.')
    label = (
        '071',
        '0025',
        )

   
    for i in range(len(data_name)):
    
        print("Processing " + label[i] + ' at time ' + timerange_select)

        with np.load(data_dir + data_name[i]) as data:
        
            # Coordinates
            X = data['X_sym']
            Y = data['Y_sym']
       
            # individual radial and circumferential distribution
            r = data['r']
            phi = data['phi']
            phi_sym = data['phi_sym']
            z = data['z']
        
            # data averaged over time and exploited symmetry
            stats_3d = data['stats_3d_sym']
            stats_2d = data['stats_2d']
            stats_1d = data['stats_1d']

    
#        # 2) Velocity statistics
#        indata_v = 'stats_' + timerange_select + '/statistics_matrix_tv1.npz'
#        with np.load(indata_v) as data:
#                    
#            stats_vel = data['stats_m']
#            deriv_vel = data['deriv_m']
    
        
        # Read user defined case-dependent parameters
        config = configparser.ConfigParser()
        config.read('case_params.ini')
        Re_b = float(config['Reynolds']['Re_b'])
        Re_t = float(config['Reynolds']['Re_t'])


        ## Calculations
        #-------------
        theta, rho= np.meshgrid(phi, r, indexing='xy')
        nu = 1/Re_b
#        u_t = my_math.get_utau(r, phi, deriv_vel, theta, nu)
#        Re_t = u_t*1/nu
        u_t = Re_t/Re_b
        y = 0.5 - r 
        y_plus = (y*u_t)/nu
        
    
        uitheta = np.zeros((np.shape(stats_3d[..., 2:5])))
    
        Theta = stats_3d[..., 0]
        theta_theta = stats_3d[..., 1]
        uitheta[..., 0] = stats_3d[..., 2]
        uitheta[..., 1] = stats_3d[..., 3]
        uitheta[..., 2] = stats_3d[..., 4]

        Nuss_z = stats_1d[..., 0]
        Nuss = stats_2d[..., 0]

        # Maximum location
        z_max_Theta = np.argmax(Theta[-1, -1, :])
        z_max_theta_theta = np.argmax(theta_theta[-1, -1, :-1])
       

        # Save 5 profiles at phi =
        # -pi/2
        # -pi/4
        # 0
        # pi/4
        # pi/2
        n_probes = 5
        probes_sep = int( (len(phi_sym)-1) / (n_probes-1))

        n_axpos = 10
        ax_sep = int( len(z) / (n_axpos) )     # z=0 does not need to be considered

        
        # Thermal statistics
        for iz in range(1, n_axpos):
            for k in range(n_probes):
                file_path = 'profiles/' + timerange_select + '/' + label[i] + '/z_{0:d}/probe_{1:d}/'.format(iz, k) + 'temp.dat'
                directory = os.path.dirname(file_path)
                if not os.path.exists(directory):
                    os.makedirs(directory)
    
                np.savetxt(file_path, 
                        np.transpose([
                            np.flipud(y_plus), 
                            np.flipud(Theta[:, probes_sep*k, ax_sep*iz]) * u_t,
                            np.flipud(theta_theta[:, probes_sep*k, ax_sep*iz]) * u_t**2,
                            np.flipud(uitheta[:, probes_sep*k, ax_sep*iz, 0]),
                            np.flipud(uitheta[:, probes_sep*k, ax_sep*iz, 1]),
                            np.flipud(uitheta[:, probes_sep*k, ax_sep*iz, 2]),
                            ]), header='y_plus\t<Theta>+\t<theta theta>+\t<u_r theta>+\t<u_t theta>+\t<u_z theta>+')

    

        # Nusselt 
        file_path = 'profiles/' + timerange_select + '/' + label[i] + '/Nusselt.dat'
        directory = os.path.dirname(file_path)
        if not os.path.exists(directory):
            os.makedirs(directory)

        np.savetxt(file_path, 
                np.transpose([
                    z[1:-1], 
                    Nuss_z[1:-1],
                    Nuss[int((len(phi)-1)/4), 1:-1],
                    ]), header='z\t<Nuss>phi\tNuss(phi=pi/2)')

        # Profiles at the wall
        file_path = 'profiles/' + timerange_select + '/' + label[i] + '/wall_profiles_z.dat'
        directory = os.path.dirname(file_path)
        if not os.path.exists(directory):
            os.makedirs(directory)

        np.savetxt(file_path, 
                np.transpose([
                    z, 
                    Theta[-1, -1, :]*u_t,       # phi_sym
                    theta_theta[-1, -1, :]*u_t**2,     # phi_sym
                    ]), header='z\t<Theta>+\t<theta theta>+ at phi=90')

        # z-location of maximum
        file_path = 'profiles/' + timerange_select + '/' + label[i] + '/wall_profiles_z_max.dat'
        directory = os.path.dirname(file_path)
        if not os.path.exists(directory):
            os.makedirs(directory)

        max_loc = np.array((z[z_max_Theta], z[z_max_theta_theta]))
        max_val = np.array((Theta[-1, -1, z_max_Theta], theta_theta[-1, -1, z_max_theta_theta]))

        np.savetxt(file_path, 
                np.transpose([max_loc,
                    max_val*u_t,
                    ]),
                header='Rows: Theta, theta_theta\nCols: z-location\t maximum value')


        file_path = 'profiles/' + timerange_select + '/' + label[i] + '/wall_profiles_phi.dat'
        directory = os.path.dirname(file_path)
        if not os.path.exists(directory):
            os.makedirs(directory)

        np.savetxt(file_path, 
                np.transpose([
                    phi_sym, 
                    Theta[-1, :, int((len(z)-1)/2)]*u_t,
                    theta_theta[-1, :, int((len(z)-1)/2)]*u_t**2,
                    ]), header='phi\t<Theta>+\t<theta theta>+ at z=Lz/2')
   



def run(nfields=2, fstart=0):
    """
    Write temperature at 5 probes in text files.

    nfields     Number of fields to interpolate
    fstart      First field starts by, e.g (0, 1)
    """

    # Call function

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    for span in timeranges_select:
        save_temp3d(span, nfields, fstart)
