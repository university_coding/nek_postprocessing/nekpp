#!/usr/bin/env python3.5
#-----------------------
# This script reads previously saved data 
# and saves  profiles 
# in a simple text file.
# ----------------------------------------------------
# Author:   Steffen Straub
# Date  :   2018/11/09
#-----------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt
from ..misc import my_math
import os
import sys

import configparser

import pdb


# Define function
def save_profiles(timerange_select):

    plt.close('all')

    indata_v = 'stats_' + timerange_select + '/statistics_matrix_tv1.npz'
    indata_t_list = (
            'stats_' + timerange_select + '/statistics_matrix_tt1.npz', 
            'stats_' + timerange_select + '/statistics_matrix_tt2.npz', 
            'stats_' + timerange_select + '/statistics_matrix_tt3.npz', 
            'stats_' + timerange_select + '/statistics_matrix_tt4.npz', 
            'stats_' + timerange_select + '/statistics_matrix_tt5.npz', 
            'stats_' + timerange_select + '/statistics_matrix_tt6.npz', 
            )
    Pr_list = (
            0.71,
            0.71,
            0.71,
            0.025,
            0.025,
            0.025,
            )
    outdata_list = (
            'profiles/' + timerange_select + '/t_balance_IT071.dat',
            'profiles/' + timerange_select + '/t_balance_MBC071.dat',
            'profiles/' + timerange_select + '/t_balance_IF071.dat',
            'profiles/' + timerange_select + '/t_balance_IT0025.dat',       
            'profiles/' + timerange_select + '/t_balance_MBC0025.dat',
            'profiles/' + timerange_select + '/t_balance_IF0025.dat',
            )

    for i in range(len(indata_t_list)):
#    for i in range(1, 2):
        indata_t = indata_t_list[i]
        Pr = Pr_list[i]
        outdata = outdata_list[i]
    
        print("Processing ", indata_t)
        ## Load my data
        #--------------
        # 1) Temperature statistics
        with np.load(indata_t) as data:
        
            # Coordinates
            X = data['X']
            Y = data['Y']
        
            # individual radial and circumferential distribution
            r = data['r']
            phi = data['phi']
        
            # runtime averages and postprocessing derivatives
            stats_temp = data['stats_m']
            deriv_temp = data['deriv_m']
        
        # 2) Velocity statistics
            with np.load(indata_v) as data:
                    
                stats_vel = data['stats_m']
                deriv_vel = data['deriv_m']
    
        
        # Read user defined case-dependent parameters
        config = configparser.ConfigParser()
        config.read('case_params.ini')
        Re_b = float(config['Reynolds']['Re_b'])

        ## Calculations
        #-------------
        theta, rho= np.meshgrid(phi, r, indexing='xy')
        nu = 1/Re_b
        Pe = Re_b * Pr
        u_t = my_math.get_utau(r, phi, deriv_vel, theta, nu)
        Re_t = u_t*1/nu
        y = 0.5 - r 
        y_plus = (y*u_t)/nu
        D = 1
        
        nr = len(r)
        nphi = len(phi)
    
        UiTheta_cart = np.zeros((np.size(r), np.size(phi), 3))
        ahat = np.zeros(2)
    
        Theta = stats_temp[:, :, 0]
        UiTheta_cart[:, :, 0:3] = stats_temp[:, :, 2:5]                # <U_1 Theta>, <U_2 Theta>, <U_3 Theta>
        Ui_cart = stats_vel[:, :, 0:3]
        Ui_cyl = my_math.transform_vect(theta, Ui_cart, 1)
        Ui = np.mean(Ui_cyl, axis=1)

    
        Theta_mean = np.mean(Theta, axis=1)
        
        uitheta_cart = UiTheta_cart[..., 0:3] - np.einsum('...i,...',Ui_cart, Theta)
        uitheta_cyl = my_math.transform_vect(theta, uitheta_cart, 1)
        uitheta = np.mean(uitheta_cyl, axis=1)
        
        # Read the previously calculated Nusselt number to determine an averaged ahat
        try:
            Nu = np.loadtxt('./nusselt/' + timerange_select + '/nuss.dat')
        except OSError:
            print('Error: Nusselt numbers are needed for ahat (source term)!')
            raise

        # Check U_bulk
#        U_int = np.trapz( r * np.trapz(Ui_cyl[:, :, 2], phi, axis=1), r)
#        A = D**2/4 * np.pi
#        U_bulk = U_int/A
#        print(U_bulk)


        # Calculate individual contributions to the average Theta
        rad_flux = np.zeros((nr, nphi))
        for i in range(nr):
            rad_flux[i, :] = - Pe * np.trapz( uitheta_cyl[i:, :, 0], r[i:], axis=0 )

        # Average in azimuthal direction (after integration has been performed!)
        rad_flux = np.mean(rad_flux, axis=1)

        
        # Individual contributions from the source term
        def integrate_S_theta(integrand):
            inner_int = np.zeros((nr, nphi))
            outer_int = np.zeros((nr, nphi))
            # Indices range from 0:i+1 because 0:0 does not exist and 0:1 is zero, 
            # but for i=1 the integration should range from r=0 to r=r[1]
            for i in range(nr):
                inner_int[i, :] = np.trapz(np.expand_dims(r[:i+1], axis=1) * integrand[:i+1], r[:i+1], axis=0)
            for i in range(1, nr):  # Skip r=0 and set it to nan to avoid division by 0
                outer_int[i, :] = Pe * np.trapz(1/np.expand_dims(r[i:], axis=1) * inner_int[i:, :], r[i:], axis=0)
            outer_int[0, :] = float('Nan')
            result = outer_int
            return result

        # IT
        if ("IT" in outdata):
            if ("071" in outdata):
                ahat = Nu[0]/Pe * 4/D
            elif ("0025" in outdata):
                ahat = Nu[3]/Pe * 4/D
            else:
                print('Error with ahat for the IT cases.')
                sys.exit(1)
            # Mean advective heat flux
            mean_adv = ahat * Theta * Ui_cyl[:, :, 2]   
            # Turbulent axial advective heat flux
            turb_ax  = ahat * uitheta_cyl[:, :, 2]
            # Mean streamwise thermal diffusion
            mean_diff = ahat**2 / Pe * Theta
        # MBC and IF
        else:
            # Mean advection term
            mean_adv = 4 * Ui_cyl[:, :, 2]


        mean_adv_int = np.mean(integrate_S_theta(mean_adv), axis=1)
        if ("IT" in outdata):
            turb_ax_int = np.mean(integrate_S_theta(turb_ax), axis=1)
            mean_diff = np.mean(integrate_S_theta(mean_diff), axis=1)


    
        file_path = outdata
        directory = os.path.dirname(file_path)
        if not os.path.exists(directory):
            os.makedirs(directory)
        
        if ("IT" in outdata):
            np.savetxt(file_path, 
                    np.transpose([
                        np.flipud(y), 
                        np.flipud(rad_flux),
                        np.flipud(mean_adv_int),
                        np.flipud(turb_ax_int),
                        np.flipud(mean_diff),
                        ]), header='r\tRad. turb. h.f.\tMean adv. h.f.\tTurb. ax. adv. h.f.\tMean streamw. th. diff')
        else:
            np.savetxt(file_path, 
                    np.transpose([
                        np.flipud(y), 
                        np.flipud(rad_flux),
                        np.flipud(mean_adv_int),
                        ]), header='r\tRad. turb. h.f.\tMean adv. h.f.')

    


def run():
    # Call function

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')
    
    for span in timeranges_select:
        save_profiles(span)
