#!/usr/bin/env python3.5
#-----------------------
# This script reads previously saved data 
# and saves  profiles of
# the turbulent momentum diffusivity nu_t
# in a simple text file.
# ----------------------------------------------------
# Author:   Steffen Straub
# Date  :   2018/04/06
#-----------------------------------------------------

import numpy as np
from ..misc import my_math
import os

import configparser

import pdb


# Define function
def save_nut(span):

    indata_v = 'stats_' + span + '/statistics_matrix_tv1.npz'
    outdata = 'profiles/' + span + '/nut.dat'

    print('Processing ' + indata_v)
   
    # Load my data
    #-------------
    with np.load(indata_v) as data:
    
        # Coordinates
        X = data['X']
        Y = data['Y']
    
        # individual radial and circumferential distribution
        r = data['r']
        phi = data['phi']
    
        # runtime averages and postprocessing derivatives
        stats_vel = data['stats_m']
        deriv_vel = data['deriv_m']
    
    
    ## Allocate variables
    #--------------------
    # Mean velocity vector
    Ui_cart = np.zeros((np.size(r), np.size(phi), 3))

    # Mean velocity correlation tensor
    UiUj_cart = np.zeros((np.size(r), np.size(phi), 3, 3))
    # Reynolds stress tensor
    uiuj_cart = np.zeros((np.size(r), np.size(phi), 3, 3))
    uiuj_cyl = np.zeros((np.size(r), np.size(phi), 3, 3))
    uiuj = np.zeros((np.size(r), 3, 3))

    # Gradient of the mean velocity
    dUjdxi_cart= np.zeros((np.size(r), np.size(phi), 3, 3))
    dUjdxi_cyl= np.zeros((np.size(r), np.size(phi), 3, 3))
    dUjdxi= np.zeros((np.size(r), 3, 3))


    # Read user defined case-dependent parameters
    config = configparser.ConfigParser()
    config.read('case_params.ini')
    Re_b = float(config['Reynolds']['Re_b'])

    ## Calculations
    #-------------
    theta, rho= np.meshgrid(phi, r, indexing='xy')
    nu = 1/Re_b
    u_t = my_math.get_utau(r, phi, deriv_vel, theta, nu)
    Re_t = u_t*1/nu
    y = 0.5 - r 
    y_plus = (y*u_t)/nu
    

    # Mean velocities <U_i>
    #----------------------
    Ui_cart = stats_vel[:, :, 0:3]

    # <U_i U_j>
    #----------
    UiUj_cart[:, :, 0, 0] = stats_vel[:,:,4]
    UiUj_cart[:, :, 0, 1] = stats_vel[:,:,8]
    UiUj_cart[:, :, 0, 2] = stats_vel[:,:,10]
    UiUj_cart[:, :, 1, 0] = UiUj_cart[:, :, 0, 1]
    UiUj_cart[:, :, 1, 1] = stats_vel[:,:,5]
    UiUj_cart[:, :, 1, 2] = stats_vel[:,:,9]
    UiUj_cart[:, :, 2, 0] = UiUj_cart[:, :, 0, 2]
    UiUj_cart[:, :, 2, 1] = UiUj_cart[:, :, 1, 2]
    UiUj_cart[:, :, 2, 2] = stats_vel[:,:,6]

    # <u_i u_j> Reynolds stress
    #--------------------------
    uiuj_cart = UiUj_cart - np.einsum('...i,...j', Ui_cart, Ui_cart)
    uiuj_cyl = my_math.transform_vect(theta, uiuj_cart, 2)
    
    # Average in circumferential direction:
    uiuj = np.mean(uiuj_cyl, axis=1)


    # d <U_j> / d x_i
    #----------------
    dUjdxi_cart[:, :, 0, 0] = deriv_vel[:,:,0] # d<U_1> / d x_1
    dUjdxi_cart[:, :, 0, 1] = deriv_vel[:,:,1] # d<U_1> / d x_2
    dUjdxi_cart[:, :, 0, 2] = 0            # d<U_1> / d x_3
    dUjdxi_cart[:, :, 1, 0] = deriv_vel[:,:,2] # d<U_2> / d x_1
    dUjdxi_cart[:, :, 1, 1] = deriv_vel[:,:,3] # d<U_2> / d x_2
    dUjdxi_cart[:, :, 1, 2] = 0            # d<U_2> / d x_3
    dUjdxi_cart[:, :, 2, 0] = deriv_vel[:,:,4] # d<U_3> / d x_1
    dUjdxi_cart[:, :, 2, 1] = deriv_vel[:,:,5] # d<U_3> / d x_2
    dUjdxi_cart[:, :, 2, 2] = 0            # d<U_3> / d x_3
    dUjdxi_cyl = my_math.transform_vect(theta, dUjdxi_cart, 2)
    
    # Average in circumferential direction:
    dUjdxi = np.mean(dUjdxi_cyl, axis=1)


    nu_t = - uiuj[:, 0, 2] / dUjdxi[:, 2, 0]
            
    file_path = outdata
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)
    
    np.savetxt(file_path, 
            np.transpose([
                np.flipud(y_plus), 
                np.flipud(nu_t*Re_b),
                ]), header='y_plus\t<nu_t>/nu')


def run():
    """ Convert npz data to text files."""

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    for span in timeranges_select:
        save_nut(span)
