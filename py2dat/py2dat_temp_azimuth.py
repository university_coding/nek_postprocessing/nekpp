#!/usr/bin/env python3.5
#-----------------------
# This script reads previously saved data 
# and saves  profiles 
# of first and second order temperature statistics
# in a simple text file.
# ----------------------------------------------------
# Author:   Steffen Straub
# Date  :   2018/03/27
#-----------------------------------------------------

import numpy as np
from ..misc import my_math
import os

import configparser


import pdb

# Define function
def save_temp_azimuth(timerange_select, nfields, fstart):
    
    # Load data
    #----------
    data_dir = 'stats_2d_sym/' + timerange_select + '/'
    data_name = ['2d_sym_temp_tt{0:d}.npz'.format(k) for k in range(fstart, nfields+fstart)]

#    data_name = (
#            '/2d_sym_temp_tt1.npz',
#            '/2d_sym_temp_tt2.npz',
#            '/2d_sym_temp_tt3.npz',
#            '/2d_sym_temp_tt4.npz',
#            )

    print('Note: label_list is hardcoded for now.')
    label = (
        'halfconst071',
        'halfsin071',
        'halfconst0025',       
        'halfsin0025',
        )

   
    for i in range(len(data_name)):
    
        print("Processing " + label[i] + ' at time ' + timerange_select)

        with np.load(data_dir + data_name[i]) as data:
        
            # Coordinates
            X = data['X_sym']
            Y = data['Y_sym']
       
            # individual radial and circumferential distribution
            r = data['r']
            phi = data['phi']
        
            # data averaged over time, streamwise direction, and exploited symmetry
            stats_sym = data['stats_sym']

    
        # 2) Velocity statistics
        indata_v = 'stats_' + timerange_select + '/statistics_matrix_tv1.npz'
        with np.load(indata_v) as data:
                    
            stats_vel = data['stats_m']
            deriv_vel = data['deriv_m']
    
        
        # Read user defined case-dependent parameters
        config = configparser.ConfigParser()
        config.read('case_params.ini')
        Re_b = float(config['Reynolds']['Re_b'])

        ## Calculations
        #-------------
        theta, rho= np.meshgrid(phi, r, indexing='xy')
        nu = 1/Re_b
        u_t = my_math.get_utau(r, phi, deriv_vel, theta, nu)
        Re_t = u_t*1/nu
        y = 0.5 - r 
        y_plus = (y*u_t)/nu
        
    
        uitheta = np.zeros((np.shape(stats_sym[..., 2:5])))
    
        Theta = stats_sym[..., 0]
        theta_theta = stats_sym[..., 1]
        uitheta[..., 0] = stats_sym[..., 2]
        uitheta[..., 1] = stats_sym[..., 3]
        uitheta[..., 2] = stats_sym[..., 4]
        

        # Save 5 profiles at phi =
        # -pi/2
        # -pi/4
        # 0
        # pi/4
        # pi/2
        n_probes = 5
        probes_sep = int( (np.shape(Theta)[1]-1) / (n_probes-1))
    
        for k in range(n_probes):
            file_path = 'profiles/' + timerange_select + '/' + label[i] + '/probe_{0:d}'.format(k) + '_temp.dat'
            directory = os.path.dirname(file_path)
            if not os.path.exists(directory):
                os.makedirs(directory)
            
        
            np.savetxt(file_path, 
                    np.transpose([
                        np.flipud(y_plus), 
                        np.flipud(Theta[:, probes_sep*k]),
                        np.flipud(theta_theta[:, probes_sep*k]),
                        np.flipud(uitheta[..., 0][:, probes_sep*k]),
                        np.flipud(uitheta[..., 1][:, probes_sep*k]),
                        np.flipud(uitheta[..., 2][:, probes_sep*k]),
                        ]), header='y_plus\t<Theta>+\t<theta theta>+\t<u_r theta>+\t<u_t theta>+\t<u_z theta>+')

    



def run(nfields=4, fstart=0):
    """
    Write temperature at 5 probes in text files.

    nfields     Number of fields to interpolate
    fstart      First field starts by, e.g (0, 1)
    """

    # Call function

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    for span in timeranges_select:
        save_temp_azimuth(span, nfields, fstart)
