#!/usr/bin/env python3.5
#-----------------------
# This script reads previously saved data 
# and plots the result as 2d contour plots
# ----------------------------------------------------
# Author:   Steffen Straub
# Date  :   2018/04/05
#-----------------------------------------------------

import numpy as np
import matplotlib
matplotlib.rc('text', usetex=True)
from matplotlib import pyplot as plt

from matplotlib import colors as cl
from ..misc import my_math
import os

import configparser

import pdb



# Define function
def plot_individual(timerange_select, alpha_max071, alpha_max0025, ifpgf):

    # Load data
    #----------
    data_dir = 'stats_2d_sym/' + timerange_select + '/'
    data_name = ['/2d_sym_turb_diff_tt{0:d}.npz'.format(k) for k in range(4)]

    data_dir_v = 'profiles/' + timerange_select + '/'
    data_name_v = 'nut.dat'
    nut = np.loadtxt(data_dir_v + data_name_v)

    label = (
        'halfconst071',
        'halfsin071',
        'halfconst0025',       
        'halfsin0025',
        )

    figwidth = my_math.cm2in(6)
    figheight = my_math.cm2in(7)
   
    for i in range(len(data_name)):
        plt.close('all')
    
        print("Processing " + label[i] + ' at time ' + timerange_select)

        with np.load(data_dir + data_name[i]) as data:
        
            # Coordinates
            X = data['X_sym']
            Y = data['Y_sym']
       
            # individual radial and circumferential distribution
            r = data['r']
            phi = data['phi']
        
            # data averaged over time, streamwise direction, and exploited symmetry
            stats_sym = data['stats_sym']


        # Read user defined case-dependent parameters
        config = configparser.ConfigParser()
        config.read('case_params.ini')
        Re_b = float(config['Reynolds']['Re_b'])

        Pr= np.array((0.71, 0.025))
        Pe = Re_b*Pr

        alpha_t = stats_sym[..., 0:3] 



        # Plot
        #-----
        n_cont = 30     # Number of contour levels
        c_max = (alpha_max071, alpha_max0025)


        rphiz_list = (
                'r',
                'phi',
                'z',
                )

        # alpha_t radial
        fig = plt.figure(figsize=(figwidth,figheight))
        if ('071' in label[i]):
            cnt = plt.contourf(X, Y, alpha_t[..., 0]*Pe[0]/Re_b, 
                 np.linspace(0, c_max[0], n_cont), 
                 extend='both',
                 cmap='magma')
            cbar = plt.colorbar()
            cbar.set_ticks(np.linspace(0, c_max[0], 7))

        else:
            cnt = plt.contourf(X, Y, alpha_t[..., 0]*Pe[1]/Re_b, 
                 np.linspace(0, c_max[1], n_cont), 
                 extend='both',
                 cmap='magma')
            cbar = plt.colorbar()
            cbar.set_ticks(np.linspace(0, c_max[1], 7))
   
        # This is the fix for the white lines between contour levels
        for c in cnt.collections:
            c.set_edgecolor("face")
        plt.axis('scaled')
        plt.xlabel('$x / D$')
        plt.ylabel('$y / D$')
        plt.xlim(0, 0.5)
        plt.ylim(-0.5, 0.5)
        plt.tight_layout()
    
        directory = 'contours/' + timerange_select + '/' + label[i] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + 'turb_diff_alpha_r.png')
        if (ifpgf):
            plt.savefig(directory + 'turb_diff_alpha_r.pgf')


        # alpha_t azimuthal
        fig = plt.figure(figsize=(figwidth,figheight))
        if ('071' in label[i]):
            cnt = plt.contourf(X, Y, alpha_t[..., 1]*Pe[0]/Re_b, 
                 np.linspace(0, c_max[0], n_cont), 
                 extend='both',
                 cmap='magma')
            cbar = plt.colorbar()
            cbar.set_ticks(np.linspace(0, c_max[0], 7))

        else:
            cnt = plt.contourf(X, Y, alpha_t[..., 1]*Pe[1]/Re_b, 
                 np.linspace(0, c_max[1], n_cont), 
                 extend='both', 
                 cmap='magma')
            cbar = plt.colorbar()
            cbar.set_ticks(np.linspace(0, c_max[1], 7))
        # This is the fix for the white lines between contour levels
        for c in cnt.collections:
            c.set_edgecolor("face")
        plt.axis('scaled')
        plt.xlabel('$x / D$')
        plt.ylabel('$y / D$')
        plt.xlim(0, 0.5)
        plt.ylim(-0.5, 0.5)
        plt.tight_layout()
    
        directory = 'contours/' + timerange_select + '/' + label[i] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + 'turb_diff_alpha_phi.png')
        if (ifpgf):
            plt.savefig(directory + 'turb_diff_alpha_phi.pgf')
   

        # Turbulent Prandtl number
        # nu_t / alpha_t
        #---------------
        c_max_Pr = (1, 1)
        c_min_Pr = (0, 0)

        # Pr_t radial
        fig = plt.figure(figsize=(figwidth,figheight))
        if (i < 3):
            cnt = plt.contourf(X, Y, np.expand_dims(nut[:, 1], axis=1) / alpha_t[..., 0], 
                 np.linspace(c_min_Pr[0], c_max_Pr[0], n_cont), extend='both')
            cbar = plt.colorbar()
            cbar.set_ticks(np.linspace(c_min_Pr[0], c_max_Pr[0], 7))

        else:
            cnt = plt.contourf(X, Y, np.expand_dims(nut[:, 1], axis=1) / alpha_t[..., 0], 
                 np.linspace(c_min_Pr[1], c_max[1], n_cont), extend='both')
            cbar = plt.colorbar()
            cbar.set_ticks(np.linspace(c_min_Pr[1], c_max_Pr[1], 7))
        plt.axis('scaled')
        plt.xlabel('$x / D$')
        plt.ylabel('$y / D$')
        plt.xlim(0, 0.5)
        plt.ylim(-0.5, 0.5)
        plt.tight_layout()
    
        directory = 'contours/' + timerange_select + '/' + label[i] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + 'turb_diff_Pr_r.png')
        if (ifpgf):
            plt.savefig(directory + 'turb_diff_Pr_r.pgf')

 
        # Pr_t azimuthal
        fig = plt.figure(figsize=(figwidth,figheight))
        if (i < 3):
            cnt = plt.contourf(X, Y, np.expand_dims(nut[:, 1], axis=1) / alpha_t[..., 1], 
                 np.linspace(c_min_Pr[0], c_max_Pr[0], n_cont), extend='both')
            cbar = plt.colorbar()
            cbar.set_ticks(np.linspace(c_min_Pr[0], c_max_Pr[0], 7))

        else:
            cnt = plt.contourf(X, Y, np.expand_dims(nut[:, 1], axis=1) / alpha_t[..., 0], 
                 np.linspace(c_min_Pr[1], c_max_Pr[1], n_cont), extend='both')
            cbar = plt.colorbar()
            cbar.set_ticks(np.linspace(c_min_Pr[1], c_max_Pr[1], 7))
        plt.axis('scaled')
        plt.xlabel('$x / D$')
        plt.ylabel('$y / D$')
        plt.xlim(0, 0.5)
        plt.ylim(-0.5, 0.5)
        plt.tight_layout()
    
        directory = 'contours/' + timerange_select + '/' + label[i] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + 'turb_diff_Pr_phi.png')
        if (ifpgf):
            plt.savefig(directory + 'turb_diff_Pr_phi.pgf')
 

def plot_compare(timerange_select, alpha_max071, alpha_max0025, ifpgf):

    # Load data
    #----------
    data_dir = 'stats_2d_sym/' + timerange_select + '/'
    data_name = ['/2d_sym_turb_diff_tt{0:d}.npz'.format(k) for k in range(4)]

    label = (
        'halfconst071',
        'halfsin071',
        'halfconst0025',       
        'halfsin0025',
        )

    nr = int(np.loadtxt('interpmesh/number_of_points.txt')[0])
    nphi = int(np.loadtxt('interpmesh/number_of_points.txt')[1])
    nphi_sym = int( (nphi-1)/2 + 1 )
    n_entries = 3
    n_labels = len(label)

    dataset = np.zeros((nr, nphi_sym, n_entries, n_labels))
    for i in range(n_labels):
        with np.load(data_dir + data_name[i]) as data:
        
            # Coordinates
            X = data['X_sym']
            Y = data['Y_sym']
       
            # individual radial and circumferential distribution
            r = data['r']
            phi = data['phi']
        
            # data averaged over time, streamwise direction, and exploited symmetry
            stats_sym = data['stats_sym']

        dataset[..., i] = stats_sym

    # Read user defined case-dependent parameters
    config = configparser.ConfigParser()
    config.read('case_params.ini')
    Re_b = float(config['Reynolds']['Re_b'])

    Pr= np.array((0.71, 0.025))
    Pe = Re_b*Pr

   
   
    # Plot
    #-----
    plt.close('all')

    n_cont = 30     # Number of contour levels
    c_max = (alpha_max071, alpha_max0025)


    figwidth = my_math.cm2in(6)
    figheight = my_math.cm2in(5)


    # Pr 071
    out_names = (
            'alpha_t_cmp_',
            'alpha_t_cmp_',
            )

    # loop over 071 and 0025
    for p in range(2):
        # compare alpha_r and alpha_phi
        fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(figwidth, figheight))
        
        axes[0].axis('off') 
        axes[0].axis('equal')
        axes[1].axis('off')
        axes[1].axis('equal')
        cnt1 = axes[0].contourf(-X, Y, dataset[:, :, 0, 1 + p*2]*Pe[p]/Re_b, 
                np.linspace(0, c_max[p], n_cont), 
                cmap='magma',
                extend='both')
    
        cnt2 = axes[1].contourf(X, Y, dataset[:, :, 1, 1 + p*2]*Pe[p]/Re_b,  
                np.linspace(0, c_max[p], n_cont), 
                cmap='magma',
                extend='both')
        
        fig.subplots_adjust(bottom=0.00, top=1.0,left=0.00, right=0.7,
                                    wspace=0.05, hspace=0.02)
        
        cb_ax = fig.add_axes([0.75, 0.1, 0.05, 0.8])
        # !
        # Caution the contourf with the larger range needs to be selected here!
        # !
        cbar = fig.colorbar(cnt2, cax=cb_ax)
        cbar.set_ticks(np.linspace(0, c_max[p], 7))
    
    
        directory = 'contours/' + timerange_select + '/compare/'
        if not os.path.exists(directory):
            os.makedirs(directory)
        # This is the fix for the white lines between contour levels
        for c in cnt1.collections:
            c.set_edgecolor("face")
        for c in cnt2.collections:
            c.set_edgecolor("face")
    
        fname = directory + out_names[0] + label[1 + p*2] 
        if (ifpgf):
            plt.savefig(fname + '.pgf')
        plt.savefig(fname + '.png')
        plt.savefig(fname + '.pdf')
   

#    # Pr 071
#    # alpha_tphi
#    fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(figwidth, figheight))
#    
#    axes[0].axis('off') 
#    axes[0].axis('equal')
#    axes[1].axis('off')
#    axes[1].axis('equal')
#    cnt1 = axes[0].contourf(-X, Y, dataset[:, :, 1, 0], 
#            np.linspace(0, c_max[0], n_cont), 
#            cmap='magma',
#            extend='both')
#    cnt2 = axes[1].contourf(X, Y, dataset[:, :, 1, 1],
#            np.linspace(0, c_max[0], n_cont), 
#            cmap='magma',
#            extend='both')
#
#    
#    fig.subplots_adjust(bottom=0.00, top=1.0,left=0.00, right=0.7,
#                                wspace=0.05, hspace=0.02)
#    
#    cb_ax = fig.add_axes([0.75, 0.1, 0.05, 0.8])
#    # !
#    # Caution the contourf with the larger range needs to be selected here!
#    # !
#    cbar = fig.colorbar(cnt2, cax=cb_ax)
#    cbar.set_ticks(np.linspace(0, c_max[0], 7))
#
#    # This is the fix for the white lines between contour levels
#    for c in cnt1.collections:
#        c.set_edgecolor("face")
#    for c in cnt2.collections:
#        c.set_edgecolor("face")
#
#
#    directory = 'contours/' + timerange_select + '/compare/'
#    if not os.path.exists(directory):
#        os.makedirs(directory)
#    if (ifpgf):
#        plt.savefig(directory + 'alpha_t_phi_cmp_' + label[0] + '_' + label[1] + '.pgf')
#    plt.savefig(directory + 'alpha_t_phi_cmp_' + label[0] + '_' + label[1] + '.png')
#    plt.savefig(directory + 'alpha_t_phi_cmp_' + label[0] + '_' + label[1] + '.pdf')

#    plt.show(0)
    


        # 2nd colorbar on the left side
        #------------------------------
#        ax = plt.subplot(111)
#        cnt_right = ax.contourf(X, Y, dataset[:, :, 0, i], n_cont)
#        cnt_left = ax.contourf(-X, Y, dataset[:, :, 0, i+1]*100, n_cont)

#        divider = make_axes_locatable(ax)
#        cax_left = divider.append_axes('left', size='5%', pad=0.5)
#        cax_right = divider.append_axes('right', size='5%', pad=0.1)
#        plt.colorbar(cnt_left, cax=cax_left)
#        plt.colorbar(cnt_right, cax=cax_right)

def run(ifpgf=False, alpha_max071=100, alpha_max0025=1):
    """Plot 2d contour plot of turb. diffusion/thermal diffusion (alpha_t/alpha) and Pr_t.
    
    Arguments:
    ifpgf       --      save also in pgf format
    alpha_max071        Maximum for contour plot of turb. therm. diff at Pr=0.71
    alpha_max0025       Maximum for contour plot of turb. therm. diff at Pr=0.025
    """

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    # Loop over specified timeranges
    for span in timeranges_select:
        # Call a postprocessing script
        plot_individual(span, alpha_max071, alpha_max0025, ifpgf)

def run_compare(ifpgf=False, alpha_max071=100, alpha_max0025=2):
    """Plot 2d contour plot of turb. diffusion/thermal diffusion (alpha_t/alpha) and Pr_t comparing cases with same Prandtl number.
    
    Arguments:
    ifpgf       --      save also in pgf format
    alpha_max071        Maximum for contour plot of turb. therm. diff at Pr=0.71
    alpha_max0025       Maximum for contour plot of turb. therm. diff at Pr=0.025

    """

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    # Loop over specified timeranges
    for span in timeranges_select:
        # Call a postprocessing script
        plot_compare(span, alpha_max071, alpha_max0025, ifpgf)

