#!usr/bin/env python3.5
#----------------------
# load 2d spectra and plot them
#----------------------------------------------------------------------
# Author:   Steffen Straub
# Date:     2018/02/02
#----------------------------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt
from ..misc import my_save_fig as msf
from ..misc import my_math

import configparser

import pdb

plt.close('all')

def p_spectra_t(timerange_select, n_temp, t_fld_inhomog):
    ## Load data
    #-----------
    dir_spectra = 'results/' + timerange_select + '/'
    f_spectra = dir_spectra + 'power_spectra_t.npz'
    with np.load(f_spectra) as data:
        E = data['E']
        r = data['r']
        phi = data['phi']
        z = data['z']
    
    
    # Some pre-calculations and definitions
    nr = len(r)
    nphi = len(phi)
    nz = len(z)
    dphi = phi[1]
    dz = z[1]
    
    theta, rho, zz = np.meshgrid(phi, r, z, indexing='xy')   
    
    # Read user defined case-dependent parameters
    config = configparser.ConfigParser()
    config.read('case_params.ini')
    Re_b = float(config['Reynolds']['Re_b'])
    Re_t = float(config['Reynolds']['Re_t'])

    nu = 1/Re_b
    R = 0.5
    u_t = Re_t / Re_b
    y = R - r
    
    # Frequency (inverse of wavelength)
    f_z = np.fft.fftfreq((nz-1), dz)
    f_phi = np.fft.fftfreq((nphi-1), dphi)
    
    # Wavelength (only positive frequency terms)
    w_z = 1/f_z[1:int(nz/2)]
    w_phi = 1/f_phi[1:int(nphi/2)]
    
    # Wavenumber (only positive frequency terms)
    k_z = 2*np.pi * f_z[1:int(nz/2)]
    k_phi = 2*np.pi * f_phi[1:int(nphi/2)]
    
    # Conversion in plus units
    y_p = y * Re_t
    w_z_p = w_z * Re_t
    w_phi_p = w_phi * Re_t
    Y_p_z, W_z_p = np.meshgrid(y_p, w_z_p, indexing='ij')
    Y_p_phi, W_phi_p = np.meshgrid(y_p, w_phi_p, indexing='ij')

    mesh_W_z_p, mesh_W_phi_p = np.meshgrid(w_z_p, w_phi_p, indexing='ij')
    mesh_k_z, mesh_k_phi = np.meshgrid(k_z, k_phi, indexing='ij')
    

    
   
    i1 = 0
    i2 = 0   
    for t_fld in range(n_temp):

        if t_fld in t_fld_inhomog:
            ## Declare arrays
            R_z = np.zeros((nr, nphi, nz, len(t_fld_inhomog)))
            t2 = np.zeros((nr, nphi, len(t_fld_inhomog)))
    
            # Correlation function R(s) (see Pope, p. 686, eq. E.25)
            R_z[..., i1] = nz * np.real(np.fft.ifft(E[..., t_fld]))
            R_z_sym = my_math.left_right_sym(R_z, phi, tensor_order=0)
            
            # Variance
            t2[..., i1] = R_z[:, :, 0, i1]


            # Save 5 profiles at phi =
            # -pi/2
            # -pi/4
            # 0
            # pi/4
            # pi/2
            n_probes = 5
            probes_sep = int( (np.shape(R_z_sym)[1]-1) / (n_probes-1))


            ## Plot
            #------
            # Two point correlations
            for k in range(n_probes):       # loop over all probe positions
                phi_probe = k*probes_sep
                plt.figure()
                plt.contourf(zz[:, phi_probe, :], rho[:, phi_probe, :], 
                        (R_z_sym[:, phi_probe, :, i1].T/R_z_sym[:, phi_probe, 0, i1]).T)
                plt.colorbar()
                plt.xlabel(r'$z / D$')
                plt.ylabel(r'$r / D$')
                plt.title(r'$R_{\vartheta \vartheta}$')
                msf.my_save_fig('probe_{0:d}_t{1:d}_2pc_z'.format(k, t_fld), '.png', 
                        'plots/' + timerange_select + '/')
    
            i1 += 1
        else:
            # Sum over modes in homogeneous direction for keeping only dependency on r and either z or phi
            E_z =  np.sum(E, axis=1)
            E_t =  np.sum(E, axis=2)
            
            ## Declare arrays
            R_z = np.zeros((nr, nz, n_temp-len(t_fld_inhomog)))
            R_t = np.zeros((nr, nphi, n_temp-len(t_fld_inhomog)))
            t2 = np.zeros((nr, n_temp-len(t_fld_inhomog)))
    
            # Correlation function R(s) (see Pope, p. 686, eq. E.25)
            R_z[..., i2] = nz * np.real(np.fft.ifft(E_z[..., t_fld]))
            R_t[..., i2] = nphi * np.real(np.fft.ifft(E_t[..., t_fld]))
            
            # Variance
            t2[..., i2] = R_z[:, 0, i2]

            # index of wall-normal location of maximum variance
            imax = np.argmax(t2[..., i2])

            # maximum for scaling
            E2d_max = (np.max(np.max(
                mesh_k_z * mesh_k_phi/r[imax] * E[imax, 1:int(nphi/2), 1:int(nz/2), t_fld].T,
                axis=0),
                axis=0)
                )

    
            ## Plot
            #------
            # Two point correlations
            plt.figure()
            plt.contourf(zz[:, 0, :], rho[:, 0, :], (R_z[..., i2].T/R_z[:, 0, i2]).T)
            plt.colorbar()
            plt.xlabel(r'$z / D$')
            plt.ylabel(r'$r / D$')
#            plt.title(r'$R_{\vartheta \vartheta}$')
            msf.my_save_fig('t{0:d}_2pc_z'.format(t_fld), '.png', 
                        'plots/' + timerange_select + '/')
        
            plt.figure()
            plt.contourf(theta[:, :, 0], rho[:, :, 0], (R_t[..., i2].T/R_t[:, 0, i2]).T)
            plt.colorbar()
            plt.xlabel(r'$\varphi$')
            plt.ylabel(r'$r / D$')
#            plt.title(r'$R_{\vartheta \vartheta}$')
            msf.my_save_fig('t{0:d}_2pc_t'.format(t_fld), '.png', 
                        'plots/' + timerange_select + '/')

            # Energy spectra   
            plt.figure()
            plt.contourf(W_z_p, Y_p_z, k_z * E_z[:, 1:int(nz/2), t_fld] * u_t**2) 
            plt.xscale('log')
            plt.yscale('log')
            plt.ylim([1, Re_t/2])
            plt.ylabel(r'$y^+$')
            plt.xlabel(r'$\lambda_z^+$')
            plt.colorbar()
#            plt.title(r'Premultiplied spectrum $k_z E_{\vartheta \vartheta}$')
            msf.my_save_fig('t{0:d}_ps_z'.format(t_fld), '.png', 
                    'plots/' + timerange_select + '/')

            plt.figure()
            plt.contourf(W_phi_p*np.expand_dims(r, 1), Y_p_phi, 
                    np.outer(r, k_phi) * E_t[:, 1:int(nphi/2), t_fld] * u_t**2)  
            plt.xscale('log')
            plt.yscale('log')
            plt.xlim([30, np.max(W_phi_p*np.expand_dims(r, 1))])
            plt.ylim([1, Re_t/2])
            plt.ylabel(r'$y^+$')
            plt.xlabel(r'$\lambda_\varphi^+$')
            plt.colorbar()
#            plt.title(r'Premultiplied spectrum $k_\varphi E_{\vartheta \vartheta}$')
            msf.my_save_fig('t{0:d}_ps_t'.format(t_fld), '.png', 
                    'plots/' + timerange_select + '/')

            
            # 2d Energy Spectra at location of maximum variance
            plt.figure()
#            plt.contourf(mesh_W_z_p, mesh_W_phi_p*r[imax],
#                    mesh_k_z * mesh_k_phi/r[imax] * E[imax, 1:int(nphi/2), 1:int(nz/2), t_fld].T * u_t**2)
            plt.contour(mesh_W_z_p, mesh_W_phi_p * r[imax],
                    (mesh_k_z * mesh_k_phi/r[imax] * E[imax, 1:int(nphi/2), 1:int(nz/2), t_fld].T
                        / E2d_max), (0.1, 0.5))
#            plt.ylim([20, 1000])
#            plt.contour(mesh_k_z, mesh_k_phi/r[imax],
#                    (mesh_k_z * mesh_k_phi/r[imax]* E[imax, 1:int(nphi/2), 1:int(nz/2), t_fld].T
#                        / E2d_max), (0.1, 0.5))
            plt.xscale('log')
            plt.yscale('log')
            plt.xlabel(r'$\lambda_z^+$')
            plt.ylabel(r'$\lambda_\varphi^+$')
            msf.my_save_fig('t{0:d}_ps_2d_maxfluct_t'.format(t_fld), '.png', 
                    'plots/' + timerange_select + '/')

    
            plt.close('all')

            i2 += 1
    
    
    
#    # Temperature Variance / RMS
#    plt.figure()
#    for t_fld in range(n_temp):
#        plt.semilogx(y_p, np.sqrt(t2[..., t_fld])*u_t)
#    plt.xlim([1, 180])
#    plt.xlabel(r'$y^+$')
#    plt.ylabel(r'$\mathbf{\vartheta}_{rms}^+$')
#    msf.my_save_fig('t_rms', '.png', 'plots/temp/')
        

def p_spectra_v(timerange_select):
    ## Load data
    #-----------
    dir_spectra = 'results/' + timerange_select + '/'
    f_spectra = dir_spectra + 'power_spectra_v.npz'
    with np.load(f_spectra) as data:
        E_2d_rr = data['E_2d_rr']
        E_2d_tt = data['E_2d_tt']
        E_2d_zz = data['E_2d_zz']
        E_2d_rz = data['E_2d_rz']
    
        r = data['r']
        phi = data['phi']
        z = data['z']
    
    
    # Some pre-calculations and definitions
    nr = len(r)
    nphi = len(phi)
    nz = len(z)
    dphi = phi[1]
    dz = z[1]
    
    theta, rho, zz = np.meshgrid(phi, r, z, indexing='xy')   
    
    
    
    # Read user defined case-dependent parameters
    config = configparser.ConfigParser()
    config.read('case_params.ini')
    Re_b = float(config['Reynolds']['Re_b'])
    Re_t = float(config['Reynolds']['Re_t'])

    nu = 1/Re_b
    R = 0.5
    u_t = Re_t / Re_b
    y = R - r
    
    # Frequency (inverse of wavelength)
    f_z = np.fft.fftfreq((nz-1), dz)
    f_phi = np.fft.fftfreq((nphi-1), dphi)
    
    # Wavelength (only positive frequency terms)
    w_z = 1/f_z[1:int(nz/2)]
    w_phi = 1/f_phi[1:int(nphi/2)]
    
    # Wavenumber (only positive frequency terms)
    k_z = 2*np.pi * f_z[1:int(nz/2)]
    k_phi = 2*np.pi * f_phi[1:int(nphi/2)]
    
    # Conversion in plus units
    y_p = y * Re_t
    w_z_p = w_z * Re_t
    w_phi_p = w_phi * Re_t
    Y_p_z, W_z_p = np.meshgrid(y_p, w_z_p, indexing='ij')
    Y_p_phi, W_phi_p = np.meshgrid(y_p, w_phi_p, indexing='ij')
    
    mesh_W_z_p, mesh_W_phi_p = np.meshgrid(w_z_p, w_phi_p, indexing='ij')
    mesh_k_z, mesh_k_phi = np.meshgrid(k_z, k_phi, indexing='ij')


    
    # Sum over modes in homogeneous direction for keeping only dependency on r and either z or phi
    E_rr_z = np.sum(E_2d_rr, axis=1)
    E_rr_t = np.sum(E_2d_rr, axis=2)
    E_tt_z = np.sum(E_2d_tt, axis=1)
    E_tt_t = np.sum(E_2d_tt, axis=2)
    E_uu_z = np.sum(E_2d_zz, axis=1)
    E_uu_t = np.sum(E_2d_zz, axis=2)
    E_rz_z = np.sum(E_2d_rz, axis=1)
    E_rz_t = np.sum(E_2d_rz, axis=2)
    
    
    # Maximum of premultiplied spectrum for plotting
    #E_tt_max = np.max(np.outer(r, k_phi) * E_tt_t_fluct[:, 1:int(nphi/2)])
    #E_uu_max = np.max(k_z * E_uu_z_fluct[:, 1:int(nz/2)])
    #E_rz_max = np.max(k_z * E_rz_z_fluct[:, 1:int(nz/2)])
    ## In viscous units
    #E_tt_max = np.max(np.outer(r, k_phi) * E_tt_t_fluct[:, 1:int(nphi/2)] * 1/Re_t * 1/u_t**2)
    #E_uu_max = np.max(k_z * E_uu_z_fluct[:, 1:int(nz/2)] * 1/Re_t * 1/u_t**2)
    #E_rz_max = np.max(k_z * E_rz_z_fluct[:, 1:int(nz/2)] * 1/Re_t * 1/u_t**2)
    
    # Correlation function R(s) (see Pope, p. 686, eq. E.25)
    R_rr_z = nz * np.real(np.fft.ifft(E_rr_z))
    R_rr_t = nphi * np.real(np.fft.ifft(E_rr_t))
    R_tt_z = nz * np.real(np.fft.ifft(E_tt_z))
    R_tt_t = nphi * np.real(np.fft.ifft(E_tt_t))
    R_uu_z = nz * np.real(np.fft.ifft(E_uu_z))
    R_uu_t = nphi * np.real(np.fft.ifft(E_uu_t))
    R_rz_z = nz * np.real(np.fft.ifft(E_rz_z))
    R_rz_t = nphi * np.real(np.fft.ifft(E_rz_t))
    
    # Variance
    urur = R_rr_z[:, 0]
    utut = R_tt_t[:, 0]
    uzuz = R_uu_z[:, 0]
    
    # index of wall-normal location of maximum variance
    imax = (np.argmax(urur), np.argmax(utut), np.argmax(uzuz))

    # maximum for scaling
    E2d_max = np.zeros(3)
    E2d_max[0] = (np.max(np.max(
        mesh_k_z * mesh_k_phi/r[imax[0]] * E_2d_rr[imax[0], 1:int(nphi/2), 1:int(nz/2)].T,
        axis=0),axis=0))
    E2d_max[1] = (np.max(np.max(
        mesh_k_z * mesh_k_phi/r[imax[1]] * E_2d_tt[imax[1], 1:int(nphi/2), 1:int(nz/2)].T,
        axis=0),axis=0))
    E2d_max[2] = (np.max(np.max(
        mesh_k_z * mesh_k_phi/r[imax[2]] * E_2d_zz[imax[2], 1:int(nphi/2), 1:int(nz/2)].T,
        axis=0),axis=0))

    
    
    
    ## Plot
    #------
    # Two point correlations
    plt.figure()
    plt.contourf(zz[:, 0, :], rho[:, 0, :], (R_uu_z.T/R_uu_z[:, 0]).T)
    plt.colorbar()
    plt.xlabel(r'$z / D$')
    plt.ylabel(r'$r / D$')
    plt.title(r'$R_{u_z u_z}$')
    msf.my_save_fig('2pc_uu_z', '.png', 
            'plots/' + timerange_select + '/')
    
    plt.figure()
    plt.contourf(theta[:, :, 0], rho[:, :, 0], (R_uu_t.T/R_uu_t[:, 0]).T)
    plt.colorbar()
    plt.xlabel(r'$\varphi$')
    plt.ylabel(r'$r / D$')
    plt.title(r'$R_{u_z u_z}$')
    msf.my_save_fig('2pc_uu_t', '.png', 
            'plots/' + timerange_select + '/')    
    
    plt.figure()
    plt.contourf(zz[:, 0, :], rho[:, 0, :], (R_tt_z.T/R_tt_z[:, 0]).T)
    plt.colorbar()
    plt.xlabel(r'$z / D$')
    plt.ylabel(r'$r / D$')
    plt.title(r'$R_{u_\varphi u_\varphi}$')
    msf.my_save_fig('2pc_tt_z', '.png', 
            'plots/' + timerange_select + '/')    

    plt.figure()
    plt.contourf(theta[:, :, 0], rho[:, :, 0], (R_tt_t.T/R_tt_t[:, 0]).T)
    plt.colorbar()
    plt.xlabel(r'$\varphi$')
    plt.ylabel(r'$r / D$')
    plt.title(r'$R_{u_\varphi u_\varphi}$')
    msf.my_save_fig('2pc_tt_t', '.png', 
            'plots/' + timerange_select + '/')
    
    ## Energy spectra   
    #---------------
    
    # Streamwise
    plt.figure()
    plt.contourf(W_z_p, Y_p_z, k_z * E_rr_z[:, 1:int(nz/2)] * 1/u_t**2)
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim([1, Re_t/2])
    plt.ylabel(r'$y^+$')
    plt.xlabel(r'$\lambda_z^+$')
    plt.colorbar()
    plt.title(r'Premultiplied spectrum $k_z E_{u_r u_r}/u_\tau^2$')
    msf.my_save_fig('ps_urur_z', '.png', 
            'plots/' + timerange_select + '/')
    
    plt.figure()
    plt.contourf(W_z_p, Y_p_z, k_z * E_tt_z[:, 1:int(nz/2)] * 1/u_t**2)
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim([1, Re_t/2])
    plt.ylabel(r'$y^+$')
    plt.xlabel(r'$\lambda_z^+$')
    plt.colorbar()
    plt.title(r'Premultiplied spectrum $k_z E_{u_t u_t}/u_\tau^2$')
    msf.my_save_fig('ps_utut_z', '.png', 
            'plots/' + timerange_select + '/')
    
    plt.figure()
    plt.contourf(W_z_p, Y_p_z, k_z * E_uu_z[:, 1:int(nz/2)] * 1/u_t**2)
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim([1, Re_t/2])
    plt.ylabel(r'$y^+$')
    plt.xlabel(r'$\lambda_z^+$')
    plt.colorbar()
    plt.title(r'Premultiplied spectrum $k_z E_{u_z u_z}/u_\tau^2$')
    msf.my_save_fig('ps_uzuz_z', '.png', 
            'plots/' + timerange_select + '/')
    
    plt.figure()
    plt.contourf(W_z_p, Y_p_z, k_z * E_rz_z[:, 1:int(nz/2)] * 1/u_t**2)
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim([1, Re_t/2])
    plt.ylabel(r'$y^+$')
    plt.xlabel(r'$\lambda_z^+$')
    plt.colorbar()
    plt.title(r'Premultiplied spectrum $k_z E_{u_r u_z}/u_\tau^2$')
    msf.my_save_fig('ps_uruz_z', '.png', 
            'plots/' + timerange_select + '/')
    
    # Azimuthal
    plt.figure()
    plt.contourf(W_phi_p*np.expand_dims(r, 1), Y_p_phi, np.outer(r, k_phi) * E_rr_t[:, 1:int(nphi/2)] * 1/u_t**2) 
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim([1, Re_t/2])
    plt.xlim([30, np.max(W_phi_p*np.expand_dims(r, 1))])
    plt.ylabel(r'$y^+$')
    plt.xlabel(r'$\lambda_\varphi^+$')
    plt.colorbar()
    plt.title(r'Premultiplied spectrum $k_\varphi E_{u_r u_r}/u_\tau^2$')
    msf.my_save_fig('ps_urur_t', '.png', 
            'plots/' + timerange_select + '/')
    
    plt.figure()
    plt.contourf(W_phi_p*np.expand_dims(r, 1), Y_p_phi, np.outer(r, k_phi) * E_tt_t[:, 1:int(nphi/2)] * 1/u_t**2) 
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim([1, Re_t/2])
    plt.xlim([30, np.max(W_phi_p*np.expand_dims(r, 1))])
    plt.ylabel(r'$y^+$')
    plt.xlabel(r'$\lambda_\varphi^+$')
    plt.colorbar()
    plt.title(r'Premultiplied spectrum $k_\varphi E_{u_t u_t}/u_\tau^2$')
    msf.my_save_fig('ps_utut_t', '.png', 
            'plots/' + timerange_select + '/')
    
    plt.figure()
    plt.contourf(W_phi_p*np.expand_dims(r, 1), Y_p_phi, np.outer(r, k_phi) * E_uu_t[:, 1:int(nphi/2)] * 1/u_t**2) 
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim([1, Re_t/2])
    plt.xlim([30, np.max(W_phi_p*np.expand_dims(r, 1))])
    plt.ylabel(r'$y^+$')
    plt.xlabel(r'$\lambda_\varphi^+$')
    plt.colorbar()
    plt.title(r'Premultiplied spectrum $k_\varphi E_{u_z u_z}/u_\tau^2$')
    msf.my_save_fig('ps_uzuz_t', '.png', 
            'plots/' + timerange_select + '/')
    
    plt.figure()
    plt.contourf(W_phi_p*np.expand_dims(r, 1), Y_p_phi, np.outer(r, k_phi) * E_rz_t[:, 1:int(nphi/2)] * 1/u_t**2) 
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim([1, Re_t/2])
    plt.xlim([30, np.max(W_phi_p*np.expand_dims(r, 1))])
    plt.ylabel(r'$y^+$')
    plt.xlabel(r'$\lambda_\varphi^+$')
    plt.colorbar()
    plt.title(r'Premultiplied spectrum $k_\varphi E_{u_r u_z}/u_\tau^2$')
    msf.my_save_fig('ps_uruz_t', '.png', 
            'plots/' + timerange_select + '/')
    
    # 2d Energy Spectra at location of maximum variance
    plt.figure()
    plt.contour(mesh_W_z_p, mesh_W_phi_p * r[imax[2]],
            (mesh_k_z * mesh_k_phi/r[imax[2]] * E_2d_zz[imax[2], 1:int(nphi/2), 1:int(nz/2)].T
                / E2d_max[2]), (0.1, 0.5))
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel(r'$\lambda_z^+$')
    plt.ylabel(r'$\lambda_\varphi^+$')
    msf.my_save_fig('ps_2d_maxfluct', '.png', 
            'plots/' + timerange_select + '/')


    plt.close('all')

    
    # RMS
    plt.figure()
    plt.semilogx(y_p, np.sqrt(uzuz)/u_t, label=r'$z$')
    plt.semilogx(y_p, np.sqrt(utut)/u_t, label=r'$\varphi$')
    plt.semilogx(y_p, np.sqrt(urur)/u_t, label=r'$r$')
    plt.xlim([1, Re_t/2])
    plt.xlabel(r'$y^+$')
    plt.ylabel(r'$\mathbf{u}_{rms}^+$')
    plt.legend()
    msf.my_save_fig('u_rms', '.png', 
            'plots/' + timerange_select + '/')


def plot_comparison(dat_path, Re_b_list, Re_t_list, n_temp, t_fld_inhomog, iftikz):

    plt.close('all')

    print('Processing ' + ','.join(dat_path))
    # Load data
    #----------
    data_name = (
            'power_spectra_t.npz'
            )

    # Load datasets, compute power spectra, plot 
    n_data = len(dat_path)
    n_names = len(data_name)

    l_style = ('solid', 'dashed', 'dashdot', 'dotted')
    max_marker = ('ko', 'k^', 'ks', 'k*')
    # Loop over datasets
    for i in range(n_data):

        # Load data
        #----------
        dir_spectra = dat_path[i] 
        f_spectra = dir_spectra + data_name
        with np.load(f_spectra) as data:
            E = data['E']
            r = data['r']
            phi = data['phi']
            z = data['z']

        # Some pre-calculations
        nr = len(r)
        nphi = len(phi)
        nz = len(z)
        dphi = phi[1]
        dz = z[1]

        theta, rho, zz = np.meshgrid(phi, r, z, indexing='xy')

        Re_b = Re_b_list[i]
        nu = 1/Re_b
        Re_t = Re_t_list[i]
        R = 0.5
        u_t = Re_t/Re_b
        y = R - r

        # Frequency (inverse of wavelength)
        f_z = np.fft.fftfreq((nz-1), dz)
        f_phi = np.fft.fftfreq((nphi-1), dphi)
        
        # Wavelength (only positive frequency terms)
        w_z = 1/f_z[1:int(nz/2)]
        w_phi = 1/f_phi[1:int(nphi/2)]
        
        # Wavenumber (only positive frequency terms)
        k_z = 2*np.pi * f_z[1:int(nz/2)]
        k_phi = 2*np.pi * f_phi[1:int(nphi/2)]
        
        # Conversion in plus units
        y_p = y * Re_t
        w_z_p = w_z * Re_t
        w_phi_p = w_phi * Re_t
        Y_p_z, W_z_p = np.meshgrid(y_p, w_z_p, indexing='ij')
        Y_p_phi, W_phi_p = np.meshgrid(y_p, w_phi_p, indexing='ij')
    
        mesh_W_z_p, mesh_W_phi_p = np.meshgrid(w_z_p, w_phi_p, indexing='ij')
        mesh_k_z, mesh_k_phi = np.meshgrid(k_z, k_phi, indexing='ij')


        # Loop over cases
        for t_fld in range(n_temp):

            i1 = 0
            i2 = 0
            if t_fld in  t_fld_inhomog:
                # Maybe fill out later when needed
                i1 += 1

            else:
                # Sum over modes in homogeneous direction for keeping only dependency on r and either z or phi
                E_z =  np.sum(E, axis=1)
                E_t =  np.sum(E, axis=2)
                
                ## Declare arrays
                R_z = np.zeros((nr, nz, n_temp-len(t_fld_inhomog)))
                R_t = np.zeros((nr, nphi, n_temp-len(t_fld_inhomog)))
                t2 = np.zeros((nr, n_temp-len(t_fld_inhomog)))
        
                # Correlation function R(s) (see Pope, p. 686, eq. E.25)
                R_z[..., i2] = nz * np.real(np.fft.ifft(E_z[..., t_fld]))
                R_t[..., i2] = nphi * np.real(np.fft.ifft(E_t[..., t_fld]))
                
                # Variance
                t2[..., i2] = R_z[:, 0, i2]
    
                # index of wall-normal location of maximum variance
                imax = np.argmax(t2[..., i2])
    
                # maximum for scaling
                E2d_max = (np.max(np.max(
                    mesh_k_z * mesh_k_phi/r[imax] * E[imax, 1:int(nphi/2), 1:int(nz/2), t_fld].T,
                    axis=0),
                    axis=0)
                    )
                E1d_max = (np.max(np.max(
                        k_z * E_z[:, 1:int(nz/2), t_fld], 
                        axis=0),
                        axis=0)
                        )
                imax_E1d = np.unravel_index(
                        np.argmax(k_z * E_z[:, 1:int(nz/2), t_fld]), 
                        (k_z*E_z[:, 1:int(nz/2), t_fld]).shape
                        )


                # Plot
                #-----
                # group TBCs together

                plt.figure(t_fld)
                plt.contour(W_z_p, Y_p_z, k_z * E_z[:, 1:int(nz/2), t_fld]/E1d_max,
                        (0.1, 0.5),
                        linestyles=l_style[i],
                        cmap='winter') 
                plt.plot(W_z_p[imax_E1d], Y_p_z[imax_E1d], max_marker[i], 
                        markerfacecolor='none')

                i2 += 1

    # Axis options
    for t_fld in range(n_temp):
        if t_fld in t_fld_inhomog:
            pass
        else:
            plt.figure(t_fld)
            plt.xscale('log')
            plt.yscale('log')
            plt.ylim([1, Re_t/2])
            plt.ylabel(r'$y^+$')
            plt.xlabel(r'$\lambda_z^+$')
    
            p_dir = 'plots/' + 'compare_Re/'
            p_name = 't{0:d}_ps_z_cmp_Re'.format(t_fld)
            p_type = '.png'
            msf.save_tikz_png(p_name, p_dir, iftikz)
    
    
    plt.close('all')

    

def plot_compensated(dat_path, Re_b_list, Re_t_list, n_temp, t_fld_inhomog, iftikz):
    """
    Plot compensated (one-sided) power spectral density.

    Parameters
    ----------
    dat_path : list
        paths to datasets
    Re_b_list : list
        correspoding bulk Reynolds numbers
    Re_t_list : list
        correspoding friction Reynolds numbers
    n_temp : scalar
        number of temperature fields within each dataset
    t_fld_inhomog : list
        each number of an inhomogeneous temp. field
    iftikz : boolean
        save plots in tikz (tex) format?

    """

    plt.close('all')

    print('Processing ' + ','.join(dat_path))
    # Load data
    #----------
    data_name = (
            'power_spectra_t.npz'
            )
    n_data = len(dat_path)
    n_names = len(data_name)

    # Loop over datasets
    for i in range(n_data):
        # Load data
        #----------
        dir_spectra = dat_path[i] 
        f_spectra = dir_spectra + data_name
        with np.load(f_spectra) as data:
            E = data['E']
            r = data['r']
            phi = data['phi']
            z = data['z']

        # Some pre-calculations
        nr = len(r)
        nphi = len(phi)
        nz = len(z)
        dphi = phi[1]
        dz = z[1]

        Re_b = Re_b_list[i]
        nu = 1/Re_b
        Re_t = Re_t_list[i]
        R = 0.5
        u_t = Re_t/Re_b
        y = R - r

        # Frequency (inverse of wavelength)
        f_z = np.fft.fftfreq((nz-1), dz)
        # Wavelength (only positive frequency terms)
        w_z = 1/f_z[1:int(nz/2)]
        # Wavenumber (only positive frequency terms)
        k_z = 2*np.pi * f_z[1:int(nz/2)]
        # Conversion in plus units
        y_p = y * Re_t
        w_z_p = w_z * Re_t
        k_z_p = k_z * Re_t
        Y_p_z, W_z_p = np.meshgrid(y_p, w_z_p, indexing='ij')
        
        for t_fld in range(n_temp):
            i1 = 0
            i2 = 0
            if t_fld in  t_fld_inhomog:
                i1 += 1

            else:
                E_z =  np.sum(E, axis=1)
                 # Correlation function R(s) (see Pope, p. 686, eq. E.25)               
                R_z = np.zeros((nr, nz, n_temp-len(t_fld_inhomog)))
                t2 = np.zeros((nr, n_temp-len(t_fld_inhomog)))
                R_z[..., i2] = nz * np.real(np.fft.ifft(E_z[..., t_fld]))

                # Variance
                t2[..., i2] = R_z[:, 0, i2]
                imax = np.argmax(t2[..., i2])
    
                # Plot
                #-----
                # Energy spectra (one-sided -> factor 2)
                plt.figure(t_fld)
                plt.contourf(W_z_p, Y_p_z, k_z**(5/3) * E_z[:, 1:int(nz/2), t_fld],
                        cmap='cividis') 
                i2 += 1

                plt.xscale('log')
                plt.yscale('log')
                plt.ylim([1, Re_t/2])
                plt.ylabel(r'$k_z^{5/3}*E_{\vartheta \vartheta}$')
                plt.xlabel(r'$k_z^+$')
    
                p_dir = 'plots/' + 'compare_Re/'
                p_name = 't{0:d}_ps_z_Re{1:d}_compensated'.format(t_fld,Re_b_list[i])
                p_type = '.png'
                msf.save_tikz_png(p_name, p_dir, iftikz)
    
    
            plt.close('all')





def run_t(n_temp = 10):
    # Call function

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    # inhomogeneous TBC in fields:
    t_fld_inhomog = [3, 4, 8, 9]

    for span in timeranges_select:
        p_spectra_t(span, n_temp, t_fld_inhomog)


def run_v():
    # Call function

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    for span in timeranges_select:
        p_spectra_v(span)








def run_t(n_temp = 10):
    # Call function

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    # inhomogeneous TBC in fields:
    t_fld_inhomog = [3, 4, 8, 9]

    for span in timeranges_select:
        p_spectra_t(span, n_temp, t_fld_inhomog)


def run_v():
    # Call function

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    for span in timeranges_select:
        p_spectra_v(span)


