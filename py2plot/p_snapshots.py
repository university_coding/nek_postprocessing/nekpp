#!/usr/bin/env python3.5
#-----------------------
# This script reads previously saved data 
# an plots the result as 2d contour plots
# ----------------------------------------------------
# Author:   Steffen Straub
# Date  :   2019/04/09
#-----------------------------------------------------
import numpy as np
import matplotlib.pyplot as plt
import NekPP.misc.my_math as my_math
import configparser
import pdb


def plot_snapshot(t_fld, n_snap=1):
    """
    Plot cross section of the chosen temperature field.

    t_fld : scalar
        number of the thermal field to plot
    n_snap : scalar
        number of the snapshot to process

    """

    plt.close('all')

    ## Load data
    #-----------
    # Number of calculated (and interpolated) statistics & derivatives
    n_fields = 1
    
    # Construct filenames
    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')[0]
    
    f_t_base = 't{0:d}_3d_'.format(t_fld)
    interp_dir_python = 'interptemp_python/' + timeranges_select + '/'
    fname0 = interp_dir_python + f_t_base + '{0:05d}.npz'.format(n_snap) 
    
    with np.load(fname0) as data:
        fld = data['fld_3d']
        r = data['r']
        phi = data['phi']
        z = data['z']
    
    
    # Mesh generation in polar coordinates
    theta, rho = np.meshgrid(phi, r, indexing='xy')
    X, Y = my_math.pol2cart(theta, rho)
    
    n_cont = 50
    figwidth = my_math.cm2in(6)
    figheight = my_math.cm2in(4)
    
    # Draw the figure without any extra padding 
    # https://stackoverflow.com/questions/8218608/scipy-savefig-without-frames-axes-only-content/8218887#8218887
    fig = plt.figure(frameon=False)
    fig.set_size_inches(figwidth,figheight)
    ax = plt.Axes(fig, [0., 0., 0.9, 1.])
    ax.set_axis_off()
    fig.add_axes(ax)

    cnt = ax.contourf(X, Y, fld[:, :, 0, 0], n_cont, cmap='inferno')
    # Remove white lines in pdf
    for c in cnt.collections:
        c.set_edgecolor("face")
    ax.axis('scaled')
    fig.colorbar(cnt)



    fname = 'snap_t{0:d}'.format(t_fld)
    fig.savefig(fname + '.png')
    fig.savefig(fname + '.pdf')


    
   
