#!/usr/bin/env python3.5
#-----------------------
# Compare DNS and LES-RT results
#----------------------------------------------------------------------
# Author:   Steffen Straub
# Date:     2018/03/15
#----------------------------------------------------------------------

from NekPP.dat2plot import plot_temp
from NekPP.py2plot import py2plot_spectra

# Location of profiles to compare
dat_path = (
        'results_360/8195-10182/',
        'results_720/669-863/',
        'results/106-153/',
        )
Re_b_list = (
        5300,
        11700,
        19000,
        )
Re_t_list = (
        360,
        720,
        1100,
        )
n_temp = 10
t_fld_inhomog = (3, 4, 8, 9)

py2plot_spectra.plot_comparison(dat_path, Re_b_list, Re_t_list, n_temp, t_fld_inhomog, iftikz=False)


