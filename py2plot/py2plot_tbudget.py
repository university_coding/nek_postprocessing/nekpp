#!/usr/bin/env python3.5
#-----------------------
# This script reads previously saved data 
# an plots the result as 2d contour plots
# ----------------------------------------------------
# Author:   Steffen Straub
# Date  :   2018/03/27
#-----------------------------------------------------

import numpy as np
import matplotlib
matplotlib.rc('text', usetex=True)
from matplotlib import pyplot as plt

from matplotlib import colors as cl
from ..misc import my_math
import os

import configparser

import pdb



# Define function
def plot_individual(timerange_select, ifpgf):

    # Load data
    #----------
    data_dir = 'stats_2d_sym/' + timerange_select + '/'
#    data_name = (
#            '/2d_sym_tbudget_tt1.npz',
#            '/2d_sym_tbudget_tt2.npz',
#            '/2d_sym_tbudget_tt3.npz',
#            '/2d_sym_tbudget_tt4.npz',
#            )
    data_name = ['/2d_sym_tbudget_tt{0:d}.npz'.format(k) for k in range(4)]

    label = (
        'halfconst071',
        'halfsin071',
        'halfconst0025',       
        'halfsin0025',
        )

   
    for i in range(len(data_name)):
        plt.close('all')
    
        print("Processing " + label[i] + ' at time ' + timerange_select)

        with np.load(data_dir + data_name[i]) as data:
        
            # Coordinates
            X = data['X_sym']
            Y = data['Y_sym']
       
            # individual radial and circumferential distribution
            r = data['r']
            phi = data['phi']
        
            # data averaged over time, streamwise direction, and exploited symmetry
            stats_sym = data['stats_sym']

        P = stats_sym[..., 0]
        eps = stats_sym[..., 1]
        MD = stats_sym[..., 2]
        TD = stats_sym[..., 3]
        S = stats_sym[..., 4]

       
    
    

        # Plot
        #-----
        n_cont = 30     # Number of contour levels

        # Production
        fig = plt.figure()
        cnt = plt.contourf(X, Y, P, n_cont)
    
    
        # This is the fix for the white lines between contour levels
    #    for c in cnt.collections:
    #        c.set_edgecolor("face")
        plt.axis('equal')
        plt.colorbar()
        plt.xlabel('$x / D$')
        plt.ylabel('$y / D$')
        plt.tight_layout()
    
        directory = 'contours/' + timerange_select + '/' + label[i] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + 'tbudget_P.png')
        if (ifpgf):
            plt.savefig(directory + 'tbudget_P.pgf')

        # Dissipation
        fig = plt.figure()
        cnt = plt.contourf(X, Y, eps, n_cont)
        plt.axis('equal')
        plt.colorbar()
        plt.xlabel('$x / D$')
        plt.ylabel('$y / D$')
        plt.tight_layout()

        directory = 'contours/' + timerange_select + '/' + label[i] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + 'tbudget_eps.png')
        if (ifpgf):
            plt.savefig(directory + 'tbudget_eps.pgf')

        # Molecular Diffusion
        fig = plt.figure()
        cnt = plt.contourf(X, Y, MD, n_cont)
        plt.axis('equal')
        plt.colorbar()
        plt.xlabel('$x / D$')
        plt.ylabel('$y / D$')
        plt.tight_layout()

        directory = 'contours/' + timerange_select + '/' + label[i] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + 'tbudget_MD.png')
        if (ifpgf):
            plt.savefig(directory + 'tbudget_MD.pgf')

        # Turbulent Diffusion
        fig = plt.figure()
        cnt = plt.contourf(X, Y, TD, n_cont)
        plt.axis('equal')
        plt.colorbar()
        plt.xlabel('$x / D$')
        plt.ylabel('$y / D$')
        plt.tight_layout()

        directory = 'contours/' + timerange_select + '/' + label[i] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + 'tbudget_TD.png')
        if (ifpgf):
            plt.savefig(directory + 'tbudget_TD.pgf')

        # Source term
        fig = plt.figure()
        cnt = plt.contourf(X, Y, S, n_cont)
        plt.axis('equal')
        plt.colorbar()
        plt.xlabel('$x / D$')
        plt.ylabel('$y / D$')
        plt.tight_layout()

        directory = 'contours/' + timerange_select + '/' + label[i] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + 'tbudget_S.png')
        if (ifpgf):
            plt.savefig(directory + 'tbudget_S.pgf')


  
def run(ifpgf=False):
    """Plot 2d contour plot of budget of temp. variance.
    
    Arguments:
    ifpgf       --      save also in pgf format
    """

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    # Loop over specified timeranges
    for span in timeranges_select:
        # Call a postprocessing script
        plot_individual(span, ifpgf)
