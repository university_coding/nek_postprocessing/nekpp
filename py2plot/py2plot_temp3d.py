#!/usr/bin/env python3.5
#-----------------------
# This script reads previously saved data 
# and plots the result as 2d contour plots
# ----------------------------------------------------
# Author:   Steffen Straub
# Date  :   2019/10/07
#-----------------------------------------------------

import numpy as np
import matplotlib 
matplotlib.rc('text', usetex=True)
from matplotlib import pyplot as plt

from ..misc import my_math
from ..misc import my_save_fig as msf
import os

import configparser

import pdb



# Define function
def plot_individual(timerange_select, n_fields, ifpgf):

    plt.ioff()      # Turn off interactive mode


    # Load data
    #----------
    data_dir = 'stats_3d/' + timerange_select + '/'
    data_name = ['temp_{0:d}.npz'.format(k) for k in range(n_fields)]

    label_list = (
            '071',
            '0025',
            )

    for i in range(len(data_name)):
        plt.close('all')
    
        print("Processing " + label_list[i] + ' at time ' + timerange_select)

        with np.load(data_dir + data_name[i]) as data:
        
            r = data['r']
            phi = data['phi']
            z = data['z']
        
            stats_3d = data['stats_3d']
            stats_2d = data['stats_2d']
            stats_1d = data['stats_1d']


        nphi = len(phi)

        # Set up an array for y-z / x-z plane
        y = np.concatenate((-r[::-1], r))
        zz, yy = np.meshgrid(z, y, indexing='xy')
        xx = yy

        # Set up an array for cross-section plane
        rr, phiphi = np.meshgrid(r, phi, indexing='xy')
    
        # Mesh generation in polar coordinates
        theta, rho = np.meshgrid(phi, r, indexing='xy')
        # Mesh transformation in cartesian coordinates
        X, Y = my_math.pol2cart(theta, rho)

        # Mesh for wall plot
        Rphi = r[-1]*phi
        zw, rw = np.meshgrid(z, Rphi, indexing='xy')


        uitheta = np.zeros((np.shape(stats_3d[..., 2:5])))

        Theta = stats_3d[..., 0]
        theta_theta = stats_3d[..., 1]
        uitheta[..., 0] = stats_3d[..., 2]
        uitheta[..., 1] = stats_3d[..., 3]
        uitheta[..., 2] = stats_3d[..., 4]

        Nuss = stats_2d[..., 0]

        Nuss_z = stats_1d[:, 0]

        def get_yz_plane(fld):
            fld_yz = np.concatenate((fld[::-1, int((nphi-1)*3/4), :], 
                fld[:, int((nphi-1)*1/4), :]))
            return fld_yz
        def get_xz_plane(fld):
            fld_xz = np.concatenate((fld[::-1, 0, :], 
                fld[:, int((nphi-1)*1/2), :]))
            return fld_xz


        # Plot
        #-----
        n_cont = 30     # Number of contour levels

        def plot_3slice(statistic, basename):
            ## yz-plane
            fig = plt.figure()
            cnt = plt.contourf(zz, yy, get_yz_plane(statistic), n_cont)
        
            # This is the fix for the white lines between contour levels
            for c in cnt.collections:
                c.set_edgecolor("face")
            plt.colorbar()
            plt.xlabel(r'$z / D$')
            plt.ylabel(r'$y / D$')
            plt.axis('equal')
        
            directory = 'contours/' + timerange_select + '/' + label_list[i] + '/'
            if not os.path.exists(directory):
                os.makedirs(directory)
        
            fname = basename + '_x'
            plt.savefig(directory + fname + '.png')
            if (ifpgf):
                plt.savefig(directory + fname + '.pgf')
    
            # xz-plane
            fig = plt.figure()
            cnt = plt.contourf(zz, xx, get_xz_plane(statistic), n_cont)
        
            # This is the fix for the white lines between contour levels
            for c in cnt.collections:
                c.set_edgecolor("face")
            plt.colorbar()
            plt.xlabel(r'$z / D$')
            plt.ylabel(r'$x / D$')
            plt.axis('equal')
        
            directory = 'contours/' + timerange_select + '/' + label_list[i] + '/'
            if not os.path.exists(directory):
                os.makedirs(directory)
        
            fname = basename + '_y'
            plt.savefig(directory + fname + '.png')
            if (ifpgf):
                plt.savefig(directory + fname + '.pgf')

            # cross-section
            fig = plt.figure()
            cnt = plt.contourf(X, Y, statistic[:, :, -1], n_cont)
        
            # This is the fix for the white lines between contour levels
            for c in cnt.collections:
                c.set_edgecolor("face")
            plt.colorbar()
            plt.xlabel(r'$x / D$')
            plt.ylabel(r'$y / D$')
            plt.axis('equal')
        
            directory = 'contours/' + timerange_select + '/' + label_list[i] + '/'
            if not os.path.exists(directory):
                os.makedirs(directory)
        
            fname = basename + '_z'
            plt.savefig(directory + fname + '.png')
            if (ifpgf):
                plt.savefig(directory + fname + '.pgf')



        
        def plot_wall(statistic, basename):
            figwidth=my_math.cm2in(6)
            figheight=my_math.cm2in(5)
            fig = plt.figure(figsize=(figwidth, figheight))
            cnt = plt.contourf(zw, rw, statistic, 
                    )

            # Maximum location
            nphi, nz = np.shape(statistic)
            phi_max = int((nphi-1)/4)
            z_max = np.argmax(statistic[phi_max, :])
            print("Location of maximum " + basename + ": z={0:f}".format(z[z_max]))
            plt.plot(zw[phi_max, z_max], rw[phi_max, z_max], 'rx', markerfacecolor='none')


            # This is the fix for the white lines between contour levels
            for c in cnt.collections:
                c.set_edgecolor("face")
            plt.colorbar()
            plt.xlabel(r'$z / D$')
            plt.ylabel(r'$R \varphi / D$')
            plt.tight_layout()
#            plt.axis('equal')
        
            directory = 'contours/' + timerange_select + '/' + label_list[i] + '/'
            fname = basename +'_wall'
            if not os.path.exists(directory):
                os.makedirs(directory)
        
            plt.savefig(directory + fname + '.png')
            if (ifpgf):
                plt.savefig(directory + fname + '.pgf')

        def plot_Nuss(Nuss, Nuss_z,iftikz=False):
            fig = plt.figure()

            # skip last point at outlet
            plt.plot(z[:-1], Nuss_z[:-1], label=r'$\langle Nu \rangle^\varphi$')        
            plt.plot(z[:-1], Nuss[int((nphi-1)/4), :-1], label=r'$Nu(\varphi=\pi/2, z)$')
            plt.xlabel(r'$z / D$')
            plt.ylabel(r'$Nu$')
            plt.legend()

            directory = 'profiles/' + timerange_select + '/' + label_list[i] + '/'
            fname = 'Nusselt'
            msf.save_tikz_png(fname, directory, iftikz)

        
        plot_3slice(Theta, 'Theta')
        plot_3slice(theta_theta, 'theta_theta')
        plot_3slice(uitheta[..., 0], 'ur_theta')
        plot_3slice(uitheta[..., 1], 'uphi_theta')
        plot_3slice(uitheta[..., 2], 'uz_theta')
        plot_wall(Theta[-1,...], 'Theta')
        plot_wall(Nuss, 'Nuss')
        plot_Nuss(Nuss, Nuss_z)

        plt.close('all')

#        plot_3slice(stats_3d[..., 5], 'Theta_Theta')
#        plot_3slice(stats_3d[..., 6], 'U1_Theta')
#        plot_3slice(stats_3d[..., 7], 'U2_Theta')
#        plot_3slice(stats_3d[..., 8], 'U3_Theta')
#        plot_3slice(stats_3d[..., 9], 'U1')
#        plot_3slice(stats_3d[..., 10], 'U2')
#        plot_3slice(stats_3d[..., 11], 'U3')


def plot_compare(dat_path, n_fields=2, ifpgf=False):
    plt.ioff()
    plt.close('all')

    print('Note: label_list is hardcoded for now.')
    label_list = (
        '071',
        '0025',
        )

    # Load data
    n_data = len(dat_path)
    data_list = []

    if (n_data != 2):
        print("Usage: 2 setups can be compared, but n_data = {0:d}".format(n_data))
        sys.exit()

    n_cont = 30     # Number of contour levels
    def plot_wall(statistic, basename):
        fig = plt.figure()
        cnt = plt.contourf(zw, rw, statistic, n_cont)

        # This is the fix for the white lines between contour levels
        for c in cnt.collections:
            c.set_edgecolor("face")
        plt.colorbar()
        plt.xlabel(r'$z / D$')
        plt.ylabel(r'$R \varphi / D$')
        plt.axis('equal')
    
        directory = 'contours/diff/' + label_list[i] + '/'
        fname = basename +'_wall'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + fname + '.png')
        if (ifpgf):
            plt.savefig(directory + fname + '.pgf')


    data_name = ['temp_{0:d}.npz'.format(k) for k in range(n_fields)]
    for i in range(len(data_name)):
        plt.close('all')

        print("Processing " + label_list[i])

        for idata in range(n_data):
            data_dir = dat_path[idata]
            with np.load(data_dir + data_name[i]) as data:
            
                r = data['r']
                phi = data['phi']
                z = data['z']
            
                stats_3d = data['stats_3d']
                stats_2d = data['stats_2d']
                stats_1d = data['stats_1d']

            data_list.append(stats_3d)

        nphi = len(phi)
    
        # Set up an array for y-z / x-z plane
        y = np.concatenate((-r[::-1], r))
        zz, yy = np.meshgrid(z, y, indexing='xy')
        xx = yy
    
        # Set up an array for cross-section plane
        rr, phiphi = np.meshgrid(r, phi, indexing='xy')
    
        # Mesh generation in polar coordinates
        theta, rho = np.meshgrid(phi, r, indexing='xy')
        # Mesh transformation in cartesian coordinates
        X, Y = my_math.pol2cart(theta, rho)
    
        # Mesh for wall plot
        Rphi = r[-1]*phi
        zw, rw = np.meshgrid(z, Rphi, indexing='xy')
    
        Theta_w_diff = ( (data_list[0][-1, ..., 0] - data_list[1][-1, ..., 0]) 
                / np.max(data_list[0][-1, ..., 0]))

        print("Maximum relative deviation: {0:f}".format(np.max(Theta_w_diff)))
        
        # reset data_list for 0025
        data_list = []
    
        plot_wall(Theta_w_diff, "Theta_diff")
        




def run(n_fields=2, ifpgf=False):
    """Plot 2d contour plot of temperature statistics.
    
    Parameters:
    n_fields : scalar
        Number of fields

    ifpgf       --      save also in pgf format
    """

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    # Loop over specified timeranges
    for span in timeranges_select:
        # Call a postprocessing script
        plot_individual(span, n_fields, ifpgf)


