#!/usr/bin/env python3.5
#-----------------------
# This script reads previously saved data 
# an plots the result as 2d contour plots
# ----------------------------------------------------
# Author:   Steffen Straub
# Date  :   2018/03/27
#-----------------------------------------------------

import numpy as np
import matplotlib 
matplotlib.rc('text', usetex=True)
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

from matplotlib import colors as cl
from ..misc import my_math
import os

import configparser

import pdb



# Define function
def plot_individual(timerange_select, ifpgf):

    # Load data
    #----------
    data_dir = 'stats_2d_sym/' + timerange_select + '/'
    data_name = ['/2d_sym_temp_tt{0:d}.npz'.format(k) for k in range(4)]

    label = (
        'halfconst071',
        'halfsin071',
        'halfconst0025',       
        'halfsin0025',
        )

    figwidth = my_math.cm2in(6)
    figheight = my_math.cm2in(7)

   
    for i in range(len(data_name)):
        plt.close('all')
    
        print("Processing " + label[i] + ' at time ' + timerange_select)

        with np.load(data_dir + data_name[i]) as data:
        
            # Coordinates
            X = data['X_sym']
            Y = data['Y_sym']
       
            # individual radial and circumferential distribution
            r = data['r']
            phi = data['phi']
        
            # data averaged over time, streamwise direction, and exploited symmetry
            stats_sym = data['stats_sym']


        uitheta = np.zeros((np.shape(stats_sym[..., 2:5])))

        Theta = stats_sym[..., 0]
        theta_theta = stats_sym[..., 1]
        uitheta[..., 0] = stats_sym[..., 2]
        uitheta[..., 1] = stats_sym[..., 3]
        uitheta[..., 2] = stats_sym[..., 4]


        # Plot
        #-----
        n_cont = 30     # Number of contour levels

        # Theta
        fig = plt.figure(figsize=(figwidth,figheight))
        cnt = plt.contourf(X, Y, Theta, n_cont)
    
    
        # This is the fix for the white lines between contour levels
        for c in cnt.collections:
            c.set_edgecolor("face")
        plt.axis('scaled')
        plt.colorbar()
        plt.xlabel('$x / D$')
        plt.ylabel('$y / D$')
        plt.xlim(0, 0.5)
        plt.ylim(-0.5, 0.5)
        plt.tight_layout()
    
        directory = 'contours/' + timerange_select + '/' + label[i] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + 'Theta.png')
        if (ifpgf):
            plt.savefig(directory + 'Theta.pgf')

        # theta theta
        fig = plt.figure(figsize=(figwidth,figheight))
        cnt = plt.contourf(X, Y, theta_theta, n_cont)
        # This is the fix for the white lines between contour levels
        for c in cnt.collections:
            c.set_edgecolor("face")
        plt.axis('scaled')
        plt.colorbar()
        plt.xlabel('$x / D$')
        plt.ylabel('$y / D$')
        plt.xlim(0, 0.5)
        plt.ylim(-0.5, 0.5)
        plt.tight_layout()

        directory = 'contours/' + timerange_select + '/' + label[i] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + 'thetatheta.png')
        if (ifpgf):
            plt.savefig(directory + 'thetatheta.pgf')

        # ur theta
        fig = plt.figure(figsize=(figwidth,figheight))
        cnt = plt.contourf(X, Y, uitheta[..., 0], n_cont)
        # This is the fix for the white lines between contour levels
        for c in cnt.collections:
            c.set_edgecolor("face")
        plt.axis('scaled')
        plt.colorbar()
        plt.xlabel('$x / D$')
        plt.ylabel('$y / D$')
        plt.xlim(0, 0.5)
        plt.ylim(-0.5, 0.5)
        plt.tight_layout()

        directory = 'contours/' + timerange_select + '/' + label[i] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + 'urtheta.png')
        if (ifpgf):
            plt.savefig(directory + 'urtheta.pgf')

        # uphi theta
        fig = plt.figure(figsize=(figwidth,figheight))
        cnt = plt.contourf(X, Y, uitheta[..., 1], n_cont)
        # This is the fix for the white lines between contour levels
        for c in cnt.collections:
            c.set_edgecolor("face")
        plt.axis('scaled')
        plt.colorbar()
        plt.xlabel('$x / D$')
        plt.ylabel('$y / D$')
        plt.xlim(0, 0.5)
        plt.ylim(-0.5, 0.5)
        plt.tight_layout()

        directory = 'contours/' + timerange_select + '/' + label[i] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + 'uphitheta.png')
        if (ifpgf):
            plt.savefig(directory + 'uphitheta.pgf')

        # uz theta
        fig = plt.figure(figsize=(figwidth,figheight))
        cnt = plt.contourf(X, Y, uitheta[..., 2], n_cont)
        # This is the fix for the white lines between contour levels
        for c in cnt.collections:
            c.set_edgecolor("face")
        plt.axis('scaled')
        plt.colorbar()
        plt.xlabel('$x / D$')
        plt.ylabel('$y / D$')
        plt.xlim(0, 0.5)
        plt.ylim(-0.5, 0.5)
        plt.tight_layout()

        directory = 'contours/' + timerange_select + '/' + label[i] + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        plt.savefig(directory + 'uztheta.png')
        if (ifpgf):
            plt.savefig(directory + 'uztheta.pgf')


def plot_compare(timerange_select, ifpgf):

    # Load data
    #----------
    data_dir = 'stats_2d_sym/' + timerange_select + '/'
    data_name = ['/2d_sym_temp_tt{0:d}.npz'.format(k) for k in range(4)]


    label = (
        'halfconst071',
        'halfsin071',
        'halfconst0025',       
        'halfsin0025',
        )

    nr = int(np.loadtxt('interpmesh/number_of_points.txt')[0])
    nphi = int(np.loadtxt('interpmesh/number_of_points.txt')[1])
    nphi_sym = int( (nphi-1)/2 + 1 )
    n_entries = 5
    n_labels = len(label)

    dataset = np.zeros((nr, nphi_sym, n_entries, n_labels))
    for i in range(n_labels):
        with np.load(data_dir + data_name[i]) as data:
        
            # Coordinates
            X = data['X_sym']
            Y = data['Y_sym']
       
            # individual radial and circumferential distribution
            r = data['r']
            phi = data['phi']
        
            # data averaged over time, streamwise direction, and exploited symmetry
            stats_sym = data['stats_sym']

        dataset[..., i] = stats_sym
   
   
    # Plot
    #-----
    plt.close('all')

    n_cont = 30     # Number of contour levels

    data_min071 = np.min(dataset[:, :, 0, 0:2])
    data_min0025 = np.min(dataset[:, :, 0, 2:4])
    data_max071 = np.max(dataset[:, :, 0, 0:2])
    data_max0025 = np.max(dataset[:, :, 0, 2:4])
    

    figwidth = my_math.cm2in(5)
    figheight = my_math.cm2in(4)


    # Pr 071
    fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(figwidth, figheight))
    
    axes[0].axis('off') 
    axes[0].axis('equal')
    axes[1].axis('off')
    axes[1].axis('equal')
    cnt1 = axes[0].contourf(-X, Y, dataset[:, :, 0, 0], n_cont, 
            vmin=data_min071, vmax=data_max071, cmap='inferno')
    cnt2 = axes[1].contourf(X, Y, dataset[:, :, 0, 1], n_cont,
            vmin=data_min071, vmax=data_max071, cmap='inferno')

    
    fig.subplots_adjust(bottom=0.00, top=1.0,left=0.00, right=0.7,
                                wspace=0.05, hspace=0.02)
    
    cb_ax = fig.add_axes([0.72, 0.1, 0.05, 0.8])
    # !
    # Caution the contourf with the larger range needs to be selected here!
    # !
    cbar = fig.colorbar(cnt2, cax=cb_ax)

    directory = 'contours/' + timerange_select + '/compare/'
    if not os.path.exists(directory):
        os.makedirs(directory)
    # This is the fix for the white lines between contour levels
    for c in cnt1.collections:
        c.set_edgecolor("face")
    for c in cnt2.collections:
        c.set_edgecolor("face")

    if (ifpgf):
        plt.savefig(directory + 'Theta_cmp_' + label[0] + '_' + label[1] + '.pgf')
    plt.savefig(directory + 'Theta_cmp_' + label[0] + '_' + label[1] + '.png')
    plt.savefig(directory + 'Theta_cmp_' + label[0] + '_' + label[1] + '.pdf')
   

    # Pr 0025
    fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(figwidth, figheight))
    
    axes[0].axis('off') 
    axes[0].axis('equal')
    axes[1].axis('off')
    axes[1].axis('equal')
    cnt1 = axes[0].contourf(-X, Y, dataset[:, :, 0, 2], n_cont, 
            vmin=data_min0025, vmax=data_max0025, cmap='inferno')
    cnt2 = axes[1].contourf(X, Y, dataset[:, :, 0, 3], n_cont,
            vmin=data_min0025, vmax=data_max0025, cmap='inferno')

    
    fig.subplots_adjust(bottom=0.00, top=1.0,left=0.00, right=0.7,
                                wspace=0.05, hspace=0.02)
    
    cb_ax = fig.add_axes([0.72, 0.1, 0.05, 0.8])
    # !
    # Caution the contourf with the larger range needs to be selected here!
    # !
    cbar = fig.colorbar(cnt2, cax=cb_ax)

    # This is the fix for the white lines between contour levels
    for c in cnt1.collections:
        c.set_edgecolor("face")
    for c in cnt2.collections:
        c.set_edgecolor("face")


    directory = 'contours/' + timerange_select + '/compare/'
    if not os.path.exists(directory):
        os.makedirs(directory)
    if (ifpgf):
        plt.savefig(directory + 'Theta_cmp_' + label[2] + '_' + label[3] + '.pgf')
    plt.savefig(directory + 'Theta_cmp_' + label[2] + '_' + label[3] + '.png')
    plt.savefig(directory + 'Theta_cmp_' + label[2] + '_' + label[3] + '.pdf')

#    plt.show()
    


        # 2nd colorbar on the left side
        #------------------------------
#        ax = plt.subplot(111)
#        cnt_right = ax.contourf(X, Y, dataset[:, :, 0, i], n_cont)
#        cnt_left = ax.contourf(-X, Y, dataset[:, :, 0, i+1]*100, n_cont)

#        divider = make_axes_locatable(ax)
#        cax_left = divider.append_axes('left', size='5%', pad=0.5)
#        cax_right = divider.append_axes('right', size='5%', pad=0.1)
#        plt.colorbar(cnt_left, cax=cax_left)
#        plt.colorbar(cnt_right, cax=cax_right)


def run(ifpgf=False):
    """Plot 2d contour plot of temperature statistics.
    
    Arguments:
    ifpgf       --      save also in pgf format
    """

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    # Loop over specified timeranges
    for span in timeranges_select:
        # Call a postprocessing script
        plot_individual(span, ifpgf)

def run_compare(ifpgf=False):
    """Plot 2d contour plot of temperature statistics comparing cases with same Prandtl.
    
    Arguments:
    ifpgf       --      save also in pgf format
    """

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    # Loop over specified timeranges
    for span in timeranges_select:
        # Call a postprocessing script
        plot_compare(span, ifpgf)

