#!/usr/bin/env python3.5
#-----------------------
# This script reads previously saved data 
# and plots the result as 2d contour plots
# ----------------------------------------------------
# Author:   Steffen Straub
# Date  :   2018/04/05
#-----------------------------------------------------

import numpy as np
import matplotlib
matplotlib.rc('text', usetex=True)
from matplotlib import pyplot as plt

from matplotlib import colors as cl
from ..misc import my_math
import os

import configparser

import pdb



# Define function
def plot_individual(timerange_select, ifpgf):

    # Load data
    #----------
    data_dir = 'stats_2d_sym/' + timerange_select + '/'
    data_name = (
            '/2d_sym_vtbudget_tt1.npz',
            '/2d_sym_vtbudget_tt2.npz',
            '/2d_sym_vtbudget_tt3.npz',
            '/2d_sym_vtbudget_tt4.npz',
            )

    label = (
        'halfconst071',
        'halfsin071',
        'halfconst0025',       
        'halfsin0025',
        )

    figwidth = my_math.cm2in(6.5)
    figheight = my_math.cm2in(7)

   
    for i in range(len(data_name)):
        plt.close('all')
    
        print("Processing " + label[i] + ' at time ' + timerange_select)

        with np.load(data_dir + data_name[i]) as data:
        
            # Coordinates
            X = data['X_sym']
            Y = data['Y_sym']
       
            # individual radial and circumferential distribution
            r = data['r']
            phi = data['phi']
        
            # data averaged over time, streamwise direction, and exploited symmetry
            stats_sym = data['stats_sym']

        P = stats_sym[..., 0:3]
        eps = stats_sym[..., 3:6]
        MD = stats_sym[..., 6:9]
        TD = stats_sym[..., 9:12]
        TPG = stats_sym[..., 12:15]
        S = stats_sym[..., 15:18]

       
    
    

        # Plot
        #-----
        n_cont = 30     # Number of contour levels

        rphiz_list = (
                'r',
                'phi',
                'z',
                )

        for rphiz in range(3):

            # Production
            fig = plt.figure(figsize=(figwidth,figheight))
            cnt = plt.contourf(X, Y, P[..., rphiz], n_cont)
        
        
            # This is the fix for the white lines between contour levels
            for c in cnt.collections:
                c.set_edgecolor("face")
            plt.axis('scaled')
            plt.colorbar()
            plt.xlabel('$x / D$')
            plt.ylabel('$y / D$')
            plt.xlim(0, 0.5)
            plt.ylim(-0.5, 0.5)
            plt.tight_layout()
        
            directory = 'contours/' + timerange_select + '/' + label[i] + '/'
            if not os.path.exists(directory):
                os.makedirs(directory)
        
            plt.savefig(directory + 'vtbudget_P_' + rphiz_list[rphiz] + '.png')
            if (ifpgf):
                plt.savefig(directory + 'vtbudget_P_' + rphiz_list[rphiz] + '.pgf')
    
            # Dissipation
            fig = plt.figure(figsize=(figwidth,figheight))
            cnt = plt.contourf(X, Y, eps[..., rphiz], n_cont)
            # This is the fix for the white lines between contour levels
            for c in cnt.collections:
                c.set_edgecolor("face")
            plt.axis('scaled')
            plt.colorbar()
            plt.xlabel('$x / D$')
            plt.ylabel('$y / D$')
            plt.xlim(0, 0.5)
            plt.ylim(-0.5, 0.5)
            plt.tight_layout()
    
            directory = 'contours/' + timerange_select + '/' + label[i] + '/'
            if not os.path.exists(directory):
                os.makedirs(directory)
        
            plt.savefig(directory + 'vtbudget_eps_' + rphiz_list[rphiz] + '.png')
            if (ifpgf):
                plt.savefig(directory + 'vtbudget_eps_' + rphiz_list[rphiz] + '.pgf')
    
            # Molecular Diffusion
            fig = plt.figure(figsize=(figwidth,figheight))
            cnt = plt.contourf(X, Y, MD[..., rphiz], n_cont)
            # This is the fix for the white lines between contour levels
            for c in cnt.collections:
                c.set_edgecolor("face")
            plt.axis('scaled')
            plt.colorbar()
            plt.xlabel('$x / D$')
            plt.ylabel('$y / D$')
            plt.xlim(0, 0.5)
            plt.ylim(-0.5, 0.5)
            plt.tight_layout()
    
            directory = 'contours/' + timerange_select + '/' + label[i] + '/'
            if not os.path.exists(directory):
                os.makedirs(directory)
        
            plt.savefig(directory + 'vtbudget_MD_' + rphiz_list[rphiz] + '.png')
            if (ifpgf):
                plt.savefig(directory + 'vtbudget_MD_' + rphiz_list[rphiz] + '.pgf')
    
            # Turbulent Diffusion
            fig = plt.figure(figsize=(figwidth,figheight))
            cnt = plt.contourf(X, Y, TD[..., rphiz], n_cont)
            # This is the fix for the white lines between contour levels
            for c in cnt.collections:
                c.set_edgecolor("face")
            plt.axis('scaled')
            plt.colorbar()
            plt.xlabel('$x / D$')
            plt.ylabel('$y / D$')
            plt.xlim(0, 0.5)
            plt.ylim(-0.5, 0.5)
            plt.tight_layout()
    
            directory = 'contours/' + timerange_select + '/' + label[i] + '/'
            if not os.path.exists(directory):
                os.makedirs(directory)

            plt.savefig(directory + 'vtbudget_TD_' + rphiz_list[rphiz] + '.png')       
            if (ifpgf):
                plt.savefig(directory + 'vtbudget_TD_' + rphiz_list[rphiz] + '.pgf')       
    
            # Turbulent Pressure Gradient
            fig = plt.figure(figsize=(figwidth,figheight))
            cnt = plt.contourf(X, Y, TPG[..., rphiz], n_cont)
            # This is the fix for the white lines between contour levels
            for c in cnt.collections:
                c.set_edgecolor("face")
            plt.axis('scaled')
            plt.colorbar()
            plt.xlabel('$x / D$')
            plt.ylabel('$y / D$')
            plt.xlim(0, 0.5)
            plt.ylim(-0.5, 0.5)
            plt.tight_layout()
    
            directory = 'contours/' + timerange_select + '/' + label[i] + '/'
            if not os.path.exists(directory):
                os.makedirs(directory)

            plt.savefig(directory + 'vtbudget_TPG_' + rphiz_list[rphiz] + '.png')       
            if (ifpgf):
                plt.savefig(directory + 'vtbudget_TPG_' + rphiz_list[rphiz] + '.pgf')   

            # Source term
            fig = plt.figure(figsize=(figwidth,figheight))
            cnt = plt.contourf(X, Y, S[..., rphiz], n_cont)
            # This is the fix for the white lines between contour levels
            for c in cnt.collections:
                c.set_edgecolor("face")
            plt.axis('scaled')
            plt.colorbar()
            plt.xlabel('$x / D$')
            plt.ylabel('$y / D$')
            plt.xlim(0, 0.5)
            plt.ylim(-0.5, 0.5)
            plt.tight_layout()
    
            directory = 'contours/' + timerange_select + '/' + label[i] + '/'
            if not os.path.exists(directory):
                os.makedirs(directory)
        
            plt.savefig(directory + 'vtbudget_S_' + rphiz_list[rphiz] + '.png')       
            if (ifpgf):
                plt.savefig(directory + 'vtbudget_S_' + rphiz_list[rphiz] + '.pgf')       


def run(ifpgf=False):
    """Plot 2d contour plot of budget of turb. heat flux.
    
    Arguments:
    ifpgf       --      save also in pgf format
    """

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    # Loop over specified timeranges
    for span in timeranges_select:
        # Call a postprocessing script
        plot_individual(span, ifpgf)
