#!/usr/bin/env python3.5
#----------------------------------------------------------------------
# load interpolated fields, compute spectra and save them in npz 
# format
#----------------------------------------------------------------------
# Author:   Steffen Straub
# Date:     2018/02/02
#----------------------------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt
from ..misc import my_math
import os

import configparser

import pdb


def save_spectra(timerange_select, n_temp, t_fld_inhomog, min_num, max_num):
    """ Load interpolated fields, compute spectra
    and save them in npz format.

    timerange_select        choice of (current) dataset to operate on
    n_temp                  number of thermal fields
    t_fld_inhomog           list of inhomogeneous fields

    """

    # Filename for output files
    dir_out = 'results/' + timerange_select + '/'
    f_out = dir_out + 'power_spectra_t.npz'
    
    ## Load data
    #-----------
    # Construct filenames
    f_t_base = ['t{0:d}_3d_'.format(i) for i in range(0,n_temp)]
    
    interp_dir_python = 'interptemp_python/' + timerange_select + '/'
    
    
    # Read first snapshot to get dimensions for array
    fname0 = interp_dir_python + f_t_base[0] + '00001.npz' 
    with np.load(fname0) as data:
        fld0 = data['fld_3d']
        r = data['r']
        phi = data['phi']
        z = data['z']
    
    nr = len(r)
    nphi = len(phi)
    nz = len(z)
    
    
    if (max_num > 0):
        fl_max = max_num
    else:
        # Loop over all files present to find maximum number
        for fl in os.listdir(interp_dir_python):
            fname = interp_dir_python + fl
            if os.path.isfile(fname):
                fl_max = int(fl[-9:-4])       # maximum file number
    
    # Declare array to store all fields
    E_tavg = np.zeros((np.size(r), np.size(phi), np.size(z), n_temp))
    
    # First, loop over all files to calculate time average
    T_avg = np.zeros((np.size(r), np.size(phi), np.size(z), n_temp))
    print('First, get time average')
    for fl_num in range(min_num,fl_max):
        for t_fld in range(n_temp):
            fname = interp_dir_python + f_t_base[t_fld] + '{0:05d}.npz'.format(fl_num+1)
            with np.load(fname) as data:
                fld = data['fld_3d']
    
            # only first field, i. e. velocity or temperature
            T_avg[..., t_fld] += fld[:, :, :, 0] 
    T_avg = T_avg / (fl_max - min_num)

    print('Second, get power spectra of fluctuations')
    for fl_num in range(min_num,fl_max):
        for t_fld in range(n_temp):
            print('Current file is {0:d}/{1:d} t_fld {2:d}/{3:d}'.format(fl_num, fl_max, t_fld, n_temp))
            fname = interp_dir_python + f_t_base[t_fld] + '{0:05d}.npz'.format(fl_num+1)
            with np.load(fname) as data:
                fld = data['fld_3d']
    
            # only first field, i. e. velocity or temperature
            T = fld[:, :, :, 0] 

            if t_fld in t_fld_inhomog:
                T_fluct = T - T_avg[..., t_fld]
                # 1D Fourier transform scaled by number of samples
                F_1d = 1/nz * np.fft.fft(T_fluct)
                # 1D power spectrum as product of coefficients and its complex conjugate
                E_1d = np.real(F_1d * np.conj(F_1d))
                E = E_1d



            else:
                T_fluct = T - T_avg[..., t_fld]
    
                # 2D Fourier transform scaled by number of samples
                F_2d = 1/nz * 1/nphi * np.fft.fft2(T_fluct)
                # 2D power spectrum as product of coefficients and its complex conjugate
                E_2d = np.real(F_2d * np.conj(F_2d))
                E = E_2d
        

                       

            # Averaging
            if (fl_num == 0):                           # Define separately 
                E_tavg[..., t_fld] = np.copy(E)
            else:
                weight_prev = fl_num / (fl_num+1)           # weight for previous average
                weight_new =  1 / (fl_num+1)                    # weight for new value
                E_tavg[..., t_fld] = (weight_prev * E_tavg[..., t_fld] 
                        + weight_new * E)
 
        
    
    ## Save to npz file
    #-----------------
    if not os.path.exists(dir_out):
        os.makedirs(dir_out)
    np.savez(f_out, E=E_tavg, 
            r=r, phi=phi, z=z)
    

def run(n_temp = 10, min_num=0, max_num=0):
    """ Load interpolated fields, compute spectra
    and save them in npz format.

    n_temp                  number of thermal fields
    min_num                 minimum snapshot number to consider
    max_num                 maximum snapshot number to consider

    """

    # Call function

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    # inhomogeneous TBC in fields:
    t_fld_inhomog = [3, 4, 8, 9]


    for span in timeranges_select:
        save_spectra(span, n_temp, t_fld_inhomog, min_num, max_num)   
