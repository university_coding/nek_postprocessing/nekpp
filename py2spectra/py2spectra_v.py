#!/usr/bin/env python3.5
#----------------------------------------------------------------------
# load interpolated fields, compute spectra and save them in npz 
# format
#----------------------------------------------------------------------
# Author:   Steffen Straub
# Date:     2018/02/02
#----------------------------------------------------------------------

import numpy as np
from ..misc import my_math
import os

import configparser


def save_spectra(timerange_select, min_num, max_num):
    """ Load interpolated fields, compute spectra
    and save them in npz format.

    timerange_select        choice of (current) dataset to operate on
    min_num                 minimum snapshot number to consider
    max_num                 maximum snapshot number to consider

    """

    # Filename for output files
    dir_out = 'results/' + timerange_select + '/'
    f_out = dir_out + 'power_spectra_v.npz'
    
    ## Load data
    #-----------
    # Construct filenames
    f_vx_base = 'vx_3d_'
    f_vy_base = 'vy_3d_'
    f_vz_base = 'vz_3d_'
    interp_dir_python = 'interpvel_python/' + timerange_select + '/'

    # Read first snapshot to get dimensions for array
    fname0 = interp_dir_python + f_vx_base + '00001.npz' 
    with np.load(fname0) as data:
        fld0 = data['fld_3d']
        r = data['r']
        phi = data['phi']
        z = data['z']
    
    nr = len(r)
    nphi = len(phi)
    nz = len(z)

    if (max_num > 0):
        fl_max = max_num
    else:
        # Loop over all files present to find maximum number
        for fl in os.listdir(interp_dir_python):
            fname = interp_dir_python + '/' + fl
            if os.path.isfile(fname):
                fl_max = int(fl[-9:-4])       # maximum file number
    
    # First, loop over all files to calculate time average
    Uz_avg = np.zeros((np.size(r), np.size(phi), np.size(z)))
    print('First, get time average')
    for fl_num in range(min_num,fl_max):
        fname = interp_dir_python + f_vz_base + '{0:05d}.npz'.format(fl_num+1)
        with np.load(fname) as data:
            fld = data['fld_3d']

        # only first field, i. e. velocity or temperature
        Uz_avg += fld[:, :, :, 0] 
    Uz_avg = Uz_avg / (fl_max - min_num)

    print('Second, get power spectra of fluctuations')
    for fl_num in range(min_num, fl_max):
        print('Current file is {0:d}'.format(fl_num))
        f_vx = interp_dir_python + f_vx_base + '{0:05d}'.format(fl_num+1) + '.npz' 
        f_vy = interp_dir_python + f_vy_base + '{0:05d}'.format(fl_num+1) + '.npz'
        f_vz = interp_dir_python + f_vz_base + '{0:05d}'.format(fl_num+1) + '.npz'
        
        with np.load(f_vx) as data:
        
            fld_vx = data['fld_3d']
            r = data['r']
            phi = data['phi']
            z = data['z']
        
        with np.load(f_vy) as data:
            fld_vy = data['fld_3d']
        with np.load(f_vz) as data:
            fld_vz = data['fld_3d']
    
        ## Some pre-calculations and definitions
        #---------------------------------------
        nr = len(r)
        nphi = len(phi)
        nz = len(z)
        
        theta, rho, zz = np.meshgrid(phi, r, z, indexing='xy')   
        
        Ui_cart = np.zeros((nr, nphi, nz, 3))
        Ui_cart[..., 0] = fld_vx[:, :, :, 0]
        Ui_cart[..., 1] = fld_vy[:, :, :, 0]
        Ui_cart[..., 2] = fld_vz[:, :, :, 0]
        
        
        ## Transform to polar coordinates
        #--------------------------------
        Ui_cyl = my_math.transform_vect(theta, Ui_cart, tensor_order=1)
        
        ## Subtract average
        #------------------
        Uz_fluct = Ui_cyl[..., 2] - Uz_avg
    
    
        # 2D Fourier transform scaled by number of samples
        F_2d_r = 1/nz * 1/nphi * np.fft.fft2(Ui_cyl[..., 0])
        F_2d_t = 1/nz * 1/nphi * np.fft.fft2(Ui_cyl[..., 1])
        F_2d_z = 1/nz * 1/nphi * np.fft.fft2(Uz_fluct)
        # 2D power spectrum as product of coefficients and its complex conjugate
        E_2d_rr = np.real(F_2d_r * np.conj(F_2d_r))
        E_2d_tt = np.real(F_2d_t * np.conj(F_2d_t))
        E_2d_zz = np.real(F_2d_z * np.conj(F_2d_z))
        E_2d_rz = np.real(F_2d_r * np.conj(F_2d_z))
    
        # Averaging
        if (fl_num == 0):                           # Define separately 
            E_2d_rr_tavg = np.copy(E_2d_rr)
            E_2d_tt_tavg = np.copy(E_2d_tt)
            E_2d_zz_tavg = np.copy(E_2d_zz)
            E_2d_rz_tavg = np.copy(E_2d_rz)
        else:
            weight_prev = fl_num / (fl_num+1)               # weight for previous average
            weight_new =  1 / (fl_num+1)                    # weight for new value
            E_2d_rr_tavg = weight_prev * E_2d_rr_tavg + weight_new * E_2d_rr
            E_2d_tt_tavg = weight_prev * E_2d_tt_tavg + weight_new * E_2d_tt
            E_2d_zz_tavg = weight_prev * E_2d_zz_tavg + weight_new * E_2d_zz
            E_2d_rz_tavg = weight_prev * E_2d_rz_tavg + weight_new * E_2d_rz
    
    
    
    ## Save to npz file
    #-----------------
    if not os.path.exists(dir_out):
        os.makedirs(dir_out)
    np.savez(f_out, E_2d_rr=E_2d_rr_tavg, E_2d_tt=E_2d_tt_tavg, E_2d_zz=E_2d_zz_tavg,  
            E_2d_rz=E_2d_rz_tavg,
            r=r, phi=phi, z=z)


def run(min_num=0, max_num=0):
    """ Load interpolated fields, compute spectra
    and save them in npz format.

    min_num                 minimum snapshot number to consider
    max_num                 maximum snapshot number to consider

    """

    # Call function

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    for span in timeranges_select:
        save_spectra(span, min_num, max_num)   


