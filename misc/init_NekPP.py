#!/usr/bin/env python3.5
#
##----------------------------------------------------------------------
# Initialize NekPP package for postprocessing
##---------------------------------------------------------------------- 

from NekPP.nek2py import *
from NekPP.py2spectra import *
from NekPP.py2dat import *
from NekPP.dat2plot import *
from NekPP.py2py_sym import *
from NekPP.py2plot import *


