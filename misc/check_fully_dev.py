#!/usr/bin/env python3.5
#----------------------------------------------------------------------
# Check if Theta is streamwise independent, i.e. thermally fully
# developed.
#----------------------------------------------------------------------
# Author:   Steffen Straub
# Date:     2018/11/09
#----------------------------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt
import os

import configparser

import pdb



def check_ful_dev(span, t_fld, s_num):

    plt.close('all')

    ## Load file(s)
    #--------------
    fdir = 'interptemp_python/'+span+'/'
    # Loop over all files
    if (s_num < 0):
        # Loop over all files present to find maximum number
        s_max = 0
        for fl in os.listdir(fdir):
            fname = fdir + fl
            if os.path.isfile(fname):
                s_max = max(s_max, int(fl[-9:-4]))       # maximum file number


        # Read first snapshot to get dimensions for array
        fname0 = fdir + 't{0:d}_3d_{1:05d}.npz'.format(t_fld, 1)
        with np.load(fname0) as data:
            fld0 = data['fld_3d']
            r = data['r']
            phi = data['phi']
            z = data['z']

        # Declare array for Theta
        Theta = np.zeros((len(r), len(z)))

        # Average in time and azimuthal direction
        for s_num in range(1, s_max+1):
            print("{0:d} / {1:d}".format(s_num, s_max))
            fname = fdir + 't{0:d}_3d_{1:05d}.npz'.format(t_fld, s_num)
            with np.load(fname) as data:
                fld = data['fld_3d']
                r = data['r']
                phi = data['phi']
                z = data['z']

            # Average in azimuthal direction
            fld = np.mean(fld, axis=1)

            # Average in time
            Theta += np.squeeze(fld[:, :])
            
        Theta = Theta / (s_max)

        
    # Only load specified snapshot
    else:
        fname = fdir + 't{0:d}_3d_{1:05d}.npz'.format(t_fld, s_num)

        with np.load(fname) as data:
            fld = data['fld_3d']
            r = data['r']
            phi = data['phi']
            z = data['z']
    
        # Average in azimuthal direction
        Theta = np.mean(fld, axis=1)

    ## Plot
    #------
    rr, zz = np.meshgrid(r, z, indexing='ij')
    plt.contour(zz, rr, Theta)
    plt.xlabel(r'$z/D$')
    plt.ylabel(r'$r/D$')
   
    directory = 'plots/'+span+'/z_dependence/'
    if not os.path.exists(directory):
        os.makedirs(directory)

    plt.savefig(directory + 't{0:d}_contour.png'.format(t_fld))


    # Average in streamwise direction
    Theta_avg = np.mean(Theta, axis=1)

    plt.figure()
    plt.plot(r, Theta_avg)
    plt.xlabel(r'$r/D$')
    plt.ylabel(r'$\langle \Theta (r) \rangle^{t, \varphi, z}$')
    plt.savefig(directory + 't{0:d}_avg.png'.format(t_fld))
    


def run(t_fld=0, s_num=1):
    """
    Check fully developed condition by plotting Theta(z).

    t_fld       Temperature field
    s_num       Number of snapshot (if < 0: average over all present)
    """

    # Read list of selected timerange to evalute
    config = configparser.ConfigParser()
    config.read('timerange.ini')
    timeranges_select = str.split(config['Time']['t_select'], ',')

    for span in timeranges_select:
        check_ful_dev(span, t_fld, s_num)

