#!/usr/bin/env python 3.5
#------------------------
# Function for saving plots
#----------------------------------------------------------------------
# Author:   Steffen Straub
# Date:     2018/02/12
#----------------------------------------------------------------------

import matplotlib.pyplot as plt
import os
from matplotlib2tikz import save as tikz_save
import tikzplotlib


def my_save_fig(p_name, p_type, p_dir):
    plot_name = p_name
    plot_type = p_type
    plot_dir = p_dir
    plot_path = p_dir + plot_name + plot_type
    directory = os.path.dirname(plot_path)
    if not os.path.exists(directory):
        os.makedirs(directory)
    
    plt.savefig(plot_path)


def save_tikz_png(fname, directory, iftikz, axis_opts=None):
    if not os.path.exists(directory):
        os.makedirs(directory)

    if (iftikz):
#        tikz_save(directory + fname + '.tex',
#                figureheight='\\figureheight',
#                figurewidth='\\figurewidth',
#                show_info=False,
#                )   

        tikzplotlib.save(directory + fname + '.tex',
                figureheight='\\figureheight',
                figurewidth='\\figurewidth',
                show_info=False,
                extra_axis_parameters=axis_opts,
                )   

    plt.savefig(directory + fname + '.png')

