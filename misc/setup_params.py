#!/usr/bin/env python3.5
#----------------------------------------------------------------------
# Collection of parameters that are needed for postprocessing scripts
# defining the setup of each case.
#----------------------------------------------------------------------

Re_b = 5300
Re_t = 360
