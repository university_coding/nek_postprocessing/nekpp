#!/usr/bin/env python3.5
# Additional mathematical functions

import numpy as np
import pdb

## Additional functions
#----------------------
def cart2cyl(a, vec, tensor_order=1):
    """ Transform from cartesian to cylindrical system.
    a:      angle
    vec:    vector or matrix

    Transfom from cartesian to cylindrical:
    
    |U_r    | = | cos(theta)  sin(theta)  0 | |U_x|
    |U_theta| = | -sin(theta) cos(theta)  0 | |U_y|
    |U_z    | = | 0           0           1 | |U_z|
    
     U_cyl     =              m       *        U_cart

    """

    rot_mat = np.array([[np.cos(a), np.sin(a), 0], [-np.sin(a), np.cos(a), 0], [0, 0, 1]])
    if (tensor_order == 1):
        vec_cyl = np.dot(rot_mat, vec)
    elif (tensor_order == 2):
        vec_cyl = np.dot(np.dot(rot_mat, vec), np.transpose(rot_mat))
        # Alternative
#        vec_cyl = np.einsum('...ij, mi, nj', vec, rot_mat, rot_mat)
    elif (tensor_order == 3):
        # Not possible to write as a matrix product
        vec_cyl = np.einsum('...ijk, mi, nj, ok', vec, rot_mat, rot_mat, rot_mat)

    return vec_cyl

def transform(theta, vec, tensor_order=1, dim=2):
    """ Transform each component of a tensor.
    theta:  angle for each component
    vec:    vector or matrix (2nd order tensor)

    """

    vec_cyl = np.zeros(np.shape(vec))
    if (dim==2):
        for i in range(np.size(theta, 0)):
            for j in range(np.size(theta, 1)):
                vec_cyl[i, j, ...] = cart2cyl(theta[i, j], vec[i, j, ...], tensor_order)
    elif (dim==3):
        for i in range(np.size(theta, 0)):
            for j in range(np.size(theta, 1)):
                for k in range(np.size(theta, 2)):
                    vec_cyl[i, j, k, ...] = cart2cyl(theta[i, j, k], vec[i, j, k, ...], tensor_order)

    return vec_cyl


def transform_vect(theta, vec, tensor_order=1):
    """ Transform each component of a tensor from cartesian to cylindrical coordinate 
    system using vectorized arrays

    theta:  angle for each component
    vec:    vector or matrix (2nd order tensor) or 3rd order
    
    Transfom from cartesian to cylindrical:
    
    |U_r    | = | cos(theta)  sin(theta)  0 | |U_x|
    |U_theta| = | -sin(theta) cos(theta)  0 | |U_y|
    |U_z    | = | 0           0           1 | |U_z|
    
    U_cyl     =              m       *        U_cart

    """

    a = theta
    rot_mat = np.array([[np.cos(a), np.sin(a), np.zeros_like(a)], 
        [-np.sin(a), np.cos(a), np.zeros_like(a)], 
        [np.zeros_like(a), np.zeros_like(a), np.ones_like(a)]]
        )

    if (tensor_order == 1):
        vec_cyl = np.einsum('ij..., ...j', rot_mat, vec)
    elif (tensor_order == 2):
        vec_cyl = np.einsum('...ij, mi..., nj...', vec, rot_mat, rot_mat)
    elif (tensor_order == 3):
        vec_cyl = np.einsum('...ijk, mi..., nj..., ok...', vec, rot_mat, rot_mat, rot_mat)

    return vec_cyl





def get_utau(r, phi, deriv, theta, nu):
    """
    Determine friction velocity u_tau from wall shear stress.
    """
    
    # Gradient of the mean velocity
    dUjdxi_cart= np.zeros((np.size(r), np.size(phi), 3, 3))
    dUjdxi_cyl= np.zeros((np.size(r), np.size(phi), 3, 3))
    dUjdxi= np.zeros((np.size(r), 3, 3))

    # d <U_j> / d x_i
    #----------------
    dUjdxi_cart[:, :, 0, 0] = deriv[:,:,0] # d<U_1> / d x_1
    dUjdxi_cart[:, :, 0, 1] = deriv[:,:,1] # d<U_1> / d x_2
    dUjdxi_cart[:, :, 0, 2] = 0            # d<U_1> / d x_3
    dUjdxi_cart[:, :, 1, 0] = deriv[:,:,2] # d<U_2> / d x_1
    dUjdxi_cart[:, :, 1, 1] = deriv[:,:,3] # d<U_2> / d x_2
    dUjdxi_cart[:, :, 1, 2] = 0            # d<U_2> / d x_3
    dUjdxi_cart[:, :, 2, 0] = deriv[:,:,4] # d<U_3> / d x_1
    dUjdxi_cart[:, :, 2, 1] = deriv[:,:,5] # d<U_3> / d x_2
    dUjdxi_cart[:, :, 2, 2] = 0            # d<U_3> / d x_3
    
    dUjdxi_cyl = transform(theta, dUjdxi_cart, 2)
    
    # Average in circumferential direction:
    dUjdxi = np.mean(dUjdxi_cyl, axis=1)

    tau_w = -nu*dUjdxi[-1, 2, 0]
    u_t = tau_w**0.5

    return u_t


def get_qw(r, phi, deriv_temp, theta, Pe):
    """
    Get wall heat flux from temperature gradient
    """

    dThetadxi_cart = np.zeros((np.size(r), np.size(phi), 3))
    dThetadxi_cart[:, :, 0:2] = deriv_temp[:, :, 0:2]              # d <Theta> / d x_1, d <Theta> / d x_2
    dThetadxi_cyl = transform(theta, dThetadxi_cart, 1)

    dThetadxi = np.mean(dThetadxi_cyl, axis=1)

    q_w = -1/Pe * dThetadxi[:, 0][-1]

    return q_w


def pol2cart(theta, rho):
    """
    Transform polar to cartesian coordinates.

    Paramerters
    -----------
    theta : float
        Angle in radians.
    rho : float
        Radial location.

    Returns
    -------
    (x, y) : (float, float)
        Cartesian coordinates

    """

    x = rho * np.cos(theta)
    y = rho * np.sin(theta)

    return x, y

def cm2in(cm):
    """
    Convert cm to inch.
    """
    return cm/2.54


def left_right_sym(field, angle, tensor_order=0):
    """ 
    Exploit left right symmetry for field where the left side is on 
    pi/2 < angle < 3/2*pi

    field:          field variable
    angle:          angle in polar coordinates
    tensor_order:   Scalar (0) or vector (1)
    """
        
    mask_q1 = ( (0 <= angle) & (angle <= np.pi/2) )
    mask_q2 = ( (np.pi/2 <= angle) & (angle <= np.pi) )
    mask_q3 = ( (np.pi <= angle) & (angle <= np.pi*3/2) )
    mask_q4 = ( (np.pi*3/2 <= angle) & (angle <= np.pi*2) )

    if (tensor_order == 0):
        field_sym_up = (field[:, mask_q1] + np.flip(field[:, mask_q2], axis=1)) / 2
        field_sym_low = (np.flip(field[:, mask_q3], axis=1) + field[:, mask_q4]) / 2
        field_sym = np.concatenate((field_sym_low, field_sym_up[:, 1:]), axis=1)
    elif (tensor_order == 1):
        shape_orig = np.shape(field) 
        nr = shape_orig[0]
        nphi = shape_orig[1]
        if (np.shape(shape_orig)[0] == 3):
            ndim = shape_orig[2]
            field_sym = np.zeros((nr, int((nphi-1)/2+1), ndim))
        elif (np.shape(shape_orig)[0] == 4):
            nz = shape_orig[2]
            ndim = shape_orig[3]
            field_sym = np.zeros((nr, int((nphi-1)/2+1), nz, ndim))

        # Last index is the dimension 
        for i in range(0, 3, 2):
            field_component = field[..., i]
            field_sym_up = (field_component[:, mask_q1] + np.flip(field_component[:, mask_q2], axis=1)) / 2
            field_sym_low = (np.flip(field_component[:, mask_q3], axis=1) + field_component[:, mask_q4]) / 2
            field_sym[..., i] = np.concatenate((field_sym_low, field_sym_up[:, 1:]), axis=1)

        # uphi_theta is antisymmetric
        field_component = field[..., 1]
        field_sym_up = (field_component[:, mask_q1] - np.flip(field_component[:, mask_q2], axis=1)) / 2
        # restore values at the y-axis, which equate exactly to zero by the above formula 
        index_90 = np.max(np.where(mask_q1 == True))
        field_sym_up[:, -1] = field_component[:, index_90] 
        
        field_sym_low = (-np.flip(field_component[:, mask_q3], axis=1) + field_component[:, mask_q4]) / 2
        # restore values at the y-axis, which equate exactly to zero by the above formula 
        index_270 = np.max(np.where(mask_q3 == True))
        field_sym_low[:, 0] = field_component[:, index_270] 

        field_sym[..., 1] = np.concatenate((field_sym_low, field_sym_up[:, 1:]), axis=1)

    return field_sym


def conv_criteria(profiles, timeranges, maximum='' ):
    """ Convergence criteria:
    Criteria:
        When maximum deviation: abs(T_max - T_i) < 1 % 
        and remains < 1%

    profiles:       Numpy array of profiles f(y) to compare to each other.
                    Last one is the final result.
    timeranges:     List of corresponding average times as a string, e.g. '900-4500'
    maximum:        Use this maximum for weighting (optional)
    """

    # Convert averaging times into integers
    n_timeranges = len(timeranges)
    T_A = np.zeros(n_timeranges)
    for i in range(n_timeranges):
        T_A[i] = np.abs(int(timeranges[i].split('-')[1]) 
                - int(timeranges[i].split('-')[0])
                )

    # Calculate maximum deviation between results
    #
    # Weighted by the maximum value of the final profile to account for very small values,
    # similar to visually inspecting plots scaled by maximum
    # Alternatively, use the specified maximum instead
    if maximum:
        weight = 1 / maximum
    else:
        weight = 1 / np.max(np.abs(profiles[:, -1]))

    # Alternatively, use the specified maximum instead
    

    n = np.shape(profiles)[1]
    dev = np.zeros(n)
    for i in range(n):
        dev[i] = np.max(np.abs(profiles[:, i] - profiles[:, -1]) * weight
                
                    )

    # Find T_A when criteria is first fullfilled
    delta = 1 * 1e-2
    dev_j = 0
    j = 1
    while dev[-j] < delta and j < len(dev):
        j += 1

    if j < len(dev):
        j -= 1      # Subtract final increment from j
        T_conv = T_A[-j]
    else:           # Test again for first element
        if dev[-j] < delta:
            T_conv = T_A[0]
        else:
            T_conv = T_A[1]

    return dev, T_conv

