#!/bin/env python3.5
# Nusselt number correlations
#----------------------------------------------------------------------
# Author:   Steffen Straub
# Date:     2018/07/04
#----------------------------------------------------------------------

import numpy as np

import pdb

def xi(Re):
    """
    Determine the (Darcy) friction factor xi = 4 c_f

    Re          Reynolds number
    """

#    xi = (1.82 * np.log10(Re) - 1.64)**(-2)

#   Newer version as in VDI heat atlas
    xi = (1.8 * np.log10(Re) - 1.5)**(-2)

    return xi


def gnielinski(Re, Pr, D, L):
    """
    Gnielinski's correlation for Nusselt number according to his 
    original paper and the version in VDI Waermeatlas.

    Re      Reynolds number
    Pr      Prandtl number
    D       Diameter
    L       Length

    """

    nuss = ( (xi(Re)/8 * (Re - 1000)*Pr) / (1 + 12.7 * (xi(Re)/8)**(0.5) * (Pr**(2/3) - 1)) ) * ( 1 + (D/L)**(2/3) )
    return nuss


def lubarsky(Pe):
    """
    Pe      Peclet number
    """

    nuss = 0.625 * Pe**0.4
    return nuss

def skupinski(Pe):
    """
    Pe      Peclet number
    """

    nuss = 4.82 + 0.0185 * Pe**0.827
    return nuss

def tricoli(Pe):
    """
    Use correlation for IF and correct it by a constant factor
    (See Pacio 2015)

    Pe      Peclet number
    """

    nuss_if = skupinski(Pe)
    nuss_it = np.pi**2 / 12 * nuss_if

    return nuss_it

def it_best_fit(Pe):
    """
    Best fit for IT data in Pacio 2015
    
    Pe      Peclet number
    """

    nuss = 2.75 + 0.02*Pe**0.8

    return nuss

def kader(R, Pr,y , yp):
    """
    Kader's correlation (1981)

    Parameters
    ----------
    R : Pipe radius
    Pr : Prandlt number
    y : wall-normal location
    yp : wall-normal location in viscous units

    Returns
    -------
    Thetap : Temperature in visous units

    """

    def beta(Pr):
        return (3.85*Pr**(1/3) - 1.3)**2 + 2.12*np.log(Pr)
    def gamma(Pr, yp):
        return 1e-2 * (Pr * yp)**   4 / (1 + 5*Pr**3*yp)

    Thetap = (Pr*yp * np.exp(-gamma(Pr, yp)) 
            + (2.12 * np.log( (1+yp) * (1.5*(2-y/R))/(1 + 2*(1-y/R)**2)) + beta(Pr))
            * np.exp(-1/gamma(Pr, yp))
            )
    return Thetap

