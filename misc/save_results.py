#!/usr/bin/env python3
#---------------------------------------------------------------------- 
# A collection of functions to save postproccessed results to file.
#----------------------------------------------------------------------
# Author:   Steffen Straub
# Date:     2019/02/15
#----------------------------------------------------------------------
import numpy as np
import os

def save_to_dat(results, description, fname):
    """ Save results to the given filename (in text format).
    
    results         Data to save
    description     Description of the data for header
    fname           File name
    """

    directory = os.path.dirname(fname)
    if not os.path.exists(directory):
        os.makedirs(directory)
     
    np.savetxt(fname, 
       results,
       header=description)

