# NekPP

NekPP stands for Nek5000 postprocessing. It is a collection of utilities designed for simple and automated data processing of results from the Nek5000 solver (https://nek5000.mcs.anl.gov/).

Features include:
- Conversion of binary Nek5000 format to npz format
- Conversion of npz format to text file
- Create plots from text files

## Howto use:
1. Initialization
  1. Provide links to 
     - averaged and interpolated data in folder "ZSTAT",
     - interpolation mesh in folder "interpmesh".
  2. Run initNekPP.py to import all necessary modules.
  3. Set parameters of current case in setup\_params.py.
2. Run nek2py\_xx.py to convert interpolated data from Nek500 to npz format.
3. Run py2dat\_xx.py to convert python to text file format.
4. Run plot\_xx.py to plot the results





